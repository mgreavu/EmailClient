#if !defined(AFX_SMTPAUTHDLG_H__AE9361B7_1000_4F79_8C32_A64C4657C402__INCLUDED_)
#define AFX_SMTPAUTHDLG_H__AE9361B7_1000_4F79_8C32_A64C4657C402__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SMTPAuthDlg.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CSMTPAuthDlg dialog

class CSMTPAuthDlg : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	BOOL m_bOnChange;
	CSMTPAuthDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSMTPAuthDlg)
	enum { IDD = IDD_SMTP_AUTH_DIALOG };
	int		m_AuthOpt;
	CString	m_sSMTPName;
	CString	m_sSMTPPsw;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSMTPAuthDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSXButton m_btnOk;
	CSXButton m_btnCancel;
	// Generated message map functions
	//{{AFX_MSG(CSMTPAuthDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnUseSmtpSett();
	afx_msg void OnUsePop3Sett();
	afx_msg void OnChange();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMTPAUTHDLG_H__AE9361B7_1000_4F79_8C32_A64C4657C402__INCLUDED_)
