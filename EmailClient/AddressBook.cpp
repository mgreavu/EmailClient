// AddressBook.cpp: implementation of the CAddressBook class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EmailClient.h"
#include "AddressBook.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
const int CAddressBook::FULLNAME = 1;
const int CAddressBook::NICK = 2;

IMPLEMENT_SERIAL (CAddressBook, CObject, 0)

CAddressBook::CAddressBook(CString addrBookPath)
{
	readAddressBook(addrBookPath);
}

CAddressBook::CAddressBook()
{
}

CAddressBook::~CAddressBook()
{

}

//AfxMessageBox("Your contact list is empty!");
void CAddressBook::addContact(CContact newContact)
{
	myContactList.push_back(newContact);
}

void CAddressBook::removeContact(CString nickName)
{
	for (ContactListIterator = myContactList.begin(); ContactListIterator != myContactList.end(); ContactListIterator++)
	{
		if(((*ContactListIterator).m_NickName) == nickName){
			myContactList.erase(ContactListIterator);
			return;
		}
	}
	
}

void CAddressBook::getContact(CContact& destinationContact, CString strIdentifier, int identifierType)
{
	for (ContactListIterator = myContactList.begin(); ContactListIterator != myContactList.end(); ContactListIterator++)
	{
		if(((*ContactListIterator).m_NickName) == strIdentifier){
			destinationContact = (*ContactListIterator);
			return;
		}
	}
}


CAddressBook& CAddressBook::operator=(CAddressBook& adrBook){
	for (ContactListIterator = adrBook.myContactList.begin(); ContactListIterator != adrBook.myContactList.end(); ContactListIterator++)
	{
		myContactList.push_back(*ContactListIterator);
	}	
	return (*this);
}





BOOL CAddressBook::saveAddressBook(CString FilePath)
{
	BOOL bResult = FALSE;
	
	CFile* pFile = new CFile();
	bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	if (!bResult)
	{
		bResult = pFile->Open (FilePath, CFile::modeCreate);
		pFile->Close();
		bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	}
	
	if (bResult)
	{
		CArchive Archive(pFile, CArchive::store);
		try
		{
			pFile->SeekToBegin();
			Serialize (Archive);
		}
		catch (CException* pException)
		{
			bResult = FALSE;
			pException->Delete();
		}
		
		Archive.Close();
		pFile->Close();
	}
	
	delete pFile;

	return bResult;
}

BOOL CAddressBook::readAddressBook(CString FilePath)
{
	BOOL bResult = FALSE;
	
	CFile* pFile = new CFile();
	bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	if (!bResult)
	{
		bResult = pFile->Open (FilePath, CFile::modeCreate);
		pFile->Close();
		bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	}
	
	if (bResult)
	{
		CArchive Archive(pFile, CArchive::load);
		try
		{
			pFile->SeekToBegin();
			Serialize (Archive);
		}
		catch (CException* pException)
		{
			bResult = FALSE;
			pException->Delete();
		}
		
		Archive.Close();
		pFile->Close();
	}
	delete pFile;
	return bResult;
	
}

void CAddressBook::clearAddressBook()
{
	myContactList.clear();
}

void CAddressBook::refreshcontact(CContact contact, CString old_Nick)
{
	for (ContactListIterator = myContactList.begin(); ContactListIterator != myContactList.end(); ContactListIterator++)
	{
		if((*ContactListIterator).m_NickName == old_Nick){
			(*ContactListIterator) = contact;
			return;
		}
	}
}


