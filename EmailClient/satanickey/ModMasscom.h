#ifndef _INCLUDE_ONCE__MODMASSCOM_H
#define _INCLUDE_ONCE__MODMASSCOM_H

#include "tfmtypes.h"
/*
 *	Helpers for Masscom.dll link dinamic
 *  Adauga pointeri dupa necesitate.
 */

#define MASSCOM_LOAD_SUCCESS				0x00
#define MASSCOM_LOAD_FAILURE_DLL			0x01
#define MASSCOM_LOAD_FAILURE_PARAM			0x02
#define MASSCOM_LOAD_FAILURE_FNC_MISSING	0x03

#define MASSCOM_UNLOAD_SUCCESS				0x00
#define MASSCOM_UNLOAD_FAILURE_DLL			0x01
#define MASSCOM_UNLOAD_FAILURE_PARAM		0x02

typedef unsigned char masscom_data_unit[8];			// ReadDataFromDevice(), WriteDataToDevice()

typedef BOOL (__stdcall *LPFNDLLVERSION)(char*);
typedef BOOL (__stdcall *LPFNCHIPVERSION)(char*);
typedef BOOL (__stdcall *LPFNFWVERSION)(char*);
typedef BOOL (__stdcall *LPFNISLOGGED)();
typedef BOOL (__stdcall *LPFNLOGIN)(LPSTR, LPSTR);
typedef BOOL (__stdcall *LPFNCHANGEID)(LPSTR);
typedef BOOL (__stdcall *LPFNCHANGEPASSWORD)(LPSTR);
typedef BOOL (__stdcall *LPFNRESETDEVICE)();
typedef BOOL (__stdcall *LPFNMASSCOMREADDATAFROMDEVICE)(unsigned short, unsigned char, masscom_data_unit);
typedef BOOL (__stdcall *LPFNMASSCOMWRITEDATATODEVICE)(unsigned short, unsigned char, masscom_data_unit);
typedef unsigned char (__stdcall *LPFNREADDATA)(unsigned int, volatile unsigned char*, unsigned char);
typedef unsigned char (__stdcall *LPFNWRITEDATA)(unsigned char*, unsigned int);
typedef unsigned char (__stdcall *LPFNENABLEWPROTECTION)();
typedef unsigned char (__stdcall *LPFNDISABLEWPROTECTION)();
typedef unsigned char (__stdcall *LPFNLOGOUT)();
typedef unsigned char (__stdcall *LPFNACTIVEDEVICE)();
typedef unsigned char (__stdcall *LPFNPRIVATEDISK)();
typedef unsigned char (__stdcall *LPFNLISTALLFINGERS)(unsigned char&);
typedef unsigned char (__stdcall *LPFNDELETEFINGER)(unsigned char);
typedef unsigned char (__stdcall *LPFNDELETEALLFINGERS)();
typedef unsigned char (__stdcall *LPFNVERIFYFINGER)(unsigned char&, unsigned char*);
typedef unsigned char (__stdcall *LPFNENROLLFINGER)(unsigned char*, unsigned char &);
typedef unsigned char (__stdcall *LPFNKEYTYPE)();
typedef unsigned int  (__stdcall *LPFNEEPROMSIZE)();
typedef unsigned char (__stdcall *LPFNSETGUICALLBACK)(PT_GUI_STREAMING_CALLBACK2);


/*
 *  15-04-2007, Mihai Greavu
 *
 *  _MASSCOM_NO_MTHREAD_IMPLEMENT: Masscom.dll nu are curent implementata protectie multi-threading.
 *  CRITICAL_SECTION folosita la nivelul aplicatiei pentru a serializa accessul la dispozitiv.
 *  Nu are efect global in sistemul de operare, ci doar in procesul curent. 
 *  Un Mutex cu nume global ar rezovla problema...
 */

#pragma pack (push, interface_modmasscom)
#pragma pack (4)

typedef struct tagSATAMasscomPointers
{
	HINSTANCE						hDLL;
	char							DllPath[MAX_PATH + 1];
#ifdef _MASSCOM_NO_MTHREAD_IMPLEMENT
	CRITICAL_SECTION				cs;	
#endif
	LPFNDLLVERSION					lpfnDllVersion;
	LPFNCHIPVERSION					lpfnChipVersion;	
	LPFNFWVERSION					lpfnFWVersion;
	LPFNISLOGGED					lpfnIsLogged;
	LPFNLOGIN						lpfnLogIn;
	LPFNCHANGEID					lpfnChangeID;
	LPFNCHANGEPASSWORD				lpfnChangePassword;
	LPFNRESETDEVICE					lpfnResetDevice;
	LPFNREADDATA					lpfnReadData;
	LPFNWRITEDATA					lpfnWriteData;	
	LPFNENABLEWPROTECTION			lpfnEnableWProtection; 	
	LPFNDISABLEWPROTECTION			lpfnDisableWProtection;
	LPFNLOGOUT						lpfnLogOut;
	LPFNACTIVEDEVICE				lpfnGetPublicDrive;
	LPFNPRIVATEDISK					lpfnGetPrivateDrive;	
	LPFNLISTALLFINGERS				lpfnGetNumberOfFingers;
	LPFNDELETEFINGER				lpfnDeleteFinger;
	LPFNDELETEALLFINGERS			lpfnDeleteAllFingers;
	LPFNVERIFYFINGER				lpfnVerifyFinger;
	LPFNENROLLFINGER				lpfnEnrollFinger;
	LPFNKEYTYPE						lpfnKeyType;
	LPFNEEPROMSIZE					lpfnEEpromSize;
	LPFNSETGUICALLBACK				lpfnSetGUICallback;
	LPFNMASSCOMREADDATAFROMDEVICE	lpfnMasscomReadDataFromDevice;		// una identica in HIDKey.dll -> ReadDataFromDevice()
	LPFNMASSCOMWRITEDATATODEVICE	lpfnMasscomWriteDataToDevice;		// una identica in HIDKey.dll -> WriteDataToDevice()
} SATAMasscomPointers, *LPSATAMASSCOMPOINTERS;

#pragma pack(pop, interface_modmasscom) 

#define SZ_INTERFACE_MODMASSCOM		sizeof (SATAMasscomPointers)


__inline int SwitchSATAMasscomModule (LPSATAMASSCOMPOINTERS lpModule)
{
		if (!lpModule) return MASSCOM_LOAD_FAILURE_PARAM;
		if (!strlen(lpModule->DllPath)) return MASSCOM_LOAD_FAILURE_PARAM;
			
		if ((lpModule->hDLL = ::LoadLibrary ((const char*)lpModule->DllPath)) == NULL) return MASSCOM_LOAD_FAILURE_DLL;

		lpModule->lpfnDllVersion =					(LPFNDLLVERSION)::GetProcAddress (lpModule->hDLL, "dllVersion");
		lpModule->lpfnChipVersion =					(LPFNCHIPVERSION)::GetProcAddress (lpModule->hDLL, "chipVersion");
		lpModule->lpfnFWVersion =					(LPFNFWVERSION)::GetProcAddress (lpModule->hDLL, "fwVersion");
		lpModule->lpfnIsLogged =					(LPFNISLOGGED)::GetProcAddress (lpModule->hDLL, "isLogged");
		lpModule->lpfnLogIn =						(LPFNLOGIN)::GetProcAddress (lpModule->hDLL, "LogIN");
		lpModule->lpfnChangeID =					(LPFNCHANGEID)::GetProcAddress (lpModule->hDLL, "ChangeID");
		lpModule->lpfnChangePassword =				(LPFNCHANGEPASSWORD)::GetProcAddress (lpModule->hDLL, "ChangePassword");
		lpModule->lpfnResetDevice =					(LPFNRESETDEVICE)::GetProcAddress (lpModule->hDLL, "ResetDevice");
		lpModule->lpfnReadData =					(LPFNREADDATA)::GetProcAddress (lpModule->hDLL, "ReadData");
		lpModule->lpfnWriteData =					(LPFNWRITEDATA)::GetProcAddress (lpModule->hDLL, "WriteData");
		lpModule->lpfnEnableWProtection =			(LPFNENABLEWPROTECTION)::GetProcAddress (lpModule->hDLL, "EnableWProtection");
		lpModule->lpfnDisableWProtection =			(LPFNDISABLEWPROTECTION)::GetProcAddress (lpModule->hDLL, "DisableWProtection");
		lpModule->lpfnLogOut =						(LPFNLOGOUT)::GetProcAddress (lpModule->hDLL, "LogOUT");
		lpModule->lpfnGetPublicDrive =				(LPFNACTIVEDEVICE)::GetProcAddress (lpModule->hDLL, "active_Device");
		lpModule->lpfnGetPrivateDrive =				(LPFNPRIVATEDISK)::GetProcAddress (lpModule->hDLL, "private_Disk");
		lpModule->lpfnGetNumberOfFingers =			(LPFNLISTALLFINGERS)::GetProcAddress (lpModule->hDLL, "ListAllFingers");
		lpModule->lpfnDeleteFinger =				(LPFNDELETEFINGER)::GetProcAddress (lpModule->hDLL, "DeleteFinger");
		lpModule->lpfnDeleteAllFingers =			(LPFNDELETEALLFINGERS)::GetProcAddress (lpModule->hDLL, "DeleteAllFingers");
		lpModule->lpfnVerifyFinger =				(LPFNVERIFYFINGER)::GetProcAddress (lpModule->hDLL, "VerifyFinger");
		lpModule->lpfnEnrollFinger =				(LPFNENROLLFINGER)::GetProcAddress (lpModule->hDLL, "EnRollFinger");
		lpModule->lpfnKeyType =						(LPFNKEYTYPE)::GetProcAddress (lpModule->hDLL, "keyType");
		lpModule->lpfnEEpromSize =					(LPFNEEPROMSIZE)::GetProcAddress (lpModule->hDLL, "eepromSize");
		lpModule->lpfnSetGUICallback =				(LPFNSETGUICALLBACK)::GetProcAddress(lpModule->hDLL, "SetGuiCallBack");
		lpModule->lpfnMasscomReadDataFromDevice =	(LPFNMASSCOMREADDATAFROMDEVICE)::GetProcAddress(lpModule->hDLL, "ReadDataFromDevice");
		lpModule->lpfnMasscomWriteDataToDevice  =	(LPFNMASSCOMWRITEDATATODEVICE)::GetProcAddress(lpModule->hDLL, "WriteDataToDevice");

/*
 *	Verificare capacitate minimala de functionare 
 */

		if ((lpModule->lpfnLogIn == NULL) || (lpModule->lpfnReadData == NULL) || (lpModule->lpfnWriteData == NULL) || 
			(lpModule->lpfnEnableWProtection == NULL) || (lpModule->lpfnDisableWProtection == NULL) || (lpModule->lpfnLogOut == NULL) || 
			(lpModule->lpfnKeyType == NULL) || (lpModule->lpfnEEpromSize == NULL) || (lpModule->lpfnIsLogged == NULL) ||
			 (lpModule->lpfnMasscomReadDataFromDevice == NULL) || (lpModule->lpfnMasscomWriteDataToDevice == NULL))
		{
			::FreeLibrary (lpModule->hDLL);

			return MASSCOM_LOAD_FAILURE_FNC_MISSING;
		}

#ifdef _MASSCOM_NO_MTHREAD_IMPLEMENT
		::InitializeCriticalSection (&lpModule->cs);
#endif

		return MASSCOM_LOAD_SUCCESS;
}


__inline int UnloadSATAMasscomModule (LPSATAMASSCOMPOINTERS lpModule)
{
	if (!lpModule) return MASSCOM_UNLOAD_FAILURE_PARAM;
	if (lpModule->hDLL == NULL) return MASSCOM_UNLOAD_FAILURE_PARAM;

	if (lpModule->hDLL)
	{
		if (!::FreeLibrary (lpModule->hDLL))
			return MASSCOM_UNLOAD_FAILURE_DLL;			// ::GetLastError() pentru extra
	}

#ifdef _MASSCOM_NO_MTHREAD_IMPLEMENT
		::DeleteCriticalSection (&lpModule->cs);
#endif

	lpModule->hDLL = NULL;

	return MASSCOM_UNLOAD_SUCCESS;
}

#ifdef _MASSCOM_NO_MTHREAD_IMPLEMENT

__inline void LockSection (LPSATAMASSCOMPOINTERS lpModule)
{
	if (!lpModule) return;
	::EnterCriticalSection(&lpModule->cs);
}

__inline void UnlockSection (LPSATAMASSCOMPOINTERS lpModule)
{
	if (!lpModule) return;
	::LeaveCriticalSection(&lpModule->cs);
}

#endif

#endif