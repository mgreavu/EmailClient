// AccountPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "AccountPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const COLORREF LTBBLUE = RGB(86, 163, 213);
/////////////////////////////////////////////////////////////////////////////
// CAccountPropertiesDlg

IMPLEMENT_DYNAMIC(CAccountPropertiesDlg, CPropertySheet)

CAccountPropertiesDlg::CAccountPropertiesDlg(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	m_psh.dwFlags &= ~PSH_HASHELP;
}

CAccountPropertiesDlg::CAccountPropertiesDlg(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	m_psh.dwFlags &= ~PSH_HASHELP;
}

CAccountPropertiesDlg::~CAccountPropertiesDlg()
{
}


BEGIN_MESSAGE_MAP(CAccountPropertiesDlg, CPropertySheet)
	//{{AFX_MSG_MAP(CAccountPropertiesDlg)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CAccountPropertiesDlg::OnInitDialog()
{
	BOOL bResult = CPropertySheet::OnInitDialog();
	
	CButton* OkButton = (CButton*) GetDlgItem(IDOK);
	CButton* CancelButton = (CButton*) GetDlgItem(IDCANCEL);
	
//	OkButton->SetButtonStyle(BS_OWNERDRAW);
//	CancelButton->SetButtonStyle(BS_OWNERDRAW);
	
	HWND hWndTab = (HWND)SendMessage(PSM_GETTABCONTROL);
//	m_tabCtrl.SubclassDlgItem(::GetDlgCtrlID(hWndTab), this);

	OwnerDrawButtons();

	return bResult;
}

HBRUSH CAccountPropertiesDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
	HBRUSH hbr = ::CreateSolidBrush(LTBBLUE);
	return hbr;
}
/*
void CAccountPropertiesDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{

		CPropertySheet::OnDrawItem(nIDCtl, lpDrawItemStruct);

}
*/
void CAccountPropertiesDlg::OwnerDrawButtons()
{
//	VERIFY(m_btnOk.Attach(IDOK, this, BBLUE, BLACK, LTBBLUE));
//	VERIFY(m_btnCancel.Attach(IDCANCEL, this, BBLUE, BLACK, LTBBLUE));
}
