// AddressBook.h: interface for the CAddressBook class.
//
//////////////////////////////////////////////////////////////////////
#include <list>
#include <algorithm>
using namespace std;

#if !defined(AFX_ADDRESSBOOK_H__6D2633C6_AE6F_49F6_BDEA_365EBE5866B2__INCLUDED_)
#define AFX_ADDRESSBOOK_H__6D2633C6_AE6F_49F6_BDEA_365EBE5866B2__INCLUDED_

#include "Contact.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



class CAddressBook: public CObject  
{
	DECLARE_SERIAL (CAddressBook)

public:
	void refreshcontact(CContact contact, CString old_Nick);
	void clearAddressBook();
	BOOL readAddressBook(CString FilePath);
	BOOL saveAddressBook(CString FilePath);
	CAddressBook& operator =(CAddressBook& adrBook);
	typedef list<CContact>	ContactList;
	static const int NICK;
	static const int FULLNAME;

	void getContact(CContact& destinationContact, CString strIdentifier, int identifierType);
	void removeContact(CString nickName);
	void addContact(CContact newContact);
	CAddressBook(CString addrBookPath);
	CAddressBook();
	virtual ~CAddressBook();
	ContactList myContactList;
	ContactList::iterator ContactListIterator;
private:
	virtual void CAddressBook::Serialize (CArchive &ar);

};

#endif // !defined(AFX_ADDRESSBOOK_H__6D2633C6_AE6F_49F6_BDEA_365EBE5866B2__INCLUDED_)
