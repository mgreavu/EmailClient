#if !defined(AFX_ADDACCOUNTDLG_H__41DDFF90_B98A_4491_9020_A1BDDF9A631A__INCLUDED_)
#define AFX_ADDACCOUNTDLG_H__41DDFF90_B98A_4491_9020_A1BDDF9A631A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddAccountDlg.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CAddAccountDlg dialog

class CAddAccountDlg : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	CAddAccountDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAddAccountDlg)
	enum { IDD = IDD_ADDACCOUNT_DIALOG };
	BOOL	m_RememberPassw;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddAccountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSXButton m_btnAdd;
	CSXButton m_btnCancel;
	virtual BOOL OnInitDialog();

	// Generated message map functions
	//{{AFX_MSG(CAddAccountDlg)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnAddBtn();
	afx_msg void OnRememberPasswOption();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDACCOUNTDLG_H__41DDFF90_B98A_4491_9020_A1BDDF9A631A__INCLUDED_)
