#include "stdafx.h"
#include "AccountListTypes.h"
#include "ConnectDefs.h"
#include "ras.h"
#include "rasdlg.h"
#include "raserror.h"
#include "resource.h"

//s_MIASettings MIASettings;

/*BOOL MIAConnected = FALSE;
BOOL OperationInProgress = FALSE;

//Events, errors
HANDLE  CMOpCompleteEvent;
int     CMLastError;
int     WSALastError;

HANDLE  MIACmdCompleteEvent;
int     MIALastCmdResult;

UINT    MIAUserID;

char    MIAAccessType;
char    MIAEmbeddedKeyA[USER_KEY_LEN];
char    MIAKeyA[USER_KEY_LEN];
char    MIAKeyB[USER_KEY_LEN];*/


// Dial-up networking
//*****************************************************************
// checks the version of the operating system and if it's >= NT4.0 the
// RAS routines are used to init a dial-up connection, else we use the Win 9x way
// to open a dial-up session
// ------------------------------------------------------------------
// RETURNS: TRUE in case of success, FALSE in case of failure
//			 useIt = 1 - if RAS usage is possible
//			 useIt = 0 - if no RAS usage is allowed
//*****************************************************************
BOOL UseRASDialUp(int* useIt)
{
OSVERSIONINFOEX osvi;
BOOL bOsVersionInfoEx;

	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	
	if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
	{
		// If OSVERSIONINFOEX doesn't work, try OSVERSIONINFO.
		
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
		if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) 
			return FALSE;
	}
	

	if ((osvi.dwPlatformId == VER_PLATFORM_WIN32_NT) && (osvi.dwMajorVersion >= 4))
	{
		//we have an NT 4.0 or newer system
		*useIt = 1;
	}
	else
		*useIt = 0;
	
	return TRUE;
}

typedef BOOL (APIENTRY *RAS_DIAL_DLG) (LPSTR, LPSTR, LPSTR, LPRASDIALDLG);

#ifdef _UNICODE
#define RasDialDialog L "RasDialDlgW"
#else
#define RasDialDialog "RasDialDlgA"
#endif


//*****************************************************************
// Opens the dial -up dialog on a windows NT 4.0+ system
// RETURNS: TRUE - if the connection is established
//		    FALSE - otherwise
//          errCode - contains the error code
//					(FALSE + errCode == 0) ---> the user cancelled the connection
//*****************************************************************
BOOL InitRASDialUp(LPSTR lpszSessionName, DWORD* errCode)
{
BOOL resConn;
RASDIALDLG rasDlgStrct;

HINSTANCE hinstLib;
RAS_DIAL_DLG ProcAddr;
BOOL fFreeResult, fRunTimeLinkSuccess = FALSE;
CString sMsg;
CString sMsg1;

	hinstLib = LoadLibrary("Rasdlg.dll");

	rasDlgStrct.dwSize = sizeof(RASDIALDLG);
	rasDlgStrct.hwndOwner = NULL;
	rasDlgStrct.dwError = 0;
	rasDlgStrct.dwFlags = 0;
	rasDlgStrct.dwSubEntry = 0;
	rasDlgStrct.reserved = 0;
	rasDlgStrct.reserved2 = 0;
	rasDlgStrct.xDlg = 0;
	rasDlgStrct.yDlg = 0;

	
    if (hinstLib != NULL) 
    { 
        ProcAddr = (RAS_DIAL_DLG) GetProcAddress(hinstLib, RasDialDialog);
		if (fRunTimeLinkSuccess = (ProcAddr != NULL))
		{
			resConn = (ProcAddr) (NULL, lpszSessionName, NULL, &rasDlgStrct);
		}
		fFreeResult = FreeLibrary(hinstLib);
	}
	else
		{
			//could not load the rasdlg.dll library!
			*errCode = 1;	//?
			return FALSE;
		}

	//resConn = RasDialDlg(NULL, lpszSessionName, NULL, &rasDlgStrct);

	if (! fRunTimeLinkSuccess) 
	{
		sMsg.LoadString(IDS_RASDIALDLG);
		sMsg1.LoadString(IDS_ERR);

		MessageBox(NULL, sMsg, sMsg1, MB_ICONERROR | MB_OK);

		*errCode = rasDlgStrct.dwError;
		return FALSE;
	}

	if (resConn == 0)
	{
		*errCode = rasDlgStrct.dwError;
		// if the errCode == 0 --- > the user pressed cancel
		//		else a system or RAS error code is returned
		return FALSE;
	}
	else
		{
			return TRUE;
		}

}


//*****************************************************************
// Opens the Dial-Up dialog box on a Win9x system
// INPUT:
//			- lpszSessionName - The name of the connection
// OUTPUT:
//			- errCode	- the Error code in case that the execution failed
//			- pHandle	- handle to the dialog box process (to allow to see if it was closed or not)
//*****************************************************************
BOOL InitRnauiDialUp(LPSTR lpszSessionName, DWORD* errCode, HANDLE* pHandle)
{
	char runLine[256] = "rnaui.dll,RnaDial ";
	SHELLEXECUTEINFO shei;

	strcat(runLine, lpszSessionName);

	shei.cbSize = sizeof(SHELLEXECUTEINFO);
	shei.fMask  = SEE_MASK_FLAG_NO_UI | SEE_MASK_NOCLOSEPROCESS;
	shei.hwnd   = NULL;
	shei.lpVerb = _T("open");
	shei.lpFile = "rundll32";
	shei.lpParameters = runLine;
	shei.lpDirectory = NULL;
	shei.nShow = SW_SHOWNORMAL;

	if (!ShellExecuteEx(&shei))
	{		*errCode = GetLastError();
		return FALSE;
	}
	else
		{
			*pHandle = shei.hProcess;
			return TRUE;
		}
}



//*****************************************************************
//	returns TRUE and if the dial-up session is active and errCode must be 0
//	returns FALSE if the dial-up session is not active or error encountered
//
// INPUT: -
//			lpszSessionName - the name of the connection we want to test if it's active
// OUTPUT:
//			errCode			- 0 on success, != 0 if error encountered
//
//*****************************************************************
BOOL ActiveDialUpSession(LPSTR lpszSessionName, DWORD* errCode)
{
	//check to see the active dial-up connections
	RASCONN*	lpRasConn;
	DWORD		lpcb;
	DWORD		lpcConnections;
	DWORD		nRet;
	
/*	lpRasConn = (LPRASCONN) GlobalAlloc(GPTR, sizeof(RASCONN));
	lpRasConn->dwSize = 412; //!!??!!?! - the size for a record of the buffer
	lpcb = lpRasConn->dwSize*256;	//the buffer size*/
	lpRasConn = (LPRASCONN) GlobalAlloc(GPTR, sizeof(RASCONN));
	lpRasConn->dwSize = sizeof(RASCONN);
	lpcb = sizeof(RASCONN);
	
	
	nRet = RasEnumConnections(lpRasConn, &lpcb, &lpcConnections);
	if (nRet != 0)
	{
		*errCode = nRet;
		return FALSE;
	}
	else
	{
		//RasEnumConnections successfully executed
		
		//check to see if our dial-up connection is found in the active connections list
		for (UINT i=0; i<lpcConnections; i++)
		{
			if (strcmp(lpszSessionName, lpRasConn->szEntryName) ==0)
				return TRUE;
			//go to the next entry
			lpRasConn++;
		}
		
		//the specified session was not found active
		return FALSE;
	}	
}



//Error functions
void ErrorMessageBox(HWND hwnd, UINT IDString, UINT IDTitle, DWORD errCode)
{
	CString mes;
	CString title;
	
	char buffer[256];
	_ultoa( errCode, buffer, 10 );
	
	mes.LoadString(IDString);
	mes += " ";
	mes += buffer;

	title.LoadString(IDTitle);
	
	MessageBox(hwnd, mes, title, MB_ICONERROR | MB_OK);
}



BOOL SetDialUpConnection()
{
	//check if our dial-up connections is active.
	//if not, then open a new dial-up connection
	DWORD errCode;
	CString account = (*AccListIterator).GetRasAccount();
	if (!ActiveDialUpSession(account.GetBuffer(255), &errCode)) 
	{
		//if the running version of windows is 2000
		int useRAS = 0;
		DWORD err = 0;
		
		if (UseRASDialUp(&useRAS)) 
		{
			if (useRAS == 1) 
			{
				//init the dial up using RAS
				if (!InitRASDialUp(account.GetBuffer(255), &err)) 
				{
					if (err == 0) 
					{
						//ShowMIAMessage(GetParent()->m_hWnd, IDS_CONN_OPC_ENGLISH, IDS_ERROR_ENGLISH, MB_OK | MB_ICONSTOP);
						return FALSE;
					}	
					else 
					{
						//ErrorMessageBox(GetParent()->m_hWnd,IDS_MIA_ERR_SYS_ENGLISH+m_Language, IDS_ERROR_ENGLISH+m_Language, err);
						return FALSE;
					}
				}
			}	
			else 
				//or if the version is 9x
			{
				//init the dial up using rundll
				HANDLE hnd;
				InitRnauiDialUp(account.GetBuffer(255), &err, &hnd);
				
				if (err != 0) 
				{
					//ErrorMessageBox(GetParent()->m_hWnd,IDS_MIA_ERR_DIALUP_ENGLISH+m_Language, IDS_ERROR_ENGLISH+m_Language, err);
					return FALSE;
				}
				
				//wait until the dialog is closed
				int x = 0;
				DWORD ExitCode;
				while (x == 0) 
				{
					GetExitCodeProcess(hnd, &ExitCode);
					if (ExitCode != 259)		//STILL_ACTIVE
						x = 1;
					Sleep(100);
				}
			}
		} 
	//	else
			//ShowMIAMessage(GetParent()->m_hWnd, IDS_MIA_ERR_WINVER_ENGLISH, IDS_ERROR_ENGLISH, MB_ICONERROR | MB_OK);
	}	
	else
		if (errCode !=0) 
		{
		//	ErrorMessageBox(GetParent()->m_hWnd,IDS_MIA_ERR_DIALUP_ENGLISH+m_Language, IDS_ERROR_ENGLISH+m_Language, errCode);
		}
		
		//check again to see if our dial-up connection is active
		if (!ActiveDialUpSession(account.GetBuffer(255), &errCode)) 
		{
			CString errMsg;
			CString errTitle;
			//errMsg.Format(IDS_MIA_ERR_DUPSTART_ENGLISH+m_Language, MIASettings.SessionName);
			//errTitle.LoadString(IDS_ERROR_ENGLISH+m_Language);
			//MessageBox(errMsg, errTitle, MB_ICONERROR | MB_OK);
			return FALSE;
		}
		
		return TRUE;
}