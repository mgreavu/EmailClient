// EmailClient.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "EmailClient.h"
#include "EmailClientDlg.h"
#include "AppSettings.h"
#ifdef _LOG
#include "log/Log.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString EmailClientFolder = "C:\\Email Client\\";


/////////////////////////////////////////////////////////////////////////////
// CEmailClientApp

BEGIN_MESSAGE_MAP(CEmailClientApp, CWinApp)
	//{{AFX_MSG_MAP(CEmailClientApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEmailClientApp construction

CEmailClientApp::CEmailClientApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CEmailClientApp object

CEmailClientApp theApp;
HANDLE hEMailClientMutex = INVALID_HANDLE_VALUE;
CAppSettings ECSettings;


#ifdef _LOG
CLog Log;	
#endif

/////////////////////////////////////////////////////////////////////////////
// CEmailClientApp initialization

		
BOOL CEmailClientApp::InitInstance()
{
	hEMailClientMutex = ::CreateMutex(NULL, FALSE, "_EMailClientMutex");

	if (GetLastError() == ERROR_ALREADY_EXISTS) 
	{
		::CloseHandle(hEMailClientMutex);
		return FALSE;
	}
	else
	{
#ifdef _LOG
		Log.SetLogFile ("EC.log");
		Log.InsertLogSystemData (0x02, "EC");

		char sFlags[512];
		memset (sFlags, 0x00, 512);
		strncpy (sFlags, "Defined compilation flags: ", strlen("Defined compilation flags: "));
		strncat(sFlags, "_LOG", strlen("_LOG"));
#ifdef _MASSCOM_NO_MTHREAD_IMPLEMENT
		strncat(sFlags, " | ", strlen(" | "));
		strncat(sFlags, "_MASSCOM_NO_MTHREAD_IMPLEMENT", strlen("_MASSCOM_NO_MTHREAD_IMPLEMENT"));
#endif
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, sFlags);
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, "CEmailClientApp::InitInstance()");
#endif

		if (!ECSettings.LoadBlackDll()) 
		{
			::MessageBox (NULL, "Unable to initialize USB Key support module. Exiting.", "Email Client", MB_OK | MB_ICONSTOP);
			return FALSE;
		}

		if (ECSettings.IfSATAMasscom.lpfnLogIn(KEY_ID, KEY_PASS) != KEY_SUCCESS)
		{
			::MessageBox (NULL, "Unable to logon to USB Key. Exiting.", "Email Client", MB_OK | MB_ICONSTOP);
			return FALSE;
		}

		if (!ECSettings.ReadUserData())
		{
			::MessageBox (NULL, "Unable to read USB Key's EEPROM. Exiting.", "Email Client", MB_OK | MB_ICONSTOP);
			return FALSE;
		}

		char PublicDriveLetter = ECSettings.IfSATAMasscom.lpfnGetPublicDrive();

		if (!AfxSocketInit())
		{
			AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
			return FALSE;
		}

		AfxEnableControlContainer();
	
		if (!ECSettings.IniReadECSettings())
		{
			::MessageBox (NULL, "Unable to initialize the application", "Email Client", MB_OK | MB_ICONSTOP);
			return FALSE;
		}

		if (ECSettings.AccountsFolder.IsEmpty())
		{
			CString sMsg;
			BROWSEINFO bi = {0};
			bi.lpszTitle = _TEXT("Choose email folder") ;
			
			LPITEMIDLIST pidl = SHBrowseForFolder (&bi);

			if (!pidl)
			{			
				sMsg.LoadString(IDS_SEL_DEF_FOLD);				
				AfxMessageBox(sMsg);
				
				return FALSE;
			}

			CString m_Path;
			char path[MAX_PATH + 1];
			memset (path, 0x00, sizeof(char)*(MAX_PATH+1));

			if (SHGetPathFromIDList (pidl, path))
			{
				m_Path.Empty();
				m_Path = path;

				if (m_Path.Mid((m_Path.GetLength() - 1),1) == "\\") 
				{
					m_Path = m_Path.Mid(0, m_Path.GetLength() - 1);	
				}

				m_Path += "\\";
				m_Path += EC_MAIN_FOLDER;

				if (!CreateDirectory(m_Path ,NULL)) 
				{
					if (::GetLastError() != ERROR_ALREADY_EXISTS) 
					{
						::MessageBox(NULL, "Error creating main Email Client folder", m_Path, MB_OK | MB_ICONSTOP);
						return FALSE;
					}
				}

				if(!CreateDirectory(m_Path + "\\Accounts", NULL))
				{
					if (::GetLastError() != ERROR_ALREADY_EXISTS) AfxMessageBox("Error creating Accounts folder");					
				}

				if(!CreateDirectory(m_Path + "\\Keys", NULL))
				{
					if (::GetLastError() != ERROR_ALREADY_EXISTS) AfxMessageBox("Error creating Keys folder");					
				}		

				ECSettings.AccountsFolder = m_Path;				
				if (ECSettings.AccountsFolder.GetAt(0) == PublicDriveLetter) ECSettings.bAccountsFolderOnKey = 1;

				ECSettings.IniWriteECSettings();
				
			}
		}
		else
		{
			WIN32_FIND_DATA w32fd;
			HANDLE hSearch = INVALID_HANDLE_VALUE;

			if ((hSearch = FindFirstFile(ECSettings.AccountsFolder, &w32fd)) == INVALID_HANDLE_VALUE)
			{
				::FindClose(hSearch);
				::MessageBox (NULL, "The application folder cannot be found or is inaccessible.", ECSettings.AccountsFolder, MB_OK | MB_ICONSTOP);
				return FALSE;
			}
			::FindClose(hSearch);
		}

		ECSettings.GetAllEncCodes();

		CEmailClientDlg dlg;
		m_pMainWnd = &dlg;

		int nResponse = dlg.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with OK
		}
		else if (nResponse == IDCANCEL)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with Cancel
		}
	}

	return FALSE;
}

int CEmailClientApp::ExitInstance() 
{
	if (ECSettings.IfSATAMasscom.lpfnIsLogged()) ECSettings.IfSATAMasscom.lpfnLogOut();
	ECSettings.UnloadBlackDll();

	if (hEMailClientMutex) ::ReleaseMutex(hEMailClientMutex);
#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("CEmailClientApp::ExitInstance()"));
	Log.CloseLogFile();
#endif
	
	return CWinApp::ExitInstance();
}
