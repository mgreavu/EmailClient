// SendMail.cpp : implementation file
//

#include "stdafx.h"
#include "emailclient.h"
#include "SendMail.h"
#include "smtp\base64.h"
#include "smtp\md5.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSendMail dialog


CSendMail::CSendMail(CWnd* pParent, CMailMsg MailMsg): CDialog(CSendMail::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSendMail)
	//}}AFX_DATA_INIT

	mailmsg = MailMsg;
	hClientSocket = INVALID_SOCKET;
	m_bConnected = FALSE;
	lpBodySendBuffer = NULL;
	dwBodyBuffSize = 0;
}


void CSendMail::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSendMail)
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_OP_PROG, m_ProgBar);
	DDX_Control(pDX, IDC_FRAME, m_Frame);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSendMail, CDialog)
	//{{AFX_MSG_MAP(CSendMail)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSendMail message handlers

BOOL CSendMail::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText (SMTPServerName);
	sMsg.LoadString(IDS_EMAIL_TRANSFER);
	m_Frame.SetWindowText (sMsg);
	
	LOGFONT fnt;
    fnt.lfHeight = 16; 
    fnt.lfWidth = 0; 
    fnt.lfEscapement = 0; 
    fnt.lfOrientation = 0; 
    fnt.lfWeight = 400; 
    fnt.lfItalic = FALSE; 
    fnt.lfUnderline = 0; 
    fnt.lfStrikeOut = 0; 
    fnt.lfCharSet = 0; 
    fnt.lfOutPrecision = 0; 
    fnt.lfClipPrecision = 0; 
    fnt.lfQuality = ANTIALIASED_QUALITY; 
    fnt.lfPitchAndFamily = 0; 
    memmove (fnt.lfFaceName, "Arial", strlen("Arial")); 


	m_ProgBar.SetFont (fnt);		
	m_ProgBar.SetPos (0);

	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );

	bSendStatus = 3;		// send failed

	::SetTimer (m_hWnd, 2006, 500, NULL);
	
	return TRUE;
}

void CSendMail::OnCancel() 
{
	bSendStatus = 1;	//user cancel
	
	if (m_bConnected) 
	{
		shutdown (hClientSocket, SD_SEND);
		::SetTimer (m_hWnd, 1980, 5000, NULL);
	}
	else
	{
		if (lpBodySendBuffer) free (lpBodySendBuffer);
		CDialog::OnCancel();
	}
}

LRESULT CSendMail::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	WSABUF	DataBuff;
	DWORD	dwFlags = 0;
	DWORD   dwBytesRead = 0;
	DWORD	dwBytesWritten = 0;
	int		WSALast = 0;
	
	switch (message)
	{
		case UWM_ASYNCSELECT: {
								SOCKET hSock = (SOCKET)wParam;
								
								WORD wRes = 0;
								if ((wRes = WSAGETSELECTERROR(lParam)))
								{
									DisplayWSAError(wRes);
									
									if (m_bConnected)
									{
										shutdown (hClientSocket, SD_SEND);
										::SetTimer (m_hWnd, 1980, 5000, NULL);
										break;
									}

									closesocket(hClientSocket);
									if (lpBodySendBuffer) free (lpBodySendBuffer);

									bSendStatus = 3;		//send failed
									CDialog::OnCancel();
									
									break;
								}

								switch(wRes = WSAGETSELECTEVENT(lParam))
								{
									case FD_CONNECT: 
										{
											m_bConnected = TRUE;
											dwCurrentSendingBufferSize = 0;

											memset (&m_receiveBuffer, 0x00, COMM_READ_BUFF_SIZE);
											dwReceivedBytes = 0;

											memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
											dwSentBytes = 0;

											nOpType = 0;
											
											sMsg.LoadString(IDS_CONNECTION_SUCCESSFULL);
											FrameCaption = sMsg;
											m_Frame.SetWindowText (FrameCaption);

											break;
										}

									case FD_WRITE: 
										{											
											if (nOpType == SMTP_BODY)	// ramura FD_WRITE
											{
												while (dwBodySentBytes < dwBodyBuffSize)
												{
													ASSERT (lpBodySendBuffer != NULL);
													
													DataBuff.buf = (lpBodySendBuffer + dwBodySentBytes);			
													DataBuff.len = __min ((dwBodyBuffSize - dwBodySentBytes), COMM_WRITE_BUFF_SIZE);

													if (WSASend(hClientSocket, &DataBuff, 1, &dwBytesWritten, dwFlags, NULL, NULL) == SOCKET_ERROR)
													{
														if ((WSALast = WSAGetLastError()) != WSAEWOULDBLOCK) 
														{
															shutdown (hClientSocket, SD_SEND);
														}
														return CDialog::WindowProc(message, wParam, lParam);
													}
													else
													{
														dwBodySentBytes += dwBytesWritten;

														UpdateProgBar();

														if (dwBodySentBytes == dwBodyBuffSize)
														{
															memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
															dwCurrentSendingBufferSize = 0;
															dwSentBytes = 0;

															return CDialog::WindowProc(message, wParam, lParam);												
														}
													}
												}												
												return CDialog::WindowProc(message, wParam, lParam);
											}

											if (!dwCurrentSendingBufferSize)
											{
												switch(nOpType) 
												{
												case SMTP_EHLO:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CMD_EHLO);
														strcat (m_sendBuffer, " ");
														strcat (m_sendBuffer, "MiG");
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen (CMD_EHLO) + strlen("MiG")  + strlen (TERM_STR) + 1;
														break;
													}
												case SMTP_AUTH_CRAM_MD5:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CMD_AUTH_CRAM_MD5);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen(CMD_AUTH_CRAM_MD5) + strlen (TERM_STR);
														break;
													}
												case SMTP_AUTH_CRAM_MD5_REPLY:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CRAMMD5ReplyString);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = CRAMMD5ReplyString.GetLength() + strlen (TERM_STR);
														break;
													}
												case SMTP_AUTH_LOGIN:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CMD_AUTH_LOGIN);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen(CMD_AUTH_LOGIN) + strlen (TERM_STR);
														break;
													}
												case SMTP_AUTH_LOGIN_USER:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														unsigned char* encuser = NULL;
														CString m_sAuthName = SMTPUserName;
														DWORD size = m_sAuthName.GetLength();
														unsigned char* lpBuff = (unsigned char*) malloc(size + 1);
														memset (lpBuff, 0x00, size + 1);
														strcpy((char *)lpBuff, m_sAuthName.GetBuffer(size));
														*(lpBuff + size) = NULL;
														DWORD dwOutSz = 0;
														encuser = Base64Encode(lpBuff, size, encuser, dwOutSz);
														CString enctmp = (CString) encuser;
														strcpy (m_sendBuffer, enctmp);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen(enctmp) + strlen (TERM_STR);

														free (encuser);			
														free(lpBuff);
														break;
													}
												case SMTP_AUTH_LOGIN_PWD:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														unsigned char* encpwd = NULL;
														DWORD size = (SMTPPassword).GetLength();
														unsigned char* lpBuff = (unsigned char*) malloc(size + 1);
														memset (lpBuff, 0x00, size + 1);
														strcpy((char *)lpBuff, (SMTPPassword).GetBuffer(size));			
														*(lpBuff + size) = NULL;
														DWORD dwOutSz = 0;
														encpwd = Base64Encode(lpBuff, size, encpwd, dwOutSz);
														CString enctmp = (CString) encpwd;
														strcpy (m_sendBuffer, enctmp);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen(enctmp) + strlen (TERM_STR);

														free (encpwd);			
														free(lpBuff);
														break;
													}
												case SMTP_HELO:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CMD_HELO);
														strcat (m_sendBuffer, " ");
														strcat (m_sendBuffer, "MiG");
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen (CMD_HELO) + strlen("MiG") + strlen (TERM_STR) + 1;

														break;
													}
												case SMTP_MAIL_FROM:
													{
														memset(m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy(m_sendBuffer, CMD_MAIL_FROM);
														strcat (m_sendBuffer, mailmsg.m_FromAddr);
														strcat(m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen(CMD_MAIL_FROM) + mailmsg.m_FromAddr.GetLength() + strlen(TERM_STR);

														break;
													}
												case SMTP_RCPT_TO:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CMD_RCPT_TO);
														strcat (m_sendBuffer, mailmsg.m_ToAddress);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen (CMD_RCPT_TO) + mailmsg.m_ToAddress.GetLength() + strlen(TERM_STR);

														break;
													}
												case SMTP_DATA:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CMD_DATA);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen(CMD_DATA) + strlen (TERM_STR);

														break;
													}
												case SMTP_QUIT:
													{
														memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
														strcpy (m_sendBuffer, CMD_QUIT);
														strcat (m_sendBuffer, TERM_STR);
														dwCurrentSendingBufferSize = strlen(CMD_QUIT) + strlen (TERM_STR);
													}
												default: return CDialog::WindowProc(message, wParam, lParam);
												}
											}

											while (dwSentBytes < dwCurrentSendingBufferSize)	// transmisia bufferului creat mai sus spre server
											{
												DataBuff.buf = (m_sendBuffer + dwSentBytes);			
												DataBuff.len = __min ((dwCurrentSendingBufferSize - dwSentBytes), COMM_WRITE_BUFF_SIZE);

												if (WSASend(hClientSocket, &DataBuff, 1, &dwBytesWritten, dwFlags, NULL, NULL) == SOCKET_ERROR)
												{
													if ((WSALast = WSAGetLastError()) != WSAEWOULDBLOCK) 
													{
														shutdown (hClientSocket, SD_SEND);
													}
													return CDialog::WindowProc(message, wParam, lParam);
												}
												else
												{
													dwSentBytes += dwBytesWritten;
	
													if (dwSentBytes == dwCurrentSendingBufferSize)		// pachet trimis complet
													{
														
														if (nOpType == SMTP_QUIT)
														{
															shutdown (hClientSocket, SD_SEND);
														}
														else
														{
															memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
															dwCurrentSendingBufferSize = 0;
															dwSentBytes = 0;
														}
														return CDialog::WindowProc(message, wParam, lParam);		
																								
													}
												}
											}

											break;
										}
										
									case FD_READ: 
										{
											ZeroMemory (&m_receiveBuffer, COMM_READ_BUFF_SIZE);
											DataBuff.buf = (char*)&m_receiveBuffer;			
											DataBuff.len = COMM_READ_BUFF_SIZE;

											if (WSARecv(hClientSocket, &DataBuff, 1, &dwBytesRead, &dwFlags, NULL, NULL) == SOCKET_ERROR)
											{
												return CDialog::WindowProc(message, wParam, lParam);
											}
											else
											{
/*
 *	sFullReply contine raspunsul partial/total receptionat de WSARecv() de la server.
 *	Verificarea unui mesaj complet se face in IsReplyComplete(), care adauga in sFullReply cat o citit curent in m_receiveBuffer.
 *	Daca raspunsul e incomplet, la urmatorul FD_READ se repeta chestia, pana cand IsReplyComplete() intoarce TRUE.
 *	
 */
												if (IsReplyComplete())			
													ProcessSMTPServerReply();	
												else 
													return CDialog::WindowProc(message, wParam, lParam);
											}


											switch(nOpType) 
											{
											case SMTP_EHLO:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CMD_EHLO);
													strcat (m_sendBuffer, " ");
													strcat (m_sendBuffer, "MiG");
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen (CMD_EHLO) + strlen("MiG")  + strlen (TERM_STR) + 1;
													break;
												}
											case SMTP_AUTH_CRAM_MD5:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CMD_AUTH_CRAM_MD5);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen(CMD_AUTH_CRAM_MD5) + strlen (TERM_STR);
													break;
												}
											case SMTP_AUTH_CRAM_MD5_REPLY:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CRAMMD5ReplyString);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = CRAMMD5ReplyString.GetLength() + strlen (TERM_STR);
													break;
												}
											case SMTP_AUTH_LOGIN:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CMD_AUTH_LOGIN);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen(CMD_AUTH_LOGIN) + strlen (TERM_STR);
													break;
												}
											case SMTP_AUTH_LOGIN_USER:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													unsigned char* encuser = NULL;
													CString m_sAuthName = SMTPUserName;
													DWORD size = m_sAuthName.GetLength();
													unsigned char* lpBuff = (unsigned char*) malloc(size + 1);
													memset (lpBuff, 0x00, size + 1);
													strcpy((char *)lpBuff, m_sAuthName.GetBuffer(size));
													*(lpBuff + size) = NULL;
													DWORD dwOutSz = 0;
													encuser = Base64Encode(lpBuff, size, encuser, dwOutSz);
													CString enctmp = (CString) encuser;
													strcpy (m_sendBuffer, enctmp);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen(enctmp) + strlen (TERM_STR);

													free (encuser);			
													free(lpBuff);
													break;
												}
											case SMTP_AUTH_LOGIN_PWD:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													unsigned char* encpwd = NULL;
													DWORD size = (SMTPPassword).GetLength();
													unsigned char* lpBuff = (unsigned char*) malloc(size + 1);
													memset (lpBuff, 0x00, size + 1);
													strcpy((char *)lpBuff, (SMTPPassword).GetBuffer(size));			
													*(lpBuff + size) = NULL;
													DWORD dwOutSz = 0;
													encpwd = Base64Encode(lpBuff, size, encpwd, dwOutSz);													
													CString enctmp = (CString) encpwd;
													strcpy (m_sendBuffer, enctmp);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen(enctmp) + strlen (TERM_STR);

													free (encpwd);			
													free(lpBuff);
													break;
												}
											case SMTP_HELO:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CMD_HELO);
													strcat (m_sendBuffer, " ");
													strcat (m_sendBuffer, "MiG");
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen (CMD_HELO) + strlen("MiG") + strlen (TERM_STR) + 1;

													break;
												}
											case SMTP_MAIL_FROM:
												{
													memset(m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy(m_sendBuffer, CMD_MAIL_FROM);
													strcat (m_sendBuffer, mailmsg.m_FromAddr);
													strcat(m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen(CMD_MAIL_FROM) + mailmsg.m_FromAddr.GetLength() + strlen(TERM_STR);

													break;
												}
											case SMTP_RCPT_TO:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CMD_RCPT_TO);
													strcat (m_sendBuffer, mailmsg.m_ToAddress);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen (CMD_RCPT_TO) + mailmsg.m_ToAddress.GetLength() + strlen(TERM_STR);

													break;
												}
											case SMTP_DATA:
												{
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CMD_DATA);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen(CMD_DATA) + strlen (TERM_STR);

													break;
												}
											case SMTP_QUIT:
												{
													if ((dwCurrentSendingBufferSize) && (dwCurrentSendingBufferSize == dwSentBytes)) //pt fd_read-urile de dupa shutdown(sd_send)
													{
														return CDialog::WindowProc(message, wParam, lParam);
													}
													memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
													strcpy (m_sendBuffer, CMD_QUIT);
													strcat (m_sendBuffer, TERM_STR);
													dwCurrentSendingBufferSize = strlen(CMD_QUIT) + strlen (TERM_STR);
												}
											}

											
											if (nOpType == SMTP_BODY)		// ramura FD_READ
											{
												dwBodySentBytes = 0;
												while (dwBodySentBytes < dwBodyBuffSize)
												{
													ASSERT (lpBodySendBuffer != NULL);

													DataBuff.buf = (lpBodySendBuffer + dwBodySentBytes);			
													DataBuff.len = __min ((dwBodyBuffSize - dwBodySentBytes), COMM_WRITE_BUFF_SIZE);

													if (WSASend(hClientSocket, &DataBuff, 1, &dwBytesWritten, dwFlags, NULL, NULL) == SOCKET_ERROR)
													{
														if ((WSALast = WSAGetLastError()) != WSAEWOULDBLOCK) 
														{
															shutdown (hClientSocket, SD_SEND);
														}
														return CDialog::WindowProc(message, wParam, lParam);
													}
													else
													{
														dwBodySentBytes += dwBytesWritten;

														UpdateProgBar();

														if (dwBodySentBytes == dwBodyBuffSize)
														{
															memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
															dwCurrentSendingBufferSize = 0;
															dwSentBytes = 0;

															return CDialog::WindowProc(message, wParam, lParam);
												
														}
													}
												}
											}
											else
											{											
												while (dwSentBytes < dwCurrentSendingBufferSize)
												{
													DataBuff.buf = (m_sendBuffer + dwSentBytes);			
													DataBuff.len = __min ((dwCurrentSendingBufferSize - dwSentBytes), COMM_WRITE_BUFF_SIZE);

													if (WSASend(hClientSocket, &DataBuff, 1, &dwBytesWritten, dwFlags, NULL, NULL) == SOCKET_ERROR)
													{
														if ((WSALast = WSAGetLastError()) != WSAEWOULDBLOCK) 
														{
															shutdown (hClientSocket, SD_SEND);
														}
														return CDialog::WindowProc(message, wParam, lParam);
													}
													else
													{
														dwSentBytes += dwBytesWritten;														

														if (dwSentBytes == dwCurrentSendingBufferSize)
														{

															if (nOpType == SMTP_QUIT)
															{
																shutdown (hClientSocket, SD_SEND);
															}
															else
															{
																memset (m_sendBuffer, 0x00, COMM_WRITE_BUFF_SIZE);
																dwCurrentSendingBufferSize = 0;
																dwSentBytes = 0;
															}

															return CDialog::WindowProc(message, wParam, lParam);		
														}
													}
												}
											}
												
											break;
										}
										
									case FD_CLOSE: 
										{
											closesocket(hClientSocket);
											m_bConnected = FALSE;
											WSACleanup();
											
											sMsg.LoadString(IDS_CONNECTION_CLOSED);
											FrameCaption = sMsg;
											m_Frame.SetWindowText (FrameCaption);

											if (lpBodySendBuffer) free (lpBodySendBuffer);

											CDialog::OnCancel();

											break;
										}
								}
								
								break;	

							  }
	}
	
	return CDialog::WindowProc(message, wParam, lParam);
}


void CSendMail::OnTimer(UINT nIDEvent) 
{
	switch (nIDEvent)
	{
		case 2006:
		{
			::KillTimer(m_hWnd, nIDEvent);
			
			dwBodyBuffSize = TheMessage.GetLength();
			dwBodyBuffSize += strlen(TERM_MSG);			// <crlf>.<crlf>
			
			if ((lpBodySendBuffer = (char*) malloc(dwBodyBuffSize + 1)) == NULL)
			{
				::MessageBox(m_hWnd, "Memorie insuficienta", "malloc()", MB_OK | MB_ICONSTOP);
				
				CDialog::OnCancel();
			}

			memset (lpBodySendBuffer, 0x00, dwBodyBuffSize + 1);
			memmove (lpBodySendBuffer, TheMessage.GetBuffer(TheMessage.GetLength()), TheMessage.GetLength());
			strncat (lpBodySendBuffer, TERM_MSG, strlen(TERM_MSG));
			
			TheMessage.Empty();

			if (!Connect())
			{
				CString sMsg1 = SMTPServerName;
				sMsg1 += " - ";
				sMsg.LoadString(IDS_MAILSERVER_NOT_RESPONDING);
				sMsg1 += sMsg;

				::MessageBox (m_hWnd, sMsg1, "Email Client", MB_OK | MB_ICONSTOP);
				
				bSendStatus = 3;			// send failed
				free (lpBodySendBuffer);
				
				CDialog::OnCancel();
			}

		}
		break;

		case 1980:
		{
			::KillTimer(m_hWnd, nIDEvent);
			closesocket (hClientSocket);
			WSACleanup();
			if (lpBodySendBuffer) free (lpBodySendBuffer);
			m_bConnected = FALSE;

			CDialog::OnCancel();
		}
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

BOOL CSendMail::Connect()
{
	WSADATA WSAData;
	WORD wVersionRequested = MAKEWORD( 2, 0 );									
	WSAStartup(wVersionRequested, &WSAData);

	sMsg.LoadString(IDS_RESOLVING);
	FrameCaption = sMsg;
	FrameCaption += SMTPServerName;
	FrameCaption += "... ";

	m_Frame.SetWindowText (FrameCaption);

	struct hostent *hp = gethostbyname (SMTPServerName);

	if (hp == NULL) 
	{
		return FALSE;
	} 
	else 
	{
		unsigned int i=0;
		while (hp -> h_addr_list[i] != NULL) 
		{			
			SockAddr.sin_addr.s_addr = ((LPIN_ADDR)hp->h_addr)->s_addr;
			break;
			i++;
		}
	}

	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons((WORD)SMTPPort);

	unsigned long ulFIONBIO = 1;

	if ((hClientSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, 0)) == INVALID_SOCKET) 
	{
		WSACleanup();
		return FALSE;
	}

	if (ioctlsocket (hClientSocket, FIONBIO, &ulFIONBIO) == SOCKET_ERROR) 
	{ 
		closesocket (hClientSocket);
		WSACleanup();

		return FALSE;
	}

	if (WSAAsyncSelect(hClientSocket, m_hWnd, UWM_ASYNCSELECT, FD_CONNECT | FD_WRITE | FD_READ | FD_CLOSE) == SOCKET_ERROR) 
	{
		closesocket(hClientSocket);
		WSACleanup();

		return FALSE;
	}

	FrameCaption = " Conectare... ";
	m_Frame.SetWindowText (FrameCaption);


	if (WSAConnect (hClientSocket, (PSOCKADDR)&SockAddr, sizeof(SOCKADDR_IN), NULL, NULL, NULL, NULL) == SOCKET_ERROR)
	{
		if (WSAGetLastError() == WSAEWOULDBLOCK) return TRUE;

		closesocket(hClientSocket);
		WSACleanup();
		
		return FALSE;
	}

	return TRUE;

}

void CSendMail::OnOK() 
{
	

}

void CSendMail::UpdateProgBar()
{
	float pos = (float)dwBodySentBytes;
	pos = pos / (float)dwBodyBuffSize;
	pos *= 100;														
	m_ProgBar.SetPos(pos);
}

void CSendMail::DisplayWSAError(WORD wsaErr)
{
	CString sErrorMsg;

	switch (wsaErr)
	{
		case 10060: {
					sErrorMsg = SMTPServerName;
					sErrorMsg += " nu raspunde"; break;
				}
		default: {
					sMsg.LoadString(IDS_MAILSERVER_NOT_RESPONDING);
					sErrorMsg = sMsg;
					break;
				 }
	}

	::MessageBox (m_hWnd, sErrorMsg, "Email Client", MB_OK | MB_ICONSTOP);
}


void CSendMail::ProcessSMTPServerReply()
{
	switch (nOpType)
	{
		case 0:				{
								sMsg.LoadString(IDS_INITIALIZING);
								FrameCaption = sMsg;
								m_Frame.SetWindowText (FrameCaption);

								nOpType = SMTP_EHLO; 
								break;
							}
			
		case SMTP_EHLO:			{
									sMsg.LoadString(IDS_AUTHENTICATION);
									FrameCaption = sMsg;

									if (sFullReply.Find("LOGIN") != -1) 
									{										
										m_Frame.SetWindowText (FrameCaption);

										nOpType = SMTP_AUTH_LOGIN; break;
									}
									if (sFullReply.Find("CRAM-MD5") != -1) 
									{										
										m_Frame.SetWindowText (FrameCaption);

										nOpType = SMTP_AUTH_CRAM_MD5; 
										break;
									}

									nOpType = SMTP_MAIL_FROM; break;
								}

		case SMTP_AUTH_LOGIN:		nOpType = SMTP_AUTH_LOGIN_USER; break;
		case SMTP_AUTH_LOGIN_USER:	nOpType = SMTP_AUTH_LOGIN_PWD; break;
		case SMTP_AUTH_LOGIN_PWD: {
									if (sFullReply.Find("235") == -1) 
									{
										sMsg.LoadString(IDS_AUTHENTICATION_FAILED);
										::MessageBox (m_hWnd, sFullReply.Left(sFullReply.GetLength()-2) , sMsg, MB_OK | MB_ICONEXCLAMATION);

										bSendStatus = 2;	//auth failed
										nOpType = SMTP_QUIT;
										break;
									}
									else
									{
										sMsg.LoadString(IDS_AUTHENTICATION_SUCCESSFULL);
										FrameCaption = sMsg;
										m_Frame.SetWindowText (FrameCaption);
									}
									nOpType = SMTP_MAIL_FROM;
									break;
								}
		case SMTP_AUTH_CRAM_MD5: {
									if (sFullReply.Find("334") == -1) {nOpType = SMTP_QUIT; break;}

								    char* challenge = b64decode_alloc(m_receiveBuffer+4);
								    b64decode(m_receiveBuffer+4, challenge);    									 

									unsigned char digest[16]; 
									unsigned char digasc[33]; 									   
									static char hextab[] = "0123456789abcdef";

									const char* username = SMTPUserName.GetBuffer(SMTPUserName.GetLength());

									hmac_md5((unsigned char*)challenge, strlen(challenge), (unsigned char*)(SMTPPassword.GetBuffer(SMTPPassword.GetLength())), SMTPPassword.GetLength(), digest); 
									free (challenge);

  								    digasc[32] = 0;
									for (int i = 0; i < 16; i++) 
									{
									  digasc[2*i] = hextab[digest[i] >> 4];
									  digasc[2*i+1] = hextab[digest[i] & 0xf];
									} 

									unsigned char* decoded = (unsigned char *) malloc(strlen(username)+strlen((const char*)digasc)+2);
									memset (decoded, 0x00, strlen(username)+strlen((const char*)digasc)+2);

									strcpy((char*)decoded, username);
									decoded[strlen(username)] = ' ';
									strcpy((char*)decoded+strlen(username)+1, (char*)digasc);
									decoded[strlen(username)+strlen((char*)digasc)+1] = 0;

									unsigned char* encoded = NULL;

									DWORD dwOutSz = 0;
									encoded = Base64Encode(decoded, strlen((char*)decoded), encoded, dwOutSz);
 
									CRAMMD5ReplyString = encoded;

									free (encoded);
									free (decoded);


									nOpType = SMTP_AUTH_CRAM_MD5_REPLY;

									break;
									}

		case SMTP_AUTH_CRAM_MD5_REPLY: 
									{
										if (sFullReply.Find("235") == -1) 
										{
											sMsg.LoadString(IDS_AUTHENTICATION_FAILED);
											::MessageBox (m_hWnd, sFullReply.Left(sFullReply.GetLength()-2) , sMsg, MB_OK | MB_ICONEXCLAMATION);

											bSendStatus = 2;	//auth failed
											nOpType = SMTP_QUIT;
										}
										else
										{
											sMsg.LoadString(IDS_AUTHENTICATION_SUCCESSFULL);
											FrameCaption = sMsg;
											m_Frame.SetWindowText (FrameCaption);
											nOpType = SMTP_MAIL_FROM; 	
										}

										break;

									}

		case SMTP_HELO:				nOpType = SMTP_MAIL_FROM; break;

		case SMTP_MAIL_FROM:		nOpType = SMTP_RCPT_TO; break;

		case SMTP_RCPT_TO:		{
									if (sFullReply.Find("250") == -1) 
									{
										sMsg.LoadString(IDS_INCORRECT_DESTINATION);
										::MessageBox (m_hWnd, sFullReply.Left(sFullReply.GetLength()-2) , sMsg, MB_OK);
										nOpType = SMTP_QUIT;
									}
									else nOpType = SMTP_DATA;
									
									break;
								}

		case SMTP_DATA:			
						{	
									if (sFullReply.Find("354") == -1) 
										nOpType = SMTP_QUIT;
									else
									{
										sMsg.LoadString(IDS_TRANSFER_IN_PROGRESS);
										FrameCaption = sMsg;
										m_Frame.SetWindowText (FrameCaption);
										nOpType = SMTP_BODY;
									}

									break;
						}

		case SMTP_BODY:			{
									bSendStatus = 0;	// send success
									nOpType = SMTP_QUIT; 
									
									break;
								}
	}

	sFullReply.Empty();
}


BOOL CSendMail::IsReplyComplete()
{
	sFullReply += m_receiveBuffer;

	switch (nOpType)
	{
		case SMTP_EHLO:
		{
			if ((sFullReply.Find("250 ") == -1)) return FALSE;
			break;
		}

		default: break;
	}

	CString sEnd = sFullReply.Mid(sFullReply.GetLength()-2);
	if (sEnd != TERM_STR) return FALSE;

	return TRUE;
}


