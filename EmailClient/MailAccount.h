// MailAccount.h: interface for the CMailAccount class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAILACCOUNT_H__34A4C33A_93DE_4E86_B041_1DA684C8BE8E__INCLUDED_)
#define AFX_MAILACCOUNT_H__34A4C33A_93DE_4E86_B041_1DA684C8BE8E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMailAccount : public CObject
{
	DECLARE_SERIAL (CMailAccount)

public:
	int GetRemoveAfter();
	void SetRemoveAfter(int days);
	CString GetRasAccount();
	void SetRasAccount(CString m_cRasAccount);
	int ReadExport(UINT Index);
	BOOL WriteExport();
	CString GetMailAccountName ();
	void SetMailAccountName (CString MailAccName);
	CString GetSMTPUserName ();
	BOOL GetSMTPUsePOP3Settings ();
	int GetSMTPTimeout ();
	BOOL GetSMTPServerRequiresAuthentication ();
	CString GetSMTPServerName ();
	BOOL GetSMTPRememberPassword ();
	int GetSMTPPort ();
	CString GetSMTPPassword ();
	CString GetSMTPEmailAddress ();
	CString GetSMTPDisplayName ();
	CString GetPOP3UserName ();
	int GetPOP3Timeout ();
	CString GetPOP3ServerName ();
	BOOL GetPOP3RememberPassword ();
	int GetPOP3Port ();
	CString GetPOP3Password ();
	CString GetOrganization ();
	BOOL GetLeaveMailOnServer ();
	BOOL GetIncludeAccountInCheck ();
	int GetConnectionType ();
	CString GetAccountName ();
	void SetAccountFilePath (CString sFile);
	BOOL ReadAccountData();
	BOOL WriteAccountData();
	void SetSMTPUserName (CString sUserName);
	void SetSMTPUsePOP3Settings (BOOL bUsePOP3Settings);
	void SetSMTPTimeout (int nTimeout);
	void SetSMTPServerRequiresAuthentication (BOOL bRequiresAuth);
	void SetSMTPRememberPassword (BOOL bRememberPassw);
	void SetSMTPPassword (CString sPassw);
	void SetSMTPEmailAddress (CString sEmailAddr);
	void SetSMTPDisplayName (CString sDispName);
	void SetPOP3UserName (CString sUserName);
	void SetPOP3Timeout (int nTimeout);
	void SetPOP3RememberPassword (BOOL bRemember);
	void SetPOP3Password (CString sPassw);
	void SetOrganization (CString sOrganization);
	void SetLeaveMailOnServer (BOOL bLeaveMail);
	void SetIncludeAccountInCheck (BOOL bInclude);
	void SetPOP3Port (int nPort);
	void SetPOP3ServerName (CString sPOP3Serv);
	void SetSMTPPort (int nPort);
	void SetSMTPServerName (CString sSMTPServ);
	void SetAccountName (CString sAccName);
	void SetConnectionType (int ConnType);
    virtual void Serialize (CArchive &ar);
	CMailAccount();
	CMailAccount (const CMailAccount& mail);
	CMailAccount& operator=(CMailAccount& mail);
	virtual ~CMailAccount();

private:
	int RemoveAfter;
	CString RasAccount;
	CString MailAccountName;
	CString FilePath;
	BOOL SMTPRememberPassword;
	CString SMTPPassword;
	CString SMTPUserName;
	BOOL SMTPUsePOP3LogonSettings;
	BOOL SMTPServerRequiresAuthentication;
	BOOL POP3RememberPassword;
	BOOL IncludeAccountInCheck;
	CString Organization;
	CString POP3Password;
	CString POP3UserName;
	CString SMTPEmailAddress;
	CString SMTPDisplayName;
	BOOL LeaveMailOnServer;
	int ConnectionType;
	int POP3Timeout;
	int SMTPTimeout;
	int SMTPPort;
	int POP3Port;
	CString AccountName;
	CString POP3ServerName;
	CString SMTPServerName;
};

#endif // !defined(AFX_MAILACCOUNT_H__34A4C33A_93DE_4E86_B041_1DA684C8BE8E__INCLUDED_)
