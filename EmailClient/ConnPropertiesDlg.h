#if !defined(AFX_CONNPROPERTIESDLG_H__6A19C8DA_AF8C_43A5_80A5_E726035DA99A__INCLUDED_)
#define AFX_CONNPROPERTIESDLG_H__6A19C8DA_AF8C_43A5_80A5_E726035DA99A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConnPropertiesDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CConnPropertiesDlg dialog

class CConnPropertiesDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CConnPropertiesDlg)

// Construction
public:
	CString m_sRasAccount;
	BOOL m_bOnChange;
	CConnPropertiesDlg();
	~CConnPropertiesDlg();

// Dialog Data
	//{{AFX_DATA(CConnPropertiesDlg)
	enum { IDD = IDD_CONN_PROP_DLG };
	CComboBox	m_cRasAccounts;
	int		m_Connection;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CConnPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CConnPropertiesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClickConn();
	afx_msg void OnSelchangeRasAccounts();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONNPROPERTIESDLG_H__6A19C8DA_AF8C_43A5_80A5_E726035DA99A__INCLUDED_)
