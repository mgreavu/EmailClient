#if !defined(AFX_RECIVEPROGRES_H__9594B27B_1B44_40DB_A9BF_5DC1EFBF8FDB__INCLUDED_)
#define AFX_RECIVEPROGRES_H__9594B27B_1B44_40DB_A9BF_5DC1EFBF8FDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReciveProgres.h : header file
//

#include "smtp/Pop3Socket.h"
#include "interface/XProgressBar.h"
#include "interface/Sxbutton.h"
#include "afxwin.h"

/////////////////////////////////////////////////////////////////////////////
// CReciveProgres dialog

class CReciveProgres : public CDialog
{
// Construction
public:
	DWORD dwTotalBytes;
	DWORD dwBytesCurrently;
	int m_iSelected;
	LRESULT OnOpComplete(WPARAM w, LPARAM l);
	CReciveProgres(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CReciveProgres)
	enum { IDD = IDD_RECIVE_PROGRES };
	CXProgressBar	m_ReciveProgres;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReciveProgres)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CReciveProgres)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CPop3Socket m_Pop3Sock;
public:
	CSXButton m_btnCancel;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RECIVEPROGRES_H__9594B27B_1B44_40DB_A9BF_5DC1EFBF8FDB__INCLUDED_)
