// Contact.h: interface for the CContact class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTACT_H__FDCD17A3_26D1_42A0_9EED_1F1857FE256C__INCLUDED_)
#define AFX_CONTACT_H__FDCD17A3_26D1_42A0_9EED_1F1857FE256C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CContact  
{


public:
	CString m_FullName;
	CString m_NickName;
	CString m_EmailAdress;
	CString m_Street;
	CString m_City;
	CString m_Zip;
	CString m_Country;
	CString m_PhoneMobile;
	CString m_PhoneOffice;
	CString m_PhoneHome;
	CContact();
	CContact& operator =(CContact& contact);
//	CContact(const CContact& contact);
	virtual ~CContact();

};

#endif // !defined(AFX_CONTACT_H__FDCD17A3_26D1_42A0_9EED_1F1857FE256C__INCLUDED_)
