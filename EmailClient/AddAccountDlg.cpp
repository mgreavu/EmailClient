// AddAccountDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "AddAccountDlg.h"
#include "MailAccount.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddAccountDlg dialog


CAddAccountDlg::CAddAccountDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddAccountDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddAccountDlg)
	m_RememberPassw = FALSE;
	//}}AFX_DATA_INIT
}


void CAddAccountDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddAccountDlg)
	DDX_Check(pDX, IDC_REMEMBER_PASSW_OPTION, m_RememberPassw);
	DDX_Control(pDX, IDC_ADD_BTN, m_btnAdd);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddAccountDlg, CDialog)
	//{{AFX_MSG_MAP(CAddAccountDlg)
	ON_BN_CLICKED(IDC_ADD_BTN, OnAddBtn)
	ON_BN_CLICKED(IDC_REMEMBER_PASSW_OPTION, OnRememberPasswOption)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddAccountDlg message handlers

void CAddAccountDlg::OnOK() 
{

}

void CAddAccountDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CAddAccountDlg::OnAddBtn() 
{	
	CMailAccount oMailAccount;
	CString sMsg;
	
	CString sFieldData;
	UpdateData (TRUE);
	
	GetDlgItem (IDC_MAIL_ACC_NAME)->GetWindowText(sFieldData);
	oMailAccount.SetMailAccountName(sFieldData);
	
	GetDlgItem (IDC_DISPLAY_NAME)->GetWindowText(sFieldData);
	oMailAccount.SetSMTPDisplayName(sFieldData);
	
	GetDlgItem (IDC_EMAIL_ADDRESS)->GetWindowText(sFieldData);
	oMailAccount.SetSMTPEmailAddress(sFieldData);

	GetDlgItem (IDC_INCOMING_SERVER)->GetWindowText (sFieldData);
	oMailAccount.SetPOP3ServerName(sFieldData);

	GetDlgItem (IDC_OUTGOING_SERVER)->GetWindowText (sFieldData);
	oMailAccount.SetSMTPServerName(sFieldData);

	GetDlgItem (IDC_PASSWORD)->GetWindowText(sFieldData);
	oMailAccount.SetPOP3Password(sFieldData);
	
	oMailAccount.SetPOP3RememberPassword(m_RememberPassw);

	GetDlgItem (IDC_ACCOUNT_NAME)->GetWindowText(sFieldData);
	oMailAccount.SetAccountName(sFieldData);
	oMailAccount.SetPOP3UserName(sFieldData);
	
	if (!CreateDirectory(EmailClientFolder + sFieldData, NULL))
	{
		sMsg.LoadString(IDS_ERR_CR_USER_FOLD);
		::MessageBox(m_hWnd, sMsg, EmailClientFolder + sFieldData, MB_OK | MB_ICONSTOP);
		return;			
	}
	else 
	{
		CString sBox = EmailClientFolder;
		sBox += sFieldData;
		sBox += "\\";
		sBox += INBOX_FOLDER_NAME;
		if (!CreateDirectory(sBox, NULL))
		{
			CString sMsg;
			sMsg.LoadString(IDS_ERR_CR_INBOX);
			AfxMessageBox(sMsg);
			return;
		}
		else {
			sBox = EmailClientFolder;
			sBox += sFieldData;
			sBox += "\\";
			sBox += OUTBOX_FOLDER_NAME;
			if (!CreateDirectory(sBox, NULL))
			{
				sMsg.LoadString(IDS_ERR_CR_OUTBOX);
				AfxMessageBox(sMsg);

				return;
			}
			else {
				sBox = EmailClientFolder;
				sBox += sFieldData;
				sBox += "\\";
				sBox += CONTACTS_FOLDER_NAME;
				if (!CreateDirectory(sBox, NULL))
				{
					sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
					AfxMessageBox(sMsg);
					return;
				}
				else {
					sBox = EmailClientFolder;
					sBox += sFieldData;
					sBox += "\\";
					sBox += DELETED_FOLDER_NAME;
					if (!CreateDirectory(sBox, NULL))
					{
						sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
						AfxMessageBox(sMsg);
						return;
					}
					else {
						sBox = EmailClientFolder;
						sBox += sFieldData;
						sBox += "\\";
						sBox += SENT_FOLDER_NAME;
						if (!CreateDirectory(sBox, NULL))
						{
							sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
							AfxMessageBox(sMsg);
							return;
						}
						else {
							sBox = EmailClientFolder;
							sBox += sFieldData;
							sBox += "\\";
							sBox += DRAFT_FOLDER_NAME;
							if (!CreateDirectory(sBox, NULL))
							{
								sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
								AfxMessageBox(sMsg);
								return;
							}
						}
					}
				}
			}
		}

	}

	CString AccSettingsFile = EmailClientFolder + sFieldData;
	AccSettingsFile += "\\";
	AccSettingsFile += "accountinfo.dat";
	oMailAccount.SetAccountFilePath(AccSettingsFile);

	if (oMailAccount.WriteAccountData())
	{
		sMsg.LoadString(IDS_SUCC_WR_ACC_INFO);
		AfxMessageBox (sMsg);
		::PostMessage(GetParent()->m_hWnd ,UWM_ACC_INSERTED,0,0);
	}
	else
	{
		sMsg.LoadString(IDS_ERR_WR_ACC_INFO);
		AfxMessageBox (sMsg);
	}

	CDialog::OnOK();
}


void CAddAccountDlg::OwnerDrawButtons()
{
	m_btnAdd.SetMaskedBitmap( IDB_ADD, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );
}



BOOL CAddAccountDlg::OnInitDialog()
{
	OwnerDrawButtons();
	m_RememberPassw = TRUE;
	UpdateData(FALSE);
	return TRUE;
}

void CAddAccountDlg::OnRememberPasswOption() 
{
	UpdateData(TRUE);
	if(m_RememberPassw)	
		GetDlgItem(IDC_PASSWORD)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_PASSWORD)->EnableWindow(FALSE);
}
