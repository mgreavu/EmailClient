#include "stdafx.h"
#include "Globals.h"
#include "AccountListTypes.h"

AccList AccountList;
AccList::iterator AccListIterator;

BOOL GetAccountList()
{
	BOOL bResult = FALSE;
	WIN32_FIND_DATA w32fd;
	WIN32_FIND_DATA w32fd2;
	HANDLE hSearch;
	HANDLE hSearch2;
	CMailAccount oMailAccount;
	
	if ((hSearch = FindFirstFile(EmailClientFolder + "*.*", &w32fd)) != INVALID_HANDLE_VALUE) 
	{
		AccountList.clear();
		CString AccountFile;
				
		while(1) 
		{
			if ((strcmp (w32fd.cFileName, ".")) && strcmp(w32fd.cFileName,".."))
			{
				if (w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					AccountFile = EmailClientFolder + w32fd.cFileName;
					hSearch2 = FindFirstFile(AccountFile + "\\*.dat", &w32fd2);
					if (hSearch2 != INVALID_HANDLE_VALUE) 
					{
						if (!strcmp (w32fd2.cFileName, "accountinfo.dat"))
						{
							AccountFile += "\\";
							AccountFile += w32fd2.cFileName;
							oMailAccount.SetAccountFilePath(AccountFile);
							oMailAccount.ReadAccountData();
							AccountList.push_back(oMailAccount);
						}
						::FindClose(hSearch2);
					}
				}
			}
			
			if (!::FindNextFile(hSearch, &w32fd)) break;
			
		};
		::FindClose(hSearch);
	}	
	return bResult;
}