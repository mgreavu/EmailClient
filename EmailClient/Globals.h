#if !defined(__GLOBALS_H)
#define __GLOBALS_H

#define UWM_ACC_INSERTED	WM_APP + 100
#define UWM_ACC_UPDATED		WM_APP + 101
#define UWM_ACC_DELETED		WM_APP + 102
#define UWM_ACC_CHANGED		WM_APP + 103	

#define MYWM_OPCOMPLETE		WM_APP + 450

#define INBOX_FOLDER_NAME			"Inbox"
#define OUTBOX_FOLDER_NAME			"Outbox"
#define CONTACTS_FOLDER_NAME		"Contacts"
#define DELETED_FOLDER_NAME			"Deleted"
#define SENT_FOLDER_NAME			"Sent"
#define DRAFT_FOLDER_NAME			"Draft"

#define COMPOSE_TYPE_NEW			0x00
#define COMPOSE_TYPE_REPLY			0x01
#define COMPOSE_TYPE_FWD			0x02

extern CString EmailClientFolder;


#endif