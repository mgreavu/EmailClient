// AppSettings.cpp: implementation of the CAppSettings class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "emailclient.h"
#include "AppSettings.h"
#include "rijndael/rsalib.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAppSettings::CAppSettings(): sSettingsFile(_T(""))
{
	iCurrentAccount = -1;
	AccountsFolder.Empty();
	memset(&IfSATAMasscom, 0x00, SZ_INTERFACE_MODMASSCOM);
	memset(XSATAUID, 0x00, 32);
	memset(aesKey, 0x00, 32);
	UserEncList.clear();
}

CAppSettings::~CAppSettings()
{
	UserEncList.clear();
}

BOOL CAppSettings::IniReadECSettings()
{
	char PublicDriveLetter = IfSATAMasscom.lpfnGetPublicDrive();
	if (!PublicDriveLetter) return FALSE;

	sSettingsFile.Empty();
	sSettingsFile = PublicDriveLetter;
	sSettingsFile += ":\\";
	sSettingsFile += EC_SETTINGS_FILE;

	char sBuff[MAX_PATH + 1];
	memset (&sBuff, 0x00, MAX_PATH + 1);
	::GetPrivateProfileString("EC", "AccountsFolder", "", (char*)&sBuff, MAX_PATH, sSettingsFile);

	bAccountsFolderOnKey = GetPrivateProfileInt("EC", "AccountsFolderOnKey", 0, sSettingsFile);
	if (!bAccountsFolderOnKey) 	AccountsFolder = sBuff;
	else
	{
		if (strlen (sBuff))
		{
			if (PublicDriveLetter)
			{
				sBuff[0] = PublicDriveLetter;
				AccountsFolder = sBuff;
			}
			else
			{
				AccountsFolder.Empty();
				return FALSE;
			}
		}
	}

	iCurrentAccount = GetPrivateProfileInt("EC", "CurrentAccount", -1, sSettingsFile);

	return TRUE;
}

BOOL CAppSettings::IniWriteECSettings()
{
	char sBuff[MAX_PATH + 1];
	memset (&sBuff, 0x00, MAX_PATH + 1);

	::WritePrivateProfileSection ("EC", NULL, sSettingsFile);

	::WritePrivateProfileString("EC", "AccountsFolder", AccountsFolder, sSettingsFile);

	char sTmp[32];
	memset (sTmp,0x00,32);
	sprintf (sTmp, "%d", iCurrentAccount);	
	::WritePrivateProfileString("EC", "CurrentAccount", sTmp, sSettingsFile);

	memset (sTmp,0x00,32);
	sprintf (sTmp, "%d", bAccountsFolderOnKey);
	::WritePrivateProfileString("EC", "AccountsFolderOnKey", sTmp, sSettingsFile);

	return TRUE;
}

BOOL CAppSettings::LoadBlackDll(void)
{
	char ModuleFName [MAX_PATH + 1];
	memset (ModuleFName, 0x00, sizeof(char)*(MAX_PATH + 1));

	if (!::GetModuleFileName(NULL, ModuleFName, MAX_PATH)) return FALSE;
	CString sPath = ModuleFName;
	sPath = sPath.Left(sPath.ReverseFind('\\') + 1);
	sPath += BLACK_USB_SUPPORT_MODULE;
	
	strncpy (IfSATAMasscom.DllPath, sPath.GetBuffer(sPath.GetLength()), __min(MAX_PATH, sPath.GetLength()));

	if (SwitchSATAMasscomModule(&IfSATAMasscom) != MASSCOM_LOAD_SUCCESS) return FALSE;

	return TRUE;
}

BOOL CAppSettings::UnloadBlackDll(void)
{
	if (UnloadSATAMasscomModule(&IfSATAMasscom) != MASSCOM_UNLOAD_SUCCESS) return FALSE;
	return TRUE;
}

BOOL CAppSettings::ReadUserData(void)
{
	if (!IfSATAMasscom.lpfnIsLogged()) return FALSE;

	unsigned char sData[33];

	memset (sData, 0x00, 33);
	if (IfSATAMasscom.lpfnReadData(MARK_ADDR, (unsigned char*)&sData, MARK_LEN) != KEY_SUCCESS) return FALSE;
	if (sData[0] != 'K') return FALSE;

	memset (sData, 0x00, 33);
	if (IfSATAMasscom.lpfnReadData(ID_ADDR, (unsigned char*)&sData, ID_LEN) != KEY_SUCCESS) return FALSE;
	memmove (XSATAUID, sData, ID_LEN);

	memset (sData, 0x00, 33);
	if (IfSATAMasscom.lpfnReadData(KEY_ADDR, (unsigned char*)&sData, KEY_LEN) != KEY_SUCCESS) return FALSE;
	memmove (aesKey, sData, KEY_LEN);

	return TRUE;
}

BOOL CAppSettings::GetAllEncCodes(void)
{
	HANDLE fHnd = INVALID_HANDLE_VALUE;
	DWORD nBytes;
	UserEncData UED = {0};
	byte Buff[32];

	byte aesDecryptionKey[32];
	memset(&aesDecryptionKey, 0, 32);
	strncpy((LPSTR)aesDecryptionKey, "MyPrivacyEasyDiskVerXFilesXKey", strlen("MyPrivacyEasyDiskVerXFilesXKey"));

	CString sKeyFolder = AccountsFolder;
	sKeyFolder += "\\";
	sKeyFolder += "Keys\\";

	WIN32_FIND_DATA w32fd;
	HANDLE hSearch = INVALID_HANDLE_VALUE;
	if ((hSearch = ::FindFirstFile(sKeyFolder + "*.*", &w32fd)) != INVALID_HANDLE_VALUE) 
	{
		while (1)
		{
			if ((strcmp (w32fd.cFileName, ".")) && strcmp(w32fd.cFileName, ".."))
			{
				if (!(w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					if ((fHnd = ::CreateFile(sKeyFolder + w32fd.cFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) != INVALID_HANDLE_VALUE) 
					{
						::SetFilePointer(fHnd, 4, 0, FILE_BEGIN);
						memset (&UED, 0x00, SZ_USERENCDATA);

						memset (Buff, 0x00, 32*sizeof(byte));					
						if (::ReadFile(fHnd, Buff, 32, &nBytes, NULL)) 
						{
							if (AESDecrypt(aesDecryptionKey, (byte*)&UED.XSATAUID, (byte*)&Buff, 32) == RE_SUCCESS)
							{
								memset (Buff, 0x00, 32*sizeof(byte));						
								if (::ReadFile(fHnd, Buff, 32, &nBytes, NULL)) 
								{
									if (AESDecrypt(aesDecryptionKey, (byte*)&UED.aesKey, (byte*)&Buff, 32) == RE_SUCCESS)
									{
										UserEncList.push_back(UED);
									}
								}
							}
						}	

						::CloseHandle(fHnd);
					}	// CreateFile()
				}	// e fisier, nu folder
			}	// nu e indirectarea '.', '..'
			if (!::FindNextFile(hSearch, &w32fd)) break;
		}	// end of while()

		::FindClose(hSearch);
	}

	return TRUE;	
}
