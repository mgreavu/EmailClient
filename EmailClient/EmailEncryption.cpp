#include "stdafx.h"
#include "EmailEncryption.h"
#include "zlib/zlib.h"
#include "rijndael/rsalib.h"

BOOL CreateCompressedFile(LPSTR ExistingFilePath, const char* m_UserCode)
{
	HANDLE fHnd = INVALID_HANDLE_VALUE;
	byte* data = NULL;
	byte* compressedData = NULL;
	DWORD Size = 0, SizeH = 0, compressedSize = 0;
	DWORD nBytes = 0;

	if ((fHnd = ::CreateFile(ExistingFilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	Size = GetFileSize(fHnd, &SizeH);
	compressedSize = Size + (int) ((double) Size * 0.01) + 12;
	
	data = (byte*) malloc(Size);
	compressedData = (byte*) malloc(compressedSize);
	
	if (data && compressedData) 
	{
		memset(data, 0x00, Size);
		memset(compressedData, 0x00, compressedSize);
		::ReadFile(fHnd, data, Size, &nBytes, NULL);
		
		int res = compress(compressedData, &compressedSize, data, Size);	
	
		if (res != Z_OK) 
		{
			free(data);
			data = NULL;

			free(compressedData); 
			compressedData = NULL;

			::CloseHandle(fHnd);
			return FALSE;
		}

		free(data);
		data = NULL;
	}

	::CloseHandle(fHnd);
	::DeleteFile(ExistingFilePath);

	if ((fHnd = ::CreateFile(ExistingFilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) != INVALID_HANDLE_VALUE) 
	{
		::WriteFile(fHnd, m_UserCode, 32, &nBytes, NULL);
		::WriteFile(fHnd, &Size, 4, &nBytes, NULL);
		::WriteFile(fHnd, compressedData, compressedSize, &nBytes, NULL);
		
		::CloseHandle(fHnd);
	}

	if (compressedData) free(compressedData);

	return TRUE;
}

BOOL CheckUserID(LPSTR EncryptedFilePath, const char* m_UserCode)
{
	HANDLE fHnd = INVALID_HANDLE_VALUE;
	BOOL Result;
	DWORD nBytes = 0;
		
	if ((fHnd = ::CreateFile(EncryptedFilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	char FileUserCode[32];	
	memset(&FileUserCode, 0, 32);	
	Result = TRUE;
	
	if (!::ReadFile(fHnd, FileUserCode, 31, &nBytes, NULL)) return FALSE;
	
	
	if (!m_UserCode || strncmp(FileUserCode, m_UserCode, 31) != 0) 
	{
		Result = FALSE;
	}

	::CloseHandle(fHnd);
		
	return Result;
}

CString FileNameWithoutExtension(LPSTR filename)
{
	CString sFName = filename;
	int nIndex = -1;

	if ((nIndex = sFName.ReverseFind('.')) == -1) return sFName;
	sFName = sFName.Left(nIndex);

	return sFName;
}

void EncFileTimeToString(FILETIME* fTime, LPSTR FileTime)
{
	SYSTEMTIME sTime;
	LPSTR temp = NULL; 	
    DWORD index;
	
    char strTime[16];	
    memset(strTime, 0, 16);
	
    temp = (LPSTR) malloc(5);
    if(temp== NULL)
	{
		return;
	}
	
	FileTimeToSystemTime(fTime, &sTime);
	
	temp[4] = '\0';
	
	//Add the day to the string
	_itoa(sTime.wDay, temp, 10);
	if(strlen(temp) == 1)
	{
		temp[1] = temp[0];
		temp[0] = '0';
	}
	strcpy(strTime, temp);
	strcat(strTime, "/");
	
	for(index=0;index<4;index++)
		temp[index] = 0;
	temp[4] = '\0';
	
	//Add the month to the string
	_itoa(sTime.wMonth, temp, 10);
	if(strlen(temp) == 1)
	{
		temp[1] = temp[0];
		temp[0] = '0';
	}
	strcat(strTime, temp);
	strcat(strTime, "/");
	
	for(index=0;index<4;index++)
		temp[index] = 0;
    temp[4] = '\0';
	
	//Add the year to the string
	_itoa(sTime.wYear, temp, 10);
	strcat(strTime, temp);
	
	
	memset(FileTime, 0, 16);
	strcpy(FileTime, strTime);
	
	if(temp != NULL) free(temp);	
}

BOOL IsDriveFixed(LPSTR Path)
{
	CString DPath;

	DPath = Path;
	DPath.SetAt(3, '\0');

	if (::GetDriveType(DPath.GetBuffer(DPath.GetLength())) == DRIVE_FIXED)
		return TRUE;
	else
		return FALSE;
} 

BOOL CreateMPYFile(LPSTR TmpFileName, LPSTR DestPath, const byte* m_AESKey)
{
	byte* InBuffer = NULL;
	byte* OutBuffer = NULL;
	
	HANDLE hTmpFile = INVALID_HANDLE_VALUE;
	HANDLE hMpfFile = INVALID_HANDLE_VALUE;
	
	BOOL Result;
	DWORD nBytes;
	
	char FilePath[MAX_PATH + 1];
	memset(FilePath, 0x00, (MAX_PATH + 1));

	strncpy(FilePath, DestPath, strlen(DestPath));
	strncat(FilePath, TmpFileName, strlen(TmpFileName));
	strncat(FilePath, ".mpy", strlen(".mpy"));

	if ((hMpfFile = CreateFile(FilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
		
	memset(FilePath, 0x00, (MAX_PATH+1));
	strncpy(FilePath, DestPath, strlen(DestPath));
	strncat(FilePath, TmpFileName, strlen(TmpFileName));
	strncat(FilePath, ".tmp", strlen(".tmp"));
	
	if ((hTmpFile = ::CreateFile(FilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
	{
		::CloseHandle(hMpfFile);
		return FALSE;
	}

	if ((InBuffer = (byte*) malloc(4096)) == NULL) 
	{
		::CloseHandle(hMpfFile);
		::CloseHandle(hTmpFile);
		return FALSE;
	}

	if ((OutBuffer = (byte*) malloc(4096)) == NULL)
	{
		free(InBuffer);
		::CloseHandle(hMpfFile);
		::CloseHandle(hTmpFile);
		return FALSE;
	}

	BOOL bEncrypted = TRUE;
	memset (InBuffer, 0x00, 4096);
	Result = ReadFile(hTmpFile, InBuffer, 4096, &nBytes, NULL);
	while (Result && nBytes)
	{
		memset(OutBuffer, 0x00, 4096);
		if (AESCrypt((byte*)m_AESKey, OutBuffer, InBuffer, 4096) != RE_SUCCESS)
		{
			bEncrypted = FALSE;
			break;
		}
		Result = ::WriteFile(hMpfFile, OutBuffer, 4096, &nBytes, NULL);
		nBytes = 0;
		memset (InBuffer, 0x00, 4096);
		Result = ::ReadFile(hTmpFile, InBuffer, 4096, &nBytes, NULL);
	}

	::CloseHandle(hMpfFile);
	::CloseHandle(hTmpFile);

	DeleteFileInSecureMode(FilePath);
	
	free(InBuffer);
	free(OutBuffer);

	return bEncrypted;
}

byte* EncryptBuffer (const byte* lpBufferToEncrypt, DWORD dwBufferToEncryptSize, DWORD& dwOutBufferSize, const byte* m_AESKey)
{
	dwOutBufferSize = 0;
	DWORD nEncryptLen = dwBufferToEncryptSize + 16;
	nEncryptLen -= (nEncryptLen % 16);

	byte* OutBuffer = (byte*)malloc (nEncryptLen);
	if (OutBuffer == NULL) return NULL;
	
	memset (OutBuffer, 0x00, nEncryptLen);

	if (AESCrypt(m_AESKey, OutBuffer, lpBufferToEncrypt, nEncryptLen) != RE_SUCCESS)
	{
		free (OutBuffer); OutBuffer = NULL;
		return NULL;
	}

	dwOutBufferSize = nEncryptLen;

	return OutBuffer;
}

byte* DecryptBuffer (const byte* lpBufferToDecrypt, DWORD dwBufferToDecryptSize, const byte* m_AESKey)
{
	byte* OutBuffer = (byte*)malloc (dwBufferToDecryptSize + 1);
	if (OutBuffer == NULL) return NULL;

	memset (OutBuffer, 0x00, dwBufferToDecryptSize + 1);

	if (AESDecrypt(m_AESKey, OutBuffer, lpBufferToDecrypt, dwBufferToDecryptSize) != RE_SUCCESS)
	{
		free (OutBuffer); OutBuffer = NULL;
		return NULL;
	}

	return OutBuffer;
}

BOOL EncryptFile(LPWIN32_FIND_DATA fData, LPSTR SourcePath, LPSTR DestPath, const char* m_UserCode, const byte* m_AESKey)
{
	char* temp = NULL;
	char FileName[MAX_PATH +1];
	char FilePath[MAX_PATH +1];
	char FileCreationData[16 + 1];
	char FileLastChangesData[16 + 1];
	HANDLE fHndNewFile = INVALID_HANDLE_VALUE;
	HANDLE fHndOldFile = INVALID_HANDLE_VALUE;

	DWORD index;
	BOOL Result;
	DWORD nBytesWritten;
	DWORD nBytesRead;
	DWORD nBytesLeftToCopy;
	__int64 CRCField = 0;
	__int64 Size = 0;

	Size = fData->nFileSizeHigh;
	Size <<= 32;
	Size |= fData->nFileSizeLow;

	memset (FileName, 0x00, (MAX_PATH + 1));
	memset (FilePath, 0x00, (MAX_PATH + 1));

	GetTempPath(MAX_PATH, FilePath);
	strncat (FilePath, fData->cFileName, strlen(fData->cFileName));
	strncat (FilePath, ".tmp", strlen(".tmp"));


	//Create the *.mpf file
	if ((fHndNewFile = ::CreateFile (FilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	//Creating the time for the file
	memset (FileCreationData, 0x00, (16 + 1));
	memset (FileLastChangesData, 0x00, (16 + 1));
	EncFileTimeToString(&(fData->ftCreationTime), FileCreationData);
	EncFileTimeToString(&(fData->ftLastWriteTime), FileLastChangesData);

	//Writing the header
	memset(FileName, 0x00, (MAX_PATH + 1));
	strncpy(FileName, fData->cFileName, strlen(fData->cFileName));

	//Writing file name to header, Calculating CRC field

	for(index=0; index<strlen(FileName); index++) CRCField = CRCField + (BYTE)FileName[index]; 
	::WriteFile(fHndNewFile, FileName, 256, &nBytesWritten, NULL);

	//Writing creation data to header, Calculating CRC field
	for(index=0;index<16;index++) CRCField = CRCField + (BYTE)FileCreationData[index];
	::WriteFile(fHndNewFile, FileCreationData, 16, &nBytesWritten, NULL);

	//Writing last changes data to header, Calculating CRC field
	for(index=0;index<16;index++) CRCField = CRCField + (BYTE)FileLastChangesData[index];
	::WriteFile(fHndNewFile, FileLastChangesData, 16, &nBytesWritten, NULL);
	
	//Writing file size to header, Calculating CRC field
	::WriteFile(fHndNewFile, &Size, sizeof(__int64), &nBytesWritten, NULL);
	CRCField = CRCField + Size;

	// Opening the original file to be encrypted
	memset(FilePath, 0x00, (MAX_PATH +1));
	strncpy(FilePath, SourcePath, strlen(SourcePath));
	strncat(FilePath, "\\", strlen("\\"));
	strncat(FilePath, fData->cFileName, strlen(fData->cFileName));
	
	if ((fHndOldFile = CreateFile(FilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
	{
		::CloseHandle(fHndNewFile);
		return FALSE;
	}

	if ((temp = (char*) malloc(4096)) == NULL) 
	{
		::CloseHandle(fHndOldFile);
		::CloseHandle(fHndNewFile);

		return FALSE;
	}

	//Copying the contents of the old file in the new file
	nBytesLeftToCopy = 0;
	if(Size > 4096)
	{
		memset(temp, 0x00, 4096);
		Result = ::ReadFile(fHndOldFile, temp, 4096, &nBytesRead, NULL);
		Size = Size - 4096;

		while((Result) && (nBytesRead != 0) && (Size > 0))
		{
			Result = ::WriteFile(fHndNewFile, temp, nBytesRead, &nBytesWritten, NULL);
			for(index=0; index<nBytesRead; index++)
			{
				CRCField = CRCField + (BYTE)temp[index];
				temp[index] = 0;
			}
			nBytesRead = 0;
			if(Size > 4096)
			{
				memset(temp, 0x00, 4096);
				Result = ::ReadFile(fHndOldFile, temp, 4096, &nBytesRead, NULL);
				Size = Size - 4096;
			}
			else
			{
				nBytesLeftToCopy = (DWORD)Size;
				Size = 0;
				memset(temp, 0x00, 4096);
				Result = ::ReadFile(fHndOldFile, temp, nBytesLeftToCopy, &nBytesRead, NULL);
				Result = ::WriteFile(fHndNewFile, temp, nBytesRead, &nBytesWritten, NULL);

				for(index=0; index<nBytesRead; index++)
				{
					CRCField = CRCField + (BYTE)temp[index];
					temp[index] = 0;
				}
			}
		}
	}
	else
	{
		nBytesLeftToCopy = (DWORD)Size;
		memset(temp, 0x00, 4096);
		Result = ::ReadFile(fHndOldFile, temp, nBytesLeftToCopy, &nBytesRead, NULL);
		Result = ::WriteFile(fHndNewFile, temp, nBytesRead, &nBytesWritten, NULL);
		for(index=0; index<nBytesRead; index++)
		{
			CRCField = CRCField + (BYTE)temp[index];
			temp[index] = 0;
		}
	}

	//Writing CRC field to file
	WriteFile(fHndNewFile, &CRCField, sizeof(__int64), &nBytesWritten, NULL);
	
	memset(temp, 0, 4096);
	
	if((nBytesLeftToCopy + 304) < 4096)
	{
		WriteFile(fHndNewFile, temp, (DWORD)(4096 - (nBytesLeftToCopy + 304)), &nBytesWritten, NULL);
	}
	else
	{
		if((nBytesLeftToCopy + 304) > 4096)
		{
			WriteFile(fHndNewFile, temp, (DWORD)(8192 - (nBytesLeftToCopy + 304)), &nBytesWritten, NULL);
		}
	}
	
	::CloseHandle(fHndNewFile);
	::CloseHandle(fHndOldFile);

	if(temp != NULL)free(temp);

	//Code for crypting the file
	if (!CreateMPYFile(fData->cFileName, DestPath, m_AESKey)) return FALSE;

	memset(FilePath, 0x00, MAX_PATH + 1);
	strncpy(FilePath, DestPath, strlen(DestPath));
	strncat(FilePath, fData->cFileName, strlen(fData->cFileName));
	strncat(FilePath, ".mpy", strlen(".mpy"));
	if (!CreateCompressedFile(FilePath, m_UserCode)) return FALSE;

	return TRUE;
}

BOOL CreateTMPFile(LPSTR FileName, LPSTR SourcePath, LPSTR DestPath, const byte* m_AESKey)
{
	byte* InBuffer = NULL;
	byte* OutBuffer = NULL;

	HANDLE hTmpFile = INVALID_HANDLE_VALUE;
	HANDLE hMpfFile = INVALID_HANDLE_VALUE;

	BOOL Result = FALSE;
	DWORD nBytes = 0;

	char FilePath[MAX_PATH + 1];
	memset (FilePath, 0x00, sizeof(char)*(MAX_PATH+1));
	
	GetTempPath(MAX_PATH, FilePath);
	strcat(FilePath, FileName);
	strcat(FilePath, ".tmpy");

	if ((hMpfFile = ::CreateFile(FilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) return FALSE;

	memset (FilePath, 0x00, sizeof(char)*(MAX_PATH+1));
	GetTempPath(MAX_PATH, FilePath);
	strcat(FilePath, FileName);
	strcat(FilePath, ".tmp");
	
	if ((hTmpFile = ::CreateFile(FilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) return FALSE;

	if ((InBuffer = (byte*) malloc(4096)) == NULL)
	{
		::CloseHandle(hTmpFile);
		::CloseHandle(hMpfFile);

		return FALSE;
	}

	if ((OutBuffer = (byte*) malloc(4096)) == NULL)
	{
		free(InBuffer); InBuffer = NULL;
		::CloseHandle(hTmpFile);
		::CloseHandle(hMpfFile);

		return FALSE;
	}

	BOOL bDecryptOk = TRUE;
	memset (InBuffer, 0x00, 4096*sizeof(byte));
	Result = ::ReadFile(hMpfFile, InBuffer, 4096, &nBytes, NULL);
	while(Result && nBytes)
	{
		memset (OutBuffer, 0x00, 4096*sizeof(byte));
		if (AESDecrypt((byte*)m_AESKey, OutBuffer, InBuffer, 4096) != RE_SUCCESS)
		{
			bDecryptOk = FALSE;
			break;
		}
		if ((Result = ::WriteFile(hTmpFile, OutBuffer, 4096, &nBytes, NULL)) == TRUE)
		{
			nBytes = 0;
			memset (InBuffer, 0x00, 4096*sizeof(byte));
			Result = ::ReadFile(hMpfFile, InBuffer, 4096, &nBytes, NULL);
		}
	}

	::CloseHandle(hMpfFile);
	::CloseHandle(hTmpFile);

	free(InBuffer); InBuffer = NULL;
	free(OutBuffer); OutBuffer = NULL;

	memset (FilePath, 0x00, sizeof(char)*(MAX_PATH+1));
	GetTempPath(MAX_PATH, FilePath);
	strcat(FilePath, FileName);
	strcat(FilePath, ".tmpy");
	::DeleteFile(FilePath);
	
	return bDecryptOk;
}

BOOL CreateDecompressedFile(LPSTR EncryptedFilePath, LPSTR FileName, const char* m_UserCode)
{
	HANDLE fHnd = INVALID_HANDLE_VALUE;
	DWORD Size = 0, uncompressedSize = 0, SizeH = 0;
	DWORD nBytes;
	byte* data = NULL;
	byte* cData = NULL;
	BOOL Continue = FALSE;

	if (!CheckUserID(EncryptedFilePath, m_UserCode)) return FALSE;
	if ((fHnd = ::CreateFile(EncryptedFilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) return FALSE;

	Size = GetFileSize(fHnd, &SizeH);
	::SetFilePointer(fHnd, 32, 0, FILE_BEGIN);

	if (::ReadFile(fHnd, &uncompressedSize, 4, &nBytes, NULL)) 
	{
		if ((data = (byte*) malloc(Size)) == NULL)
		{
			::CloseHandle(fHnd);
			return FALSE;
		}
		if ((cData = (byte*) malloc(uncompressedSize)) == NULL)
		{
			free (data); data = NULL;
			::CloseHandle(fHnd);
			return FALSE;
		}

		if (data && cData) 
		{
			memset (data, 0x00, Size);
			if (::ReadFile(fHnd, data, Size, &nBytes, NULL))
			{
				memset(cData, 0x00, uncompressedSize);
				if (uncompress(cData, &uncompressedSize, data, Size) == Z_OK) 
				{
					Continue = TRUE;
				}
				else
				{
					free(cData);
					cData = NULL;
				}
			}

			free(data); data = NULL;
		}
	}

	::CloseHandle(fHnd);

	if (Continue) 
	{
		CString FPath;
		char FilePath[MAX_PATH + 1];
		memset (FilePath, 0x00, sizeof(char)*(MAX_PATH+1));

		GetTempPath(MAX_PATH, FilePath);
		FPath = FilePath; 
		FPath += FileNameWithoutExtension(FileName);
		FPath += ".tmpy";

		if ((fHnd = ::CreateFile(FPath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) != INVALID_HANDLE_VALUE) 
		{
			Continue = ::WriteFile(fHnd, cData, uncompressedSize, &nBytes, NULL);
			::CloseHandle(fHnd);
		}
		free(cData); cData = NULL;
	}

	return Continue;
}

BOOL DecryptFile(LPSTR FileName, LPSTR SourcePath, LPSTR DestPath, const char* m_UserCode, const byte* m_AESKey)
{
	HANDLE fHndFileToDecrypt = INVALID_HANDLE_VALUE;
	HANDLE fHndNewFile = INVALID_HANDLE_VALUE;

	BOOL Result;
	int NrSteps;
	DWORD nBytesLeft;
	DWORD nBytesWritten;
	DWORD nBytesRead;
	DWORD index;
	__int64 Size;
	__int64 CRCFieldRead;
	__int64 CRCField = 0;
	__int64 nBytesLeftToRead;

	char FilePath[MAX_PATH + 1];
	char NewFileName[MAX_PATH + 1];
	
	memset(FilePath, 0x00, sizeof(char)*(MAX_PATH + 1));
	strcpy(FilePath, SourcePath);
	strcat(FilePath, FileName);
	
	if (!CreateDecompressedFile(FilePath, FileName, m_UserCode)) return FALSE;
	
	//Code for decrypting the file
	CString sFName = FileNameWithoutExtension(FileName);
	if (!CreateTMPFile(sFName.GetBuffer(sFName.GetLength()), SourcePath, DestPath, m_AESKey)) return FALSE;

	//Opening the decrypted file 

	memset(FilePath, 0x00, sizeof(char)*(MAX_PATH + 1));
	GetTempPath(MAX_PATH, FilePath);
	strcat(FilePath, FileNameWithoutExtension(FileName));
	strcat(FilePath, ".tmp");
	
	if ((fHndFileToDecrypt = CreateFile(FilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) return FALSE;

	memset(NewFileName, 0x00, sizeof(char)*(MAX_PATH + 1));
	//Reading the file name of the file to open
	ReadFile(fHndFileToDecrypt, NewFileName, 256, &nBytesRead, NULL);
	for(index=0;index<strlen(NewFileName);index++)
		CRCField = CRCField + (BYTE)NewFileName[index];

	char fData[16];
	memset (fData, 0x00, 16);

	//Reading the creation data
	ReadFile(fHndFileToDecrypt, fData, 16, &nBytesRead, NULL);
	for(index=0;index<16;index++)
		CRCField = CRCField + (BYTE)fData[index];

	memset (fData, 0x00, 16);
	
	//Reading the last changes data
	ReadFile(fHndFileToDecrypt, fData, 16, &nBytesRead, NULL);
	for(index=0;index<16;index++)
		CRCField = CRCField + (BYTE)fData[index];
	

	//Reading the size of the file
	ReadFile(fHndFileToDecrypt, &Size, sizeof(__int64), &nBytesRead, NULL);
	CRCField = CRCField + Size;

	//Creating the file to use

	memset(FilePath, 0x00, sizeof(char)*(MAX_PATH + 1));
	strcpy(FilePath, DestPath);
	strcat(FilePath, FileNameWithoutExtension(FileName));


	if ((fHndNewFile = ::CreateFile(FilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) 
	{
		::CloseHandle(fHndFileToDecrypt);
		return FALSE;
	}

	char* temp = NULL;
	if ((temp = (LPSTR) malloc(4096)) == NULL)
	{
		::CloseHandle(fHndNewFile);
		::CloseHandle(fHndFileToDecrypt);
		return FALSE;
	}

	//	Copy the contents of the mpf file (without the header and CRC field) into the new file
	nBytesLeftToRead = Size;
	nBytesLeft = 0;
	NrSteps = 0;
	if(nBytesLeftToRead > 4096)
	{
		memset(temp, 0x00, 4096*sizeof(char));
		Result = ::ReadFile(fHndFileToDecrypt, temp, 4096, &nBytesRead, NULL);
		NrSteps ++;
		nBytesLeftToRead = nBytesLeftToRead - 4096;

		while((Result) && (nBytesRead) && (nBytesLeftToRead > 0))
		{
			WriteFile(fHndNewFile, temp, nBytesRead, &nBytesWritten, NULL);	
			for(index=0;index<nBytesRead;index++)
			{
				CRCField = CRCField + (BYTE)temp[index];
				temp[index] = 0;
			}
			if(nBytesLeftToRead > 4096)
			{
				memset(temp, 0x00, 4096*sizeof(char));
				Result = ::ReadFile(fHndFileToDecrypt, temp, 4096, &nBytesRead, NULL);
				NrSteps ++;
				nBytesLeftToRead = nBytesLeftToRead - 4096;
			}
			else
			{
				memset(temp, 0x00, 4096*sizeof(char));
				Result = ::ReadFile(fHndFileToDecrypt, temp, (DWORD)nBytesLeftToRead, &nBytesRead, NULL);
				WriteFile(fHndNewFile, temp, (DWORD)nBytesLeftToRead, &nBytesWritten, NULL);	
				for(index=0;index<(DWORD)nBytesLeftToRead;index++)
				{
					CRCField = CRCField + (BYTE)temp[index];
				}
				nBytesLeft = (int) nBytesLeftToRead;
				nBytesLeftToRead = 0;
			}
		}
	}
	else
	{
		memset(temp, 0x00, 4096*sizeof(char));
		::ReadFile(fHndFileToDecrypt, temp, (DWORD)nBytesLeftToRead, &nBytesRead, NULL);
		::WriteFile(fHndNewFile, temp, (DWORD)nBytesLeftToRead, &nBytesWritten, NULL);
		for(index=0;index<(DWORD)nBytesLeftToRead;index++)
		{
			CRCField = CRCField + (BYTE)temp[index];
		}
		nBytesLeft = (int) nBytesLeftToRead;
	}

	free(temp); temp = NULL;


	::ReadFile(fHndFileToDecrypt, &CRCFieldRead, sizeof(__int64), &nBytesRead, NULL);		
	
	::CloseHandle(fHndFileToDecrypt);
	::CloseHandle(fHndNewFile);
	
	//The CRC calculated is different from the CRC read from the mpf file
	if(CRCField != CRCFieldRead)
	{
		memset(FilePath, 0x00, sizeof(char)*(MAX_PATH + 1));
		strcpy(FilePath, DestPath);
		strcat(FilePath, FileNameWithoutExtension(FileName));
		DeleteFileInSecureMode(FilePath);	
	}
	
	memset(FilePath, 0x00, sizeof(char)*(MAX_PATH + 1));
	GetTempPath(MAX_PATH, FilePath);
	strcat(FilePath, FileNameWithoutExtension(FileName));
	strcat(FilePath, ".tmp");
	
	DeleteFileInSecureMode(FilePath);

	return TRUE;
}

void DeleteFileInSecureMode(LPSTR Path)
{
	HANDLE fHnd = INVALID_HANDLE_VALUE;
	BOOL Result;
	DWORD nBytes;
	DWORD Size, SizeHigh;
	byte* Buffer = NULL;
	DWORD Index, uNumberOfOverwrites = 1;

	if (IsDriveFixed(Path)) uNumberOfOverwrites = 3;

	BOOL ErrorDelete = FALSE;

	Buffer = (byte*) malloc(4096);
	memset(Buffer, 0, 4096);

	Index = 0;

	SetFileAttributes(Path, FILE_ATTRIBUTE_ARCHIVE);

	while (Index < uNumberOfOverwrites) 
	{
		fHnd = CreateFile(Path, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (fHnd != INVALID_HANDLE_VALUE) 
		{
			Size = GetFileSize(fHnd, &SizeHigh);

			if (Size > 4096) 
			{
				Result = WriteFile(fHnd, Buffer, 4096, &nBytes, NULL);

				while (Result && nBytes) 
				{
					Size -= 4096;

					if (Size > 4096) 
					{
						Result = WriteFile(fHnd, Buffer, 4096, &nBytes, NULL);
					}
					else
					{
						Result = WriteFile(fHnd, Buffer, Size, &nBytes, NULL);
						nBytes = 0;
					}
				}
			}
			else
			{
				Result = WriteFile(fHnd, Buffer, Size, &nBytes, NULL);
			}
		}

		FILETIME fTime;
		SYSTEMTIME sTime;

		sTime.wDay = 1;
		sTime.wDayOfWeek = 0;
		sTime.wHour = 0;
		sTime.wMilliseconds = 0;
		sTime.wMinute = 0;
		sTime.wMonth = 1;
		sTime.wSecond = 0;
		sTime.wYear = 1980;

		SystemTimeToFileTime(&sTime, &fTime);

		SetFileTime(fHnd, &fTime, &fTime, &fTime);

		FlushFileBuffers(fHnd);
		CloseHandle(fHnd);

		Index ++;
	}

	if (DeleteFile(Path))
	{
		::SHChangeNotify(SHCNE_DELETE, SHCNF_PATH, Path, NULL);
	}
	else
	{
		ErrorDelete = TRUE;
	}

	if (Buffer) 
	{
		free(Buffer);
	}	
}
