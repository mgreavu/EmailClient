#if !defined(AFX_SELECTCONTACT_H__2585868B_C8FD_432E_B316_E9052B7820BC__INCLUDED_)
#define AFX_SELECTCONTACT_H__2585868B_C8FD_432E_B316_E9052B7820BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectContact.h : header file
//
#include "AddressBook.h"
#include "EmailClientDlg.h"
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CSelectContact dialog

class CSelectContact : public CDialog
{
// Construction
public:
	CSelectContact(CWnd* pParent = NULL);   // standard constructor
	int		m_iSelected;
	char m_sEmailAddr[MAX_PATH];
	CEmailClientDlg* parentWindow;
	
	
// Dialog Data
	//{{AFX_DATA(CSelectContact)
	enum { IDD = IDD_SELECTCONTACT };
	CSXButton	m_btnOk;
	CSXButton	m_btnCancel;
	CListCtrl	m_ContactsList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectContact)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectContact)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
//	void ReadContacts();
	void refresContactsList();
	void OwnerDrawButtons();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTCONTACT_H__2585868B_C8FD_432E_B316_E9052B7820BC__INCLUDED_)
