#if !defined(AFX_IMPORTCODES_H__C6EDEE7D_93E0_43E2_9924_108BD1800EDE__INCLUDED_)
#define AFX_IMPORTCODES_H__C6EDEE7D_93E0_43E2_9924_108BD1800EDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportCodes.h : header file
//
#include "interface/ColorEdit.h"
/////////////////////////////////////////////////////////////////////////////
// CImportCodes dialog

class CImportCodes : public CDialog
{
// Construction
public:
	CString sKeyDestPath;
	CImportCodes(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CImportCodes)
	enum { IDD = IDD_IMPORTCODES_DIALOG };
	CColorEdit	m_ctrlEdit;
	CString	m_FilePath;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImportCodes)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	HFONT m_font;
// Implementation
protected:
	BOOL ReadDataFromFile(LPSTR ImportFilePath, byte* AESKey, char* UserCode);

	// Generated message map functions
	//{{AFX_MSG(CImportCodes)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnOpenfile();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTCODES_H__C6EDEE7D_93E0_43E2_9924_108BD1800EDE__INCLUDED_)
