// EmailClientDlg.h : header file
//

#if !defined(AFX_EMAILCLIENTDLG_H__56B2D134_DC41_4058_9C2F_3766E0A6C067__INCLUDED_)
#define AFX_EMAILCLIENTDLG_H__56B2D134_DC41_4058_9C2F_3766E0A6C067__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AddressBook.h"
#include "smtp/MailMsg.h"
#include "interface/ColorEdit.h"
#include "interface/XListCtrl.h"
#include "interface/GroupBox.h"
#include "interface/SXButton.h"
#include "interface/XListBox.h"

#define NUMBER_OF_EMAIL_LIST_COLUMNS	4

/////////////////////////////////////////////////////////////////////////////
// CEmailClientDlg dialog
	
class CEmailClientDlg : public CDialog
{
// Construction
public:	
	void OwnerDrawButtons();
	LRESULT OnOpComplete(WPARAM w, LPARAM l);
	void SetEmailList();
	void ShowContactsMenu();
	void DisplayAddrBook();
	void AddContact();
	void ShowMessagesMenu ();
	void AddAccount();
	void DisplayAccounts();
	void ShowAccountsMenu();
	void SetClientToolbarButtons();
	void SetClientToolbarStrings();
	void SetClientToolbar();
	CEmailClientDlg(CWnd* pParent = NULL);	// standard constructor

	CString m_sCrtSelFile;
	CAddressBook m_addressBook;	
	int m_iSelection;	
	CToolBarCtrl m_MainTb;
	CString m_strMsg;

	CString m_strInbox;
	CString m_strOutbox;
	CString m_strSent;
	CString m_strDeleted;
	CString m_strDraft;

// Dialog Data
	//{{AFX_DATA(CEmailClientDlg)
	enum { IDD = IDD_EMAILCLIENT_DIALOG };
	CTreeCtrl	m_cTreeCtrl;
	CXListCtrl	m_Msgs;
	CXListBox	m_ContactList;
	CColorEdit	m_ctrlEditMailText;
	CGroupBox	m_EmailTextGroup;
	CGroupBox	m_EmailsGroup;
	CGroupBox	m_AddrBGroup;
	CGroupBox	m_FoldersGroup;
	CImageList	m_imgTree;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEmailClientDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	LRESULT OnAccChanged (WPARAM wParam, LPARAM lParam);
	void UpdateStatusBarMailPanels (int nUnread, int nTotal);
	void UpdateFolderTree();
	BOOL bWindowInitialized;
	CString RootFolder;
	BOOL ReadLocalUserCode ();
	CString XSATAUID;
	byte AESKey[33];
	HICON m_hIcon;

	CSXButton	m_btnDeleteMail;
	CSXButton	m_btnReply;
	CSXButton	m_btnForward;
	CSXButton	m_btnContinueDraft;
	CSXButton	m_btnSaveAttach;
	// Generated message map functions
	//{{AFX_MSG(CEmailClientDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnToolbarDropDown(NMHDR *pNotifyStruct, LRESULT *result);
	afx_msg void OnTbMessages();
	afx_msg void OnTbAddrBook();
	afx_msg void OnTbAccounts();
	afx_msg void OnTbExit();
	afx_msg void OnAccountsAddaccount();
	afx_msg void OnAccountsViewaccounts();
	afx_msg void OnReceiveMessages();
	afx_msg void OnNewMessage();
	afx_msg void OnSendMessage();
	afx_msg void OnContactsAddcontact();
	afx_msg void OnContactsViewcontacts();
	afx_msg void OnSaveAttach();
	afx_msg void OnSelchangedFolderTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeleteMail();
	afx_msg void OnKeydownListMsgs(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnContinueDraft();
	afx_msg void OnReply();
	afx_msg void OnForward();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDblclkListMsgs(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusFolderTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusFolderTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusListMsgs(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusListMsgs(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusEDITMailTxt();
	afx_msg void OnSetfocusEDITMailTxt();
	afx_msg void OnItemchangedListMsgs(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnAddCode();
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDblclkContactsList();
	afx_msg void OnSetfocusContactsList();
	afx_msg void OnKillfocusContactsList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString CountUnread (CString Folder, int& nTotal, int& nUnread);
	void ViewFolder(CString Folder);
	void RefreshSelectedTreeFolder();
	CStringArray arrFilesInFolder;
	float OrigColWidths[NUMBER_OF_EMAIL_LIST_COLUMNS];
	CStatusBarCtrl m_SBar;
	CRect rcInitial;
	BOOL m_bSendingInProgres;
	CString msgFile;
	void StartSendMessage();
	CString m_sCurrentSelectedFile;
	BOOL m_bCurentRead;
	HTREEITEM m_htreeDraft;
	HTREEITEM m_htreeDeleted;
	HTREEITEM m_htreeSent;
	HTREEITEM m_htreeOutbox;
	HTREEITEM m_htreeInbox;
	HTREEITEM m_htreeRoot;
	void refresContactsList();
	void ReadContacts();
	int m_SelectedMail;
	CMailMsg m_Mail;
	CString FileName;
	CBitmap tbMainBmp;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EMAILCLIENTDLG_H__56B2D134_DC41_4058_9C2F_3766E0A6C067__INCLUDED_)
