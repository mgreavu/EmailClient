// GeneralPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "GeneralPropertiesDlg.h"
#include "AccountListTypes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGeneralPropertiesDlg property page

IMPLEMENT_DYNCREATE(CGeneralPropertiesDlg, CPropertyPage)

CGeneralPropertiesDlg::CGeneralPropertiesDlg() : CPropertyPage(CGeneralPropertiesDlg::IDD)
{
	//{{AFX_DATA_INIT(CGeneralPropertiesDlg)
	m_sEmailAdress = _T("");
	m_sMailAcount = _T("");
	m_sName = _T("");
	m_sOrganization = _T("");
	//}}AFX_DATA_INIT
	m_psp.dwFlags &= ~PSP_HASHELP;
}

CGeneralPropertiesDlg::~CGeneralPropertiesDlg()
{
}

void CGeneralPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGeneralPropertiesDlg)
	DDX_Text(pDX, IDC_EMAIL_ADDRESS, m_sEmailAdress);
	DDX_Text(pDX, IDC_MAIL_ACCOUNT, m_sMailAcount);
	DDX_Text(pDX, IDC_NAME, m_sName);
	DDX_Text(pDX, IDC_ORGANIZATION, m_sOrganization);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGeneralPropertiesDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CGeneralPropertiesDlg)
	ON_EN_CHANGE(IDC_ORGANIZATION, OnChange)
	ON_EN_CHANGE(IDC_NAME, OnChange)
	ON_EN_CHANGE(IDC_MAIL_ACCOUNT, OnChange)
	ON_EN_CHANGE(IDC_EMAIL_ADDRESS, OnChange)
	ON_BN_CLICKED(IDC_ACCOUNT_OPTION, OnChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGeneralPropertiesDlg message handlers

BOOL CGeneralPropertiesDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	m_sMailAcount = (*AccListIterator).GetMailAccountName();
	m_sName = (*AccListIterator).GetSMTPDisplayName();
	m_sOrganization = (*AccListIterator).GetOrganization();
	m_sEmailAdress = (*AccListIterator).GetSMTPEmailAddress();
	UpdateData (FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGeneralPropertiesDlg::OnChange() 
{
	UpdateData(TRUE);
	m_bOnChange = TRUE;
}
