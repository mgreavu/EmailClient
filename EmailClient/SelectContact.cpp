// SelectContact.cpp : implementation file
//

#include "stdafx.h"
#include "emailclient.h"
#include "SelectContact.h"
#include "AccountListTypes.h"
#include <string>
#include <list>
#include <algorithm>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectContact dialog


CSelectContact::CSelectContact(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectContact::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectContact)
	//}}AFX_DATA_INIT
	m_sEmailAddr[0] = 0;
}


void CSelectContact::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectContact)
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_CONTACTSLIST, m_ContactsList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectContact, CDialog)
	//{{AFX_MSG_MAP(CSelectContact)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectContact message handlers

BOOL CSelectContact::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect rect;
	m_ContactsList.GetWindowRect(&rect);

	m_ContactsList.InsertColumn(0, "Email Address", LVCFMT_LEFT, rect.Width()*0.65);
	m_ContactsList.InsertColumn(0, "NickName", LVCFMT_LEFT, rect.Width()*0.35);
	m_ContactsList.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	
	refresContactsList();
	OwnerDrawButtons();
	
	return TRUE;
}


void CSelectContact::refresContactsList()
{
	int i=0;
	CString	strText;

	for (parentWindow->m_addressBook.ContactListIterator = parentWindow->m_addressBook.myContactList.begin() ; parentWindow->m_addressBook.ContactListIterator != parentWindow->m_addressBook.myContactList.end(); parentWindow->m_addressBook.ContactListIterator++, i++)
	{
		strText = (*(parentWindow->m_addressBook.ContactListIterator)).m_NickName;
		m_ContactsList.InsertItem(i, strText);
		

		strText = (*(parentWindow->m_addressBook.ContactListIterator)).m_EmailAdress;
		m_ContactsList.SetItem(i, 1, LVIF_TEXT, strText, 0, 0, 0, 0);
	}

	m_ContactsList.SetItemState( 0, LVIS_SELECTED | LVIS_FOCUSED , 
		LVIS_SELECTED | LVIS_FOCUSED);	
}

void CSelectContact::OnOK() 
{
	//get the selected E-mail address

	int iSel = m_ContactsList.GetSelectionMark();
	
	if (iSel != -1)
		m_ContactsList.GetItemText(iSel, 1, m_sEmailAddr, MAX_PATH);
	
	CDialog::OnOK();
}



void CSelectContact::OwnerDrawButtons()
{
	m_btnOk.SetMaskedBitmap( IDB_OK, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );
}
