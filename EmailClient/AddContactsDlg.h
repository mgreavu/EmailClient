#if !defined(AFX_ADDCONTACTSDLG_H__ACEA0A04_1C86_479B_A677_448969779666__INCLUDED_)
#define AFX_ADDCONTACTSDLG_H__ACEA0A04_1C86_479B_A677_448969779666__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Contact.h"
// AddContactsDlg.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CAddContactsDlg dialog

class CAddContactsDlg : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	CContact getContact();
	CAddContactsDlg(CContact contact, CWnd* pParent = NULL);   // standard constructor
	CAddContactsDlg(CWnd* pParent = NULL);   // standard constructor
	CContact m_contact;

// Dialog Data
	//{{AFX_DATA(CAddContactsDlg)
	enum { IDD = IDD_ADDCONTACT_DLG };
//	CButton	m_btnOk; 
//	CButton	m_btnCancel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddContactsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSXButton m_btnOk;
	CSXButton m_btnCancel;
	// Generated message map functions
	//{{AFX_MSG(CAddContactsDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDCONTACTSDLG_H__ACEA0A04_1C86_479B_A677_448969779666__INCLUDED_)
