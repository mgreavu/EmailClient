#if !defined(AFX_ADVANCEDPROPERTIESDLG_H__0F2892A3_1484_4787_9346_2EC6A2939C89__INCLUDED_)
#define AFX_ADVANCEDPROPERTIESDLG_H__0F2892A3_1484_4787_9346_2EC6A2939C89__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AdvancedPropertiesDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAdvancedPropertiesDlg dialog

class CAdvancedPropertiesDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CAdvancedPropertiesDlg)

// Construction
public:
	BOOL m_bOnChange;
	CAdvancedPropertiesDlg();
	~CAdvancedPropertiesDlg();

// Dialog Data
	//{{AFX_DATA(CAdvancedPropertiesDlg)
	enum { IDD = IDD_ADV_PROP_DLG };
	BOOL	m_LeaveMailOnServer;
	int		m_iPOP3Port;
	int		m_iSMTPPort;
	int		m_iPOP3Timeout;
	int		m_RemoveAfter;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAdvancedPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAdvancedPropertiesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDefaultPortsBtn();
	afx_msg void OnChange();
	afx_msg void OnLeaveMsgsOnServerOption();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADVANCEDPROPERTIESDLG_H__0F2892A3_1484_4787_9346_2EC6A2939C89__INCLUDED_)
