#pragma once

BOOL	CreateCompressedFile(LPSTR ExistingFilePath,  const char* m_UserCode);
BOOL	CreateMPYFile(LPSTR TmpFileName, LPSTR DestPath, const byte* m_AESKey);
BOOL	EncryptFile(LPWIN32_FIND_DATA fData, LPSTR SourcePath, LPSTR DestPath, const char* m_UserCode, const byte* m_AESKey);
byte*	EncryptBuffer (const byte* lpBufferToEncrypt, DWORD dwBufferToEncryptSize, DWORD& dwOutBufferSize, const byte* m_AESKey);

BOOL	CheckUserID(LPSTR EncryptedFilePath, const char* m_UserCode);
BOOL	CreateDecompressedFile(LPSTR EncryptedFilePath, LPSTR FileName, const char* m_UserCode);
BOOL	CreateTMPFile(LPSTR FileName, LPSTR SourcePath, LPSTR DestPath, const byte* m_AESKey);
BOOL	DecryptFile(LPSTR FileName, LPSTR SourcePath, LPSTR DestPath, const char* m_UserCode, const byte* m_AESKey);
byte*	DecryptBuffer (const byte* lpBufferToDecrypt, DWORD dwBufferToDecryptSize, const byte* m_AESKey);

CString	FileNameWithoutExtension(LPSTR filename);
void	EncFileTimeToString(FILETIME* fTime, LPSTR FileTime);
BOOL	IsDriveFixed(LPSTR Path);
void	DeleteFileInSecureMode(LPSTR Path);
