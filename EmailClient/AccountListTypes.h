#if !defined (ACCLISTTYPES__H)
#define ACCLISTTYPES__H

#include <list>
#include <algorithm>
#include "MailAccount.h"

using namespace std;

typedef list<CMailAccount>	AccList;

extern AccList AccountList;
extern AccList::iterator AccListIterator;

BOOL GetAccountList();

#endif