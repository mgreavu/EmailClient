// ConnPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "ConnPropertiesDlg.h"
#include "AccountListTypes.h"
#include "ConnectDefs.h"
#include "ras.h"
#include "raserror.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConnPropertiesDlg property page

IMPLEMENT_DYNCREATE(CConnPropertiesDlg, CPropertyPage)

CConnPropertiesDlg::CConnPropertiesDlg() : CPropertyPage(CConnPropertiesDlg::IDD)
{
	//{{AFX_DATA_INIT(CConnPropertiesDlg)
	m_Connection = -1;
	//}}AFX_DATA_INIT
	m_psp.dwFlags &= ~PSP_HASHELP;
}

CConnPropertiesDlg::~CConnPropertiesDlg()
{
}

void CConnPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConnPropertiesDlg)
	DDX_Control(pDX, IDC_RAS_ACCOUNTS, m_cRasAccounts);
	DDX_Radio(pDX, IDC_GROUP_CONN, m_Connection);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConnPropertiesDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CConnPropertiesDlg)
	ON_BN_CLICKED(IDC_GROUP_CONN, OnClickConn)
	ON_BN_CLICKED(IDC_GROUP_MODEM, OnClickConn)
	ON_CBN_SELCHANGE(IDC_RAS_ACCOUNTS, OnSelchangeRasAccounts)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConnPropertiesDlg message handlers

BOOL CConnPropertiesDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	CString RasAccount;
	CString sMsg;
	CString sMsg1;
	

	m_Connection = (*AccListIterator).GetConnectionType();
	RasAccount = (*AccListIterator).GetRasAccount();
		

	if((m_Connection < 0) || (m_Connection > 1))
	{
		m_Connection = 0;
		m_bOnChange = TRUE;
	}

	if(m_Connection == 0)
	{
		GetDlgItem(IDC_RAS_ACCOUNTS)->EnableWindow(FALSE);
		GetDlgItem(IDC_TEXT)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_RAS_ACCOUNTS)->EnableWindow(TRUE);
		GetDlgItem(IDC_TEXT)->EnableWindow(TRUE);
	}

	UpdateData(FALSE);

	DWORD nRet;
	DWORD cb;			//the dimension of the buffer to receive the defined connections
	DWORD cEntries;		//the number of dial-up defined connections
	
	LPRASENTRYNAME lpRasEntryName = NULL;
	//!!!!!!!!!!!!!!!!!!!!! NOWHERE GlobalFree is used!!!!!
	lpRasEntryName = (LPRASENTRYNAME) GlobalAlloc(GPTR, sizeof(RASENTRYNAME));
	lpRasEntryName->dwSize = sizeof(RASENTRYNAME);
	cb = sizeof(RASENTRYNAME);
	
	//first find the necessary dimension for the buffer to receive the requested infos
	if ((nRet = RasEnumEntries(NULL, NULL, lpRasEntryName, &cb, &cEntries)) == ERROR_BUFFER_TOO_SMALL)
	{
		//cb will contain now the right buffer dimension
		//if the first dimension of the buffer was too small, do again a larger allocation
		lpRasEntryName = (LPRASENTRYNAME) GlobalAlloc(GPTR, cb);
		lpRasEntryName->dwSize = sizeof(RASENTRYNAME);
	}
	
	// now Calling RasEnumEntries again to enumerate the phone-book entries    
	nRet = RasEnumEntries(NULL, NULL, lpRasEntryName, &cb, &cEntries);
	if (nRet != ERROR_SUCCESS)
	{
		
		sMsg.LoadString(IDS_ERR_FIND_DIALUP);
		sMsg1.LoadString(IDS_ERR);
		
		MessageBox(sMsg, sMsg1, MB_ICONERROR | MB_OK );
	}
	else
	{
		for (UINT i=0;i < cEntries;i++)
		{
			//AfxMessageBox(lpRasEntryName->szEntryName);
			m_cRasAccounts.AddString(lpRasEntryName->szEntryName);
			if(_stricmp(lpRasEntryName->szEntryName , RasAccount) == 0)
				m_cRasAccounts.SetCurSel(i);
			lpRasEntryName++;
		}
	}
	
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConnPropertiesDlg::OnClickConn() 
{
	m_bOnChange = TRUE;
	UpdateData(TRUE);
	if(m_Connection == 0)
	{
		GetDlgItem(IDC_RAS_ACCOUNTS)->EnableWindow(FALSE);
		GetDlgItem(IDC_TEXT)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_RAS_ACCOUNTS)->EnableWindow(TRUE);
		GetDlgItem(IDC_TEXT)->EnableWindow(TRUE);
		GetDlgItem(IDC_RAS_ACCOUNTS)->GetWindowText(m_sRasAccount);
	}
}

void CConnPropertiesDlg::OnSelchangeRasAccounts() 
{
	m_bOnChange = TRUE;
	GetDlgItem(IDC_RAS_ACCOUNTS)->GetWindowText(m_sRasAccount);
}
