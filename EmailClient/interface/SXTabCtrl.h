#if !defined(AFX_SXTABCTRL_H__86499C29_B25E_4C1E_AD7D_B446F9901691__INCLUDED_)
#define AFX_SXTABCTRL_H__86499C29_B25E_4C1E_AD7D_B446F9901691__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SXTabCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSXTabCtrl window

class CSXTabCtrl : public CTabCtrl
{
// Construction
public:
	CSXTabCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSXTabCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL SubclassDlgItem(UINT nID, CWnd *pParent);
	virtual ~CSXTabCtrl();

	// Generated message map functions
protected:
	void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//{{AFX_MSG(CSXTabCtrl)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SXTABCTRL_H__86499C29_B25E_4C1E_AD7D_B446F9901691__INCLUDED_)
