// XListBox.cpp : implementation file
//

#include "stdafx.h"
#include "XListBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXListBox

CXListBox::CXListBox()
{
	m_ImageList.Create(16, 16, ILC_COLOR32 | ILC_MASK, 8, 1);
	nItemHeight = 32;
	nImageCount = 0;
}

CXListBox::~CXListBox()
{
	m_ImageList.DeleteImageList();
}


BEGIN_MESSAGE_MAP(CXListBox, CListBox)
	//{{AFX_MSG_MAP(CXListBox)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXListBox message handlers

void CXListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	CRect rItem;
	CRect rText;
	CRect rIcon;
	
	CDC* dc = CDC::FromHandle(lpDrawItemStruct->hDC);

	rItem = lpDrawItemStruct->rcItem;	

	if ((int)lpDrawItemStruct->itemID < 0)
	{
		if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && (lpDrawItemStruct->itemState & ODS_FOCUS))
		{
			dc->DrawFocusRect(&lpDrawItemStruct->rcItem);
		}
		else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && !(lpDrawItemStruct->itemState & ODS_FOCUS))
		{
			dc->DrawFocusRect(&lpDrawItemStruct->rcItem);
		}
		return;
	}

	CString strText;
	GetText(lpDrawItemStruct->itemID, strText);

	rIcon.top = rItem.top + (nItemHeight - 16) / 2;
	rIcon.left = rItem.left + 2;
	rIcon.bottom = rIcon.top + 16;
	rIcon.right = rIcon.left + 16;

	rText.left = rIcon.right + 5;
	rText.top = rIcon.top;

	UINT nFormat = DT_LEFT | DT_SINGLELINE | DT_VCENTER;
	if (GetStyle() & LBS_USETABSTOPS) nFormat |= DT_EXPANDTABS;

	if ((lpDrawItemStruct->itemState & ODS_SELECTED) && (lpDrawItemStruct->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		CBrush br(::GetSysColor(COLOR_HIGHLIGHT));
		dc->FillRect(&rItem, &br);
	}
	else if (!(lpDrawItemStruct->itemState & ODS_SELECTED) && (lpDrawItemStruct->itemAction & ODA_SELECT))
	{
		CBrush br(::GetSysColor(COLOR_WINDOW));
		dc->FillRect(&rItem, &br);
	}

	// If the item has focus, draw the focus rect. If the item does not have focus, erase the focus rect.
	if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && (lpDrawItemStruct->itemState & ODS_FOCUS))
	{
		dc->DrawFocusRect(&rItem);
	}
	else if ((lpDrawItemStruct->itemAction & ODA_FOCUS) && !(lpDrawItemStruct->itemState & ODS_FOCUS))
	{
		dc->DrawFocusRect(&rItem);
	}

	int iBkMode = dc->SetBkMode(TRANSPARENT);

	COLORREF crText;

	if (lpDrawItemStruct->itemState & ODS_SELECTED)
		crText = dc->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
	else if (lpDrawItemStruct->itemState & ODS_DISABLED)
		crText = dc->SetTextColor(::GetSysColor(COLOR_GRAYTEXT));
	else
		crText = dc->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));


	CPoint pt(rIcon.left, rIcon.top);
	
	if (nImageCount > (int)lpDrawItemStruct->itemID)
		m_ImageList.Draw(dc, (int)lpDrawItemStruct->itemID, pt, ILD_NORMAL);
	else
		m_ImageList.Draw(dc, 0, pt, ILD_NORMAL);

	dc->TextOut(rText.left,rText.top,strText);
	dc->SetTextColor(crText);
}

void CXListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{

	lpMeasureItemStruct->itemHeight = nItemHeight;	
}

void CXListBox::AddIcon(WORD wIconID)
{
	m_ImageList.Add(LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE (wIconID)));
	nImageCount++;

}

void CXListBox::SetItemHeight(int nHeight)
{
	nItemHeight = nHeight;
}
