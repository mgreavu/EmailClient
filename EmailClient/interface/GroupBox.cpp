// GroupBox.cpp : implementation file
//

#include "stdafx.h"
#include "GroupBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGroupBox

CGroupBox::CGroupBox()
{
	m_Active            = FALSE;
	m_ActiveColor       = RGB(10, 36, 106);
	m_InactiveColor     = ::GetSysColor(COLOR_3DSHADOW);
	m_ActiveShadow      = RGB(10, 36, 106);
	m_InactiveShadow    = RGB(255, 255, 255);
	m_InactiveTextColor = RGB(0, 0, 0);
	m_ActiveTextColor   = RGB(10, 36, 106);
	m_TextStartPoint    = 20;
	m_ActiveFontItalic  = TRUE;

	memset(&m_LogFont, 0x00, sizeof(LOGFONT));
}

CGroupBox::~CGroupBox()
{
}


BEGIN_MESSAGE_MAP(CGroupBox, CButton)
	//{{AFX_MSG_MAP(CGroupBox)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_SETTEXT, OnSetText)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupBox message handlers
void CGroupBox::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	int nSavedDC = dc.SaveDC();

	HRGN hRgn = ::CreateRectRgn(0, 0, 0, 0);

	GetWindowRgn(hRgn);
	::SelectClipRgn(dc.GetSafeHdc(), hRgn);

	CBrush* oldBrush = NULL;
	CPen* oldPen = NULL;

	CBrush newBrush;
	CPen InnerPen, OutterPen;
	CSize TitleSize;
	COLORREF OLineColor, ILineColor, TextColor;

	SetActiveFont(m_Active);
	TextFont.CreateFontIndirect(&m_LogFont);

	CFont* oldFont = dc.SelectObject(&TextFont);
	
	if (!m_strText.IsEmpty()) TitleSize = dc.GetTextExtent(m_strText);
	else TitleSize = CSize(0, 2);
	
	//Set the color for the box
	if (m_Active) 
	{
		OLineColor = m_ActiveColor;
		ILineColor = m_ActiveShadow;
		TextColor  = m_ActiveTextColor; 
	}
	else
	{
		OLineColor = m_InactiveColor;
		ILineColor = m_InactiveShadow;
		TextColor  = m_InactiveTextColor;
	}
	
	//Create the new brush and pen
	newBrush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

	InnerPen.CreatePen(PS_SOLID, 2, ILineColor);
	OutterPen.CreatePen(PS_SOLID, 1, OLineColor);

	oldBrush = dc.SelectObject(&newBrush);
	oldPen = dc.SelectObject(&InnerPen);

	RECT rect;
	RECT BoxRect;
	POINT point;

	point.x = TitleSize.cx;
	point.y = TitleSize.cy;

	GetClientRect(&rect);

	dc.FillRect(&rect, &newBrush);

	//Draw the box
	//Draw inner line
	BoxRect.left   = rect.left+1;
	BoxRect.top    = rect.top + (point.y/2);
	BoxRect.right  = rect.right;
	BoxRect.bottom = rect.bottom;
	dc.Rectangle(&BoxRect);

	//Draw outter line
	BoxRect.left   = rect.left;
	BoxRect.top    = rect.top + (point.y/2)-1;
	BoxRect.right  = rect.right-1;
	BoxRect.bottom = rect.bottom-1;	

	HBRUSH hbr = (HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
	dc.SelectObject(CBrush::FromHandle(hbr));

	dc.SelectObject(&OutterPen);
	dc.Rectangle(&BoxRect);
	

	//Draw text
	dc.SetBkColor(::GetSysColor(COLOR_BTNFACE));
	dc.SetTextColor(TextColor);
	dc.TextOut(rect.left + m_TextStartPoint, rect.top, m_strText);

	::DeleteObject(hbr);

	dc.SelectObject(oldPen);
	OutterPen.DeleteObject();
	InnerPen.DeleteObject();

	dc.SelectObject(oldBrush);
	newBrush.DeleteObject();

	dc.SelectObject(oldFont);
	TextFont.DeleteObject();

	::DeleteObject(hRgn);

	dc.RestoreDC(nSavedDC);
}

void CGroupBox::SetDefaultFont()
{
	m_LogFont.lfCharSet = ANSI_CHARSET;
	m_LogFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	m_LogFont.lfEscapement = 0;
	strncpy(m_LogFont.lfFaceName, "Verdana", strlen("Verdana"));
	m_LogFont.lfHeight = 16;
	m_LogFont.lfItalic = FALSE;
	m_LogFont.lfOrientation = 0;
	m_LogFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	m_LogFont.lfPitchAndFamily = DEFAULT_PITCH | FF_MODERN;
	m_LogFont.lfQuality = PROOF_QUALITY;
	m_LogFont.lfStrikeOut = FALSE;
	m_LogFont.lfUnderline = FALSE;
	m_LogFont.lfWeight = FW_BOLD;
	m_LogFont.lfWidth = 0;
}

void CGroupBox::ActivateGroupBox(BOOL bActivate)
{
	m_Active = bActivate;

	Invalidate();
	UpdateWindow();
}

void CGroupBox::SetActiveFont(BOOL bActive)
{
	if (bActive && m_ActiveFontItalic) 
		m_LogFont.lfItalic = TRUE;
	else 
		m_LogFont.lfItalic = FALSE;
}

void CGroupBox::PreSubclassWindow() 
{
	SetButtonStyle(BS_GROUPBOX, FALSE);

	GetWindowText(m_strText);
	SetDefaultFont();

	SetGroupBoxRegion();
	
	CButton::PreSubclassWindow();
}

void CGroupBox::SetGroupBoxRegion()
{
	CString WndTitle;
	CSize Size;
	RECT rect;
	
	GetClientRect(&rect);

	if (m_strText.IsEmpty()) 
	{
		Size = CSize(0, 2);
	}
	else
	{
		CDC* pDC = GetDC();

		CFont TextFontLocal;		
		TextFontLocal.CreateFontIndirect(&m_LogFont);
		CFont* oldFont = pDC->SelectObject(&TextFontLocal);		
		Size = pDC->GetTextExtent(m_strText);
		
		pDC->SelectObject(oldFont);
		TextFontLocal.DeleteObject();

		ReleaseDC(pDC);
	}
	
	CRgn UpRgn;
	CRgn LeftRgn;
	CRgn RightRgn;
	CRgn BottomRgn;
	
	UpRgn.CreateRectRgn(rect.left, rect.top, rect.right, rect.top+Size.cy);
	LeftRgn.CreateRectRgn(rect.left, rect.top, rect.left+2, rect.bottom-2);
	RightRgn.CreateRectRgn(rect.right-2, rect.top, rect.right, rect.bottom-2);
	BottomRgn.CreateRectRgn(rect.left, rect.bottom-2, rect.right, rect.bottom);
	
	CRgn ResRgn;
	
	ResRgn.CreateRectRgn(0, 0, 0, 0);
	
	ResRgn.CombineRgn(&UpRgn, NULL, RGN_COPY);
	ResRgn.CombineRgn(&ResRgn, &LeftRgn, RGN_OR);
	ResRgn.CombineRgn(&ResRgn, &RightRgn, RGN_OR);
	ResRgn.CombineRgn(&ResRgn, &BottomRgn, RGN_OR);
	
	SetWindowRgn((HRGN) ResRgn, TRUE);
}

void CGroupBox::InvalidateGroupBox(BOOL bErase)
{
	SetGroupBoxRegion();
	Invalidate(bErase);
	UpdateWindow();
}

void CGroupBox::SetGroupBoxPos(CWnd* pInsertAfter, int x, int y, int cx, int cy, UINT uFlags)
{
	SetWindowPos(pInsertAfter, x, y, cx, cy, uFlags);
	InvalidateGroupBox(TRUE);
}

HBRUSH CGroupBox::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	Invalidate();
	
	return NULL;
}

void CGroupBox::SetWndTitle(LPSTR sTitle)
{
	SetWindowText(sTitle);
	SetGroupBoxRegion();
	Invalidate();
	UpdateWindow();
}

LRESULT CGroupBox::OnSetText(WPARAM wParam, LPARAM lParam)
{
	m_strText = (const _TCHAR*)lParam;
	SetGroupBoxRegion();
	Invalidate();
	UpdateWindow();
	
	return 0;
}
