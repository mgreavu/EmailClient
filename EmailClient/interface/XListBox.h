#if !defined(AFX_XLISTBOX_H__1F8D863B_B5F3_4122_9034_937A48783FC9__INCLUDED_)
#define AFX_XLISTBOX_H__1F8D863B_B5F3_4122_9034_937A48783FC9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// XListBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CXListBox window

class CXListBox : public CListBox
{
// Construction
public:
	CXListBox();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXListBox)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetItemHeight (int nHeight);
	void AddIcon(WORD wIconID);
	virtual ~CXListBox();

	// Generated message map functions
protected:
	CImageList m_ImageList;
	//{{AFX_MSG(CXListBox)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	int nImageCount;
	int nItemHeight;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XLISTBOX_H__1F8D863B_B5F3_4122_9034_937A48783FC9__INCLUDED_)
