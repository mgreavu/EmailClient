// SXTabCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "SXTabCtrl.h"

const COLORREF TABLTBBLUE = RGB(128, 184, 223);

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSXTabCtrl

CSXTabCtrl::CSXTabCtrl()
{
}

CSXTabCtrl::~CSXTabCtrl()
{
}


BEGIN_MESSAGE_MAP(CSXTabCtrl, CTabCtrl)
	//{{AFX_MSG_MAP(CSXTabCtrl)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSXTabCtrl message handlers

BOOL CSXTabCtrl::OnEraseBkgnd(CDC* pDC) 
{
	RECT rect;

	GetClientRect(&rect);
	pDC->FillSolidRect(&rect, TABLTBBLUE); // bgnd

	return FALSE;
}

void CSXTabCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	DRAWITEMSTRUCT& ds = *lpDrawItemStruct;	
	CDC dc;
	dc.Attach(ds.hDC);
	
	int iItem = ds.itemID;	
	
	char text[128];
	TCITEM tci;
	tci.mask = TCIF_TEXT;
	tci.pszText = text;
	tci.cchTextMax = sizeof(text);
	GetItem(iItem, &tci);
	
	CRect rc = ds.rcItem;

	dc.FillSolidRect(rc, TABLTBBLUE); // tabs
	if (!(lpDrawItemStruct->itemState & ODS_SELECTED)) 
	{
		rc.top    += 2;
		rc.bottom += 2;
	}
	dc.DrawText(text, strlen(text), rc, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
	
	dc.Detach();
}

BOOL CSXTabCtrl::SubclassDlgItem(UINT nID, CWnd *pParent)
{
	if (!CTabCtrl::SubclassDlgItem(nID, pParent))
		return FALSE;
    ModifyStyle(0, TCS_OWNERDRAWFIXED);	
	
	return TRUE;
}
