#include "stdafx.h"
#include "ColorEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorEdit

CColorEdit::CColorEdit()
{
//	InitializeFlatSB(GetSafeHwnd());
	m_crBkColor = ::GetSysColor(COLOR_3DFACE); // Initializing background color to the system face color.
	m_crTextColor = BLACK;					   // Initializing text color to black
	m_brBkgnd.CreateSolidBrush(m_crBkColor);   // Creating the Brush Color For the Edit Box Background
}

CColorEdit::~CColorEdit()
{
}


BEGIN_MESSAGE_MAP(CColorEdit, CEdit)
	//{{AFX_MSG_MAP(CColorEdit)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_MOUSEWHEEL()
//	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorEdit message handlers

BOOL CColorEdit::OnEraseBkgnd(CDC* pDC)
{
/*
	CBitmap bmpMem, *pOldMemBmp;
	CRect rctPaint;
	GetWindowRect (&rctPaint);
	
	CDC dc;
	dc.CreateCompatibleDC(pDC);

	bmpMem.CreateCompatibleBitmap (pDC, rctPaint.Width(),rctPaint.Height());
	pOldMemBmp = dc.SelectObject(&bmpMem);

	dc.FillSolidRect(&rctPaint, RGB(255,0,0));
	pDC->BitBlt(0, 0, rctPaint.Width(),rctPaint.Height(), &dc, 0, 0, SRCCOPY);

	dc.DeleteDC();
*/
	return FALSE;
} 

void CColorEdit::SetTextColor(COLORREF crColor)
{
	m_crTextColor = crColor;
	RedrawWindow();	
}

void CColorEdit::SetBkColor(COLORREF crColor)
{
	m_crBkColor = crColor;
	m_brBkgnd.DeleteObject();
	m_brBkgnd.CreateSolidBrush(crColor);
	RedrawWindow();
}

HBRUSH CColorEdit::CtlColor(CDC* pDC, UINT nCtlColor)
{
	HBRUSH hbr;
	hbr = (HBRUSH)m_brBkgnd; // Passing a Handle to the Brush

	pDC->SetBkColor(m_crBkColor);
	pDC->SetTextColor(m_crTextColor);

	return hbr;
}

BOOL CColorEdit::SetReadOnly(BOOL flag)
{
   if (flag == TRUE)
      SetBkColor(m_crBkColor);
   else
      SetBkColor(WHITE);

   return CEdit::SetReadOnly(flag);
}


void CColorEdit::InsertLine(CString NewLine)
{
	char TimeBuffer[128];
	memset (TimeBuffer, 0x00, 128);

	SYSTEMTIME st;
	GetLocalTime (&st);
	sprintf (TimeBuffer, "%02d-%02d-%d %02d:%02d:%02d  ", st.wDay, st.wMonth, st.wYear, st.wHour, st.wMinute, st.wSecond);

	CString CurrentContent;
	GetWindowText (CurrentContent);
	CurrentContent += TimeBuffer;
	CurrentContent += NewLine;
	CurrentContent += "\r\n";
	SetWindowText (CurrentContent.GetBuffer(65536));
	LineScroll(GetLineCount());
}


BOOL CColorEdit::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{

 	if (zDelta > 0) 
 		LineScroll(-3);
 	else 
 		LineScroll(3);
	
	return CEdit::OnMouseWheel(nFlags, zDelta, pt);
}

