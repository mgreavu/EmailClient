// XProgressBar.cpp : implementation file
//

#include "stdafx.h"
#include "XProgressBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma warning (disable:4244)
/////////////////////////////////////////////////////////////////////////////
// CXProgressBar

CXProgressBar::CXProgressBar()
{
	m_TextColor = RGB (255,255,0);
	hFont = NULL;
}

CXProgressBar::~CXProgressBar()
{
	if (hFont) ::DeleteObject (hFont);
}


BEGIN_MESSAGE_MAP(CXProgressBar, CProgressCtrl)
	//{{AFX_MSG_MAP(CXProgressBar)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXProgressBar message handlers

void CXProgressBar::OnPaint() 
{
	CPaintDC dc(this); 

	RECT rcSurface;
	GetClientRect(&rcSurface);

	LONG lWidth = rcSurface.right - rcSurface.left;
	LONG lHeight = rcSurface.bottom - rcSurface.top;

	HDC hWndDC = dc.GetSafeHdc();
	HDC hMemDC = ::CreateCompatibleDC (hWndDC);
	if (!hMemDC) return;

	HBITMAP hMemBM = CreateCompatibleBitmap (hWndDC, lWidth, lHeight);
	::SelectObject (hMemDC, hMemBM);

	int nLow, nHigh;
	GetRange(nLow, nHigh);
	int nPos = GetPos();

	float fWndPerc = (float)(nPos*lWidth)/(float)(nHigh-nLow);

	RECT rcNow;
	rcNow.top = rcSurface.top;
	rcNow.bottom = rcSurface.bottom;
	rcNow.left = rcSurface.left;
	rcNow.right = fWndPerc;
	
	COLORREF clrBase = RGB (0,0,0);
	HBRUSH hNewBrush = NULL;

	for (int i=1; i<=lHeight; i++)
	{
		RECT rcGradient;
		rcGradient.top = i-1;
		rcGradient.bottom = i;
		rcGradient.left = rcNow.left;
		rcGradient.right = rcNow.right;

		if (nPos == nHigh) clrBase = RGB(0, 255-8*i, 0);
		else clrBase = RGB(255-8*i, 0, 0);

		hNewBrush = ::CreateSolidBrush (clrBase);
		::SelectObject (hMemDC, hNewBrush);
		::FillRect (hMemDC, &rcGradient, hNewBrush);
		if (hNewBrush) ::DeleteObject (hNewBrush);
	}


	::SetBkMode (hMemDC, TRANSPARENT);
	if (hFont) ::SelectObject (hMemDC, hFont);
	::SetTextColor (hMemDC, m_TextColor);

	CString sFmt;
	sFmt.Format ("%s%d%%",strWndText.GetBuffer(strWndText.GetLength()),(int)((float)(nPos*100)/(float)(nHigh-nLow)));

	::DrawText(hMemDC, sFmt, -1, &rcSurface, DT_SINGLELINE|DT_VCENTER|DT_CENTER);

	::BitBlt (hWndDC, 0, 0, lWidth, lHeight, hMemDC, 0, 0, SRCCOPY);

	if (hMemBM)  ::DeleteObject (hMemBM);
	if (hMemDC)  ::DeleteDC (hMemDC);

}

void CXProgressBar::SetTextColor(COLORREF TextColor)
{
	m_TextColor = TextColor;
}

void CXProgressBar::SetWindowText(LPCSTR lpszString)
{
	strWndText = lpszString;
	Invalidate();
}

void CXProgressBar::SetFont(LOGFONT &logFont)
{
	hFont = ::CreateFontIndirect (&logFont);
}

void CXProgressBar::Step()
{
	StepIt();
//	Invalidate();
}
