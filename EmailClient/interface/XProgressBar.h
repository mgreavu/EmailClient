#if !defined(AFX_XPROGRESSBAR_H__188C01EF_1C88_41E8_A9F5_9A7CAA4A85FD__INCLUDED_)
#define AFX_XPROGRESSBAR_H__188C01EF_1C88_41E8_A9F5_9A7CAA4A85FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CXProgressBar window

class CXProgressBar : public CProgressCtrl
{
// Construction
public:
	CXProgressBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXProgressBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	void Step();
	void SetFont (LOGFONT& logFont);
	void SetWindowText(LPCSTR lpszString);
	void SetTextColor (COLORREF TextColor);
	virtual ~CXProgressBar();

	// Generated message map functions
protected:
	HFONT hFont;
	//{{AFX_MSG(CXProgressBar)
	afx_msg void OnPaint();
	//}}AFX_MSG
	CString strWndText;
	COLORREF m_TextColor;
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XPROGRESSBAR_H__188C01EF_1C88_41E8_A9F5_9A7CAA4A85FD__INCLUDED_)
