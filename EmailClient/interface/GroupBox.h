#if !defined(AFX_GROUPBOX_H__443A47F3_6773_4147_A0D1_3C5476178ADF__INCLUDED_)
#define AFX_GROUPBOX_H__443A47F3_6773_4147_A0D1_3C5476178ADF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGroupBox window

class CGroupBox : public CButton
{
// Construction
public:
	CGroupBox();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupBox)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
private:
	CFont TextFont;
	void SetDefaultFont();
public:
	CString m_strText;
	void SetWndTitle(LPSTR sTitle);
	void SetActiveFont(BOOL bActive);
	void SetGroupBoxPos(CWnd* pInsertAfter, int x, int y, int cx, int cy, UINT uFlags);
	void InvalidateGroupBox(BOOL bErase);
	void SetGroupBoxRegion();
	void ActivateGroupBox(BOOL bActivate);
	virtual ~CGroupBox();

	BOOL m_Active;
	COLORREF m_ActiveColor;
	COLORREF m_InactiveColor;
	COLORREF m_InactiveShadow;
	COLORREF m_ActiveShadow;
	COLORREF m_InactiveTextColor;
	COLORREF m_ActiveTextColor;
	DWORD m_TextStartPoint;
	BOOL m_ActiveFontItalic;
	LOGFONT m_LogFont;

	// Generated message map functions
protected:
	//{{AFX_MSG(CGroupBox)
	afx_msg void OnPaint();
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG
	afx_msg LRESULT OnSetText(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPBOX_H__443A47F3_6773_4147_A0D1_3C5476178ADF__INCLUDED_)
