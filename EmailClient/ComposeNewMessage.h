#if !defined(AFX_COMPOSENEWMESSAGE_H__E61B1925_EA67_4CA7_AE2B_29953FB85639__INCLUDED_)
#define AFX_COMPOSENEWMESSAGE_H__E61B1925_EA67_4CA7_AE2B_29953FB85639__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ComposeNewMessage.h : header file
//

#include "Globals.h"
#include "interface/SXButton.h"
#include "EmailClientDlg.h"
/////////////////////////////////////////////////////////////////////////////
// CComposeNewMessage dialog

class CComposeNewMessage : public CDialog
{
// Construction
public:
	CString m_sCrtSelFile;
	void OwnerDrawButtons();
	int m_iSelected;
	CString MsgDestination;
	CComposeNewMessage(CWnd* pParent = NULL);   // standard constructor
	CComposeNewMessage(CWnd* pParent, BYTE ComposeType);

// Dialog Data
	//{{AFX_DATA(CComposeNewMessage)
	enum { IDD = IDD_COMPOSENEWMESSAGE_DIALOG };
	CSXButton	m_btnAddFromContacts;
	CEdit	m_MailBodyEdit;
	CComboBox	m_ComboEmails;
	CString	m_MailBody;
	CString	m_sTo;
	BOOL	m_bSendEncrypted;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComposeNewMessage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL WriteMsgToOutbox (CString FPath, CString Message);

	CSXButton m_btnRemove;
	CSXButton m_btnAttach;
	CSXButton m_btnSaveDraft;
	CSXButton m_btnSend;
	CSXButton m_btnCancel;
	// Generated message map functions
	//{{AFX_MSG(CComposeNewMessage)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnAttach();
	afx_msg void OnSelchangeFromAcc();
	afx_msg void OnChangeSubject();
	afx_msg void OnSavedraft();
	afx_msg void OnRemove();
	afx_msg void OnAddfromcontacts();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BYTE nComposeType;
	CString FileName;
	CString sFileList;
	CStringList AttachmentsPaths;
	CEmailClientDlg* parentWindow;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPOSENEWMESSAGE_H__E61B1925_EA67_4CA7_AE2B_29953FB85639__INCLUDED_)
