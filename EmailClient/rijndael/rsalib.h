//#include "stdafx.h"
//RSALib definitions

#ifndef _RSALIB_H
#define _RSALIB_H

//#define _WIN32_WINNT 0x0400

#include "windows.h"

#define VC_EXTRALEAN

/* ------------------------
|    RSA definitions      |
-------------------------*/

// POINTER defines a generic pointer type
typedef unsigned char *POINTER;

// UINT16 defines a two byte word
typedef unsigned __int16 RSAUINT16;

// UINT32 defines a four byte word 
typedef unsigned __int32 RSAUINT32;

// General
#define RANDOM_BYTES_NEEDED 256

// The RANDOM_BYTES_NEEDED must be a multiple of RND_SEED_SIZE
// max(RND_SEED_SIZE) <= 16
#define RND_SEED_SIZE 8
#define SYSTEMTIME_SIZE sizeof(SYSTEMTIME)

//RSA key lengths.
#define MIN_RSA_MODULUS_BITS 508
#define MAX_RSA_MODULUS_BITS 1024
#define MAX_RSA_MODULUS_LEN ((MAX_RSA_MODULUS_BITS + 7) / 8)
#define MAX_RSA_PRIME_BITS ((MAX_RSA_MODULUS_BITS + 1) / 2)
#define MAX_RSA_PRIME_LEN ((MAX_RSA_PRIME_BITS + 7) / 8)

//Success
#define RE_SUCCESS 0x410
//Error codes.
#define RE_CONTENT_ENCODING 0x0400
#define RE_DATA 0x0401
#define RE_DIGEST_ALGORITHM 0x0402
#define RE_ENCODING 0x0403
#define RE_KEY 0x0404
#define RE_KEY_ENCODING 0x0405
#define RE_LEN 0x0406
#define RE_MODULUS_LEN 0x0407
#define RE_NEED_RANDOM 0x0408
#define RE_PRIVATE_KEY 0x0409
#define RE_PUBLIC_KEY 0x040a
#define RE_SIGNATURE 0x040b
#define RE_SIGNATURE_ENCODING 0x040c

//CryptoAPI error codes
#define RE_ACQUIRE_CONTEXT 0x040d
#define RE_RELEASE_CONTEXT 0x040e
#define RE_GEN_RANDOM 0x0410
#define RE_BAD_SEED_SIZE 0x040f

//Random structure.
typedef struct {
  unsigned int bytesNeeded;
  unsigned char state[16];
  unsigned int outputAvailable;
  unsigned char output[16];
} R_RANDOM_STRUCT;

//RSA public and private key.
typedef struct {
  unsigned int bits;                           /* length in bits of modulus */
  unsigned char modulus[MAX_RSA_MODULUS_LEN];                    /* modulus */
  unsigned char exponent[MAX_RSA_MODULUS_LEN];           /* public exponent */
} R_RSA_PUBLIC_KEY;

typedef struct {
  unsigned int bits;                           /* length in bits of modulus */
  unsigned char modulus[MAX_RSA_MODULUS_LEN];                    /* modulus */
  unsigned char publicExponent[MAX_RSA_MODULUS_LEN];     /* public exponent */
  unsigned char exponent[MAX_RSA_MODULUS_LEN];          /* private exponent */
  unsigned char prime[2][MAX_RSA_PRIME_LEN];               /* prime factors */
  unsigned char primeExponent[2][MAX_RSA_PRIME_LEN];   /* exponents for CRT */
  unsigned char coefficient[MAX_RSA_PRIME_LEN];          /* CRT coefficient */
} R_RSA_PRIVATE_KEY;

//RSA prototype key.
typedef struct {
  unsigned int bits;                           /* length in bits of modulus */
  int useFermat4;                        /* public exponent (1 = F4, 0 = 3) */
} R_RSA_PROTO_KEY;

/* ----------------------------------
|   Rinjdael (AES) definitions      |
-----------------------------------*/

// types definitions
typedef unsigned __int32 AESUINT32;

// errors definitions
#define G10ERR_SELFTEST_FAILED -1
#define G10ERR_WRONG_KEYLEN -2

#define MAXKC			(256/32)
#define MAXROUNDS		14

typedef struct {
    int   ROUNDS;                   /* key-length-dependent number of rounds */
    int decryption_prepared;
    byte  keySched[MAXROUNDS+1][4][4];	/* key schedule		*/
    byte  keySched2[MAXROUNDS+1][4][4];	/* key schedule		*/
} RIJNDAEL_context;

/* -----------------------
|   RSA routines         |
------------------------*/

int RSAPublicEncrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                      R_RSA_PUBLIC_KEY *);
int RSAPrivateEncrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                      R_RSA_PRIVATE_KEY *);
int RSAPublicDecrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                      R_RSA_PUBLIC_KEY *);
int RSAPrivateDecrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                      R_RSA_PRIVATE_KEY *);
int RSAGenerateKeys (R_RSA_PUBLIC_KEY *, R_RSA_PRIVATE_KEY *, unsigned int);

/* ----------------------------------
|   Rinjdael (AES) routines         |
-----------------------------------*/

int rijndael_setkey (RIJNDAEL_context *, const byte *, const unsigned);
void rijndael_encrypt (const RIJNDAEL_context *, byte *, const byte *);
void rijndael_decrypt (RIJNDAEL_context *, byte *, const byte *);
const char *selftest(void);

/* -----------------------
|   TSF definitions      |
------------------------*/

// Key lengths
#define RSA_KEY_LENGTH 1024 // bits
#define AES_KEY_LENGTH 32 // specified in bytes (32 bytes = 256 bits)

// Error Messages
#define TSF_BAD_BLOCKSIZE 0x100     // AES bad block size (must by a multiple of 16)

/* -----------------------
|   TSF routines         |
------------------------*/

/*  !!! WARNING !!!

  The following routines are can be used only with those conditions:

    - The Rijndael (AES) key length set to 256 bit
    - The RSA key length set to 1024 bit

  For others key lengths the results are unpredictable

*/

// param1 : output : buffer - 32 bytes
int GenerateAESKey(byte *);

// param1 : output : public key
// param2 : output : private key
int GenerateRSAKeys (R_RSA_PUBLIC_KEY *, R_RSA_PRIVATE_KEY *);

// param1 : input  : public key
// param2 : output : buffer - 128 bytes (crypted AES key)
// param3 : input  : buffer - 32 bytes (clear AES key)
int RSACryptAESKey(R_RSA_PUBLIC_KEY *, byte *, const byte *);

// param1 : input  : private key
// param2 : output : buffer - 32 bytes (clear AES key)
// param3 : input  : buffer - 128 bytes (crypted AES key)
int RSADecryptAESKey(R_RSA_PRIVATE_KEY *, byte *, const byte *);

// param1 : input  : AES key - 32 bytes
// param2 : output : buffer - 16 bytes boundaries - crypted block
// param3 : input  : buffer - 16 bytes boundaries - clear block
// param4 : input  : block size (16 bytes boundaries)
int AESCrypt(const byte *, byte *, const byte *, unsigned long);

// param1 : input  : AES key - 32 bytes
// param2 : output : buffer - 16 bytes boundaries - clear block
// param3 : input  : buffer - 16 bytes boundaries - crypted block
// param4 : input  : block size (16 bytes boundaries)
int AESDecrypt(const byte *, byte *, const byte *, unsigned long);

#endif

