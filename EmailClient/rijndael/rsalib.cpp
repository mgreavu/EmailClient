#include "stdafx.h"
#include "rsalib.h"
#include "rsa.h"
#include "rijndael.h"
#include <wincrypt.h>

int GenerateAESKey(byte *AESKey)
{
  HCRYPTPROV lHCryptprov;
  SYSTEMTIME systime;

  if (!CryptAcquireContext(&lHCryptprov, NULL, MS_DEF_PROV, PROV_RSA_FULL, 0))
  {
    	if (!CryptAcquireContext(&lHCryptprov, NULL, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET))
			return RE_ACQUIRE_CONTEXT;
  }
	
  GetSystemTime(&systime);
  
  memmove(AESKey, (char*) &systime, SYSTEMTIME_SIZE);
  
  if (!CryptGenRandom(lHCryptprov, AES_KEY_LENGTH, (unsigned char*) AESKey))
    return RE_GEN_RANDOM;

  return RE_SUCCESS;
}

int GenerateRSAKeys(R_RSA_PUBLIC_KEY *publicKey, R_RSA_PRIVATE_KEY *privateKey)
{
  return RSAGenerateKeys(publicKey, privateKey, RSA_KEY_LENGTH);
}

int RSACryptAESKey(R_RSA_PUBLIC_KEY *pubKey, byte *AESKeyOutput, const byte *AESKeyInput)
{
  unsigned int OutputSize;
  return RSAPublicEncrypt((unsigned char *) AESKeyOutput, &OutputSize,
                          (unsigned char *) AESKeyInput, AES_KEY_LENGTH, pubKey);

}

int RSADecryptAESKey(R_RSA_PRIVATE_KEY *privKey, byte *AESKeyOutput, const byte *AESKeyInput)
{
  unsigned int OutputSize;
  return RSAPrivateDecrypt((unsigned char *) AESKeyOutput, &OutputSize, 
                           (unsigned char *) AESKeyInput, RSA_KEY_LENGTH/8, privKey);

}

int AESCrypt(const byte *AESKey, byte *output, const byte *input, unsigned long blocksize)
{
  int i,iterations;
  RIJNDAEL_context rijc;

  if (blocksize%16!=0) return TSF_BAD_BLOCKSIZE;

  iterations = blocksize / 16;

  rijndael_setkey(&rijc, AESKey , AES_KEY_LENGTH);
  
  for (i=0;i<iterations;i++)
    rijndael_encrypt(&rijc, output + i*16, input + i*16);

  return RE_SUCCESS;
}

int AESDecrypt(const byte *AESKey, byte *output, const byte *input, unsigned long blocksize)
{
  int i,iterations;
  RIJNDAEL_context rijc;

  if (blocksize%16!=0) return TSF_BAD_BLOCKSIZE;

  iterations = blocksize / 16;

  rijndael_setkey(&rijc, AESKey , AES_KEY_LENGTH);
  
  for (i=0;i<iterations;i++)
    rijndael_decrypt(&rijc, output + i*16, input + i*16);

  return RE_SUCCESS;
}
