#include "stdafx.h"
#include "rsalib.h"
#include "r_stdlib.h"
#include <string.h>

void R_memset (POINTER output, int value, unsigned int len)
{
  if (len)
    memset (output, value, len);
}

void R_memcpy (POINTER output, POINTER input, unsigned int len)
{
  if (len)
    memcpy (output, input, len);
}

int R_memcmp (POINTER firstBlock, POINTER secondBlock, unsigned int len)
{
  if (len)
    return (memcmp (firstBlock, secondBlock, len));
  else
    return (0);
}
