#include "stdafx.h"
#include "rsalib.h"
#include "wincrypt.h"
#include "string.h"
#include "r_random.h"
#include "md5.h"
#include "r_stdlib.h"

int R_RandomInit (R_RANDOM_STRUCT *randomStruct)
{
  randomStruct->bytesNeeded = RANDOM_BYTES_NEEDED;
  R_memset ((POINTER)randomStruct->state, 0, sizeof (randomStruct->state));
  randomStruct->outputAvailable = 0;
  
  return (0);
}

int InitRandomStruct (R_RANDOM_STRUCT *randomStruct)
{
  unsigned int bytesNeeded;
  unsigned char seedbytes[RND_SEED_SIZE];

  HCRYPTPROV lHCryptprov;
  SYSTEMTIME systime;

  R_RandomInit (randomStruct);

	if (!CryptAcquireContext(&lHCryptprov, NULL, MS_DEF_PROV, PROV_RSA_FULL, 0))
    return RE_ACQUIRE_CONTEXT;
	
  GetSystemTime(&systime);
  
  if ((RND_SEED_SIZE > SYSTEMTIME_SIZE) || (RANDOM_BYTES_NEEDED%RND_SEED_SIZE!=0))
    return RE_BAD_SEED_SIZE;
  
  memmove((char*) seedbytes, (char*) (&systime + (SYSTEMTIME_SIZE - RND_SEED_SIZE)), RND_SEED_SIZE);
  
  if (!CryptGenRandom(lHCryptprov, RND_SEED_SIZE, (unsigned char*)seedbytes))
    return RE_GEN_RANDOM;
    
  if (!CryptReleaseContext(lHCryptprov, 0))
    return RE_RELEASE_CONTEXT;

  while (1) {
    R_GetRandomBytesNeeded (&bytesNeeded, randomStruct);
    if (bytesNeeded == 0)
      break;
    
    R_RandomUpdate (randomStruct, (unsigned char *) seedbytes, RND_SEED_SIZE);
  }

  return RE_SUCCESS;
}

int R_RandomUpdate (R_RANDOM_STRUCT *randomStruct, unsigned char *block, unsigned int blockLen)
{
  MD5_CTX context;
  unsigned char digest[16];
  unsigned int i, x;
  
  MD5Init (&context);
  MD5Update (&context, block, blockLen);
  MD5Final (digest, &context);

  /* add digest to state */
  x = 0;
  for (i = 0; i < 16; i++) {
    x += randomStruct->state[15-i] + digest[15-i];
    randomStruct->state[15-i] = (unsigned char)x;
    x >>= 8;
  }
  
  if (randomStruct->bytesNeeded < blockLen)
    randomStruct->bytesNeeded = 0;
  else
    randomStruct->bytesNeeded -= blockLen;
  
  /* Zeroize sensitive information.
   */
  R_memset ((POINTER)digest, 0, sizeof (digest));
  x = 0;
  
  return (0);
}

int R_GetRandomBytesNeeded (unsigned int *bytesNeeded, R_RANDOM_STRUCT *randomStruct)
{
  *bytesNeeded = randomStruct->bytesNeeded;
  
  return (0);
}

int R_GenerateBytes (unsigned char *block, unsigned int blockLen, R_RANDOM_STRUCT *randomStruct)
{
  MD5_CTX context;
  unsigned int available, i;
  
  if (randomStruct->bytesNeeded)
    return (RE_NEED_RANDOM);
  
  available = randomStruct->outputAvailable;
  
  while (blockLen > available) {
    R_memcpy
      ((POINTER)block, (POINTER)&randomStruct->output[16-available],
       available);
    block += available;
    blockLen -= available;

    /* generate new output */
    MD5Init (&context);
    MD5Update (&context, randomStruct->state, 16);
    MD5Final (randomStruct->output, &context);
    available = 16;

    /* increment state */
    for (i = 0; i < 16; i++)
      if (randomStruct->state[15-i]++)
        break;
  }

  R_memcpy ((POINTER)block, (POINTER)&randomStruct->output[16-available], blockLen);
  randomStruct->outputAvailable = available - blockLen;

  return (0);
}

void R_RandomFinal (R_RANDOM_STRUCT *randomStruct)
{
  R_memset ((POINTER)randomStruct, 0, sizeof (*randomStruct));
}
