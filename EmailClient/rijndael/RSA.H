//#include "stdafx.h"
/* RSA.H - header file for RSA.C
 */

/* Copyright (C) 1991-2 RSA Laboratories, a division of RSA Data
   Security, Inc. All rights reserved.
 */

#ifndef _RSA_H__
#define _RSA_H__

int RSAPublicEncrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                      R_RSA_PUBLIC_KEY *);
int RSAPrivateEncrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                       R_RSA_PRIVATE_KEY *);
int RSAPublicDecrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                      R_RSA_PUBLIC_KEY *);
int RSAPrivateDecrypt (unsigned char *, unsigned int *, unsigned char *, unsigned int,
                      R_RSA_PRIVATE_KEY *);

#endif