/* Rijndael (AES) for GnuPG
 *	Copyright (C) 2000, 2001 Free Software Foundation, Inc.
 *
 * This file is part of GnuPG.
 *
 * GnuPG is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GnuPG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 *******************************************************************
 * The code here is based on the optimized implementation taken from
 * http://www.esat.kuleuven.ac.be/~rijmen/rijndael/ on Oct 2, 2000,
 * which carries this notice:
 *------------------------------------------
 * rijndael-alg-fst.c   v2.3   April '2000
 *
 * Optimised ANSI C code
 *
 * authors: v1.0: Antoon Bosselaers
 *          v2.0: Vincent Rijmen
 *          v2.3: Paulo Barreto
 *
 * This code is placed in the public domain.
 *------------------------------------------
 */
#include "stdafx.h"
#include "rijndael.h"

void burn_stack (int bytes)
{
    char buf[64];
    
    memset (buf, 0, sizeof buf);
    bytes -= sizeof buf;
    if (bytes > 0)
        burn_stack (bytes);
}


/* Perform the key setup.
 */  
int do_setkey (RIJNDAEL_context *ctx, const byte *key, const unsigned keylen)
{
    static int initialized = 0;
    static const char *selftest_failed=0;
    int ROUNDS;
    byte k[MAXKC][4];
    int j, r, t, rconpointer = 0;
    unsigned int i;
    byte tk[MAXKC][4];
    int KC;
    /* space for automatic variables is about 64 + 11*int */

    if (!initialized) {
        initialized = 1;
        selftest_failed = selftest ();
        if( selftest_failed )
            fprintf(stderr, "%s\n", selftest_failed );
    }
    if( selftest_failed )
        return G10ERR_SELFTEST_FAILED;

    if( keylen == 128/8 ) {
        ROUNDS = 10;
        KC = 4;
    }
    else if ( keylen == 192/8 ) {
        ROUNDS = 12;
        KC = 6;
    }
    else if ( keylen == 256/8 ) {
        ROUNDS = 14;
        KC = 8;
    }
    else
	    return G10ERR_WRONG_KEYLEN;

    ctx->ROUNDS = ROUNDS;
    ctx->decryption_prepared = 0;

    for (i = 0; i < keylen; i++) {
        k[i >> 2][i & 3] = key[i]; 
    }
 #define W (ctx->keySched)

    for (j = KC-1; j >= 0; j--) {
        *((AESUINT32*)tk[j]) = *((AESUINT32*)k[j]);
    }
    r = 0;
    t = 0;
    /* copy values into round key array */
    for (j = 0; (j < KC) && (r < ROUNDS + 1); ) {
        for (; (j < KC) && (t < 4); j++, t++) {
            *((AESUINT32*)W[r][t]) = *((AESUINT32*)tk[j]);
        }
        if (t == 4) {
            r++;
            t = 0;
        }
    }
		
    while (r < ROUNDS + 1) {
        /* while not enough round key material calculated */
        /* calculate new values */
        tk[0][0] ^= S[tk[KC-1][1]];
        tk[0][1] ^= S[tk[KC-1][2]];
        tk[0][2] ^= S[tk[KC-1][3]];
        tk[0][3] ^= S[tk[KC-1][0]];
        tk[0][0] ^= rcon[rconpointer++];
        
        if (KC != 8) {
            for (j = 1; j < KC; j++) {
                *((AESUINT32*)tk[j]) ^= *((AESUINT32*)tk[j-1]);
            }
        } else {
            for (j = 1; j < KC/2; j++) {
                *((AESUINT32*)tk[j]) ^= *((AESUINT32*)tk[j-1]);
            }
            tk[KC/2][0] ^= S[tk[KC/2 - 1][0]];
            tk[KC/2][1] ^= S[tk[KC/2 - 1][1]];
            tk[KC/2][2] ^= S[tk[KC/2 - 1][2]];
            tk[KC/2][3] ^= S[tk[KC/2 - 1][3]];
            for (j = KC/2 + 1; j < KC; j++) {
                *((AESUINT32*)tk[j]) ^= *((AESUINT32*)tk[j-1]);
            }
        }
        /* copy values into round key array */
        for (j = 0; (j < KC) && (r < ROUNDS + 1); ) {
            for (; (j < KC) && (t < 4); j++, t++) {
                *((AESUINT32*)W[r][t]) = *((AESUINT32*)tk[j]);
            }
            if (t == 4) {
                r++;
                t = 0;
            }
        }
    }		
    
  #undef W    
    return 0;
}

int rijndael_setkey (RIJNDAEL_context *ctx, const byte *key, const unsigned keylen)
{
    int rc = do_setkey (ctx, key, keylen);
    burn_stack ( 100 + 16*sizeof(int));
    return rc;
}

/* make a decryption  key from an encryption key */
void prepare_decryption( RIJNDAEL_context *ctx )
{
    int r;
    byte *w;

    for (r=0; r < MAXROUNDS+1; r++ ) {
        *((AESUINT32*)ctx->keySched2[r][0]) = *((AESUINT32*)ctx->keySched[r][0]);
        *((AESUINT32*)ctx->keySched2[r][1]) = *((AESUINT32*)ctx->keySched[r][1]);
        *((AESUINT32*)ctx->keySched2[r][2]) = *((AESUINT32*)ctx->keySched[r][2]);
        *((AESUINT32*)ctx->keySched2[r][3]) = *((AESUINT32*)ctx->keySched[r][3]);
    }
  #define W (ctx->keySched2)
    for (r = 1; r < ctx->ROUNDS; r++) {
        w = W[r][0];
        *((AESUINT32*)w) = *((AESUINT32*)U1[w[0]]) ^ *((AESUINT32*)U2[w[1]])
                   ^ *((AESUINT32*)U3[w[2]]) ^ *((AESUINT32*)U4[w[3]]);
       
        w = W[r][1];
        *((AESUINT32*)w) = *((AESUINT32*)U1[w[0]]) ^ *((AESUINT32*)U2[w[1]])
                   ^ *((AESUINT32*)U3[w[2]]) ^ *((AESUINT32*)U4[w[3]]);
        
        w = W[r][2];
        *((AESUINT32*)w) = *((AESUINT32*)U1[w[0]]) ^ *((AESUINT32*)U2[w[1]])
                   ^ *((AESUINT32*)U3[w[2]]) ^ *((AESUINT32*)U4[w[3]]);
        
        w = W[r][3];
        *((AESUINT32*)w) = *((AESUINT32*)U1[w[0]]) ^ *((AESUINT32*)U2[w[1]])
                   ^ *((AESUINT32*)U3[w[2]]) ^ *((AESUINT32*)U4[w[3]]);
    }
  #undef W
}	


/* Encrypt one block.  A and B may be the same. */
void do_encrypt (const RIJNDAEL_context *ctx, byte *b, const byte *a)
{
    int r;
    byte temp[4][4];
    int ROUNDS = ctx->ROUNDS;
  #define rk (ctx->keySched)

    *((AESUINT32*)temp[0]) = *((AESUINT32*)(a   )) ^ *((AESUINT32*)rk[0][0]);
    *((AESUINT32*)temp[1]) = *((AESUINT32*)(a+ 4)) ^ *((AESUINT32*)rk[0][1]);
    *((AESUINT32*)temp[2]) = *((AESUINT32*)(a+ 8)) ^ *((AESUINT32*)rk[0][2]);
    *((AESUINT32*)temp[3]) = *((AESUINT32*)(a+12)) ^ *((AESUINT32*)rk[0][3]);
    *((AESUINT32*)(b    )) = *((AESUINT32*)T1[temp[0][0]])
        ^ *((AESUINT32*)T2[temp[1][1]])
        ^ *((AESUINT32*)T3[temp[2][2]]) 
        ^ *((AESUINT32*)T4[temp[3][3]]);
    *((AESUINT32*)(b + 4)) = *((AESUINT32*)T1[temp[1][0]])
        ^ *((AESUINT32*)T2[temp[2][1]])
        ^ *((AESUINT32*)T3[temp[3][2]]) 
        ^ *((AESUINT32*)T4[temp[0][3]]);
    *((AESUINT32*)(b + 8)) = *((AESUINT32*)T1[temp[2][0]])
        ^ *((AESUINT32*)T2[temp[3][1]])
        ^ *((AESUINT32*)T3[temp[0][2]]) 
        ^ *((AESUINT32*)T4[temp[1][3]]);
    *((AESUINT32*)(b +12)) = *((AESUINT32*)T1[temp[3][0]])
        ^ *((AESUINT32*)T2[temp[0][1]])
        ^ *((AESUINT32*)T3[temp[1][2]]) 
        ^ *((AESUINT32*)T4[temp[2][3]]);
    for (r = 1; r < ROUNDS-1; r++) {
        *((AESUINT32*)temp[0]) = *((AESUINT32*)(b   )) ^ *((AESUINT32*)rk[r][0]);
        *((AESUINT32*)temp[1]) = *((AESUINT32*)(b+ 4)) ^ *((AESUINT32*)rk[r][1]);
        *((AESUINT32*)temp[2]) = *((AESUINT32*)(b+ 8)) ^ *((AESUINT32*)rk[r][2]);
        *((AESUINT32*)temp[3]) = *((AESUINT32*)(b+12)) ^ *((AESUINT32*)rk[r][3]);

        *((AESUINT32*)(b    )) = *((AESUINT32*)T1[temp[0][0]])
            ^ *((AESUINT32*)T2[temp[1][1]])
            ^ *((AESUINT32*)T3[temp[2][2]]) 
            ^ *((AESUINT32*)T4[temp[3][3]]);
        *((AESUINT32*)(b + 4)) = *((AESUINT32*)T1[temp[1][0]])
            ^ *((AESUINT32*)T2[temp[2][1]])
            ^ *((AESUINT32*)T3[temp[3][2]]) 
            ^ *((AESUINT32*)T4[temp[0][3]]);
        *((AESUINT32*)(b + 8)) = *((AESUINT32*)T1[temp[2][0]])
            ^ *((AESUINT32*)T2[temp[3][1]])
            ^ *((AESUINT32*)T3[temp[0][2]]) 
            ^ *((AESUINT32*)T4[temp[1][3]]);
        *((AESUINT32*)(b +12)) = *((AESUINT32*)T1[temp[3][0]])
            ^ *((AESUINT32*)T2[temp[0][1]])
            ^ *((AESUINT32*)T3[temp[1][2]]) 
            ^ *((AESUINT32*)T4[temp[2][3]]);
    }
    /* last round is special */   
    *((AESUINT32*)temp[0]) = *((AESUINT32*)(b   )) ^ *((AESUINT32*)rk[ROUNDS-1][0]);
    *((AESUINT32*)temp[1]) = *((AESUINT32*)(b+ 4)) ^ *((AESUINT32*)rk[ROUNDS-1][1]);
    *((AESUINT32*)temp[2]) = *((AESUINT32*)(b+ 8)) ^ *((AESUINT32*)rk[ROUNDS-1][2]);
    *((AESUINT32*)temp[3]) = *((AESUINT32*)(b+12)) ^ *((AESUINT32*)rk[ROUNDS-1][3]);
    b[ 0] = T1[temp[0][0]][1];
    b[ 1] = T1[temp[1][1]][1];
    b[ 2] = T1[temp[2][2]][1];
    b[ 3] = T1[temp[3][3]][1];
    b[ 4] = T1[temp[1][0]][1];
    b[ 5] = T1[temp[2][1]][1];
    b[ 6] = T1[temp[3][2]][1];
    b[ 7] = T1[temp[0][3]][1];
    b[ 8] = T1[temp[2][0]][1];
    b[ 9] = T1[temp[3][1]][1];
    b[10] = T1[temp[0][2]][1];
    b[11] = T1[temp[1][3]][1];
    b[12] = T1[temp[3][0]][1];
    b[13] = T1[temp[0][1]][1];
    b[14] = T1[temp[1][2]][1];
    b[15] = T1[temp[2][3]][1];
    *((AESUINT32*)(b   )) ^= *((AESUINT32*)rk[ROUNDS][0]);
    *((AESUINT32*)(b+ 4)) ^= *((AESUINT32*)rk[ROUNDS][1]);
    *((AESUINT32*)(b+ 8)) ^= *((AESUINT32*)rk[ROUNDS][2]);
    *((AESUINT32*)(b+12)) ^= *((AESUINT32*)rk[ROUNDS][3]);
  #undef rk
}

void rijndael_encrypt (const RIJNDAEL_context *ctx, byte *b, const byte *a)
{
    do_encrypt (ctx, b, a);
    burn_stack (16 + 2*sizeof(int));
}

/* Decrypt one block.  a and b may be the same. */
void do_decrypt (RIJNDAEL_context *ctx, byte *b, const byte *a)
{
  #define rk  (ctx->keySched2)
    int ROUNDS = ctx->ROUNDS; 
    int r;
    byte temp[4][4];

    if ( !ctx->decryption_prepared ) {
        prepare_decryption ( ctx );
        burn_stack (64);
        ctx->decryption_prepared = 1;
    }
    
    *((AESUINT32*)temp[0]) = *((AESUINT32*)(a   )) ^ *((AESUINT32*)rk[ROUNDS][0]);
    *((AESUINT32*)temp[1]) = *((AESUINT32*)(a+ 4)) ^ *((AESUINT32*)rk[ROUNDS][1]);
    *((AESUINT32*)temp[2]) = *((AESUINT32*)(a+ 8)) ^ *((AESUINT32*)rk[ROUNDS][2]);
    *((AESUINT32*)temp[3]) = *((AESUINT32*)(a+12)) ^ *((AESUINT32*)rk[ROUNDS][3]);
  
    *((AESUINT32*)(b   )) = *((AESUINT32*)T5[temp[0][0]])
        ^ *((AESUINT32*)T6[temp[3][1]])
        ^ *((AESUINT32*)T7[temp[2][2]]) 
        ^ *((AESUINT32*)T8[temp[1][3]]);
    *((AESUINT32*)(b+ 4)) = *((AESUINT32*)T5[temp[1][0]])
        ^ *((AESUINT32*)T6[temp[0][1]])
        ^ *((AESUINT32*)T7[temp[3][2]]) 
        ^ *((AESUINT32*)T8[temp[2][3]]);
    *((AESUINT32*)(b+ 8)) = *((AESUINT32*)T5[temp[2][0]])
        ^ *((AESUINT32*)T6[temp[1][1]])
        ^ *((AESUINT32*)T7[temp[0][2]]) 
        ^ *((AESUINT32*)T8[temp[3][3]]);
    *((AESUINT32*)(b+12)) = *((AESUINT32*)T5[temp[3][0]])
        ^ *((AESUINT32*)T6[temp[2][1]])
        ^ *((AESUINT32*)T7[temp[1][2]]) 
        ^ *((AESUINT32*)T8[temp[0][3]]);
    for (r = ROUNDS-1; r > 1; r--) {
		*((AESUINT32*)temp[0]) = *((AESUINT32*)(b   )) ^ *((AESUINT32*)rk[r][0]);
		*((AESUINT32*)temp[1]) = *((AESUINT32*)(b+ 4)) ^ *((AESUINT32*)rk[r][1]);
		*((AESUINT32*)temp[2]) = *((AESUINT32*)(b+ 8)) ^ *((AESUINT32*)rk[r][2]);
		*((AESUINT32*)temp[3]) = *((AESUINT32*)(b+12)) ^ *((AESUINT32*)rk[r][3]);
		*((AESUINT32*)(b   )) = *((AESUINT32*)T5[temp[0][0]])
           ^ *((AESUINT32*)T6[temp[3][1]])
           ^ *((AESUINT32*)T7[temp[2][2]]) 
           ^ *((AESUINT32*)T8[temp[1][3]]);
		*((AESUINT32*)(b+ 4)) = *((AESUINT32*)T5[temp[1][0]])
           ^ *((AESUINT32*)T6[temp[0][1]])
           ^ *((AESUINT32*)T7[temp[3][2]]) 
           ^ *((AESUINT32*)T8[temp[2][3]]);
		*((AESUINT32*)(b+ 8)) = *((AESUINT32*)T5[temp[2][0]])
           ^ *((AESUINT32*)T6[temp[1][1]])
           ^ *((AESUINT32*)T7[temp[0][2]]) 
           ^ *((AESUINT32*)T8[temp[3][3]]);
		*((AESUINT32*)(b+12)) = *((AESUINT32*)T5[temp[3][0]])
           ^ *((AESUINT32*)T6[temp[2][1]])
           ^ *((AESUINT32*)T7[temp[1][2]]) 
           ^ *((AESUINT32*)T8[temp[0][3]]);
	}
	/* last round is special */   
	*((AESUINT32*)temp[0]) = *((AESUINT32*)(b   )) ^ *((AESUINT32*)rk[1][0]);
	*((AESUINT32*)temp[1]) = *((AESUINT32*)(b+ 4)) ^ *((AESUINT32*)rk[1][1]);
	*((AESUINT32*)temp[2]) = *((AESUINT32*)(b+ 8)) ^ *((AESUINT32*)rk[1][2]);
	*((AESUINT32*)temp[3]) = *((AESUINT32*)(b+12)) ^ *((AESUINT32*)rk[1][3]);
	b[ 0] = S5[temp[0][0]];
	b[ 1] = S5[temp[3][1]];
	b[ 2] = S5[temp[2][2]];
	b[ 3] = S5[temp[1][3]];
	b[ 4] = S5[temp[1][0]];
	b[ 5] = S5[temp[0][1]];
	b[ 6] = S5[temp[3][2]];
	b[ 7] = S5[temp[2][3]];
	b[ 8] = S5[temp[2][0]];
	b[ 9] = S5[temp[1][1]];
	b[10] = S5[temp[0][2]];
	b[11] = S5[temp[3][3]];
	b[12] = S5[temp[3][0]];
	b[13] = S5[temp[2][1]];
	b[14] = S5[temp[1][2]];
	b[15] = S5[temp[0][3]];
	*((AESUINT32*)(b   )) ^= *((AESUINT32*)rk[0][0]);
	*((AESUINT32*)(b+ 4)) ^= *((AESUINT32*)rk[0][1]);
	*((AESUINT32*)(b+ 8)) ^= *((AESUINT32*)rk[0][2]);
	*((AESUINT32*)(b+12)) ^= *((AESUINT32*)rk[0][3]);
  #undef rk
}

void rijndael_decrypt (RIJNDAEL_context *ctx, byte *b, const byte *a)
{
    do_decrypt (ctx, b, a);
    burn_stack (16+2*sizeof(int));
}

/* Test a single encryption and decryption with each key size. */
const char *selftest(void)
{
    RIJNDAEL_context ctx;
    byte scratch[16];	   

    /* The test vectors are from the AES supplied ones; more or less 
     * randomly taken from ecb_tbl.txt (I=42,81,14)
     */
    static const byte plaintext[16] = {
       0x01,0x4B,0xAF,0x22,0x78,0xA6,0x9D,0x33,
       0x1D,0x51,0x80,0x10,0x36,0x43,0xE9,0x9A
    };
    static const byte key[16] = {
        0xE8,0xE9,0xEA,0xEB,0xED,0xEE,0xEF,0xF0,
        0xF2,0xF3,0xF4,0xF5,0xF7,0xF8,0xF9,0xFA
    };
    static const byte ciphertext[16] = {
        0x67,0x43,0xC3,0xD1,0x51,0x9A,0xB4,0xF2,
        0xCD,0x9A,0x78,0xAB,0x09,0xA5,0x11,0xBD
    };

    static const byte plaintext_192[16] = {
        0x76,0x77,0x74,0x75,0xF1,0xF2,0xF3,0xF4,
        0xF8,0xF9,0xE6,0xE7,0x77,0x70,0x71,0x72
    };
    static const byte key_192[24] = {
        0x04,0x05,0x06,0x07,0x09,0x0A,0x0B,0x0C,
        0x0E,0x0F,0x10,0x11,0x13,0x14,0x15,0x16,
        0x18,0x19,0x1A,0x1B,0x1D,0x1E,0x1F,0x20
    };
    static const byte ciphertext_192[16] = {
        0x5D,0x1E,0xF2,0x0D,0xCE,0xD6,0xBC,0xBC,
        0x12,0x13,0x1A,0xC7,0xC5,0x47,0x88,0xAA
    };
    
    static const byte plaintext_256[16] = {
        0x06,0x9A,0x00,0x7F,0xC7,0x6A,0x45,0x9F,
        0x98,0xBA,0xF9,0x17,0xFE,0xDF,0x95,0x21
    };
    static const byte key_256[32] = {
        0x08,0x09,0x0A,0x0B,0x0D,0x0E,0x0F,0x10,
        0x12,0x13,0x14,0x15,0x17,0x18,0x19,0x1A,
        0x1C,0x1D,0x1E,0x1F,0x21,0x22,0x23,0x24,
        0x26,0x27,0x28,0x29,0x2B,0x2C,0x2D,0x2E
    };
    static const byte ciphertext_256[16] = {
        0x08,0x0E,0x95,0x17,0xEB,0x16,0x77,0x71,
        0x9A,0xCF,0x72,0x80,0x86,0x04,0x0A,0xE3
    };

    rijndael_setkey (&ctx, key, sizeof(key));
    rijndael_encrypt (&ctx, scratch, plaintext);
    if (memcmp (scratch, ciphertext, sizeof (ciphertext)))
        return "Rijndael-128 test encryption failed.";
    rijndael_decrypt (&ctx, scratch, scratch);
    if (memcmp (scratch, plaintext, sizeof (plaintext)))
        return "Rijndael-128 test decryption failed.";

    rijndael_setkey (&ctx, key_192, sizeof(key_192));
    rijndael_encrypt (&ctx, scratch, plaintext_192);
    if (memcmp (scratch, ciphertext_192, sizeof (ciphertext_192)))
        return "Rijndael-192 test encryption failed.";
    rijndael_decrypt (&ctx, scratch, scratch);
    if (memcmp (scratch, plaintext_192, sizeof (plaintext_192)))
        return "Rijndael-192 test decryption failed.";
    
    rijndael_setkey (&ctx, key_256, sizeof(key_256));
    rijndael_encrypt (&ctx, scratch, plaintext_256);
    if (memcmp (scratch, ciphertext_256, sizeof (ciphertext_256)))
        return "Rijndael-256 test encryption failed.";
    rijndael_decrypt (&ctx, scratch, scratch);
    if (memcmp (scratch, plaintext_256, sizeof (plaintext_256)))
        return "Rijndael-256 test decryption failed.";
    
    return NULL;
}

