#if !defined(AFX_SERVERSPROPERTIESDLG_H__B1BA5394_09CB_4A7D_84EE_FF643A896097__INCLUDED_)
#define AFX_SERVERSPROPERTIESDLG_H__B1BA5394_09CB_4A7D_84EE_FF643A896097__INCLUDED_

#include "SMTPAuthDlg.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServersPropertiesDlg.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CServersPropertiesDlg dialog

class CServersPropertiesDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CServersPropertiesDlg)

// Construction
public:
	void OwnerDrawButtons();
	BOOL m_bOnChange2;
	CSMTPAuthDlg SettDlg;
	BOOL m_bOnChange;
	CServersPropertiesDlg();
	~CServersPropertiesDlg();

// Dialog Data
	//{{AFX_DATA(CServersPropertiesDlg)
	enum { IDD = IDD_SERVERS_PROP_DLG };
	int		m_SmtpLogonOpt;
	BOOL	m_SMTPAuthOpt;
	BOOL	m_RememberPassword;
	CString	m_sPOP3AcountName;
	CString	m_sPOP3AcountPsw;
	CString	m_sPOP3ServerAddr;
	CString	m_sSMTPServerAddr;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CServersPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CSXButton m_btnSMTPSett;
	// Generated message map functions
	//{{AFX_MSG(CServersPropertiesDlg)
	afx_msg void OnSmtpSettBtn();
	afx_msg void OnSmtpServerAuthOption();
	virtual BOOL OnInitDialog();
	afx_msg void OnChange();
	afx_msg void OnRememberPasswOption();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVERSPROPERTIESDLG_H__B1BA5394_09CB_4A7D_84EE_FF643A896097__INCLUDED_)
