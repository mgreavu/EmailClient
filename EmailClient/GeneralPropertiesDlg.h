#if !defined(AFX_GENERALPROPERTIESDLG_H__4DC58670_A377_449E_9398_976DFE7E8F1F__INCLUDED_)
#define AFX_GENERALPROPERTIESDLG_H__4DC58670_A377_449E_9398_976DFE7E8F1F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GeneralPropertiesDlg.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CGeneralPropertiesDlg dialog

class CGeneralPropertiesDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CGeneralPropertiesDlg)

// Construction
public:
	BOOL m_bOnChange;
	CGeneralPropertiesDlg();
	~CGeneralPropertiesDlg();

// Dialog Data
	//{{AFX_DATA(CGeneralPropertiesDlg)
	enum { IDD = IDD_GENERAL_PROP_DLG };
	CString	m_sEmailAdress;
	CString	m_sMailAcount;
	CString	m_sName;
	CString	m_sOrganization;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CGeneralPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGeneralPropertiesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChange();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENERALPROPERTIESDLG_H__4DC58670_A377_449E_9398_976DFE7E8F1F__INCLUDED_)
