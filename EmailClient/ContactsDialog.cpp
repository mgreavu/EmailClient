// ContactsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AccountListTypes.h"
#include "EmailClient.h"
#include "ContactsDialog.h"
#include "AddContactsDlg.h"
#include "AddressBook.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CContactsDialog dialog


CContactsDialog::CContactsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CContactsDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CContactsDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	parentWindow = (CEmailClientDlg*)pParent;
}

CContactsDialog::CContactsDialog(CAddressBook& adressBook, CWnd* pParent /*=NULL*/)
	: CDialog(CContactsDialog::IDD, pParent)
{
}

void CContactsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CContactsDialog)
	DDX_Control(pDX, IDC_CONTACTS_LIST, m_ContactList);
	DDX_Control(pDX, IDC_ADD_BTN, m_btnAdd);
	DDX_Control(pDX, IDC_REMOVE_BTN, m_btnRemove);
	DDX_Control(pDX, IDC_DETAILS_BTN, m_btnDetails);
	DDX_Control(pDX, IDC_EXPORT_BTN, m_btnExport);
	DDX_Control(pDX, IDC_IMPORT_BTN, m_btnImport);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CContactsDialog, CDialog)
	//{{AFX_MSG_MAP(CContactsDialog)
	ON_BN_CLICKED(IDC_ADD_BTN, OnAddBtn)
	ON_BN_CLICKED(IDC_DETAILS_BTN, OnDetailsBtn)
	ON_BN_CLICKED(IDC_REMOVE_BTN, OnRemoveBtn)
	ON_BN_CLICKED(IDC_EXPORT_BTN, OnExportBtn)
	ON_BN_CLICKED(IDC_IMPORT_BTN, OnImportBtn)
	ON_NOTIFY(LVN_KEYDOWN, IDC_CONTACTS_LIST, OnKeydownContactsList)
	ON_NOTIFY(NM_DBLCLK, IDC_CONTACTS_LIST, OnDblclkListContacts)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CContactsDialog message handlers

BOOL CContactsDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect rcContactList;
	m_ContactList.GetWindowRect(&rcContactList);

	CString sMsg;
	sMsg.LoadString(IDS_NICKNAME);
	m_ContactList.InsertColumn(0, sMsg, LVCFMT_LEFT, rcContactList.Width()*0.35);
	sMsg.LoadString(IDS_EMAILADDR);
	m_ContactList.InsertColumn(1, sMsg, LVCFMT_LEFT, rcContactList.Width()*0.65);
	m_ContactList.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	ReadContacts();	
	refresContactsList();
	OwnerDrawButtons();

	return TRUE;
}

void CContactsDialog::OnAddBtn() 
{
	CAddContactsDlg addContactDlg(this);
	int retValue = addContactDlg.DoModal();

	if (retValue == IDOK)
	{
		parentWindow->m_addressBook.addContact(addContactDlg.m_contact); 
		WriteContacts();
		refresContactsList();
	}
}

void CContactsDialog::OnDetailsBtn() 
{
	if(m_ContactList.GetSelectionMark()== -1)
	{
		m_ContactList.SetSelectionMark(0);
	}
	int iSelectedIndex = m_ContactList.GetSelectionMark();
	CString selectedNick = m_ContactList.GetItemText(iSelectedIndex, 0);
	if(!selectedNick.IsEmpty())
	{
		CContact retrievedContact;
		parentWindow->m_addressBook.getContact(retrievedContact, selectedNick, CAddressBook::NICK);	
		CAddContactsDlg addContactDlg(retrievedContact, this);
		if(addContactDlg.DoModal() == IDOK)
		{
			parentWindow->m_addressBook.refreshcontact(addContactDlg.m_contact,retrievedContact.m_NickName);
			WriteContacts();
			refresContactsList();
		}
	}
}

void CContactsDialog::refresContactsList()
{
	m_ContactList.DeleteAllItems();
	int index1=0;
	int index2=0;
	CString strNick;
	CString strEmail;
	CString selectedNick;
	CContact retrievedContact;
	for (parentWindow->m_addressBook.ContactListIterator = parentWindow->m_addressBook.myContactList.begin() ; parentWindow->m_addressBook.ContactListIterator != parentWindow->m_addressBook.myContactList.end(); parentWindow->m_addressBook.ContactListIterator++, index1++)
	{
		strNick = (*(parentWindow->m_addressBook.ContactListIterator)).m_NickName;
		m_ContactList.InsertItem(index1, strNick);
	}
	for (index2 = 0; index2 < index1; index2++)
	{
		selectedNick = m_ContactList.GetItemText(index2, 0);
		parentWindow->m_addressBook.getContact(retrievedContact, selectedNick, CAddressBook::NICK);	
		strEmail = retrievedContact.m_EmailAdress;
		m_ContactList.SetItemText(index2, 1, strEmail);
	}
	
}

void CContactsDialog::OnRemoveBtn() 
{
	CString sMsg;
	
	if(m_ContactList.GetSelectionMark()== -1){
		{
			sMsg.LoadString(IDS_ERR_NO_CONTAT_SEL);
			AfxMessageBox(sMsg);
		}
	}
	else{
		
		sMsg.LoadString(IDS_DEL_CONTACT);
		
		if(AfxMessageBox(sMsg ,MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			int iSelectedIndex = m_ContactList.GetSelectionMark();
			CString selectedNick = m_ContactList.GetItemText(iSelectedIndex, 0);
			if(!selectedNick.IsEmpty()){
				parentWindow->m_addressBook.removeContact(selectedNick);
				WriteContacts();
				refresContactsList();
			}
			else
			{
				sMsg.LoadString(IDS_CONT_LIST_EMPTY);
				
				AfxMessageBox(sMsg);
			}
		}
	}
}

void CAddressBook::Serialize (CArchive &ar)
{

	CObject::Serialize (ar);
	
	try {
	
		if (ar.IsStoring())
		{
			ar << myContactList.size();
			for (ContactListIterator = myContactList.begin(); ContactListIterator != myContactList.end(); ContactListIterator++)
			{
				ar  << (*ContactListIterator).m_FullName
					<< (*ContactListIterator).m_NickName
					<< (*ContactListIterator).m_EmailAdress
					<< (*ContactListIterator).m_PhoneHome
					<< (*ContactListIterator).m_PhoneMobile
					<< (*ContactListIterator).m_PhoneOffice
					<< (*ContactListIterator).m_City
					<< (*ContactListIterator).m_Street
					<< (*ContactListIterator).m_Zip
					<< (*ContactListIterator).m_Country;
			}
		}
		else
		{
			clearAddressBook();
			int numberOfContacts = 0;
			ar >> numberOfContacts;
			for (int i=0; i<numberOfContacts; i++)
			{
				CContact storedContact;
				ar  >> storedContact.m_FullName
					>> storedContact.m_NickName
					>> storedContact.m_EmailAdress
					>> storedContact.m_PhoneHome
					>> storedContact.m_PhoneMobile
					>> storedContact.m_PhoneOffice
					>> storedContact.m_City
					>> storedContact.m_Street
					>> storedContact.m_Zip
					>> storedContact.m_Country;
				addContact(storedContact);
			}
		}
	}
	catch (CException* pException)
    {	
		CString sMsg;
		sMsg.LoadString(IDS_ERR_SER);
		AfxMessageBox(sMsg);

		pException->Delete();
    }
}


void CContactsDialog::OnExportBtn() 
{
	CString sMsg;

	sMsg.LoadString(IDS_ADDRB);
	CFileDialog fileDialog(FALSE, "dat", sMsg, OFN_OVERWRITEPROMPT);	
	if(fileDialog.DoModal()==IDOK){
		CString path = fileDialog.GetPathName();
		if(parentWindow->m_addressBook.saveAddressBook(path))
		{
			sMsg.LoadString(IDS_SUCC_ADR_SAVE);
			AfxMessageBox(sMsg);
		}
		else
		{
			sMsg.LoadString(IDS_ERR_SAVE_FILE);
			AfxMessageBox(sMsg);
		}
		
	}
}

void CContactsDialog::OnImportBtn() 
{
	CString sMsg;

	sMsg.LoadString(IDS_ADDRB);
	CFileDialog fileDialog(TRUE, "dat", sMsg, OFN_OVERWRITEPROMPT);	
	if(fileDialog.DoModal()==IDOK){
		CString path = fileDialog.GetPathName();
		if(parentWindow->m_addressBook.readAddressBook(path))
		{
			sMsg.LoadString(IDS_SUCC_IMPORT_ADRB);		
			AfxMessageBox(sMsg);

			WriteContacts();
		}
		else
		{
			sMsg.LoadString(IDS_ERR_LOAD_FILE);		
			AfxMessageBox(sMsg);
		}
		
	}
	refresContactsList();
}

void CContactsDialog::ReadContacts()
{
	int nIndex;
	AccListIterator = AccountList.begin();
	for(nIndex = 0; nIndex < m_iSelected;nIndex++)
		AccListIterator++;
	CString Path;
	Path = EmailClientFolder;
	Path += (*AccListIterator).GetAccountName();
	Path += "\\";
	Path += CONTACTS_FOLDER_NAME;
	Path += "\\";
	Path += "contacts.sav";
	BOOL bResult = FALSE;
	CFile* pFile = new CFile();
	bResult = pFile->Open (Path, CFile::modeReadWrite);
	if (bResult)
	{
		pFile->Close();
		parentWindow->m_addressBook.readAddressBook(Path);
	}
	else
		parentWindow->m_addressBook.clearAddressBook();

	delete pFile;
}

void CContactsDialog::WriteContacts()
{
	int nIndex;
	AccListIterator = AccountList.begin();
	for(nIndex = 0; nIndex < m_iSelected;nIndex++)
		AccListIterator++;
	CString Path;
	Path = EmailClientFolder;
	Path += (*AccListIterator).GetAccountName();
	Path += "\\";
	Path += CONTACTS_FOLDER_NAME;
	Path += "\\";
	Path += "contacts.sav";
	parentWindow->m_addressBook.saveAddressBook(Path);
}

void CContactsDialog::OnKeydownContactsList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	if(pLVKeyDow->wVKey == VK_DELETE)
		OnRemoveBtn();
	*pResult = 0;
}


void CContactsDialog::OwnerDrawButtons()
{
	m_btnAdd.SetMaskedBitmap( IDB_ADD, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnRemove.SetMaskedBitmap( IDB_REMOVE, 16, 16, RGB( 255, 255, 255 ) );

	m_btnDetails.SetMaskedBitmap( IDB_DETAILS, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnExport.SetMaskedBitmap( IDB_EXPORT, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnImport.SetMaskedBitmap( IDB_IMPORT, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );	
}

void CContactsDialog::OnDblclkListContacts(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if(m_ContactList.GetSelectionMark() != (-1))
	{
		OnDetailsBtn();
	}
	*pResult = 0;
}


