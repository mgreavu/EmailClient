// ComposeNewMessage.cpp : implementation file
//

#include "stdafx.h"
#include "emailclient.h"
#include "ComposeNewMessage.h"
#include "AccountListTypes.h"
#include "smtp/pop3.h"
#include "RemAttachDlg.h"
#include "ConnectDefs.h"
#include "SelectContact.h"
#include "EmailEncryption.h"
#include "smtp/SMTPMsg.h"
#include "AppSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern CAppSettings ECSettings;
/////////////////////////////////////////////////////////////////////////////
// CComposeNewMessage dialog


CComposeNewMessage::CComposeNewMessage(CWnd* pParent /*=NULL*/): CDialog(CComposeNewMessage::IDD, pParent)
{
	//{{AFX_DATA_INIT(CComposeNewMessage)
	m_MailBody = _T("");
	m_sTo = _T("");
	m_bSendEncrypted = FALSE;
	//}}AFX_DATA_INIT
	parentWindow = (CEmailClientDlg*)pParent;
	nComposeType = COMPOSE_TYPE_NEW;
}

CComposeNewMessage::CComposeNewMessage(CWnd* pParent, BYTE ComposeType): CDialog(CComposeNewMessage::IDD, pParent)
{
	parentWindow = (CEmailClientDlg*)pParent;
	nComposeType = ComposeType;
}

void CComposeNewMessage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CComposeNewMessage)
	DDX_Control(pDX, IDC_ADDFROMCONTACTS, m_btnAddFromContacts);
	DDX_Control(pDX, IDC_MAILBODY, m_MailBodyEdit);
	DDX_Control(pDX, IDC_FROM_ACC, m_ComboEmails);
	DDX_Text(pDX, IDC_MAILBODY, m_MailBody);
	DDX_Text(pDX, IDC_TO, m_sTo);
	DDX_Control(pDX, IDC_ATTACH, m_btnAttach);
	DDX_Control(pDX, IDC_REMOVE, m_btnRemove);
	DDX_Control(pDX, IDC_SAVEDRAFT, m_btnSaveDraft);
	DDX_Control(pDX, IDOK, m_btnSend);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Check(pDX, IDC_CHK_SENDENCR, m_bSendEncrypted);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CComposeNewMessage, CDialog)
	//{{AFX_MSG_MAP(CComposeNewMessage)
	ON_BN_CLICKED(IDC_ATTACH, OnAttach)
	ON_CBN_SELCHANGE(IDC_FROM_ACC, OnSelchangeFromAcc)
	ON_EN_CHANGE(IDC_SUBJECT, OnChangeSubject)
	ON_BN_CLICKED(IDC_SAVEDRAFT, OnSavedraft)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_ADDFROMCONTACTS, OnAddfromcontacts)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CComposeNewMessage message handlers

void CComposeNewMessage::OnCancel()
{
	CDialog::OnCancel();
}

BOOL CComposeNewMessage::OnInitDialog() 
{
	CString sMsg;

	CDialog::OnInitDialog();

	m_bSendEncrypted = FALSE;

	UpdateData(FALSE);

	GetAccountList();

	CString ComboEmails;
	for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++)
	{
		ComboEmails = (*AccListIterator).GetSMTPEmailAddress();
		ComboEmails += " (";
		ComboEmails += (*AccListIterator).GetSMTPServerName();
		ComboEmails += ")";

		m_ComboEmails.AddString(ComboEmails);
	}

	m_ComboEmails.SetCurSel(m_iSelected);

	int nIndex = 0;
	AccListIterator = AccountList.begin();
	for(nIndex = 0; nIndex < m_iSelected; nIndex++) AccListIterator++;

	if (nComposeType != COMPOSE_TYPE_NEW)
	{
		if (!m_sCrtSelFile.IsEmpty())
		{
			CString sFileBuffer;
			int size = 0;

			try 
			{
				CFile pFile (m_sCrtSelFile, CFile::modeRead);
				size = pFile.GetLength();

				unsigned char* lpBuff = NULL;
				if ((lpBuff = (unsigned char*) malloc(size + 1)) == NULL)
				{
					pFile.Close();
					::MessageBox(m_hWnd, "Cannot allocate memory to read mail file", m_sCrtSelFile, MB_OK | MB_ICONSTOP);
					
					return FALSE;
				}

				memset (lpBuff, 0x00, size + 1);

				if (pFile.Read((void *) lpBuff, size) != size)
				{
					free(lpBuff);
					pFile.Close();
					::MessageBox(m_hWnd, "Cannot correctly read mail file", m_sCrtSelFile, MB_OK | MB_ICONSTOP);

					return FALSE;
				}

				pFile.Close();

				LPSTR pCS = sFileBuffer.GetBuffer(size);
				ASSERT (pCS != NULL);
				memset(pCS, 0x00, size);
				memmove (pCS, lpBuff, size);
				sFileBuffer.ReleaseBuffer(size);
							
				free(lpBuff);

				CMailMsg msg = ParseMessage(sFileBuffer);

				CString stext;
				stext = "";
				
				if ((nComposeType == COMPOSE_TYPE_REPLY) || (nComposeType == COMPOSE_TYPE_FWD))
				{
					stext = "\r\n\r\n-- -- -- Original Message -- -- --\r\n";
					stext += "From: " + msg.m_FromAddr + "\r\n";
					stext += "To: " + msg.m_ToAddress + "\r\n";
					stext += "Sent: " + msg.m_Date + "\r\n";
					stext += "Subject: " + msg.m_Subject + "\r\n";
					stext += "\r\n";
				}
				stext += msg.m_TextBody;

				GetDlgItem(IDC_MAILBODY)->SetWindowText(stext);

				if (nComposeType != COMPOSE_TYPE_FWD) GetDlgItem(IDC_TO)->SetWindowText(msg.m_FromAddr);

				GetDlgItem(IDC_SUBJECT)->SetWindowText(msg.m_Subject);
				sFileBuffer.Empty();
			} 
			catch (CFileException *ex) 		
			{ 
				sMsg.LoadString(IDS_ERR_RD_FILE);			
				AfxMessageBox(sMsg);
				ex->Delete();
			}
			
			m_sCrtSelFile.Empty();
		}
	}

	sMsg.LoadString(IDS_NO_FILES_ATT);			
	GetDlgItem(IDC_FILELIST)->SetWindowText(sMsg);

	OwnerDrawButtons();

	m_MailBodyEdit.SetFocus();

	return FALSE;
}

void CComposeNewMessage::OnAttach() 
{
	CString sMsg;
	
	int nrFis = -1;
	int index = 0;
	
	CString Files;		
	GetDlgItem(IDC_FILELIST)->GetWindowText(Files);
	Files.TrimLeft(" ");
		
	while (index != -1)
	{
		index = Files.Find(';', index + 1);
		nrFis++;
	}

	if (nrFis >= 9)
	{
		sMsg.LoadString(IDS_TOO_MANY_FILES);
		AfxMessageBox(sMsg);
		return;
	}

	sMsg.LoadString(IDS_SEL_ATT_FILE);

	CFileDialog* pdlgSelectFile = new CFileDialog(TRUE, NULL, NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR, NULL);
	if (pdlgSelectFile == NULL) return;

	pdlgSelectFile->m_ofn.lpstrTitle = sMsg;
	pdlgSelectFile->DoModal();
	
	CString sSourcePathName = pdlgSelectFile->m_ofn.lpstrFile;
	CString sFileNameOnly = pdlgSelectFile->m_ofn.lpstrFileTitle;

	delete pdlgSelectFile;

	if (!sSourcePathName.IsEmpty())
	{
		CString already;
		GetDlgItem (IDC_FILELIST)->GetWindowText (already);
		already.TrimLeft();
		already.TrimRight();

		if (already.Find (sFileNameOnly, 0) != -1)
		{
			sMsg.LoadString(IDS_ERR_ATTACH_SAME_FNAME);
			AfxMessageBox(sMsg);

			return;
		}

		AttachmentsPaths.AddTail(sSourcePathName);
		sFileList += sFileNameOnly;
		sFileList += "; ";
		
		GetDlgItem(IDC_FILELIST)->SetWindowText (sFileList);
		GetDlgItem (IDC_REMOVE)->EnableWindow(TRUE);
	}	
}

void CComposeNewMessage::OnSelchangeFromAcc() 
{
	CString sEmail;
	GetDlgItem(IDC_FROM_ACC)->GetWindowText(sEmail);

	CString ComboEmails;
	for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++)
	{
		ComboEmails = (*AccListIterator).GetSMTPEmailAddress();
		ComboEmails += " (";
		ComboEmails += (*AccListIterator).GetSMTPServerName();
		ComboEmails += ")";
		if (!sEmail.Compare(ComboEmails)) break;
	}
}

void CComposeNewMessage::OnChangeSubject() 
{
	CString sSubject;
	GetDlgItem(IDC_SUBJECT)->GetWindowText (sSubject);
	SetWindowText (sSubject);
	if (sSubject.IsEmpty())
	{
		CString sMsg;
		sMsg.LoadString(IDS_NEW_MSG);
		
		SetWindowText(sMsg);
	}
}

void CComposeNewMessage::OnOK() 
{
	CString sMsg;

	if((*AccListIterator).GetConnectionType() == 1)
		if(!SetDialUpConnection()) return;

	UpdateData (TRUE);
	if (!m_bSendEncrypted) m_MailBody.Replace (MSG_TERM, "..\r\n");
	
	CString DataFieldValue;
	GetDlgItem (IDC_TO)->GetWindowText (DataFieldValue);

	DataFieldValue.TrimLeft(" ");
	DataFieldValue.TrimRight(" ");
	
	if (DataFieldValue.IsEmpty()) 
	{
		sMsg.LoadString(IDS_NEED_RECP);
		MessageBox (sMsg, "Email Client", MB_OK | MB_ICONSTOP);
		GetDlgItem (IDC_TO)->SetFocus();

		return;
	}
	
	GetDlgItem (IDC_SUBJECT)->GetWindowText(DataFieldValue);

	DataFieldValue.TrimLeft(" ");
	DataFieldValue.TrimRight(" ");
	
	if (DataFieldValue.IsEmpty())
	{ 
		sMsg.LoadString(IDS_NO_SUBJ);
		int res = MessageBox (sMsg, "Email Client", MB_YESNO | MB_ICONQUESTION);
		if (res == IDNO)
		{
			GetDlgItem (IDC_SUBJECT)->SetWindowText("");
			GetDlgItem (IDC_SUBJECT)->SetFocus();

			return;
		}
	}

	CString sToAddr;
	GetDlgItem (IDC_TO)->GetWindowText(sToAddr);

	CString sSubj;
	GetDlgItem (IDC_SUBJECT)->GetWindowText(sSubj);

	CTime zTime = CTime::GetCurrentTime();	
	CString sDate;
	sDate = zTime.FormatGmt("%H:%M:%S %a %b %d %Y");

	CSMTPMsg SMTPMsg;
	if (m_bSendEncrypted)
	{
		SMTPMsg.SetHeaderMembers ((*AccListIterator).GetSMTPDisplayName(), (*AccListIterator).GetSMTPEmailAddress(), "", sToAddr, sSubj, sDate, ECSettings.XSATAUID);

		DWORD dwOutBufferSize = 0;
		byte* pEncryptedMailBody = EncryptBuffer ((const byte*)m_MailBody.GetBuffer(m_MailBody.GetLength()), m_MailBody.GetLength(), dwOutBufferSize, ECSettings.aesKey);
		if (pEncryptedMailBody == NULL)
		{
			::MessageBox (m_hWnd, "Mail body encryption failed", "Email Client", MB_OK | MB_ICONSTOP);
			return;
		}

		SMTPMsg.SetMailTextEncoded(pEncryptedMailBody, dwOutBufferSize);

		free (pEncryptedMailBody); pEncryptedMailBody = NULL;
	}
	else
	{
		SMTPMsg.SetHeaderMembers ((*AccListIterator).GetSMTPDisplayName(), (*AccListIterator).GetSMTPEmailAddress(), "", sToAddr, sSubj, sDate, "");
		SMTPMsg.SetMailText(m_MailBody);
	}

	POSITION pos;	
	for (pos = AttachmentsPaths.GetHeadPosition(); pos != NULL; )
	{
		CString sPath = AttachmentsPaths.GetNext(pos);

		WIN32_FIND_DATA w32fd = {0};
		HANDLE hSearch = FindFirstFile (sPath, &w32fd);
		if (hSearch != INVALID_HANDLE_VALUE) ::FindClose(hSearch);
		else
		{
			::MessageBox (m_hWnd, "Cannot locate attachment file", sPath, MB_OK | MB_ICONSTOP);
			return;
		}
		
		if (m_bSendEncrypted)
		{
			CString sPathOnly = sPath.Left(sPath.ReverseFind('\\'));
			CString sFileNameOnly = sPath.Right(sPath.GetLength() - sPath.ReverseFind('\\') - 1);

			char TempFolder[MAX_PATH + 1];
			memset (TempFolder, 0x00, MAX_PATH + 1);
			GetTempPath(MAX_PATH, TempFolder);
			CString sDestPath = TempFolder + sFileNameOnly;
			sDestPath += ".mpy";

			if (!EncryptFile(&w32fd, sPathOnly.GetBuffer(sPathOnly.GetLength()), TempFolder, ECSettings.XSATAUID, (const byte*)&ECSettings.aesKey))
			{
				::MessageBox (m_hWnd, "Attachment encryption failed.", sPath, MB_OK | MB_ICONSTOP);
				return;
			}
			sPath = sDestPath;
		}

		SMTPMsg.AddAttachment(sPath);
	}
	
	CString sMailFull = SMTPMsg.GetMessage();

	if (sMailFull.IsEmpty())
	{
		::MessageBox (m_hWnd, "Failed to build the mail message", "CComposeNewMessage::OnOK()", MB_OK | MB_ICONSTOP);

		return;
	}
	
	MsgDestination = EmailClientFolder;
	MsgDestination += (*AccListIterator).GetAccountName();
	MsgDestination += "\\";
	MsgDestination += OUTBOX_FOLDER_NAME;
	MsgDestination += "\\";
	MsgDestination += "u";
	CTime theTime = CTime::GetCurrentTime();
	
	CString sFormat;
	sFormat = theTime.FormatGmt("%H%M%S%a%b%d%Y");
	FileName = sFormat;
	MsgDestination += sFormat;
	MsgDestination += ".mil";

	WriteMsgToOutbox (MsgDestination, sMailFull);
	
	AttachmentsPaths.RemoveAll();
	sMailFull.Empty();
	
	CDialog::OnOK();
}

void CComposeNewMessage::OnSavedraft() 
{
	CString sMsg;

	UpdateData (TRUE);
	if (!m_bSendEncrypted) m_MailBody.Replace (MSG_TERM, "..\r\n");

	CString DataFieldValue;	
	GetDlgItem (IDC_TO)->GetWindowText (DataFieldValue);

	DataFieldValue.TrimLeft(" ");
	DataFieldValue.TrimRight(" ");

	if (DataFieldValue.IsEmpty()) 
	{
		CString sMsg;
		sMsg.LoadString(IDS_NEED_RECP);
		
		MessageBox (sMsg, "Email Client", MB_OK | MB_ICONSTOP);
		GetDlgItem (IDC_TO)->SetFocus();
		return;
	}
	GetDlgItem (IDC_SUBJECT)->GetWindowText(DataFieldValue);

	DataFieldValue.TrimLeft(" ");
	DataFieldValue.TrimRight(" ");

	if (DataFieldValue.IsEmpty())
	{
		sMsg.LoadString(IDS_NO_SUBJ);
		int res = MessageBox (sMsg, "Email Client", MB_YESNO | MB_ICONQUESTION);
		if (res == IDNO)
		{
			GetDlgItem (IDC_SUBJECT)->SetWindowText("");
			GetDlgItem (IDC_SUBJECT)->SetFocus();

			return;
		}
	}

	CString sToAddr;
	GetDlgItem (IDC_TO)->GetWindowText(sToAddr);

	CString sSubj;
	GetDlgItem (IDC_SUBJECT)->GetWindowText(sSubj);

	CTime zTime = CTime::GetCurrentTime();	
	CString sDate;
	sDate = zTime.FormatGmt("%H:%M:%S %a %b %d %Y");

	CSMTPMsg SMTPMsg;
	if (m_bSendEncrypted)
	{
		SMTPMsg.SetHeaderMembers ((*AccListIterator).GetSMTPDisplayName(), (*AccListIterator).GetSMTPEmailAddress(), "", sToAddr, sSubj, sDate, ECSettings.XSATAUID);
		DWORD dwOutBufferSize = 0;
		byte* pEncryptedMailBody = EncryptBuffer ((const byte*)m_MailBody.GetBuffer(m_MailBody.GetLength()), m_MailBody.GetLength(), dwOutBufferSize, ECSettings.aesKey);
		if (pEncryptedMailBody == NULL)
		{
			::MessageBox (m_hWnd, "Mail body encryption failed", "Email Client", MB_OK | MB_ICONSTOP);
			return;
		}

		SMTPMsg.SetMailTextEncoded(pEncryptedMailBody, dwOutBufferSize);

		free (pEncryptedMailBody); pEncryptedMailBody = NULL;
	}
	else
	{
		SMTPMsg.SetHeaderMembers ((*AccListIterator).GetSMTPDisplayName(), (*AccListIterator).GetSMTPEmailAddress(), "", sToAddr, sSubj, sDate, "");
		SMTPMsg.SetMailText(m_MailBody);
	}

	POSITION pos;	
	for (pos = AttachmentsPaths.GetHeadPosition(); pos != NULL; )
	{
		CString sPath = AttachmentsPaths.GetNext(pos);

		WIN32_FIND_DATA w32fd = {0};
		HANDLE hSearch = FindFirstFile (sPath, &w32fd);
		if (hSearch != INVALID_HANDLE_VALUE) ::FindClose(hSearch);
		else
		{
			::MessageBox (m_hWnd, "Cannot locate attachment file", sPath, MB_OK | MB_ICONSTOP);
			return;
		}

		if (m_bSendEncrypted)
		{
			CString sPathOnly = sPath.Left(sPath.ReverseFind('\\'));
			CString sFileNameOnly = sPath.Right(sPath.GetLength() - sPath.ReverseFind('\\') - 1);

			char TempFolder[MAX_PATH + 1];
			memset (TempFolder, 0x00, MAX_PATH + 1);
			GetTempPath(MAX_PATH, TempFolder);
			CString sDestPath = TempFolder + sFileNameOnly;
			sDestPath += ".mpy";

			if (!EncryptFile(&w32fd, sPathOnly.GetBuffer(sPathOnly.GetLength()), TempFolder, ECSettings.XSATAUID, (const byte*)&ECSettings.aesKey))
			{
				::MessageBox (m_hWnd, "Attachment encryption failed.", sPath, MB_OK | MB_ICONSTOP);
				return;
			}
			sPath = sDestPath;
		}

		SMTPMsg.AddAttachment(sPath);
	}

	CString sMailFull = SMTPMsg.GetMessage();

	if (sMailFull.IsEmpty())
	{
		::MessageBox (m_hWnd, "Failed to build the mail message", "CComposeNewMessage::OnOK()", MB_OK | MB_ICONSTOP);

		return;
	}

	MsgDestination = EmailClientFolder;
	MsgDestination += (*AccListIterator).GetAccountName();
	MsgDestination += "\\";
	MsgDestination += DRAFT_FOLDER_NAME;
	MsgDestination += "\\";
	MsgDestination += "u";
	CTime theTime = CTime::GetCurrentTime();

	CString sFormat;
	sFormat = theTime.FormatGmt("%H%M%S%a%b%d%Y");
	FileName = sFormat;
	MsgDestination += sFormat;
	MsgDestination += ".mil";

	WriteMsgToOutbox (MsgDestination, sMailFull);
	
	AttachmentsPaths.RemoveAll();
	sMailFull.Empty();

	OnCancel();
}

void CComposeNewMessage::OnRemove() 
{
	UpdateData(TRUE);

	int nrFis = -1;

	CString DataFieldValue;
	GetDlgItem (IDC_FILELIST)->GetWindowText (DataFieldValue);
	DataFieldValue.TrimLeft(" ");

	CString Files = DataFieldValue;

	int index = 0;
	while (index != -1)
	{
		index = Files.Find(';', index + 1);
		nrFis++;
	}
	
	CRemAttachDlg dlg;

	for (int iItem=0; iItem < nrFis-1; iItem++)
	{
		dlg.m_AList.AddTail(Files.Mid(0, Files.Find(";", 0)));
		Files = Files.Mid(Files.Find(";", 0) + 2);
	}
	dlg.m_AList.AddTail(Files.Mid(0, Files.Find(";", 0)));
	
	if (dlg.DoModal() == IDOK) 
	{
		int len = dlg.m_AttachName.GetLength();

		Files = DataFieldValue;
		Files.Delete(Files.Find(dlg.m_AttachName, 0), len + 2);

		GetDlgItem (IDC_FILELIST)->SetWindowText(Files);
		
		CString crt;
		POSITION pos1, pos2;
		
		for (pos1 = AttachmentsPaths.GetHeadPosition(); (pos2 = pos1) != NULL; )
		{
			crt = AttachmentsPaths.GetNext(pos1);
			
			if(crt.Find(dlg.m_AttachName, 0) != -1)
			{
				AttachmentsPaths.RemoveAt(pos2);
			}
		}
	}

	if(Files.IsEmpty()) GetDlgItem (IDC_REMOVE)->EnableWindow(FALSE);

	sFileList = Files;
}

void CComposeNewMessage::OwnerDrawButtons()
{
	m_btnRemove.SetMaskedBitmap(IDB_REMOVE, 16, 16, RGB(255, 255, 255));
	m_btnAttach.SetMaskedBitmap(IDB_SAVEATTACH, 16, 16, RGB(255, 255, 255));
	m_btnSaveDraft.SetMaskedBitmap(IDB_DRAFT, 16, 16, RGB(255, 255, 255));
	m_btnSend.SetMaskedBitmap(IDB_SEND, 16, 16, RGB(255, 255, 255 ));
	m_btnCancel.SetMaskedBitmap(IDB_CANCEL, 16, 16, RGB(255, 255, 255));	
	m_btnAddFromContacts.SetMaskedBitmap(IDB_ADDFROMCONTACTS, 16, 16, RGB( 255, 255, 255));
}

void CComposeNewMessage::OnAddfromcontacts() 
{
	UpdateData(TRUE);

	CSelectContact dlgSelCont;
	dlgSelCont.m_iSelected = m_iSelected;
	dlgSelCont.parentWindow = parentWindow;

	if (dlgSelCont.DoModal() == IDOK)
	{
		m_sTo = dlgSelCont.m_sEmailAddr;
		UpdateData(FALSE);
	}

	m_MailBodyEdit.SetFocus();
}

BOOL CComposeNewMessage::WriteMsgToOutbox(CString FPath, CString Message)
{
	BOOL bWriteStatus = FALSE;

	CFile* pFile = new CFile();
	if (pFile == NULL) return FALSE;

	if ((bWriteStatus = pFile->Open (FPath, CFile::modeCreate | CFile::modeWrite)))
	{
		pFile->SeekToBegin();
		pFile->Write(Message, Message.GetLength());
		pFile->Close();
	}
	
	delete pFile;
	pFile = NULL;

	return bWriteStatus;
}
