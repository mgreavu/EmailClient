// RemAttachDlg.cpp : implementation file
//

#include "stdafx.h"
#include "emailclient.h"
#include "RemAttachDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRemAttachDlg dialog


CRemAttachDlg::CRemAttachDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRemAttachDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRemAttachDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CRemAttachDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRemAttachDlg)
	DDX_Control(pDX, IDC_REMATTACHLIST, m_List);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRemAttachDlg, CDialog)
	//{{AFX_MSG_MAP(CRemAttachDlg)
	ON_LBN_SELCHANGE(IDC_REMATTACHLIST, OnSelchangeRemattachlist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRemAttachDlg message handlers

BOOL CRemAttachDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	int i = m_AList.GetCount();

	while (i > 0) 
	{
		m_List.AddString(m_AList.RemoveHead());
		i--;
	}
	
	m_List.SetSel(0, TRUE);
	m_AttachIndex = 0;
	m_List.GetText(0, m_AttachName);

	OwnerDrawButtons();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CRemAttachDlg::OnSelchangeRemattachlist() 
{
	m_AttachIndex = m_List.GetCurSel();	
	m_List.GetText(m_AttachIndex, m_AttachName);
}

void CRemAttachDlg::OwnerDrawButtons()
{
	m_btnOk.SetMaskedBitmap( IDB_REMOVE, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );
}
