// MailAccount.cpp: implementation of the CMailAccount class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MailAccount.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL (CMailAccount, CObject, 0)

CMailAccount::CMailAccount()
{
	AccountName.Empty();
	ConnectionType = 0;
	IncludeAccountInCheck = TRUE;
	LeaveMailOnServer = FALSE;
	Organization.Empty();
	POP3Password.Empty();
	POP3Port = 110;
	POP3RememberPassword = FALSE;
	POP3ServerName.Empty();
	POP3Timeout = 30;
	POP3UserName.Empty();
	SMTPDisplayName.Empty();
	SMTPEmailAddress.Empty();
	SMTPPassword.Empty();
	SMTPPort = 25;
	SMTPRememberPassword = FALSE;
	SMTPServerName.Empty();
	SMTPServerRequiresAuthentication = FALSE;
	SMTPTimeout = 30;
	SMTPUsePOP3LogonSettings = TRUE;
	SMTPUserName.Empty();
	MailAccountName.Empty();
	RasAccount.Empty();
	RemoveAfter = 5;

	FilePath.Empty();

}

CMailAccount::CMailAccount (const CMailAccount& mail)
{
	AccountName = mail.AccountName;
	ConnectionType = mail.ConnectionType;
	IncludeAccountInCheck = mail.IncludeAccountInCheck;
	LeaveMailOnServer = mail.LeaveMailOnServer;
	Organization = mail.Organization;
	POP3Password = mail.POP3Password;
	POP3Port = mail.POP3Port;
	POP3RememberPassword = mail.POP3RememberPassword;
	POP3ServerName = mail.POP3ServerName;
	POP3Timeout = mail.POP3Timeout;
	POP3UserName = mail.POP3UserName;
	SMTPDisplayName = mail.SMTPDisplayName;
	SMTPEmailAddress = mail.SMTPEmailAddress;
	SMTPPassword = mail.SMTPPassword;
	SMTPPort = mail.SMTPPort;
	SMTPRememberPassword = mail.SMTPRememberPassword;
	SMTPServerName = mail.SMTPServerName;
	SMTPServerRequiresAuthentication = mail.SMTPServerRequiresAuthentication;
	SMTPTimeout = mail.SMTPTimeout;
	SMTPUsePOP3LogonSettings = mail.SMTPUsePOP3LogonSettings;
	SMTPUserName = mail.SMTPUserName;
	MailAccountName = mail.MailAccountName;
	RasAccount = mail.RasAccount;
	RemoveAfter = mail.RemoveAfter;
}

CMailAccount& CMailAccount::operator=(CMailAccount& mail)
{
	AccountName = mail.AccountName;
	ConnectionType = mail.ConnectionType;
	IncludeAccountInCheck = mail.IncludeAccountInCheck;
	LeaveMailOnServer = mail.LeaveMailOnServer;
	Organization = mail.Organization;
	POP3Password = mail.POP3Password;
	POP3Port = mail.POP3Port;
	POP3RememberPassword = mail.POP3RememberPassword;
	POP3ServerName = mail.POP3ServerName;
	POP3Timeout = mail.POP3Timeout;
	POP3UserName = mail.POP3UserName;
	SMTPDisplayName = mail.SMTPDisplayName;
	SMTPEmailAddress = mail.SMTPEmailAddress;
	SMTPPassword = mail.SMTPPassword;
	SMTPPort = mail.SMTPPort;
	SMTPRememberPassword = mail.SMTPRememberPassword;
	SMTPServerName = mail.SMTPServerName;
	SMTPServerRequiresAuthentication = mail.SMTPServerRequiresAuthentication;
	SMTPTimeout = mail.SMTPTimeout;
	SMTPUsePOP3LogonSettings = mail.SMTPUsePOP3LogonSettings;
	SMTPUserName = mail.SMTPUserName;
	MailAccountName = mail.MailAccountName;
	RasAccount = mail.RasAccount;
	RemoveAfter = mail.RemoveAfter;

	return (*this);
}

CMailAccount::~CMailAccount()
{

}

void CMailAccount::Serialize (CArchive &ar)
{

	CObject::Serialize (ar);
	
	try {
	
		if (ar.IsStoring())
		{
			ar << AccountName << ConnectionType << IncludeAccountInCheck << LeaveMailOnServer << Organization << POP3Password << POP3Port << POP3RememberPassword << POP3ServerName << POP3Timeout << POP3UserName << SMTPDisplayName << SMTPEmailAddress << SMTPPassword << SMTPPort << SMTPRememberPassword << SMTPServerName << SMTPServerRequiresAuthentication << SMTPTimeout << SMTPUsePOP3LogonSettings << SMTPUserName << MailAccountName << RasAccount << RemoveAfter;
		}
		else
		{
			ar >> AccountName >> ConnectionType >> IncludeAccountInCheck >> LeaveMailOnServer >> Organization >> POP3Password >> POP3Port >> POP3RememberPassword >> POP3ServerName >> POP3Timeout >> POP3UserName >> SMTPDisplayName >> SMTPEmailAddress >> SMTPPassword >> SMTPPort >> SMTPRememberPassword >> SMTPServerName >> SMTPServerRequiresAuthentication >> SMTPTimeout >> SMTPUsePOP3LogonSettings >> SMTPUserName >> MailAccountName >> RasAccount >> RemoveAfter;
		}
	}
	catch (CException* pException)
    {		
		pException->Delete();
    }
}

void CMailAccount::SetAccountName(CString sAccName)
{
	AccountName = sAccName;		
}

void CMailAccount::SetSMTPServerName(CString sSMTPServ)
{
	SMTPServerName = sSMTPServ;
}

void CMailAccount::SetSMTPPort(int nPort)
{
	SMTPPort = nPort;
}

void CMailAccount::SetPOP3ServerName(CString sPOP3Serv)
{
	POP3ServerName = sPOP3Serv;
}

void CMailAccount::SetPOP3Port(int nPort)
{
	POP3Port = nPort;
}

void CMailAccount::SetConnectionType(int ConnType)
{
	ConnectionType = ConnType;
}

void CMailAccount::SetIncludeAccountInCheck(BOOL bInclude)
{
	IncludeAccountInCheck = bInclude;
}

void CMailAccount::SetLeaveMailOnServer(BOOL bLeaveMail)
{
	LeaveMailOnServer = bLeaveMail;
}

void CMailAccount::SetOrganization(CString sOrganization)
{
	Organization = sOrganization;
}

void CMailAccount::SetPOP3Password(CString sPassw)
{
	POP3Password = sPassw;
}

void CMailAccount::SetPOP3RememberPassword(BOOL bRemember)
{
	POP3RememberPassword = bRemember;
}

void CMailAccount::SetPOP3Timeout(int nTimeout)
{
	POP3Timeout = nTimeout;
}

void CMailAccount::SetPOP3UserName(CString sUserName)
{
	POP3UserName = sUserName;
}

void CMailAccount::SetSMTPDisplayName(CString sDispName)
{
	SMTPDisplayName = sDispName;
}

void CMailAccount::SetSMTPEmailAddress(CString sEmailAddr)
{
	SMTPEmailAddress = sEmailAddr;
}

void CMailAccount::SetSMTPPassword(CString sPassw)
{
	SMTPPassword = sPassw;
}

void CMailAccount::SetSMTPRememberPassword(BOOL bRememberPassw)
{
	SMTPRememberPassword = bRememberPassw;
}

void CMailAccount::SetSMTPServerRequiresAuthentication(BOOL bRequiresAuth)
{
	SMTPServerRequiresAuthentication = bRequiresAuth;
}

void CMailAccount::SetSMTPTimeout(int nTimeout)
{
	SMTPTimeout = nTimeout;
}

void CMailAccount::SetSMTPUsePOP3Settings(BOOL bUsePOP3Settings)
{
	SMTPUsePOP3LogonSettings = bUsePOP3Settings;
}

void CMailAccount::SetSMTPUserName(CString sUserName)
{
	SMTPUserName = sUserName;
}

BOOL CMailAccount::WriteAccountData()
{
	BOOL bResult = FALSE;
	
	CFile* pFile = new CFile();
	bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	if (!bResult)
	{
		bResult = pFile->Open (FilePath, CFile::modeCreate);
		pFile->Close();
		bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	}
	
	if (bResult)
	{
		CArchive Archive(pFile, CArchive::store);
		try
		{
			pFile->SeekToBegin();
			Serialize (Archive);
		}
		catch (CException* pException)
		{
			bResult = FALSE;
			pException->Delete();
		}
		
		Archive.Close();
		pFile->Close();
	}
	
	delete pFile;

	return bResult;
}

BOOL CMailAccount::ReadAccountData()
{
	BOOL bResult = FALSE;
	
	CFile* pFile = new CFile();
	bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	if (!bResult)
	{
		bResult = pFile->Open (FilePath, CFile::modeCreate);
		pFile->Close();
		bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	}
	
	if (bResult)
	{
		CArchive Archive(pFile, CArchive::load);
		try
		{
			pFile->SeekToBegin();
			Serialize (Archive);
		}
		catch (CException* pException)
		{
			bResult = FALSE;
			pException->Delete();
		}
		
		Archive.Close();
		pFile->Close();
	}
	
	delete pFile;
	
	return bResult;
}

void CMailAccount::SetAccountFilePath(CString sFile)
{
	FilePath = sFile;
}

CString CMailAccount::GetAccountName()
{
	return AccountName;
}

int CMailAccount::GetConnectionType()
{
	return ConnectionType;
}

BOOL CMailAccount::GetIncludeAccountInCheck()
{
	return IncludeAccountInCheck;
}

BOOL CMailAccount::GetLeaveMailOnServer()
{
	return LeaveMailOnServer;
}

CString CMailAccount::GetOrganization()
{
	return Organization;
}

CString CMailAccount::GetPOP3Password()
{
	return POP3Password;
}

int CMailAccount::GetPOP3Port()
{
	return POP3Port;
}

BOOL CMailAccount::GetPOP3RememberPassword()
{
	return POP3RememberPassword;
}

CString CMailAccount::GetPOP3ServerName()
{
	return POP3ServerName;
}

int CMailAccount::GetPOP3Timeout()
{
	return POP3Timeout;
}

CString CMailAccount::GetPOP3UserName()
{
	return POP3UserName;
}

CString CMailAccount::GetSMTPDisplayName()
{
	return SMTPDisplayName;
}

CString CMailAccount::GetSMTPEmailAddress()
{
	return SMTPEmailAddress;
}

CString CMailAccount::GetSMTPPassword()
{
	return SMTPPassword;
}

int CMailAccount::GetSMTPPort()
{
	return SMTPPort;
}

BOOL CMailAccount::GetSMTPRememberPassword()
{
	return SMTPRememberPassword;
}

CString CMailAccount::GetSMTPServerName()
{
	return SMTPServerName;
}

BOOL CMailAccount::GetSMTPServerRequiresAuthentication()
{
	return SMTPServerRequiresAuthentication;
}

int CMailAccount::GetSMTPTimeout()
{
	return SMTPTimeout;
}

BOOL CMailAccount::GetSMTPUsePOP3Settings()
{
	return SMTPUsePOP3LogonSettings;
}

CString CMailAccount::GetSMTPUserName()
{
	return SMTPUserName;
}

void CMailAccount::SetMailAccountName(CString MailAccName)
{
	MailAccountName = MailAccName;
}

CString CMailAccount::GetMailAccountName()
{
	return MailAccountName;
}

BOOL CMailAccount::WriteExport()
{
	BOOL bResult = FALSE;
	
	CFile* pFile = new CFile();
	bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	if (!bResult)
	{
		bResult = pFile->Open (FilePath, CFile::modeCreate);
		pFile->Close();
		bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	}
	
	if (bResult)
	{
		CArchive Archive(pFile, CArchive::store);
		try
		{
			pFile->SeekToEnd();
			Serialize (Archive);
			
		}
		catch (CException* pException)
		{
			bResult = FALSE;
			pException->Delete();
		}
		
		
		
		Archive.Close();
		
		pFile->Close();
	}
	
	delete pFile;
	
	return bResult;
}

int CMailAccount::ReadExport(UINT Index)
{
	int iResult;
	BOOL bResult;
	
	CFile* pFile = new CFile();
	bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	if (!bResult)
	{
		bResult = pFile->Open (FilePath, CFile::modeCreate);
		pFile->Close();
		bResult = pFile->Open (FilePath, CFile::modeReadWrite);
	}
	
	if (bResult)
	{
		CArchive Archive(pFile, CArchive::load);
		try
		{
			pFile->Seek(Index,CFile::begin );
			Serialize (Archive);
		}
		catch (CException* pException)
		{
			bResult = FALSE;
			pException->Delete();
		}
		
		Archive.Close();
		iResult = pFile->GetPosition();
		pFile->Close();
	}
	
	delete pFile;
	
	return iResult;
}

void CMailAccount::SetRasAccount(CString m_cRasAccount)
{
	RasAccount = m_cRasAccount;
}

CString CMailAccount::GetRasAccount()
{
	return RasAccount;
}

void CMailAccount::SetRemoveAfter(int days)
{
	RemoveAfter = days;
}

int CMailAccount::GetRemoveAfter()
{
	return RemoveAfter;
}
