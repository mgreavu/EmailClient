// EmailClient.h : main header file for the EMAILCLIENT application
//

#if !defined(AFX_EMAILCLIENT_H__E967CA51_37A8_4613_BED9_C0A76C78A214__INCLUDED_)
#define AFX_EMAILCLIENT_H__E967CA51_37A8_4613_BED9_C0A76C78A214__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include "Globals.h"


/////////////////////////////////////////////////////////////////////////////
// CEmailClientApp:
// See EmailClient.cpp for the implementation of this class
//

class CEmailClientApp : public CWinApp
{
public:
	CEmailClientApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEmailClientApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CEmailClientApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EMAILCLIENT_H__E967CA51_37A8_4613_BED9_C0A76C78A214__INCLUDED_)
