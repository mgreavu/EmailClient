// SaveAttachDlg.cpp : implementation file
//

#include "stdafx.h"
#include "emailclient.h"
#include "SaveAttachDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSaveAttachDlg dialog

CSaveAttachDlg::CSaveAttachDlg(CWnd* pParent /*=NULL*/): CDialog(CSaveAttachDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSaveAttachDlg)
	//}}AFX_DATA_INIT

	width = 0;
}


void CSaveAttachDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSaveAttachDlg)
	DDX_Control(pDX, IDC_ATTACHLIST, m_List);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSaveAttachDlg, CDialog)
	//{{AFX_MSG_MAP(CSaveAttachDlg)
	ON_LBN_SELCHANGE(IDC_ATTACHLIST, OnSelchangeAttachlist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaveAttachDlg message handlers


BOOL CSaveAttachDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	int i = m_AList.GetCount();
	while (i > 0) 
	{
		CString sAdd = m_AList.RemoveHead();
		m_List.AddString(sAdd);
		UpdateSearchListWidth(sAdd);
		i--;
	}
	
	m_List.SetSel (0, TRUE);
	m_AttachIndex = 0;
	m_List.GetText(0, m_AttachName);

	OwnerDrawButtons();
	
	return TRUE;
}

void CSaveAttachDlg::OnSelchangeAttachlist() 
{
	m_AttachIndex = m_List.GetCurSel();	
	m_List.GetText(m_AttachIndex, m_AttachName);
}

void CSaveAttachDlg::OwnerDrawButtons()
{
	m_btnOk.SetMaskedBitmap( IDB_SAVE, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );
}

void CSaveAttachDlg::UpdateSearchListWidth(LPCSTR s)
{
     CClientDC dc(this);

     CFont * f = m_List.GetFont();
     dc.SelectObject(f);

     CSize sz = dc.GetTextExtent(s, _tcslen(s));
     sz.cx += 3 * ::GetSystemMetrics(SM_CXBORDER);
     if (sz.cx > width)
	 {
		width = sz.cx;
		m_List.SetHorizontalExtent(width);
	 } 
}
