#include "stdafx.h"
#include "pop3.h"
#include "base64.h"
#include "AppSettings.h"
#include "EmailEncryption.h"
#include "resource.h"

extern CAppSettings ECSettings;

int FindNoCase(CString& String, CString& strSearch)
{
	int i;
	int thisLength = String.GetLength();	
	int strLength = strSearch.GetLength();
	const char* thisData = (LPCTSTR) String;
	const char* strData = (LPCTSTR) strSearch;
	
	
	for (i = 0; i < thisLength-strLength+1; i ++)		
	{		
		if (_strnicmp(thisData+i,strData,strLength) == 0)			
			return i;		
	}
	
	
	return -1;
	
}

int FindNoCase(CString& String, const char* strSearch)
{
	int i;
	int thisLength = String.GetLength();	
	int strLength = strlen(strSearch);

	const char* thisData = (LPCTSTR) String;
	
	for (i = 0; i < thisLength-strLength+1; i ++)		
	{		
		if (!_strnicmp (thisData+i,strSearch,strLength)) return i;		
	}
	
	
	return -1;
	
}

///////////////////////////////////////////////////////////////////////
// Miscellaneous message functions and data
///////////////////////////////////////////////////////////////////////
CMailMsg ParseMessage(CString sMsg)
{
	Str2Word DayMap;
	Str2Word MonthMap;
	
	DayMap.insert(Str2Word::value_type(CString("Sun"), 0));
	DayMap.insert(Str2Word::value_type(CString("Mon"), 1));
	DayMap.insert(Str2Word::value_type(CString("Tue"), 2));
	DayMap.insert(Str2Word::value_type(CString("Wed"), 3));
	DayMap.insert(Str2Word::value_type(CString("Thu"), 4));
	DayMap.insert(Str2Word::value_type(CString("Fri"), 5));
	DayMap.insert(Str2Word::value_type(CString("Sat"), 6));

	MonthMap.insert(Str2Word::value_type(CString("Jan"), 1));
	MonthMap.insert(Str2Word::value_type(CString("Feb"), 2));
	MonthMap.insert(Str2Word::value_type(CString("Mar"), 3));
	MonthMap.insert(Str2Word::value_type(CString("Apr"), 4));
	MonthMap.insert(Str2Word::value_type(CString("May"), 5));
	MonthMap.insert(Str2Word::value_type(CString("Jun"), 6));
	MonthMap.insert(Str2Word::value_type(CString("Jul"), 7));
	MonthMap.insert(Str2Word::value_type(CString("Aug"), 8));
	MonthMap.insert(Str2Word::value_type(CString("Sep"), 9));
	MonthMap.insert(Str2Word::value_type(CString("Oct"), 10));
	MonthMap.insert(Str2Word::value_type(CString("Nov"), 11));
	MonthMap.insert(Str2Word::value_type(CString("Dec"), 12));

	CMailMsg msg;

	int index, index1;
	CString str, str1;
	CString sAltBoundary, sMixedBoundary, sMixedBoundaryFinish;
	CString sMessageID;
	BOOL	bUnix = FALSE;

	CString XSATAUID;
	BOOL bAESKeyFound = FALSE;
	unsigned char aesKey[32];
	memset (aesKey, 0x00, 32);

	int nXSATAUIDIndex = sMsg.Find(TOKEN_XUID);
	if (nXSATAUIDIndex != -1)
	{
		int nLast = sMsg.Find(TERM_STR, nXSATAUIDIndex);
		XSATAUID = sMsg.Mid (nXSATAUIDIndex + strlen(TOKEN_XUID), nLast - nXSATAUIDIndex - strlen(TOKEN_XUID));

		UserEncCodesList::iterator UECListIterator;
		UECListIterator = ECSettings.UserEncList.begin();

		for (UECListIterator = ECSettings.UserEncList.begin(); UECListIterator != ECSettings.UserEncList.end(); UECListIterator++)
		{
			if (!strncmp ((*UECListIterator).XSATAUID, XSATAUID.GetBuffer(31), 31))
			{
				memmove (aesKey, (*UECListIterator).aesKey, 32);
				bAESKeyFound = TRUE;
				break;
			}
		}
	}

	//Get the message ID
	index      = FindNoCase(sMsg, MESSAGE_ID);
	index1     = sMsg.Find(TERM_STR, index);

	if (index1 == -1)
	{
		index1 = sMsg.Find(TERM_STR_UNIX, index);
		bUnix = TRUE;
	}

	sMessageID = sMsg.Mid(index, index1-index);
	index      = sMessageID.Find("<");
	sMessageID = sMessageID.Mid(index+1);
	sMessageID = sMessageID.Left(16);

	//Get the message subject
	msg.m_Subject.Empty();
	index  = sMsg.Find(CT_SUBJECT);

	index1 = sMsg.Find(TERM_STR, index);	
	if (index1 == -1) index1 = sMsg.Find(TERM_STR_UNIX, index);
	
	msg.m_Subject = sMsg.Mid(index+strlen(CT_SUBJECT), index1-(index+strlen(CT_SUBJECT)));
	msg.m_Subject.TrimLeft();	
	
	//Get the message sender
	index  = sMsg.Find(CT_FROM);
	index1 = sMsg.Find(TERM_STR, index);	
	
	if (index1 == -1)
		index1 = sMsg.Find(TERM_STR_UNIX, index);
	
	str = sMsg.Mid(index+strlen(CT_FROM), index1-(index+strlen(CT_FROM)));
	str.TrimLeft();
//	index = str.Find(" ");
	index = str.ReverseFind(' ');
	msg.m_FromName = str.Left(index);
	msg.m_FromName.TrimLeft('\"');
	msg.m_FromName.TrimRight('\"');
	msg.m_FromAddr = str.Right(str.GetLength()-index-1);
	msg.m_FromAddr.TrimLeft('<');
	msg.m_FromAddr.TrimRight('>');
	
	
	//Get the message destination
	index  = sMsg.Find(CT_TO);

	index1 = sMsg.Find(TERM_STR, index);	
	if (index1 == -1) index1 = sMsg.Find(TERM_STR_UNIX, index);
	
	str = sMsg.Mid(index+strlen(CT_TO), index1-(index+strlen(CT_TO)));
	str.TrimLeft();
	index = str.Find(" ");
	msg.m_ToName = str.Left(index);
	msg.m_ToName.TrimLeft('\"');
	msg.m_ToName.TrimRight('\"');
	msg.m_ToAddress = str.Right(str.GetLength()-index-1);
	msg.m_ToAddress.TrimLeft('<');
	msg.m_ToAddress.TrimRight('>');

	//Get the message date
	index  = sMsg.Find(CT_DATE);
	index1 = sMsg.Find(TERM_STR, index);
	
	if (index1 == -1)
		index1 = sMsg.Find(TERM_STR_UNIX, index);

	str = sMsg.Mid(index+strlen(CT_DATE), index1-(index+strlen(CT_DATE)));
	str.TrimLeft();

	msg.m_Date = str;
	str1 = str.Left(3);
	str  = str.Mid(6);

	memset(&msg.m_Time, 0, sizeof(SYSTEMTIME));

	//Transform day of week from string to number
	msg.m_Time.wDayOfWeek = DayMap[str1];
	
	//Extract day
	str1 = str.Left(1);
	str = str.Mid(2);
	msg.m_Time.wDay = atoi(str1);	

	//Transform motnh from string to number
	str1 = str.Left(3);
	str  = str.Mid(4);
	msg.m_Time.wMonth = MonthMap[str1];
	
	//Extract year
	str1 = str.Left(4);
	str  = str.Mid(5);
	msg.m_Time.wYear = atoi(str1);

	//Extract hour/minute/second
	str1 = str.Left(2);
	msg.m_Time.wHour = atoi(str1);
	str1 = str.Mid(3, 2);
	msg.m_Time.wMinute = atoi(str1);
	str1 = str.Mid(6, 2);
	msg.m_Time.wSecond = atoi(str1);

	//Find the message parts boundaries
	index = sMsg.Find(CONTENT_TYPE_MIXED, 0);
	if (index != -1)							// Extract mixed boundary
	{
		sMixedBoundary = "--";
		str	  = sMsg.Mid(index);
		index = str.Find(FIELD_BOUNDARY);
		str   = str.Mid(index + 9);

		index = str.Find(TERM_STR);		
		if (index == -1)index = str.Find(TERM_STR_UNIX);
		
		str   = str.Left(index);
		str.TrimLeft('\"');
		str.TrimRight('\"');
		sMixedBoundary		 += str;
		sMixedBoundaryFinish = sMixedBoundary;
		sMixedBoundaryFinish += "--";

		if (sMsg.Find(sMixedBoundaryFinish) == -1)
		{
			AfxMessageBox("Parsing interrupted. Email file damaged ?", MB_OK | MB_ICONSTOP);
			return msg;
		}
	}

	index = sMsg.Find(CONTENT_TYPE_ALTERNATIVE, 0);
	if (index != -1)
	{
		sAltBoundary = "--";
		str   = sMsg.Mid(index);
		index = str.Find(FIELD_BOUNDARY);
		str   = str.Mid(index+9);

		index = str.Find(TERM_STR);		
		if (index == -1) index = str.Find(TERM_STR_UNIX);
		
		str   = str.Left(index);
		str.TrimLeft('\"');
		str.TrimRight('\"');
		sAltBoundary += str;
	}

	//Extract message text body
	if (!sMixedBoundary.IsEmpty())
	{
		if (!sAltBoundary.IsEmpty()) 
		{
			index = sMsg.Find(sMixedBoundary);

			if (bUnix) str   = sMsg.Mid(index + sMixedBoundary.GetLength() + strlen(TERM_STR_UNIX));
			else str   = sMsg.Mid(index + sMixedBoundary.GetLength() + strlen(TERM_STR));

			index = str.Find(sMixedBoundary);
			str   = str.Left(index);
			index = str.Find(sAltBoundary);
			
			if (bUnix)
				str   = str.Mid(index + sAltBoundary.GetLength() + strlen(TERM_STR_UNIX));
			else
				str   = str.Mid(index + sAltBoundary.GetLength() + strlen(TERM_STR));
			index = str.Find(sAltBoundary);
			str   = str.Left(index);
//			index = str.Find(CONTENT_TRANSFER_ENCODING);
			index = FindNoCase(str, CONTENT_TRANSFER_ENCODING);
			str   = str.Mid(index);

			index = str.Find(TERM_STR);			
			if (index == -1) index = str.Find(TERM_STR_UNIX);
			
			str   = str.Mid(index);
			str.TrimLeft();
			str.TrimRight();
			msg.m_TextBody = str;
			
			// change \n with \r\n
			if (bUnix)
				msg.m_TextBody.Replace(TERM_STR_UNIX, TERM_STR);
		}
		else
		{
			index = sMsg.Find(sMixedBoundary);
			str   = sMsg.Mid(index + sMixedBoundary.GetLength() + strlen(TERM_STR));
			str   = str.Left(str.Find(sMixedBoundary));
//			index = str.Find(CONTENT_TRANSFER_ENCODING);
			index = FindNoCase(str, CONTENT_TRANSFER_ENCODING);
			str   = str.Mid(index);

			index = str.Find(TERM_STR);			
			if (index == -1) index = str.Find(TERM_STR_UNIX);
			
			str   = str.Mid(index);
			str.TrimLeft();
			str.TrimRight();	
		}

		if (nXSATAUIDIndex != -1)
		{
			if (bAESKeyFound)
			{
				unsigned char* decoded = NULL;
				DWORD Size = Base64DecodeStream(str, &decoded);
				if (decoded != NULL)
				{
					byte* pDecryptedBuffer = DecryptBuffer (decoded, Size, aesKey);
					free (decoded); decoded = NULL;

					if (pDecryptedBuffer != NULL)
					{
						msg.m_TextBody = "The following message is encrypted: \r\n\r\n";
						msg.m_TextBody += (CString)pDecryptedBuffer;
						free (pDecryptedBuffer); pDecryptedBuffer = NULL;
					}						
				}
				else 
					msg.m_TextBody = "Message text unavailable. Decoding failed.";
			}
			else
			{
				msg.m_TextBody = "Cannot decrypt message. Sender's encryption key not found.";
			}
		}
		else 
			msg.m_TextBody = str;


		//Extract attachments
		CString sBoundary;
		CString sPartBody, sAttachName, sEncoding;
		BOOL	Finished = FALSE;
		unsigned char* lpAttach = NULL;

		index     = sMsg.Find(sMixedBoundary);

		if (bUnix)
			str	      = sMsg.Mid(index + sMixedBoundary.GetLength() + strlen(TERM_STR_UNIX));
		else
			str	      = sMsg.Mid(index + sMixedBoundary.GetLength() + strlen(TERM_STR));

		index     = str.Find(sMixedBoundary);
		str       = str.Mid(index);

		index	  = str.Find(TERM_STR);		
		if (index == -1) index = str.Find(TERM_STR_UNIX);
		
		sBoundary = str.Left(index);
		str		  = str.Mid(index);
		if (sBoundary == sMixedBoundaryFinish) Finished = TRUE;
		
		while (!Finished) 
		{
			index     = str.Find(sMixedBoundary);
			if(index == -1) break;

			sPartBody = str.Left(index);
			str       = str.Mid(index);

			index	  = str.Find(TERM_STR);			
			if (index == -1) index = str.Find(TERM_STR_UNIX);
			
			sBoundary = str.Left(index);
			str		  = str.Mid(index);
			if (sBoundary == sMixedBoundaryFinish) Finished = TRUE;

			//Parse the part
			//Check if it is an attachment
						
			index = sPartBody.Find(CONTENT_DISPOSITION_ATTACH);
			if (index != -1) 
			{				
				sEncoding.Empty();
//				index = sPartBody.Find(CONTENT_TRANSFER_ENCODING);
				index = FindNoCase(sPartBody, CONTENT_TRANSFER_ENCODING);
				if (index != -1)
				{
					sEncoding = sPartBody.Mid(index + strlen(CONTENT_TRANSFER_ENCODING));
					index     = sEncoding.Find(TERM_STR);
					
					if (index == -1)
						index = sEncoding.Find(TERM_STR_UNIX);
					
					sEncoding = sEncoding.Left(index);
					sEncoding.TrimLeft();
					sEncoding.TrimRight();
				}
				
				index = sPartBody.Find(ATTACH_FILENAME, index);				
				if (index != -1) 
				{
					sAttachName = sPartBody.Mid(index + strlen(ATTACH_FILENAME));
					index       = sAttachName.Find(TERM_STR);

					if (index == -1) index = sAttachName.Find(TERM_STR_UNIX);

					sPartBody   = sAttachName.Mid(index);
					sPartBody.TrimLeft();
					sPartBody.TrimRight();
					sAttachName = sAttachName.Left(index);
					sAttachName.TrimLeft('\"');
					sAttachName.TrimRight('\"');
					
					msg.AttachmentList.AddTail(sAttachName);
//					AfxMessageBox(sAttachName);
															
					int attsize = 0;
					if (sEncoding == ENCODING_BASE64) 
						attsize = ((sPartBody.GetLength()-4)*3)/4;
					else 
						attsize = sPartBody.GetLength();
					
					CString atachsize;
					if(attsize < 10240)
						atachsize.Format("%db",attsize);
					else
						atachsize.Format("%dk",attsize/1024);
					
					msg.AttachmentSize.AddTail (atachsize);
				}
			}
			else
			{
				index = sPartBody.Find(CONTENT_DISPOSITION_INLINE);
				if (index != -1) 
				{					
					index = sPartBody.Find(ATTACH_FILENAME, index);				
					if (index != -1) 
					{
						sAttachName = sPartBody.Mid(index + strlen(ATTACH_FILENAME));
						index       = sAttachName.Find(TERM_STR);

						if (index == -1)
							index       = sAttachName.Find(TERM_STR_UNIX);
						
						sPartBody   = sAttachName.Mid(index);
						sPartBody.TrimLeft();
						sPartBody.TrimRight();
						sAttachName = sAttachName.Left(index);
						sAttachName.TrimLeft('\"');
						sAttachName.TrimRight('\"');
						msg.m_TextBody += TERM_STR;
						msg.m_TextBody += TERM_STR;
						msg.m_TextBody += "File Atached: " + sAttachName;
						msg.m_TextBody += TERM_STR;
						msg.m_TextBody += TERM_STR;
						msg.m_TextBody += sPartBody.Mid(index);
//						AfxMessageBox(sAttachName);
					}
				}
			}
		}
	}
	else
	{
		if (!sAltBoundary.IsEmpty())
		{
			index = sMsg.Find(sAltBoundary);

			if (bUnix)
				str   = sMsg.Mid(index + sAltBoundary.GetLength() + strlen(TERM_STR_UNIX));
			else
				str   = sMsg.Mid(index + sAltBoundary.GetLength() + strlen(TERM_STR));
//			index = str.Find(CONTENT_TRANSFER_ENCODING);
			index = FindNoCase(str, CONTENT_TRANSFER_ENCODING);	
			str   = str.Mid(index);
			index = str.Find(TERM_STR);
			
			if (index == -1)
				index = str.Find(TERM_STR_UNIX);
			
			str   = str.Mid(index);
			index = str.Find(sAltBoundary);
			str   = str.Left(index);
			str.TrimLeft();
			str.TrimRight();			
			msg.m_TextBody = str;
		}
		else
		{
			index = sMsg.Find(CONTENT_TYPE_TEXT_PLAIN);
			str   = sMsg.Mid(index);
			index = FindNoCase(str, CONTENT_TRANSFER_ENCODING);
			str   = str.Mid(index);

			index = str.Find(TERM_STR, 0);			
			if (index == -1) index = str.Find(TERM_STR_UNIX, index);

			index = str.Find("\r\n\r\n", index);	// look for end of the header data <crlf><crlf>
			
			str   = str.Mid(index);
			str.TrimLeft();
			str.TrimRight();

			if (nXSATAUIDIndex != -1)
			{
				if (bAESKeyFound)
				{
					unsigned char* decoded = NULL;
					DWORD Size = Base64DecodeStream(str, &decoded);
					if (decoded != NULL)
					{
						byte* pDecryptedBuffer = DecryptBuffer (decoded, Size, aesKey);
						free (decoded); decoded = NULL;

						if (pDecryptedBuffer != NULL)
						{
							msg.m_TextBody = "The following message is encrypted: \r\n\r\n";
							msg.m_TextBody += (CString)pDecryptedBuffer;
							free (pDecryptedBuffer); pDecryptedBuffer = NULL;
						}						
					}
					else 
						msg.m_TextBody = "Message text unavailable. Decoding failed.";
				}
				else
				{
					msg.m_TextBody = "Cannot decrypt message. Sender's encryption key not found.";
				}
			}
			else 
				msg.m_TextBody = str;
		}
	}

	return msg;	
}

void ShowErrMessage(DWORD iErrCode)
{
	char lpMsgBuf[1204];
	char MsgFormat[4] = "%0";
	
	if (iErrCode > 0) 
	{
		memset(lpMsgBuf, 0x00, 1024);
		FormatMessage( 
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ARGUMENT_ARRAY,
			NULL,
			iErrCode,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			lpMsgBuf,
			1023,
			(char **) &MsgFormat);
		::MessageBox(NULL, lpMsgBuf, "Email Client", MB_OK | MB_ICONERROR);
	};
}

CString GenerateNewID(int index, CString strID)
{
	if (strID[index] == 126) 
	{
		strID.SetAt(index, 33);
		if (index < 15) 
		{
			strID = GenerateNewID(index+1, strID);
		}
		else
			strID = GenerateNewID(0, strID);
	}
	else
		strID.SetAt(index, strID[index]+1);

	return strID;
}

CString GenerateNewMsgID(CString MsgID)
{	
	MsgID = GenerateNewID(0, MsgID);
	char ch;

	ch = '.';
	MsgID.Replace(ch, ch+2);
	ch = '/';
	MsgID.Replace(ch, ch+1);
	ch = '\\';
	MsgID.Replace(ch, ch+1);
	ch = '*';
	MsgID.Replace(ch, ch+1);
	ch = '?';
	MsgID.Replace(ch, ch+1);
	ch = '\"';
	MsgID.Replace(ch, ch+1);
	ch = '<';
	MsgID.Replace(ch, ch+1);
	ch = '>';
	MsgID.Replace(ch, ch+2);
	ch = ':';
	MsgID.Replace(ch, ch+3);

	return MsgID;
}

void SaveAttachment (CString MsgPath, int AttachIndex, CString SavePath)
{
	HANDLE  fHnd = INVALID_HANDLE_VALUE;
	HANDLE  fNewHnd = INVALID_HANDLE_VALUE;
	DWORD   nBytes = 0;
	CString sMsg, str, sMixedBoundary, sEncoding;
	BOOL	Result;
	int     index, index1;
	BOOL	bUnix = FALSE;
	CString XSATAUID;
	int		nSataUid = -1;

	if ((fHnd = ::CreateFile((LPCTSTR) MsgPath, GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0)) == INVALID_HANDLE_VALUE)
	{
		::MessageBox (NULL, "Error opening mail file for reading", MsgPath, MB_OK | MB_ICONSTOP);

		return;
	}

	if ((fNewHnd = ::CreateFile((LPCTSTR) SavePath, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0)) == INVALID_HANDLE_VALUE)
	{
		::CloseHandle(fHnd);
		::MessageBox (NULL, "Error creating destination file.", SavePath, MB_OK | MB_ICONSTOP);
		
		return;
	}

	unsigned char* buffer = NULL; 
	DWORD SizeH = 0;
	DWORD Size = 0;
	if ((Size = GetFileSize(fHnd, &SizeH)) == INVALID_FILE_SIZE)
	{
		ShowErrMessage(GetLastError());
		goto error_save_attach;
	}

	buffer = (unsigned char*) malloc(Size + 1);
	if (!buffer)
		goto error_save_attach;

	memset (buffer, 0x00, Size + 1);

	Result = ::ReadFile(fHnd, buffer, Size, &nBytes, 0);
	if (!Result || (nBytes != Size)) 
	{
		ShowErrMessage(GetLastError());
		free(buffer);
		goto error_save_attach;
	}

	sMsg = buffer;
	free(buffer); buffer = NULL; 

	nSataUid = sMsg.Find(TOKEN_XUID);
	if (nSataUid != -1)
	{	
		int nLast = sMsg.Find(TERM_STR, nSataUid);
		XSATAUID = sMsg.Mid (nSataUid + strlen(TOKEN_XUID), nLast - nSataUid - strlen(TOKEN_XUID));
	}

	index = sMsg.Find(CONTENT_TYPE_MIXED);
	if (index == -1)
		goto error_save_attach;

	sMixedBoundary = "--";
	//Extract mixed boundary
	str	  = sMsg.Mid(index);
	index = str.Find(FIELD_BOUNDARY);
	str   = str.Mid(index+9);
	index = str.Find(TERM_STR);
	
	if (index == -1)
	{
		index = str.Find(TERM_STR_UNIX);
		bUnix = TRUE;
	}
	
	str   = str.Left(index);
	str.TrimLeft('\"');
	str.TrimRight('\"');
	sMixedBoundary		 += str;

	index1 = 0;
	index  = 0;
	while (index1 < (AttachIndex+2)) 
	{
		index = sMsg.Find(sMixedBoundary);
		
		if (bUnix)
			sMsg = sMsg.Mid(index+sMixedBoundary.GetLength()+strlen(TERM_STR_UNIX));
		else
			sMsg = sMsg.Mid(index+sMixedBoundary.GetLength()+strlen(TERM_STR));
		index1 ++;
	}

	index = sMsg.Find(sMixedBoundary);
	sMsg  = sMsg.Left(index);
	if (bUnix)
		sMsg.TrimRight(TERM_STR_UNIX);
	else
		sMsg.TrimRight(TERM_STR);

	index =		FindNoCase(sMsg, CONTENT_TRANSFER_ENCODING);
	
	index1    = sMsg.Find(TERM_STR, index);
	
	if (index1 == -1)
		index1 = sMsg.Find(TERM_STR_UNIX, index);
	
	sEncoding = sMsg.Mid(index, index1-index);
	sEncoding = sEncoding.Mid(strlen(CONTENT_TRANSFER_ENCODING)+1);
	
	index     = sMsg.Find(CONTENT_DISPOSITION_ATTACH);
	index1    = sMsg.Find(TERM_STR, index);
	
	if (index1 == -1)
		index1 = sMsg.Find(TERM_STR_UNIX, index);
	
	sMsg      = sMsg.Mid(index1);
	index     = sMsg.Find(ATTACH_FILENAME);
	index1    = sMsg.Find(TERM_STR, index);
	
	if (index1 == -1)
		index1 = sMsg.Find(TERM_STR_UNIX, index);
	
	sMsg      = sMsg.Mid(index1);
	
	if (bUnix)
		sMsg.TrimLeft(TERM_STR_UNIX);
	else
		sMsg.TrimLeft(TERM_STR);
		
	if (strcmp(sEncoding, ENCODING_BASE64) == 0)
	{
		Size = Base64DecodeStream(sMsg, &buffer);

		Result = ::WriteFile(fNewHnd, buffer, Size, &nBytes, 0);
		if (!Result || (nBytes != Size)) 
		{
			ShowErrMessage(GetLastError());
			free(buffer);
			goto error_save_attach;
		}

		free(buffer);		
	}
	else
	{
		Result = ::WriteFile(fNewHnd, (LPCTSTR) sMsg, sMsg.GetLength(), &nBytes, 0);
		if (!Result || (nBytes != sMsg.GetLength())) 
		{
			ShowErrMessage(GetLastError());
			goto error_save_attach;
		}
	}

	::CloseHandle(fHnd);
	::CloseHandle(fNewHnd);	

	if (nSataUid != -1)
	{
		CString sPathOnly = SavePath.Left(SavePath.ReverseFind('\\'));
		sPathOnly += '\\';
		CString sFileNameOnly = SavePath.Right(SavePath.GetLength() - SavePath.ReverseFind('\\') - 1);

		UserEncCodesList::iterator UECListIterator;
		UECListIterator = ECSettings.UserEncList.begin();

		BOOL bFound = FALSE;
		for (UECListIterator = ECSettings.UserEncList.begin(); UECListIterator != ECSettings.UserEncList.end(); UECListIterator++)
		{
			if (!strncmp ((*UECListIterator).XSATAUID, XSATAUID.GetBuffer(31), 31))
			{
				bFound = TRUE;
				break;
			}
		}	

		if (!bFound)
		{
			CString sMsg;
			sMsg.LoadString(IDS_DECR_FAIL);
			::MessageBox (NULL, sMsg, "Email Client", MB_OK | MB_ICONEXCLAMATION);
		}
		else
		{

			if (!DecryptFile(sFileNameOnly.GetBuffer(sFileNameOnly.GetLength()), sPathOnly.GetBuffer(sPathOnly.GetLength()), sPathOnly.GetBuffer(sPathOnly.GetLength()), XSATAUID.GetBuffer(31), (const byte*)((*UECListIterator).aesKey)))
			{
				::MessageBox (NULL, "Attachment decryption failed.", "Email Client", MB_OK | MB_ICONEXCLAMATION);
			}
		}
	}

	return;


error_save_attach:
	::CloseHandle(fHnd);
	::CloseHandle(fNewHnd);

	return;
}


