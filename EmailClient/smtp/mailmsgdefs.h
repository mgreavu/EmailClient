#ifndef __MAILMSGDEFS__H
#define __MAILMSGDEFS__H

#define APP_NAME	"CMail"

//Constants
#define CT_SUBJECT "Subject:"
#define CT_FROM	   "From:" 	
#define CT_TO	   "To:" 	
#define CT_DATE	   "Date:"

#define TOKEN_XMAILER "X-Mailer: "	
#define TOKEN_XUID "X-SATAUID: "
#define TOKEN_XUIDL	"X-UIDL: "

#define TOKEN_MIME_VERSION			"MIME-Version: "
#define TOKEN_MIME_CONTENT_TYPE		"Content-Type: "
#define TOKEN_MIME_BOUNDARY			"SATAHTSMIMEBoundary"

#define OUTBOX						"Outbox\\"

#define CONTENT_TYPE_ALTERNATIVE	"Content-Type: multipart/alternative"
#define CONTENT_TYPE_MIXED			"Content-Type: multipart/mixed"
#define CONTENT_TYPE_TEXT_PLAIN		"Content-Type: text/plain"
#define CONTENT_TRANSFER_ENCODING	"Content-Transfer-Encoding:"
#define FIELD_BOUNDARY				"boundary="
#define CONTENT_DISPOSITION_ATTACH  "Content-Disposition: attachment"
#define CONTENT_DISPOSITION_INLINE  "Content-Disposition: inline"
#define ATTACH_FILENAME				"filename="
#define MESSAGE_ID					"Message-ID:"

#define ENCODING_BASE64				"base64"
#define MIME_MAX_LINE_LEN			74

//Other
#define TERM_STR			"\r\n"
#define TERM_STR_UNIX		"\n"
#define MSG_TERM			".\r\n"

#endif