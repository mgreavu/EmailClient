#pragma once

/* POINTER defines a generic pointer type */
typedef unsigned char *POINTER;

/* UINT2 defines a two byte word */
typedef unsigned short int UINT2;

/* UINT4 defines a four byte word */
typedef unsigned long int UINT4;

/* PROTO_LIST is defined depending on how PROTOTYPES is defined above.
If using PROTOTYPES, then PROTO_LIST returns the list, otherwise it
  returns an empty list.
 */


/* MD5 context. */
typedef struct {
  UINT4 state[4];                                   /* state (ABCD) */
  UINT4 count[2];        /* number of bits, modulo 2^64 (lsb first) */
  unsigned char buffer[64];                         /* input buffer */
} MD5_CTX;

void MD5InitCram (MD5_CTX *context);
void MD5UpdateCram  (MD5_CTX *context, unsigned char *input, unsigned int inputLen);
void MD5FinalCram  (unsigned char digest[16], MD5_CTX *context);
void hmac_md5 (unsigned char *text, int text_len, unsigned char *key, int key_len, unsigned char *digest);
