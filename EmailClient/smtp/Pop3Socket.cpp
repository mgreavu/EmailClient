// Pop3Socket.cpp : implementation file
//

#include "stdafx.h"
#include "Pop3Socket.h"
#include "mailmsgdefs.h"

#ifdef _LOG
#include "log/Log.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _LOG
extern CLog Log;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPop3Socket

CPop3Socket::CPop3Socket()
{
	m_Connected	= FALSE;
	iBytesSent = 0;
	iBytesReceived = 0;
	iSendBufferLen = 0;
	dwMessagesInMailbox = 0;
	dwDownloadMsgIndex = 0;
	lpMsgInfo = NULL;
	hEmailFile = INVALID_HANDLE_VALUE;
	bFirstFileUnit = FALSE;
	hMother = NULL;
	AccountFolder.Empty();
}

CPop3Socket::~CPop3Socket()
{
	CloseEmailFile();
	if (lpMsgInfo != NULL) 
	{
		delete [] lpMsgInfo;
		lpMsgInfo = NULL;
	}
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CPop3Socket, CAsyncSocket)
	//{{AFX_MSG_MAP(CPop3Socket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CPop3Socket member functions

void CPop3Socket::SetAccountFolder (CString sPath)
{
	AccountFolder = sPath;
}


BOOL CPop3Socket::GetInboxUIDLs()
{
	WIN32_FIND_DATA w32fd;
	HANDLE hSearch = INVALID_HANDLE_VALUE;
	CString MailFile;
	CString sFileBuffer;

	InboxUIDLs.RemoveAll();
	if ((hSearch = FindFirstFile(AccountFolder + "*.mil", &w32fd)) == INVALID_HANDLE_VALUE) return FALSE;
	
	while(1) 
	{
		if ((strcmp (w32fd.cFileName, ".")) && strcmp(w32fd.cFileName, ".."))
		{
			MailFile = AccountFolder + w32fd.cFileName;

			unsigned char* lpBuff = NULL;

			try 
			{
				CFile pFile (MailFile, CFile::modeRead);
				DWORD size = pFile.GetLength();
				size = __min (size, 4096);		// i only need the message-id...

				unsigned char* lpBuff = NULL;
				if ((lpBuff = (unsigned char*) malloc(size + 1)) == NULL)
				{
					pFile.Close();
					::FindClose (hSearch);

					return FALSE;
				}

				memset (lpBuff, 0x00, size + 1);

				if (pFile.Read((void *) lpBuff, size) == size)
				{
					LPSTR pCS = sFileBuffer.GetBuffer(size);
					ASSERT (pCS != NULL);
					memset(pCS, 0x00, size);
					memmove (pCS, lpBuff, size);
					sFileBuffer.ReleaseBuffer(size);
					int nIndex = sFileBuffer.Find (TOKEN_XUIDL);
					if (nIndex != -1)
					{
						int nEndXUIDL = sFileBuffer.Find(TERM_STR, nIndex);
						if (nEndXUIDL != -1)
						{
							CString sUIDL = sFileBuffer.Mid(nIndex + strlen(TOKEN_XUIDL), nEndXUIDL - nIndex - strlen(TOKEN_XUIDL));
							if (!sUIDL.IsEmpty()) InboxUIDLs.Add(sUIDL);
						}
					}
				}

				pFile.Close();
				free(lpBuff);

			}
			catch (CFileException *ex)
			{
				ex->Delete();
			}
		}
		if (!FindNextFile(hSearch, &w32fd)) break;

	}
	::FindClose(hSearch);
	
	return TRUE;
}

void CPop3Socket::OnConnect(int nErrorCode) 
{
	if (nErrorCode) 
	{
		ShowErrorMessage(nErrorCode);
		::PostMessage(hMother, UWM_PROG_BAR, PROG_BAR_CLOSE, 0);

		CAsyncSocket::OnConnect(nErrorCode);
		return;
	}

	ResetReceiveBuffer();

	iBytesSent = 0;
	iSendBufferLen = 0;
	memset(lpSendBuffer, 0x00, SEND_BUFF_SZ);

	m_NextOp = OP_HELLO;
	m_Connected = TRUE;
#ifdef _LOG	
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnConnect()::connected with %s"), m_ServerAddress);
#endif
	CAsyncSocket::OnConnect(nErrorCode);
}

void CPop3Socket::ConnectToServer()
{
	if (m_Connected) return;

	GetInboxUIDLs();
	
	int iErrorCode = 0;

	if (!Create(0, SOCK_STREAM, FD_READ | FD_WRITE | FD_CONNECT | FD_CLOSE, NULL))
	{
		iErrorCode = GetLastError();
		ShowErrorMessage(iErrorCode);
		::PostMessage(hMother, UWM_PROG_BAR, PROG_BAR_CLOSE, 0);
		return;
	}
	
	if (!AsyncSelect(FD_READ | FD_WRITE | FD_CONNECT | FD_CLOSE))
	{
		iErrorCode = GetLastError();
		ShowErrorMessage(iErrorCode);
		::PostMessage(hMother, UWM_PROG_BAR, PROG_BAR_CLOSE, 0);
		return;
	}
#ifdef _LOG	
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("ConnectToServer()::AsyncSelect(FD_READ | FD_WRITE | FD_CONNECT | FD_CLOSE) [FIONBIO] success."));
#endif

	BOOL bNagle = 1;
	if (!SetSockOpt(TCP_NODELAY, &bNagle, sizeof(BOOL), IPPROTO_TCP))
	{
		iErrorCode = ::GetLastError(); 
#ifdef _LOG	
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("SetSockOpt(TCP_NODELAY)::failure with error = %d"), iErrorCode);
#endif	
	}
	else
	{
#ifdef _LOG	
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("SetSockOpt(TCP_NODELAY)::Nagle algorithm disabled"));
#endif
	}

	if (!Connect((LPCTSTR) m_ServerAddress, m_ServerPort))
	{
		if ((iErrorCode = GetLastError()) != WSAEWOULDBLOCK) 
		{
			ShowErrorMessage(iErrorCode);
			::PostMessage(hMother, UWM_PROG_BAR, PROG_BAR_CLOSE, 0);

			return;
		}
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("ConnectToServer()::Connect() returns WSAEWOULDBLOCK"));
#endif
	}
}

void CPop3Socket::ShowErrorMessage(int iErrorCode)
{
	char lpMsgBuf[1204];
	char MsgFormat[4] = "%0";
	
	if (iErrorCode > 0) 
	{
		memset(lpMsgBuf, 0, 1024);
		FormatMessage( 
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ARGUMENT_ARRAY,
			NULL,
			iErrorCode,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			lpMsgBuf,
			1023,
			(char **) &MsgFormat);
		AfxMessageBox(lpMsgBuf, MB_OK | MB_ICONERROR);
	};
}

void CPop3Socket::OnReceive(int nErrorCode) 
{		
	if (nErrorCode)
	{
		ShowErrorMessage(nErrorCode);
		CloseConnection();
		CAsyncSocket::OnReceive(nErrorCode);		

		return;
	}

	DWORD iBytes = 0;

	if (m_NextOp == OP_RETR)
	{
		if ((iBytes = Receive ((char*)&lpReceiveBuffer[iBytesReceived], __min (RCV_BUFF_SZ - iBytesReceived - 1, lpMsgInfo[dwDownloadMsgIndex-1].dwMsgSize))) == SOCKET_ERROR)
		{
			int iErr = ::GetLastError();
			if (iErr != WSAEWOULDBLOCK) 
			{
				AfxMessageBox ("Receive() failure. Closing connection");
				ShutDown(SD_SEND);
			}
			else
			{
#ifdef _LOG
				Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::Receive() returns WSAEWOULDBLOCK: requested %d bytes, current iBytesReceived = %d bytes"), __min (RCV_BUFF_SZ - 1, lpMsgInfo[dwDownloadMsgIndex-1].dwMsgSize), iBytesReceived);
#endif
				Sleep (200);	// lasa serverul sa mai respire
			}

			CAsyncSocket::OnReceive(nErrorCode);
			return;
		}

#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::Receive():: has received %d bytes"), iBytes);
#endif

	}
	else
	{
		if ((iBytes = Receive ((char*)&lpReceiveBuffer[iBytesReceived], (RCV_BUFF_SZ - iBytesReceived - 1))) == SOCKET_ERROR)
		{
			int iErr = ::GetLastError();
			if (iErr != WSAEWOULDBLOCK) 
			{
				AfxMessageBox ("Receive() failure. Closing connection");
				ShutDown(SD_SEND);
			}
			else
			{
#ifdef _LOG
				Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::Receive() returns WSAEWOULDBLOCK: requested %d bytes, current iBytesReceived = %d bytes"), (RCV_BUFF_SZ - iBytesReceived - 1), iBytesReceived);
#endif
				Sleep (200);	// lasa serverul sa mai respire
			}

			CAsyncSocket::OnReceive(nErrorCode);
			return;
		}

		if (!m_NextOp)		// pe aici dupa shutdown (SD_SEND)
		{
#ifdef _LOG
			Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::m_NextOp is %d"), m_NextOp);
#endif
			CAsyncSocket::OnReceive(nErrorCode);
			return;
		}

	}

	if (!iBytes)
	{
         AfxMessageBox ("Connection closed by remote host");
 		 CloseConnection();
		 
		 CAsyncSocket::OnReceive(nErrorCode);
		 return;
	}

	iBytesReceived += iBytes;		// actualizez totalul

	if (!IsReplyComplete())
	{
		CAsyncSocket::OnReceive(nErrorCode);
		return;
	}

	if (!DispatchForProcessing())			// seteaza m_NextOp pentru urmatoarea transmisie
	{
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::DispatchForProcessing() failed. m_NextOp = %d"), m_NextOp);
#endif
 		ShutDown(SD_SEND);
		
		CAsyncSocket::OnReceive(nErrorCode);
		return;
	}

	ResetReceiveBuffer();		// ultimul mesaj a fost procesat, curat bufferul

	if (!m_NextOp)							// state machine completed
	{
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::pop3 communication with mailgate completed."));
#endif
		ShutDown (SD_SEND);
		CAsyncSocket::OnReceive(nErrorCode);		

		return;
	}

	BuildSendBuffer();		// bufferul pentru urmatoarea transmisie spre mailserver
#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::BuildSendBuffer() has assembled lpSendBuffer = %s"), lpSendBuffer);
#endif
	iBytesSent = 0;

	while (iBytesSent < iSendBufferLen)
	{
		int iBytes = 0;
		if ((iBytes = Send ((LPCTSTR)lpSendBuffer + iBytesSent, iSendBufferLen - iBytesSent)) == SOCKET_ERROR)
		{
			int iErrorCode = 0;
			if ((iErrorCode = GetLastError()) == WSAEWOULDBLOCK) 
			{
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::Send() returns WSAEWOULDBLOCK: request to send %d bytes"), iSendBufferLen - iBytesSent);
#endif
			}
			else
			{
				ShowErrorMessage(iErrorCode);
				ShutDown(SD_SEND);
			}
			
			CAsyncSocket::OnReceive(nErrorCode);
			return;
		}
		else
		{
		 iBytesSent += iBytes;
		}
	}

	if (iBytesSent == iSendBufferLen)
	{
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnReceive()::data send complete: %d bytes"), iSendBufferLen);
#endif
		iBytesSent = iSendBufferLen = 0;
		memset (lpSendBuffer, 0x00, SEND_BUFF_SZ);
	}
	
	CAsyncSocket::OnReceive(nErrorCode);
}

BOOL CPop3Socket::ProcessHelloResponse()
{
	CString sResponse = lpReceiveBuffer;

	if (!_stricmp((LPCTSTR) sResponse.Left(3), "+OK"))
	{
		m_NextOp = OP_CAPA;
		return TRUE;
	}

	sResponse = sResponse.Mid(5);
	AfxMessageBox(sResponse, MB_OK | MB_ICONSTOP);

	return FALSE;
}

void CPop3Socket::OnSend(int nErrorCode) 
{
	if (nErrorCode)
	{
		ShowErrorMessage(nErrorCode);
		CloseConnection();
		CAsyncSocket::OnSend(nErrorCode);

		return;
	}

	if (!iSendBufferLen)	// nimic de trimis deocamdata
	{
		CAsyncSocket::OnSend(nErrorCode);
		return;
	}

	int iErrorCode = 0;
	
	while (iBytesSent < iSendBufferLen)
	{
		int iBytes = 0;
		if ((iBytes = Send ((LPCTSTR)lpSendBuffer + iBytesSent, iSendBufferLen - iBytesSent)) == SOCKET_ERROR)
		{

			if ((iErrorCode = GetLastError()) == WSAEWOULDBLOCK) 
			{
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnSend()::Send() returns WSAEWOULDBLOCK: request to send %d bytes"), iSendBufferLen - iBytesSent);
#endif
			}
			else
			{
				ShowErrorMessage(iErrorCode);
				ShutDown(SD_SEND);
			}

			CAsyncSocket::OnSend(nErrorCode);
			return;

		}
		else
		{
		 iBytesSent += iBytes;
		}
	}

	::MessageBox(NULL, lpSendBuffer + iBytesSent, "OnSend()::lpSendBuffer + iBytesSent", MB_OK | MB_ICONINFORMATION);

	if (iBytesSent == iSendBufferLen)
	{
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnSend()::data send complete: %d bytes"), iSendBufferLen);
#endif
		iBytesSent = iSendBufferLen = 0;
		memset (lpSendBuffer, 0x00, SEND_BUFF_SZ);
	}

	CAsyncSocket::OnSend(nErrorCode);
}

BOOL CPop3Socket::ProcessUserAuthResponse()
{
	CString sResponse = lpReceiveBuffer;
	
	if (!_stricmp((LPCTSTR) sResponse.Left(3), "+OK"))
	{
		m_NextOp = OP_PASS_AUTH;
		return TRUE;
	}
	if (!_stricmp((LPCTSTR) sResponse.Left(4), "-ERR"))
	{
		sResponse = sResponse.Mid(5);
		AfxMessageBox(sResponse, MB_OK | MB_ICONEXCLAMATION);
		
		m_NextOp = OP_QUIT;
		return TRUE;
	}

	sResponse = sResponse.Mid(5);
	AfxMessageBox(sResponse, MB_OK | MB_ICONSTOP);

	return FALSE;
}

BOOL CPop3Socket::ProcessPassAuthResponse()
{
	CString sResponse = lpReceiveBuffer;
	
	if (!_stricmp((LPCTSTR) sResponse.Left(3), "+OK"))
	{
		m_NextOp = OP_STAT;
		return TRUE;
	}
	if (!_stricmp((LPCTSTR) sResponse.Left(4), "-ERR"))
	{
		sResponse = sResponse.Mid(5);
		AfxMessageBox(sResponse, MB_OK | MB_ICONEXCLAMATION);
		
		m_NextOp = OP_QUIT;
		return TRUE;
	}

	sResponse = sResponse.Mid(5);
	AfxMessageBox(sResponse, MB_OK | MB_ICONSTOP);

	return FALSE;
}

void CPop3Socket::CloseConnection()
{
	if (!m_Connected) return;

	CloseEmailFile();
	if (lpMsgInfo != NULL) 
	{
		delete [] lpMsgInfo;
		lpMsgInfo = NULL;
	}
	
	m_Connected = FALSE;
	Close();
#ifdef _LOG	
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("CloseConnection()::released memory data structures, closed file handles"));
#endif
	::PostMessage(hMother, UWM_PROG_BAR, PROG_BAR_CLOSE, 0);
}

BOOL CPop3Socket::ProcessStatResponse()
{
	CString sResponse = lpReceiveBuffer;
	
	if (!_stricmp((LPCTSTR) sResponse.Left(3), "+OK"))
	{
		CString sCode = sResponse.Left(4);
		sResponse	  = sResponse.Mid(4);

		int idxSpace  = sResponse.FindOneOf(" ");
		sCode		  = sResponse.Left(idxSpace);
		sResponse	  = sResponse.Right(sResponse.GetLength()-idxSpace-1);

		dwMessagesInMailbox  = atol(sCode);
		DWORD m_MailboxSize = atol(sResponse);
		
		if (!dwMessagesInMailbox)
		{
			::PostMessage (hMother, UWM_PROG_BAR, PROG_BAR_FULL, 0);

			m_NextOp = OP_QUIT;
			return TRUE;

		}

		::PostMessage (hMother, UWM_PROG_BAR, PROG_BAR_RANGE, (LPARAM)m_MailboxSize);
		
		m_NextOp = OP_LIST;

		return TRUE;
	}
	if (!_stricmp((LPCTSTR) sResponse.Left(4), "-ERR"))
	{
		sResponse = sResponse.Mid(5);
		AfxMessageBox(sResponse, MB_OK | MB_ICONEXCLAMATION);
		
		m_NextOp = OP_QUIT;
		return TRUE;
	}

	sResponse = sResponse.Mid(5);
	AfxMessageBox(sResponse, MB_OK | MB_ICONSTOP);

	return FALSE;
}

BOOL CPop3Socket::ProcessDeleteResponse()
{
	CString sResponse = lpReceiveBuffer;

	if (!_stricmp((LPCTSTR) sResponse.Left(3), "+OK"))
	{
		if (dwDownloadMsgIndex < dwMessagesInMailbox)
		{
			m_NextOp = OP_RETR;
		}
		else
		{
			m_NextOp = OP_QUIT;
		}

		return TRUE;
	}
	if (!_stricmp((LPCTSTR) sResponse.Left(4), "-ERR"))
	{
		sResponse = sResponse.Mid(5);
		AfxMessageBox(sResponse, MB_OK | MB_ICONEXCLAMATION);
		
		m_NextOp = OP_QUIT;
		return TRUE;
	}
	
	sResponse = sResponse.Mid(5);
	AfxMessageBox(sResponse, MB_OK | MB_ICONSTOP);

	return FALSE;
}


BOOL CPop3Socket::ProcessListResponse()
{
	CString sCode;
	CString sResponse = lpReceiveBuffer;
	int nIndex = 0;
	int nTermStrLen = strlen(TERM_STR);
	int nSpaceStrLen = strlen (" ");
	int nLastInfoUnitLen = 0;
	
	if (!_stricmp((LPCTSTR) sResponse.Left(3), "+OK"))
	{
		nIndex = sResponse.Find (TERM_STR);
		nIndex += nTermStrLen;

		for (int i=0; i<dwMessagesInMailbox; i++)
		{
			sResponse = sResponse.Mid(nIndex);	// sResponse incepe acum de la prima linie cu date despre mesaje
			nIndex = sResponse.Find (" ");		// indexul primului SPACE in sResponse

			sCode = sResponse.Left(nIndex);

			lpMsgInfo[i].dwMsgIndex = atol(sCode.GetBuffer(sCode.GetLength()));

			nLastInfoUnitLen = sCode.GetLength();
			nIndex = nLastInfoUnitLen;
			nIndex += nSpaceStrLen;							// index inceput ID (de la SPACE)

			int nNext = sResponse.Find (TERM_STR, nIndex);
			sCode = sResponse.Mid(nIndex, nNext);

			lpMsgInfo[i].dwMsgSize = atol(sCode);

			nIndex = nNext + nTermStrLen;
		}
		
		m_NextOp = OP_UIDL;
		
		return TRUE;
	}
	if (!_stricmp((LPCTSTR) sResponse.Left(4), "-ERR"))
	{
		sResponse = sResponse.Mid(5);
		AfxMessageBox(sResponse, MB_OK | MB_ICONEXCLAMATION);
		
		m_NextOp = OP_QUIT;
		return TRUE;
	}
	
	sResponse = sResponse.Mid(5);
	AfxMessageBox (sResponse, MB_OK | MB_ICONSTOP);

	return FALSE;
}

void CPop3Socket::BuildSendBuffer()
{
	iSendBufferLen = 0;
	memset(lpSendBuffer, 0, SEND_BUFF_SZ);	

	switch(m_NextOp) 
	{
	case OP_CAPA:
		{
			strcpy(lpSendBuffer, CMD_CAPA);			
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_CAPA) + strlen(TERM_STR);
		}
		break;
	case OP_USER_AUTH:
		{
			strcpy(lpSendBuffer, CMD_USER_AUTH);			
			strcat(lpSendBuffer, " ");
			strcat(lpSendBuffer, m_UserName);
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_USER_AUTH) + 1 + m_UserName.GetLength() + strlen(TERM_STR);
		}
		break;
	case OP_PASS_AUTH:
		{
			strcpy(lpSendBuffer, CMD_PASS_AUTH);			
			strcat(lpSendBuffer, " ");
			strcat(lpSendBuffer, m_UserPassw);
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_PASS_AUTH) + 1 + m_UserPassw.GetLength() + strlen(TERM_STR);
		}
		break;
	case OP_STAT:
		{
			strcpy(lpSendBuffer, CMD_STAT);			
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_STAT) + strlen(TERM_STR);
		}
		break;
	case OP_LIST:
		{
			strcpy(lpSendBuffer, CMD_LIST);			
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_LIST) + strlen(TERM_STR);
		}
		break;
	case OP_UIDL:
		{
			strcpy(lpSendBuffer, CMD_UIDL);			
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_UIDL) + strlen(TERM_STR);
		}
		break;
	case OP_RETR:
		{
			char strIndex[8];
			memset(strIndex, 0x00, 8);
			sprintf(strIndex, "%d", dwDownloadMsgIndex);

			strcpy(lpSendBuffer, CMD_RETR);			
			strcat(lpSendBuffer, " ");
			strcat(lpSendBuffer, strIndex);
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_RETR) + 1 + strlen(strIndex) + strlen(TERM_STR);
		}
		break;
	case OP_DELE:
		{
			char strIndex[8];
			memset(strIndex, 0x00, 8);
			sprintf(strIndex, "%d", dwDownloadMsgIndex);
			
			strcpy(lpSendBuffer, CMD_DELETE);			
			strcat(lpSendBuffer, " ");
			strcat(lpSendBuffer, strIndex);
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_DELETE) + 1 + strlen(strIndex) + strlen(TERM_STR);
		}
		break;
	case OP_QUIT:
		{
			strcpy(lpSendBuffer, CMD_QUIT);			
			strcat(lpSendBuffer, TERM_STR);
			iSendBufferLen = strlen(CMD_QUIT) + strlen(TERM_STR);
		}
		break;
		
	default:
		{}
	}
	
	return;
}

BOOL CPop3Socket::DispatchForProcessing()
{
	BOOL bStateMachineCanContinue = FALSE;
	BYTE bDispatchOp = m_NextOp;

	switch(bDispatchOp) 
	{
	case OP_HELLO:
		{
			bStateMachineCanContinue = ProcessHelloResponse();
		}
		break;

	case OP_CAPA:
		{
			ProcessCapaResponse();
			bStateMachineCanContinue = TRUE;
		}
		break;

	case OP_USER_AUTH:
		{
			bStateMachineCanContinue = ProcessUserAuthResponse();
		}
		break;

	case OP_PASS_AUTH:
		{
			bStateMachineCanContinue = ProcessPassAuthResponse();
		}
		break;

	case OP_STAT:
		{
			if ((bStateMachineCanContinue = ProcessStatResponse()))
			{
				if (m_NextOp == OP_LIST)
				{
					lpMsgInfo = new MsgInfo[dwMessagesInMailbox + 1];
					if (lpMsgInfo == NULL)
					{
						AfxMessageBox("Memory allocation failure. Abandoning...", MB_ICONSTOP);
						m_NextOp = OP_QUIT;
					}
					memset (lpMsgInfo, 0x00, (dwMessagesInMailbox + 1)*SZ_MSGINFO);
#ifdef _LOG
					Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("DispatchForProcessing()::has allocated a %d data units array, %d bytes in total"), (dwMessagesInMailbox+1), ((dwMessagesInMailbox + 1)*SZ_MSGINFO));
#endif
				}
			}
		}
		break;

	case OP_LIST:
		{
			bStateMachineCanContinue = ProcessListResponse();			
		}
		break;

	case OP_UIDL:
		{
			ProcessUidlResponse();
			
			if (m_NextOp == OP_RETR)
			{
				dwDownloadMsgIndex = 1;		// parametru pentru RETR/DELE
				if (FetchNextDownloadIndex())
				{
					if (!CreateEmailFile())
					{
						AfxMessageBox("Failure creating the file for storing the mail. Abandoning...", MB_OK | MB_ICONEXCLAMATION);
						m_NextOp = OP_QUIT;
					}
					else bFirstFileUnit = TRUE;
				}
				else m_NextOp = OP_QUIT;
			}

			bStateMachineCanContinue = TRUE;
		}
		break;
	
	case OP_RETR:
		{
			if (CloseEmailFile()) MoveDownloadedFileToInbox();
			if ((bStateMachineCanContinue = ProcessRetrResponse()))
			{			
				if (m_NextOp == OP_RETR)
				{
					dwDownloadMsgIndex++;
					if (FetchNextDownloadIndex())
					{
						if (!CreateEmailFile())
						{
							AfxMessageBox("Failure creating the file for storing the mail. Abandoning...", MB_OK | MB_ICONEXCLAMATION);
							m_NextOp = OP_QUIT;
						}
						else bFirstFileUnit = TRUE;
					}
					else m_NextOp = OP_QUIT;
				}
			}
#ifdef _LOG
			Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("DispatchForProcessing()::OP_RETR:: bStateMachineCanContinue = %d, m_NextOp = %d"), bStateMachineCanContinue, m_NextOp);
#endif
		}
		break;

	case OP_DELE:
		{
			if (CloseEmailFile()) MoveDownloadedFileToInbox();
			if ((bStateMachineCanContinue = ProcessDeleteResponse()))
			{
				if (m_NextOp == OP_RETR)
				{
					dwDownloadMsgIndex++;		// parametru pentru RETR/DELE
					if (FetchNextDownloadIndex())
					{
						if (!CreateEmailFile())
						{
							AfxMessageBox("Failure creating the file for storing the mail. Abandoning...", MB_OK | MB_ICONEXCLAMATION);
							m_NextOp = OP_QUIT;
						}
						else bFirstFileUnit = TRUE;
					}
					else m_NextOp = OP_QUIT;

				}
			}
		}
		break;

	case OP_QUIT:
		{
			bStateMachineCanContinue = ProcessQuitResponse();		// OK, the mailgate should say sayonara at this point
#ifdef _LOG
			Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("DispatchForProcessing()::OP_QUIT::bStateMachineCanContinue = %d, m_NextOp = %d"), bStateMachineCanContinue, m_NextOp);
#endif
		}
		break;		

	default: return FALSE;	// stare aiurea, n-ar trebui sa ajung aici
	}		

	return bStateMachineCanContinue;
}

BOOL CPop3Socket::FetchNextDownloadIndex()
{
	if (!dwDownloadMsgIndex) return FALSE;
	int nUidls = InboxUIDLs.GetCount();
	if (!nUidls) return TRUE;

	while (dwDownloadMsgIndex <= dwMessagesInMailbox)
	{
		CString sCurrentMsgUIDL = lpMsgInfo[dwDownloadMsgIndex-1].sMsgID;

		BOOL bFound = FALSE;
		for (int i=0; i<nUidls; i++)
		{
			CString sCurrent = InboxUIDLs.GetAt(i);
			if (sCurrent == sCurrentMsgUIDL)
			{
				dwDownloadMsgIndex++;
				bFound = TRUE;
				break;
			}
		}
		if (!bFound) break;
	}
#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("FetchNextDownloadIndex()::has next dwDownloadMsgIndex = %d"), dwDownloadMsgIndex);
#endif

	if (dwDownloadMsgIndex > dwMessagesInMailbox) return FALSE;	// no other message to check
	return TRUE;	// this dwDownloadIndex is the one to be used for next RETR/DELE
}

BOOL CPop3Socket::ProcessQuitResponse()
{
	CString sResponse = lpReceiveBuffer;

	if (!_stricmp((LPCTSTR)sResponse.Left(3), "+OK"))
	{
		m_NextOp = 0;
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("ProcessQuitResponse()::Ci vediamo dopo, %s !"), m_ServerAddress);
#endif
		return TRUE;
	}
#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("ProcessQuitResponse()::lpReceiveBuffer = %s"), lpReceiveBuffer);
#endif
	sResponse = sResponse.Mid(5);
	::MessageBox(NULL, sResponse, "ProcessQuitResponse()", MB_OK | MB_ICONSTOP);

	return FALSE;
}

BOOL CPop3Socket::IsReplyComplete()
{
	CString sBuffer;
	sBuffer.Empty();
	BYTE bDispatchOp = m_NextOp;

	switch (bDispatchOp) 
	{
		case OP_CAPA:
		case OP_UIDL:
			{
				sBuffer = lpReceiveBuffer;
				
				CString sEnd;
				sEnd.Empty();

				if (!_stricmp((LPCTSTR)sBuffer.Left(4), "-ERR"))
				{
					CString sEnd = sBuffer.Mid(sBuffer.GetLength()-2);
					if (sEnd != TERM_STR) return FALSE;				
				}
				else if (!_stricmp((LPCTSTR)sBuffer.Left(3), "+OK"))
				{
					CString sEnd = sBuffer.Mid(sBuffer.GetLength()-5);
					if (sEnd != "\r\n.\r\n") return FALSE;					
				}
#ifdef _LOG
				Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("IsReplyComplete(%d)::m_NextOp = %d, lpReceiveBuffer = %s"), bDispatchOp, m_NextOp, lpReceiveBuffer);
#endif
			}
			break;

		case OP_HELLO:
		case OP_USER_AUTH:	
		case OP_PASS_AUTH:		
		case OP_STAT:
		case OP_DELE:
			{
				sBuffer = lpReceiveBuffer;
				CString sEnd = sBuffer.Mid(sBuffer.GetLength()-2);
				
				if (sEnd != TERM_STR)
				{
					return FALSE;
				}
#ifdef _LOG
				Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("IsReplyComplete(%d)::m_NextOp = %d, lpReceiveBuffer = %s"), bDispatchOp, m_NextOp, lpReceiveBuffer);
#endif
				break;
			}

		case OP_LIST:
			{
				sBuffer = lpReceiveBuffer;
				CString sEnd = sBuffer.Mid(sBuffer.GetLength()-5);
				
				if (sEnd != "\r\n.\r\n")
				{
					return FALSE;
				}
#ifdef _LOG
				Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("IsReplyComplete(%d)::m_NextOp = %d, lpReceiveBuffer = %s"), bDispatchOp, m_NextOp, lpReceiveBuffer);
#endif
				break;
			}

		case OP_RETR:
			{
				BOOL bRet = FALSE;
				
				int nTermLen = strlen (".\r\n");
				if (bFirstFileUnit)
				{
					char sFirstLine[513];
					memset (sFirstLine, 0x00, 513);
					memmove (sFirstLine, lpReceiveBuffer, __min(iBytesReceived, 512));	// lasa un zero in coada

					CString sPrima = sFirstLine;
					int nSzPrima = sPrima.Find(TERM_STR);
					if (nSzPrima == -1) 
					{
						return FALSE;	// mai astept din prima linie
					}

					nSzPrima += strlen(TERM_STR);
					sPrima = sPrima.Mid(0, nSzPrima);
#ifdef _LOG
					Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, "IsReplyComplete()::First RETR %d reply line was: %s", dwDownloadMsgIndex, sPrima);
#endif
					iBytesReceived -= nSzPrima;
					bFirstFileUnit = FALSE;

					if (!iBytesReceived)		// prima linie "+OK xxx bytes" discard
					{
						ResetReceiveBuffer();
#ifdef _LOG
						Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, "IsReplyComplete()::ResetReceiveBuffer() has reset the buffer.");
#endif
						return FALSE;
					}
					memmove (lpReceiveBuffer, &lpReceiveBuffer[nSzPrima], iBytesReceived);

				}

				int iTermBytes = 0;

				if (iTotalBytesWrittenToFile + iBytesReceived >= lpMsgInfo[dwDownloadMsgIndex-1].dwMsgSize)	// finalul RETR: .<crlf>
				{
					CString sFinalPart = lpReceiveBuffer;
					int LastDotIndex = sFinalPart.ReverseFind('.');
					if (LastDotIndex != -1)
					{
						int nIndex = sFinalPart.Find(".\r\n", LastDotIndex-1);

						if (nIndex != -1)
						{
							iTermBytes = iBytesReceived - nIndex;
							bRet = TRUE;
						}
					}
#ifdef _LOG
					Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, "IsReplyComplete()::iTermBytes = %d, iBytesReceived = %d", iTermBytes, iBytesReceived);
#endif
				}

				DWORD dwWritten = 0;

				if (!::WriteFile(hEmailFile, lpReceiveBuffer, iBytesReceived - iTermBytes, &dwWritten, NULL))
				{
					int iErr = ::GetLastError();

					ShowErrorMessage(iErr);

					ShutDown(SD_SEND);

					return FALSE;
				}
				if ((iBytesReceived - iTermBytes) != dwWritten)
				{
					AfxMessageBox ("The total number of received bytes could not be written");
					ShutDown(SD_SEND);

					return FALSE;
				}
				iTotalBytesWrittenToFile += dwWritten;
#ifdef _LOG
				Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("IsReplyComplete()::WriteFile():: %d bytes written. File size now: %d bytes"), dwWritten, iTotalBytesWrittenToFile);
#endif
				::PostMessage (hMother, UWM_PROG_BAR, PROG_BAR_NOW, (LPARAM)dwWritten);
				

				ResetReceiveBuffer();

				return bRet;
			}

		case OP_QUIT:
			{
				sBuffer = lpReceiveBuffer;
				CString sEnd = sBuffer.Mid(sBuffer.GetLength()-2);
				
				if (sEnd != TERM_STR)
				{
					return FALSE;
				}
#ifdef _LOG
				Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("IsReplyComplete(%d)::OP_QUIT::m_NextOp = %d, lpReceiveBuffer = %s"), bDispatchOp, m_NextOp, lpReceiveBuffer);
#endif
				break;
			}

		default: return FALSE;
	}

	return TRUE;
}

BOOL CPop3Socket::ProcessRetrResponse()
{
	if (dwDownloadMsgIndex == dwMessagesInMailbox)
	{
		if (!m_bLeaveMessages) m_NextOp = OP_DELE;
		else m_NextOp = OP_QUIT;

		return TRUE;
	}

	if (!m_bLeaveMessages) m_NextOp = OP_DELE;
	else m_NextOp = OP_RETR;

	return TRUE;
}

BOOL CPop3Socket::MoveDownloadedFileToInbox()
{
	if (CurrentDownloadFilePath.IsEmpty()) return FALSE;
	int nPos = CurrentDownloadFilePath.ReverseFind('\\');
	if (nPos == -1) return FALSE;

	CString sFileName = CurrentDownloadFilePath.Mid(nPos + 1);
	CString sInboxPath = AccountFolder;
	sInboxPath += sFileName;
	
	int nErr = ERROR_SUCCESS;
	BOOL bMove = ::MoveFile (CurrentDownloadFilePath, sInboxPath);
	if (!bMove) nErr = ::GetLastError();

#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("MoveDownloadedFileToInbox()::MoveFile(%s, %s) returns %d"), CurrentDownloadFilePath, sInboxPath, nErr);
#endif

	::DeleteFile (CurrentDownloadFilePath);

	CurrentDownloadFilePath.Empty();

	return bMove;
}

BOOL CPop3Socket::CreateEmailFile()
{
	if ((dwDownloadMsgIndex <= 0) || (dwDownloadMsgIndex > dwMessagesInMailbox)) return FALSE;

	char TempPath[MAX_PATH + 1];
	memset (TempPath, 0x00, (MAX_PATH + 1));
	if (!GetTempPath(MAX_PATH, TempPath)) 
	{
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("CreateEmailFile()::GetTempPath() failed"));
#endif
		return FALSE;
	}

	CString MsgDestination = TempPath;
	if (MsgDestination.Mid((MsgDestination.GetLength() - 1), 1) != "\\") MsgDestination += "\\";

	MsgDestination += "u";
	CString sFormat;
	sFormat.Format("%d",dwDownloadMsgIndex);
	MsgDestination += sFormat;
	sFormat.Empty();
	CTime theTime = CTime::GetCurrentTime(); 
	sFormat = theTime.FormatGmt("%H%M%S%a%b%d%Y");

	MsgDestination += sFormat;
	MsgDestination += ".mil";

	CurrentDownloadFilePath = MsgDestination;

	iTotalBytesWrittenToFile = 0;
	if ((hEmailFile = ::CreateFile(CurrentDownloadFilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) 
	{
		int nErr = ::GetLastError();
#ifdef _LOG
		Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("CreateEmailFile(%s)::failed with error %d"), CurrentDownloadFilePath, nErr);
#endif
		return FALSE;
	}

	if (strlen(lpMsgInfo[dwDownloadMsgIndex-1].sMsgID))
	{
		char sBuff[128];
		memset (sBuff, 0x00, 128*sizeof(char));
		sprintf (sBuff, "%s%s\r\n", TOKEN_XUIDL, lpMsgInfo[dwDownloadMsgIndex-1].sMsgID);
		DWORD dwWritten = 0;
		::WriteFile (hEmailFile, sBuff, strlen(sBuff), &dwWritten, NULL);
	}

#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("CreateEmailFile()::acquired a valid handle on %s. File lock exclusive."), CurrentDownloadFilePath);
#endif
	return TRUE;
}

BOOL CPop3Socket::CloseEmailFile()
{
	BOOL bClosed = TRUE;
	if (hEmailFile != INVALID_HANDLE_VALUE) 
		if ((bClosed = ::CloseHandle (hEmailFile))) 
		{
#ifdef _LOG
			Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("CloseEmailFile(%s)::has released the file handle"), CurrentDownloadFilePath);
#endif
			hEmailFile = INVALID_HANDLE_VALUE;
		}
		
	return bClosed;
}

void CPop3Socket::ResetReceiveBuffer()
{
	iBytesReceived = 0;
	memset (lpReceiveBuffer, 0x00, RCV_BUFF_SZ);
}

void CPop3Socket::OnClose(int nErrorCode) 
{
	if (nErrorCode)
	{
		ShowErrorMessage(nErrorCode);
	}
#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("OnClose()::socket going down in blaze of glory with nErrorCode = %d"), nErrorCode);
#endif
	CloseConnection();
	
	CAsyncSocket::OnClose(nErrorCode);
}

BOOL CPop3Socket::IsConnected()
{
	return m_Connected;
}

void CPop3Socket::SetMotherWindow(HWND hMom)
{
	if (::IsWindow(hMom)) hMother = hMom;
}

BOOL CPop3Socket::ProcessCapaResponse()
{
	CString sResponse = lpReceiveBuffer;

	m_NextOp = OP_USER_AUTH;

	if (!_stricmp((LPCTSTR)sResponse.Left(3), "+OK"))
	{
//	...should check if USER and UIDL are available

		return TRUE;
	}
	
#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("ProcessCapaResponse()::mailgate doesn't support CAPA command"));
#endif
			
	return TRUE;
}

BOOL CPop3Socket::ProcessUidlResponse ()
{
	CString sCode;
	CString sResponse = lpReceiveBuffer;
	int nIndex = 0;
	int nTermStrLen = strlen(TERM_STR);
	int nSpaceStrLen = strlen (" ");
	int nLastInfoUnitLen = 0;

	m_NextOp = OP_RETR;	

	if (!_stricmp((LPCTSTR) sResponse.Left(3), "+OK"))
	{
		nIndex = sResponse.Find (TERM_STR);
		nIndex += nTermStrLen;

		for (int i=0; i<dwMessagesInMailbox; i++)
		{
			sResponse = sResponse.Mid(nIndex);	// sResponse incepe acum de la prima linie cu date despre mesaje			
			nIndex = sResponse.Find (" ");		// indexul primului SPACE in sResponse

			sCode = sResponse.Left(nIndex);
			int nIndexInArr = atol(sCode.GetBuffer(sCode.GetLength()));

			if ((nIndexInArr-1) != i)
			{
				AfxMessageBox("Data inconsistency detected. Abandoning...");
				m_NextOp = OP_QUIT;	

				return TRUE;
			}

			nLastInfoUnitLen = sCode.GetLength();
			nIndex = nLastInfoUnitLen;
			nIndex += nSpaceStrLen;							// index inceput ID (de la SPACE)

			int nNext = sResponse.Find (TERM_STR, nIndex);	// index final ID (pana la <crlf>)

			sCode = sResponse.Mid(nIndex, nNext);

			memmove (lpMsgInfo[i].sMsgID, sCode.GetBuffer(nNext - nIndex), nNext - nIndex);

			nIndex = nNext + nTermStrLen;
			/*
			char sBuff[128];
			memset(sBuff, 0x00, 128);
			sprintf (sBuff, "Index = %d\r\nSize = %d bytes\r\nID = %s", lpMsgInfo[i].dwMsgIndex, lpMsgInfo[i].dwMsgSize, lpMsgInfo[i].sMsgID);
			AfxMessageBox(sBuff);
			*/
		}		
		return TRUE;
	}

#ifdef _LOG
	Log.InsertLineWithDateTime(LEVEL_INFORMATIONAL, _TEXT("ProcessUidlResponse()::mailgate doesn't support UIDL command"));
#endif

	return TRUE;
}