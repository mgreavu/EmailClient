// MailMsg.cpp: implementation of the CMailMsg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EmailEncryption.h"
#include "rijndael/rsalib.h"
#include "MailMsg.h"
#include "base64.h"


#pragma warning(disable:4786)

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMailMsg::CMailMsg()
{

}

CMailMsg::CMailMsg(CMailMsg& msg)
{
	m_Date = msg.m_Date;
	m_FromAddr  = msg.m_FromAddr;
	m_FromName  = msg.m_FromName;
	m_Subject   = msg.m_Subject;
	m_TextBody  = msg.m_TextBody;
	m_ToAddress = msg.m_ToAddress;
	m_ToName	= msg.m_ToName;
	m_MessageID = msg.m_MessageID;
	XSATAUID	= msg.XSATAUID;
	bEncrypt	= msg.bEncrypt;

	memcpy(&m_Time, &msg.m_Time, sizeof(SYSTEMTIME));
	int index = 0;
	POSITION pos;
	int NrAttachs = msg.AttachmentList.GetCount();

	AttachmentList.RemoveAll();
	AttachmentSize.RemoveAll();
	
	while(index < NrAttachs)
	{
		pos = msg.AttachmentList.FindIndex(index);
		AttachmentList.AddTail(msg.AttachmentList.GetAt(pos));
		pos = msg.AttachmentSize.FindIndex(index);
		AttachmentSize.AddTail(msg.AttachmentSize.GetAt(pos));
		index++;
	}
}


CMailMsg::~CMailMsg()
{
	AttachmentList.RemoveAll();
	AttachmentSize.RemoveAll();
}

CMailMsg& CMailMsg::operator =(CMailMsg& msg)
{
	m_Date = msg.m_Date;
	m_FromAddr  = msg.m_FromAddr;
	m_FromName  = msg.m_FromName;
	m_Subject   = msg.m_Subject;
	m_TextBody  = msg.m_TextBody;
	m_ToAddress = msg.m_ToAddress;
	m_ToName	= msg.m_ToName;
	m_MessageID = msg.m_MessageID;
	XSATAUID	= msg.XSATAUID;
	bEncrypt	= msg.bEncrypt;

	memcpy(&m_Time, &msg.m_Time, sizeof(SYSTEMTIME));
	int index = 0;
	POSITION pos;
	int NrAttachs = msg.AttachmentList.GetCount();

	AttachmentList.RemoveAll();
	AttachmentSize.RemoveAll();
	while(index < NrAttachs)
	{
		pos = msg.AttachmentList.FindIndex(index);
		AttachmentList.AddTail(msg.AttachmentList.GetAt(pos));
		pos = msg.AttachmentSize.FindIndex(index);
		AttachmentSize.AddTail(msg.AttachmentSize.GetAt(pos));
		index++;
	}

	return *this;
}

void CMailMsg::AddAttachment(CString sAttachPath)
{
	CString DashedAttachPath;
	DashedAttachPath.Empty();
	int DashPos = 0;
	int DashPrev = 0;

	while ((DashPos = sAttachPath.Find ("\\", DashPrev)) != -1)
	{			
		DashedAttachPath += sAttachPath.Mid (DashPrev, DashPos - DashPrev);
		DashedAttachPath += "\\";
		DashedAttachPath += "\\";
		DashPrev = DashPos + 1;
	}
	
	DashedAttachPath += sAttachPath.Mid (DashPrev, sAttachPath.GetLength() - DashPrev);

	AttachmentList.AddHead(DashedAttachPath);
}





