// MailMsg.h: interface for the CMailMsg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAILMSG_H__D57ABC24_F9D7_469A_B6AD_071FD124BF8A__INCLUDED_)
#define AFX_MAILMSG_H__D57ABC24_F9D7_469A_B6AD_071FD124BF8A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include "smtp/mailmsgdefs.h"

class CMailMsg  
{
public:
	BOOL bEncrypt;
	CString XSATAUID;
	void AddAttachment (CString sAttachPath);
	CMailMsg();
	CMailMsg(CMailMsg& msg);
	virtual ~CMailMsg();

	CMailMsg& operator=(CMailMsg& msg);

	CString					 m_Date;
	CString					 m_FromName;
	CString					 m_FromAddr;
	CString					 m_ToName;
	CString					 m_ToAddress;
	CString					 m_Subject;
	SYSTEMTIME				 m_Time;
	CString					 m_TextBody;
	CStringList				 AttachmentList;
	CStringList				 AttachmentSize;
	CString					 m_MessageID;
private:
};

#endif // !defined(AFX_MAILMSG_H__D57ABC24_F9D7_469A_B6AD_071FD124BF8A__INCLUDED_)


