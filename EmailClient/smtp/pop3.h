#ifndef __INCLUDE_ONCE__POP3_H
#define __INCLUDE_ONCE__POP3_H

#include "windows.h"
#include "MailMsg.h"

#pragma warning(disable:4786)
#include <map>
using namespace std;

typedef map<CString, WORD> Str2Word;

CMailMsg ParseMessage(CString sMsg);
CString GenerateNewMsgID(CString MsgID);
void SaveAttachment(CString MsgPath, int AttachIndex, CString SavePath);
void ShowErrMessage(DWORD iErrCode);


#endif