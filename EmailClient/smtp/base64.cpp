#include "stdafx.h"
#include "base64.h"
#include <malloc.h>
#include <string.h>

static void b64dec(char *in, int inlen, char *out)
{
  unsigned char t0 = (inlen > 0) ? b64d[in[0] & 0x7f] : 0;
  unsigned char t1 = (inlen > 1) ? b64d[in[1] & 0x7f] : 0;
  unsigned char t2 = (inlen > 2) ? b64d[in[2] & 0x7f] : 0;
  unsigned char t3 = (inlen > 3) ? b64d[in[3] & 0x7f] : 0;

  if (inlen <= 0) return;
  out[0] = (t0 << 2) | (t1 >> 4);
  if (inlen <= 1) { 
    out[1] = 0;
    return;
  }
  out[1] = (t1 << 4) | ((t2 & 0x3c) >> 2);
  if (inlen <= 2) {
    out[2] = 0;
    return;
  }
  out[2] = (t2 << 6) | t3;
}

int b64decode_len(char *in)
{
  int l = strlen(in);
  return 3*((l+3)/4);
}

char *b64decode_alloc(char *in)
{
  int l = b64decode_len(in);
  char *n = (char*)malloc(l+1);
  
  if (n != NULL) {
    while (l >= 0) {
      n[l--] = 0;
    }
  }
  return n;
}

void b64decode(char *in, char *out) 
{
  int inlen = strlen(in);
  while (inlen > 0) 
  {
    b64dec(in, inlen, out);
    inlen -= 4;
    in += 4;
    out += 3;
  }
}


unsigned char* Base64Encode (const unsigned char *lpOriginalBuffer, DWORD dwInBuffSz, unsigned char *lpEncodedBuffer, DWORD& dwOutBuffSz)
{	
	unsigned char InputBlock[4];
	unsigned char OutputBlock[5];
	
	dwOutBuffSz = (DWORD)((4 * dwInBuffSz)/3) + 4;

	lpEncodedBuffer = (unsigned char*) malloc (dwOutBuffSz);
	memset (lpEncodedBuffer, 0x00, dwOutBuffSz);
	
	unsigned long nLoopCounter = 0;
	char k = 3;
	do 
	{		
		memset (InputBlock, 0x00, 4);
		memset (OutputBlock, 0x00, 5);
		
		switch (dwInBuffSz - nLoopCounter)
		{
		case 1:{
			memmove (InputBlock, lpOriginalBuffer + nLoopCounter, 1);
			k = 1;
			break;
			   }
		case 2:
			{
				memmove (InputBlock, lpOriginalBuffer + nLoopCounter, 2);
				k = 2;
				break;
			}
		default: 
			{
				memmove (InputBlock, lpOriginalBuffer + nLoopCounter, 3);
				k = 3;
				break;
			}
			
		}		
		
		OutputBlock[0] = InputBlock[0] >> 2;
		OutputBlock[1] = ((InputBlock[0] & 0x03) << 4) + (InputBlock[1] >> 4);
		OutputBlock[2] = ((InputBlock[1] & 0x0F) << 2) + (InputBlock[2] >> 6);
		OutputBlock[3] = InputBlock[2] & 0x3F;
		
		for (unsigned char i=0; i<k+1; i++)
		{
			lpEncodedBuffer[4*(DWORD)(nLoopCounter/3)+i] = Base64Table[OutputBlock[i]];
			
		}
		
		if (k<3)
		{
			for (unsigned char j=0; j<(3-k); j++)
				lpEncodedBuffer[4*(DWORD)(nLoopCounter/3)+k+j+1] = '=';
		}				
		
		nLoopCounter+=3;
	}
	while (nLoopCounter < dwInBuffSz);
	
	
	return (lpEncodedBuffer);
}


//Base64 decoding
DWORD Base64DecodeStream(CString sStream, unsigned char** lpResult)
{
	Base64Char2Byte CharMap;
	byte index = 0;

	//Chars from 'A' to 'Z'
	for(index=65;index<=90;index++)
		CharMap.insert(Base64Char2Byte::value_type(index, index-65));
	
	//Chars from 'a' to 'z'
	for(index=97;index<=122;index++)
		CharMap.insert(Base64Char2Byte::value_type(index, index-97+26));
	
	//Chars from '0' to '9'
	for(index=48;index<=57;index++)
		CharMap.insert(Base64Char2Byte::value_type(index, index-48+52));

	//Char '+'
	CharMap.insert(Base64Char2Byte::value_type(43, 62));
	//Char '/'
	CharMap.insert(Base64Char2Byte::value_type(47, 63));


	DWORD iLength, iSteps, iResSize = 0;
	int indexCoded = 0, indexDecoded = 0;
	unsigned char* lpCoded = NULL;
	
	sStream.Replace("\r\n", "");
	sStream.Replace("\n", "");
	iLength = sStream.GetLength();
	
	//Get the number of steps
	iSteps   = iLength/4;

	//Get the result size
	iResSize = (iSteps-1)*3;
	if (sStream[sStream.GetLength()-2] == '=') 
		iResSize += 1;
	else
		if (sStream[sStream.GetLength()-1] == '=') 
			iResSize += 2;
		else
			iResSize += 3;	

	*lpResult = (unsigned char*) malloc(iResSize+1);
	lpCoded  = (unsigned char*) malloc(iLength+1); 
	memset(*lpResult, 0, iResSize+1);
	memset(lpCoded, 0, iLength+1);
	memcpy(lpCoded, (LPCTSTR) sStream, sStream.GetLength());

	while (iSteps > 1) 
	{		
		(*lpResult)[indexDecoded]   = (CharMap[lpCoded[indexCoded]] << 2) | (CharMap[lpCoded[indexCoded+1]] >> 4); 
		(*lpResult)[indexDecoded+1] = (CharMap[lpCoded[indexCoded+1]] << 4) | (CharMap[lpCoded[indexCoded+2]] >> 2);
		(*lpResult)[indexDecoded+2] = (CharMap[lpCoded[indexCoded+2]] << 6) | CharMap[lpCoded[indexCoded+3]];

		indexCoded   += 4;
		indexDecoded += 3;		
		iSteps --;
	}

	if (sStream[sStream.GetLength()-2] == '=') 
	{
		(*lpResult)[indexDecoded]   = (CharMap[lpCoded[indexCoded]] << 2) | (CharMap[lpCoded[indexCoded+1]] >> 4); 
	}
	else
	{
		if (sStream[sStream.GetLength()-1] == '=') 
		{
			(*lpResult)[indexDecoded]   = (CharMap[lpCoded[indexCoded]] << 2) | (CharMap[lpCoded[indexCoded+1]] >> 4); 
			(*lpResult)[indexDecoded+1] = (CharMap[lpCoded[indexCoded+1]] << 4) | (CharMap[lpCoded[indexCoded+2]] >> 2);
		}
		else
		{
			(*lpResult)[indexDecoded]   = (CharMap[lpCoded[indexCoded]] << 2) | (CharMap[lpCoded[indexCoded+1]] >> 4); 
			(*lpResult)[indexDecoded+1] = (CharMap[lpCoded[indexCoded+1]] << 4) | (CharMap[lpCoded[indexCoded+2]] >> 2);
			(*lpResult)[indexDecoded+2] = (CharMap[lpCoded[indexCoded+2]] << 6) | CharMap[lpCoded[indexCoded+3]];
		}
	}

	free(lpCoded);
	return iResSize;
}

