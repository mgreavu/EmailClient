// SMTPMsg.cpp: implementation of the CSMTPMsg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMTPMsg.h"
#include "mailmsgdefs.h"
#include "base64.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMTPMsg::CSMTPMsg()
{
	m_MIMEHeader.Empty();
	m_Header.Empty();
	TheSMTPMessage.Empty();
}

CSMTPMsg::~CSMTPMsg()
{
	TheSMTPMessage.Empty();
	m_Header.Empty();
	m_MIMEHeader.Empty();
}

CString CSMTPMsg::GetHeader()
{
	return m_Header;
}

CString CSMTPMsg::GetMessage()
{
	TheSMTPMessage = m_Header;

	int nCountAttachments = AttachmentList.GetCount();

	if (!nCountAttachments)
	{
		TheSMTPMessage += TOKEN_MIME_VERSION;
		TheSMTPMessage += "1.0";
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += "Content-Type: text/plain; charset=\"us-ascii\"";
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += "Content-Transfer-Encoding: 7bit";		
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += TERM_STR;

		TheSMTPMessage += m_MailText;

		TheSMTPMessage += TERM_STR;

		return TheSMTPMessage;
	}

	AddMIMEHeader();

	TheSMTPMessage += TERM_STR;
	TheSMTPMessage += "--";
	TheSMTPMessage += TOKEN_MIME_BOUNDARY;
	TheSMTPMessage += TERM_STR;
	
	TheSMTPMessage += "Content-Type: text/plain; charset=\"us-ascii\"";
	TheSMTPMessage += TERM_STR;
	TheSMTPMessage += "Content-Transfer-Encoding: 7bit";		
	TheSMTPMessage += TERM_STR;
	TheSMTPMessage += TERM_STR;

	TheSMTPMessage += m_MailText;

	CString sCurrentFile;
	CString sFileTitle;	

	POSITION pos = AttachmentList.GetHeadPosition();
	
	for (int i=0; i<nCountAttachments; i++)
	{
		sCurrentFile = AttachmentList.GetNext(pos);
		sFileTitle = sCurrentFile.Right(sCurrentFile.GetLength() - sCurrentFile.ReverseFind('\\') - 1);
		
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += "--";
		TheSMTPMessage += TOKEN_MIME_BOUNDARY;
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += "Content-Type: application/octet-stream; name=\"";
		TheSMTPMessage += sFileTitle;

		TheSMTPMessage += "\"";
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += "Content-Transfer-Encoding: base64";
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += "Content-Disposition: attachment; filename=\"";
		TheSMTPMessage += sFileTitle;

		TheSMTPMessage +=	"\"";
		TheSMTPMessage += TERM_STR;
		TheSMTPMessage += TERM_STR;

		if (!AttachFile(sCurrentFile))
		{
			TheSMTPMessage.Empty();

			return TheSMTPMessage;
		}
	}
	
	TheSMTPMessage += TERM_STR;
	TheSMTPMessage += "--";
	TheSMTPMessage += TOKEN_MIME_BOUNDARY;
	TheSMTPMessage += "--";
	TheSMTPMessage += TERM_STR;

	return TheSMTPMessage;
}

void CSMTPMsg::SetHeaderMembers(CString sFromName, CString sFromAddr, CString sToName, CString sToAddr, CString sSubject, CString sDate, CString XSATAUID)
{
	m_FromName = sFromName;
	m_FromAddr = sFromAddr;
	m_ToName = sToName;
	m_ToAddress = sToAddr;
	m_Subject = sSubject;
	m_Date = sDate;
	
	m_Header = CT_FROM;
	if (m_FromName.IsEmpty())
	{
		m_Header += m_FromAddr;
	}
	else
	{
		m_Header += m_FromName;
		m_Header += " ";
		m_Header += "<";
		m_Header += m_FromAddr;
		m_Header += ">";
	}
	m_Header += TERM_STR;
	m_Header += CT_TO;
	m_Header += m_ToAddress;
	m_Header += TERM_STR;
	if (!m_Subject.IsEmpty())
	{
		m_Header += CT_SUBJECT;
		m_Header += m_Subject;
		m_Header += TERM_STR;
	}
	m_Header += CT_DATE;
	m_Header += m_Date;
	m_Header += TERM_STR;

	m_Header += TOKEN_XMAILER;
	m_Header += "SATA HTS EmailClient";
	m_Header += TERM_STR;

	if (!XSATAUID.IsEmpty())
	{
		m_Header += TOKEN_XUID;
		m_Header += XSATAUID;
		m_Header += TERM_STR;
	}
}


void CSMTPMsg::SetMailText(CString sMailText)
{
	m_MailText = sMailText;
}

BOOL CSMTPMsg::SetMailTextEncoded(const byte* lpBuffer, DWORD dwBufferSize)
{
	if (lpBuffer == NULL) return FALSE;
	if (!dwBufferSize) return FALSE;

	CString sEncodedMailTextBuffer;	

	unsigned char* encbuff = NULL;
	DWORD dwOutSz = 0;
	encbuff = Base64Encode(lpBuffer, dwBufferSize, encbuff, dwOutSz);
	ASSERT(encbuff != NULL);

	DWORD nInsertions = dwOutSz / MIME_MAX_LINE_LEN;
	DWORD dwLastUnitLen = dwOutSz % MIME_MAX_LINE_LEN;

	LPSTR pCS = sEncodedMailTextBuffer.GetBuffer(dwOutSz + 2*nInsertions + 1);
	ASSERT (pCS != NULL);
	memset (pCS, 0x00, dwOutSz + 2*nInsertions + 1);
	sEncodedMailTextBuffer.ReleaseBuffer(0);

	char buffMIMELine[MIME_MAX_LINE_LEN + 3];

	DWORD i=0;
	for (i=0; i<nInsertions; i++)
	{
		memset (buffMIMELine, 0x00, MIME_MAX_LINE_LEN + 3);
		memmove (buffMIMELine, encbuff+(i*MIME_MAX_LINE_LEN), MIME_MAX_LINE_LEN);
		buffMIMELine[MIME_MAX_LINE_LEN] = 0x0D;
		buffMIMELine[MIME_MAX_LINE_LEN + 1] = 0x0A;
		sEncodedMailTextBuffer += buffMIMELine;
	}

	if (dwLastUnitLen)
	{
		memset (buffMIMELine, 0x00, MIME_MAX_LINE_LEN + 3);
		memmove (buffMIMELine, encbuff+(i*MIME_MAX_LINE_LEN), dwLastUnitLen);
		sEncodedMailTextBuffer += buffMIMELine;
	}

	free (encbuff); encbuff = NULL;

	m_MailText = sEncodedMailTextBuffer;

	return TRUE;
}

void CSMTPMsg::AddMIMEHeader()
{
	m_MIMEHeader = TOKEN_MIME_VERSION;
	m_MIMEHeader += "1.0";
	m_MIMEHeader += TERM_STR;
	m_MIMEHeader += TOKEN_MIME_CONTENT_TYPE;
	m_MIMEHeader += "multipart/mixed; ";
	m_MIMEHeader += "boundary=\"";
	m_MIMEHeader += TOKEN_MIME_BOUNDARY;
	m_MIMEHeader += "\"";
	m_MIMEHeader += TERM_STR;

	TheSMTPMessage += m_MIMEHeader;
}

void CSMTPMsg::AddAttachment(CString sAttachPath)
{
	CString DashedAttachPath;
	DashedAttachPath.Empty();
	int DashPos = 0;
	int DashPrev = 0;

	while ((DashPos = sAttachPath.Find ("\\", DashPrev)) != -1)
	{			
		DashedAttachPath += sAttachPath.Mid (DashPrev, DashPos - DashPrev);
		DashedAttachPath += "\\";
		DashedAttachPath += "\\";
		DashPrev = DashPos + 1;
	}
	
	DashedAttachPath += sAttachPath.Mid (DashPrev, sAttachPath.GetLength() - DashPrev);

	AttachmentList.AddHead(DashedAttachPath);
}

BOOL CSMTPMsg::AttachFile(CString sPath)
{
	HANDLE hSearch = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA w32fd = {0};

	CString sFinalPath = sPath;
	unsigned char* lpBuff = NULL;
	CString sAttachedFileBuffer;	

	DWORD size = 0;

	if ((hSearch = FindFirstFile (sPath, &w32fd)) != INVALID_HANDLE_VALUE)
	{
		try 
		{
			CFile file(sFinalPath, CFile::modeRead);
			size = file.GetLength();

			lpBuff = (unsigned char*) malloc(size);
			memset (lpBuff, 0x00, size);

			file.Read((void *) lpBuff, size);

			file.Close();
			
			unsigned char* encbuff = NULL;
			DWORD dwOutSz = 0;
			encbuff = Base64Encode(lpBuff, size, encbuff, dwOutSz);
			ASSERT(encbuff != NULL);
			
			free(lpBuff);

			DWORD nInsertions = dwOutSz / MIME_MAX_LINE_LEN;
			DWORD dwLastUnitLen = dwOutSz % MIME_MAX_LINE_LEN;

			LPSTR pCS = sAttachedFileBuffer.GetBuffer(dwOutSz + 2*nInsertions + 1);
			ASSERT (pCS != NULL);
			memset (pCS, 0x00, dwOutSz + 2*nInsertions + 1);
			sAttachedFileBuffer.ReleaseBuffer(0);

			char buffMIMELine[MIME_MAX_LINE_LEN + 3];

			DWORD i=0;
			for (i=0; i<nInsertions; i++)
			{
				memset (buffMIMELine, 0x00, MIME_MAX_LINE_LEN + 3);
				memmove (buffMIMELine, encbuff+(i*MIME_MAX_LINE_LEN), MIME_MAX_LINE_LEN);
				buffMIMELine[MIME_MAX_LINE_LEN] = 0x0D;
				buffMIMELine[MIME_MAX_LINE_LEN + 1] = 0x0A;
				sAttachedFileBuffer += buffMIMELine;
			}

			if (dwLastUnitLen)
			{
				memset (buffMIMELine, 0x00, MIME_MAX_LINE_LEN + 3);
				memmove (buffMIMELine, encbuff+(i*MIME_MAX_LINE_LEN), dwLastUnitLen);
				sAttachedFileBuffer += buffMIMELine;
			}

			free (encbuff);
		} 
		catch (CFileException *ex) 		
		{
			::FindClose (hSearch);
			::MessageBox(NULL, "Fisierul nu a putut fi citit", sPath, MB_OK | MB_ICONEXCLAMATION);
			ex->Delete();

			return FALSE;
		}
		
		::FindClose (hSearch);
		
		TheSMTPMessage += sAttachedFileBuffer;

		return TRUE;
	}

	return FALSE;
}
