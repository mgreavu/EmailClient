// SMTPMsg.h: interface for the CSMTPMsg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMTPMSG_H__4A6A4D2A_7A0F_4043_A8C0_610A497C6348__INCLUDED_)
#define AFX_SMTPMSG_H__4A6A4D2A_7A0F_4043_A8C0_610A497C6348__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSMTPMsg  
{
public:
	void AddAttachment(CString sAttachPath);
	void SetMailText (CString sMailText);
	BOOL SetMailTextEncoded(const byte* lpBuffer, DWORD dwBufferSize);
	CString GetHeader();
	void SetHeaderMembers (CString sFromName, CString sFromAddr, CString sToName, CString sToAddr, CString sSubject, CString sDate, CString XSATAUID);
	CString GetMessage ();
	CSMTPMsg();
	virtual ~CSMTPMsg();

protected:
	CString TheSMTPMessage;
	CString	m_FromName;
	CString	m_FromAddr;
	CString	m_ToName;
	CString	m_ToAddress;
	CString	m_Subject;
	CString	m_Date;
	CString m_MailText;

private:
	void AddMIMEHeader();
	BOOL AttachFile(CString sPath);
	CStringList	AttachmentList;
	CString m_Header;
	CString m_MIMEHeader;
};

#endif // !defined(AFX_SMTPMSG_H__4A6A4D2A_7A0F_4043_A8C0_610A497C6348__INCLUDED_)
