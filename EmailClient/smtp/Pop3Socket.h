#if !defined(AFX_POP3SOCKET_H__4A5EE5E9_4431_4CBB_9CD1_D21D091AE405__INCLUDED_)
#define AFX_POP3SOCKET_H__4A5EE5E9_4431_4CBB_9CD1_D21D091AE405__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Pop3Socket.h : header file
//

#include "afxsock.h"

#define SEND_BUFF_SZ	 2048
#define RCV_BUFF_SZ		 4096		

//Operations
#define OP_HELLO			 1
#define OP_CAPA				 2
#define OP_USER_AUTH		 3
#define OP_PASS_AUTH		 4
#define OP_STAT				 5
#define OP_LIST				 6
#define OP_UIDL				 7
#define OP_RETR				 8
#define OP_DELE				 9
#define OP_QUIT				10

//Interface
#define UWM_PROG_BAR		WM_APP + 0x356
#define PROG_BAR_CLOSE		0
#define PROG_BAR_RANGE		1
#define PROG_BAR_NOW		2
#define PROG_BAR_FULL		3


//Commands
#define CMD_QUIT			"QUIT"
#define CMD_USER_AUTH		"USER"
#define CMD_PASS_AUTH		"PASS"
#define CMD_STAT			"STAT"
#define CMD_RETR			"RETR"
#define CMD_DELETE			"DELE"
#define CMD_UIDL			"UIDL"	/* RFC 1939 optional command */
#define CMD_LIST			"LIST"
#define CMD_CAPA			"CAPA"	/* RFC 2449 extension command */

#define MAX_UIDL_LEN		70

typedef struct tagMsgInfo
{
	DWORD  dwMsgIndex;
	DWORD  dwMsgSize;
	char   sMsgID[MAX_UIDL_LEN + 1];
} MsgInfo, *LPMSGINFO;

#define SZ_MSGINFO	sizeof (MsgInfo)

/////////////////////////////////////////////////////////////////////////////
// CPop3Socket command target

class CPop3Socket : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CPop3Socket();
	virtual ~CPop3Socket();
	CString m_ServerAddress;
	UINT    m_ServerPort;
	BOOL	m_bLeaveMessages;
	CString m_UserName;
	CString m_UserPassw;

// Overrides
public:
	void SetAccountFolder (CString sPath);
	void SetMotherWindow(HWND hMom);
	BOOL IsConnected();
	void CloseConnection();
	void ConnectToServer();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPop3Socket)
	public:
	virtual void OnConnect(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CPop3Socket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	CString AccountFolder;
	HWND hMother;
	BOOL m_Connected;
private:
	CStringArray InboxUIDLs;
	CString CurrentDownloadFilePath;
	BOOL GetInboxUIDLs();
	BOOL FetchNextDownloadIndex();
	BOOL ProcessCapaResponse();
	BOOL bFirstFileUnit;
	HANDLE hEmailFile;
	void ResetReceiveBuffer ();
	BOOL CloseEmailFile();
	BOOL CreateEmailFile ();
	BOOL MoveDownloadedFileToInbox();
	BOOL ProcessRetrResponse();
	LPMSGINFO lpMsgInfo;
	DWORD dwMessagesInMailbox;
	DWORD dwDownloadMsgIndex;
	char lpReceiveBuffer[RCV_BUFF_SZ];
	char lpSendBuffer[SEND_BUFF_SZ]; 
	BYTE m_NextOp;
	int  iSendBufferLen;
	int  iBytesSent;
	int  iBytesReceived;
	int  iTotalBytesWrittenToFile;
	void ShowErrorMessage(int iErrorCode);
	BOOL IsReplyComplete ();
	BOOL ProcessQuitResponse();
	void BuildSendBuffer ();
	BOOL DispatchForProcessing();
	BOOL ProcessHelloResponse();
	BOOL ProcessStatResponse();
	BOOL ProcessPassAuthResponse();
	BOOL ProcessUserAuthResponse();
	BOOL ProcessDeleteResponse();
	BOOL ProcessUidlResponse();
	BOOL ProcessListResponse();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POP3SOCKET_H__4A5EE5E9_4431_4CBB_9CD1_D21D091AE405__INCLUDED_)
