#include "stdafx.h"

//Toolbar ID
#define IDC_TBMAIN            0x10001
#define TBSTYLE_TERM          0xFFFFFFFF

//Messages for the toolbar buttons
#define WM_MP_APP				WM_APP + 450

#define WM_TBM_MESSAGES			WM_MP_APP
#define WM_TBM_ADDRBOOK			WM_MP_APP + 1
#define WM_TBM_ACCOUNTS			WM_MP_APP + 2
#define WM_TBM_EXIT				WM_MP_APP + 3


UINT tbMainStyles[] = { TBSTYLE_BUTTON | TBSTYLE_DROPDOWN,
						TBSTYLE_BUTTON | TBSTYLE_DROPDOWN,
						TBSTYLE_BUTTON | TBSTYLE_DROPDOWN,
						TBSTYLE_BUTTON,
						TBSTYLE_TERM};

UINT tbMainStates[] = { TBSTATE_ENABLED,  
						TBSTATE_ENABLED,  
						TBSTATE_ENABLED,                        
						TBSTATE_ENABLED};

UINT tbMainCommands[] = { WM_TBM_MESSAGES,
                          WM_TBM_ADDRBOOK, 
						  WM_TBM_ACCOUNTS,
						  WM_TBM_EXIT};    

