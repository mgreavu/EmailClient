// AccountsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "AccountsDlg.h"
#include "AccountPropertiesDlg.h"
#include "GeneralPropertiesDlg.h"
#include "ServersPropertiesDlg.h"
#include "ConnPropertiesDlg.h"
#include "AdvancedPropertiesDlg.h"
#include "AddAccountDlg.h"
#include "MailAccount.h"
#include "Globals.h"
#include "AppSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CAppSettings ECSettings;

/////////////////////////////////////////////////////////////////////////////
// CAccountsDlg dialog


CAccountsDlg::CAccountsDlg(CWnd* pParent /*=NULL*/): CDialog(CAccountsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAccountsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAccountsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAccountsDlg)
	DDX_Control(pDX, IDC_ACCOUNTS_LIST, m_AccList);
	DDX_Control(pDX, IDC_ADD_BTN, m_btnAdd);
	DDX_Control(pDX, IDC_REMOVE_BTN, m_btnRemove);
	DDX_Control(pDX, IDC_PROP_BTN, m_btnProp);
	DDX_Control(pDX, IDC_DEFAULT_BTN, m_btnDefault);
	DDX_Control(pDX, IDC_SET_FOLDER, m_btnSetFolder);
	DDX_Control(pDX, IDC_EXPORT_BTN, m_btnExport);
	DDX_Control(pDX, IDC_IMPORT_BTN, m_btnImport);
	DDX_Control(pDX, IDC_CLOSE_BTN, m_btnClose);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAccountsDlg, CDialog)

ON_MESSAGE(UWM_ACC_INSERTED, OnAccInserted)
	//{{AFX_MSG_MAP(CAccountsDlg)
	ON_BN_CLICKED(IDC_PROP_BTN, OnPropBtn)
	ON_BN_CLICKED(IDC_ADD_BTN, OnAddBtn)
	ON_BN_CLICKED(IDC_CLOSE_BTN, OnCloseBtn)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_REMOVE_BTN, OnRemoveBtn)
	ON_NOTIFY(NM_DBLCLK, IDC_ACCOUNTS_LIST, OnDblclkAccountsList)
	ON_BN_CLICKED(IDC_IMPORT_BTN, OnImportBtn)
	ON_BN_CLICKED(IDC_EXPORT_BTN, OnExportBtn)
	ON_BN_CLICKED(IDC_DEFAULT_BTN, OnDefaultBtn)
	ON_NOTIFY(LVN_KEYDOWN, IDC_ACCOUNTS_LIST, OnKeydownAccountsList)
	ON_BN_CLICKED(IDC_SET_FOLDER, OnSetFolder)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAccountsDlg message handlers

void CAccountsDlg::OnOK() 
{
}

void CAccountsDlg::OnCancel() 
{
	CDialog::OnCancel();
}


void CAccountsDlg::OnPropBtn() 
{

	if (m_AccList.GetNextItem(-1, LVIS_SELECTED) != -1)
	{
		if (m_AccList.GetSelectionMark() == -1)
		{
			m_AccList.SetSelectionMark(0);
		}
		int iListPointer = m_AccList.GetSelectionMark();
		CString sField = m_AccList.GetItemText(iListPointer, 0);		
		if (sField.IsEmpty()) return;

		for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++)
		{
			if ((*AccListIterator).GetMailAccountName() == sField)
			{
				break;
			}
		}	

		CAccountPropertiesDlg  PropertiesDlg("Account Properties");
		CGeneralPropertiesDlg  GeneralPropDlg;
		CServersPropertiesDlg  ServersPropDlg;
		CConnPropertiesDlg     ConnPropDlg;
		CAdvancedPropertiesDlg AdvPropDlg;
		
		PropertiesDlg.m_psh.dwFlags |= PSH_NOAPPLYNOW;	
		
		PropertiesDlg.AddPage(&GeneralPropDlg);
		PropertiesDlg.AddPage(&ServersPropDlg);
		PropertiesDlg.AddPage(&ConnPropDlg);
		PropertiesDlg.AddPage(&AdvPropDlg);
		
		GeneralPropDlg.m_bOnChange = FALSE;
		ServersPropDlg.m_bOnChange = FALSE;
		ServersPropDlg.m_bOnChange2 = FALSE;
		ConnPropDlg.m_bOnChange = FALSE;
		AdvPropDlg.m_bOnChange = FALSE;
		
		
		if(PropertiesDlg.DoModal() == IDOK)
		{
			CString AccSettingsFile = EmailClientFolder + (*AccListIterator).GetAccountName();
			AccSettingsFile += "\\";
			AccSettingsFile += "accountinfo.dat";
			(*AccListIterator).SetAccountFilePath(AccSettingsFile);	
			if(GeneralPropDlg.m_bOnChange)
			{
				(*AccListIterator).SetMailAccountName(GeneralPropDlg.m_sMailAcount);
				(*AccListIterator).SetSMTPEmailAddress(GeneralPropDlg.m_sEmailAdress);
				(*AccListIterator).SetSMTPDisplayName(GeneralPropDlg.m_sName);
				(*AccListIterator).SetIncludeAccountInCheck(TRUE);
				(*AccListIterator).SetOrganization(GeneralPropDlg.m_sOrganization);
			}
			if(ServersPropDlg.m_bOnChange)
			{
				(*AccListIterator).SetPOP3ServerName(ServersPropDlg.m_sPOP3ServerAddr);
				(*AccListIterator).SetSMTPServerName(ServersPropDlg.m_sSMTPServerAddr);
				(*AccListIterator).SetPOP3UserName(ServersPropDlg.m_sPOP3AcountName);
				(*AccListIterator).SetPOP3Password(ServersPropDlg.m_sPOP3AcountPsw);
				(*AccListIterator).SetPOP3RememberPassword(ServersPropDlg.m_RememberPassword);
				(*AccListIterator).SetSMTPServerRequiresAuthentication(ServersPropDlg.m_SMTPAuthOpt);
			}
			if(ServersPropDlg.m_bOnChange2)
			{
				(*AccListIterator).SetSMTPUsePOP3Settings(ServersPropDlg.SettDlg.m_AuthOpt);
				if(ServersPropDlg.SettDlg.m_AuthOpt == 0)
				{
					(*AccListIterator).SetSMTPUserName((*AccListIterator).GetPOP3UserName());
					(*AccListIterator).SetSMTPPassword((*AccListIterator).GetPOP3Password());
				}
				else
				{
					(*AccListIterator).SetSMTPUserName(ServersPropDlg.SettDlg.m_sSMTPName);
					(*AccListIterator).SetSMTPPassword(ServersPropDlg.SettDlg.m_sSMTPPsw);
				}
			}
			if(ConnPropDlg.m_bOnChange)
			{
				(*AccListIterator).SetConnectionType(ConnPropDlg.m_Connection);
				if(ConnPropDlg.m_Connection == 1)
					(*AccListIterator).SetRasAccount(ConnPropDlg.m_sRasAccount);
				
			}
			if(AdvPropDlg.m_bOnChange)
			{
				(*AccListIterator).SetSMTPPort(AdvPropDlg.m_iSMTPPort);	
				(*AccListIterator).SetPOP3Port(AdvPropDlg.m_iPOP3Port);
				(*AccListIterator).SetPOP3Timeout(AdvPropDlg.m_iPOP3Timeout);
				(*AccListIterator).SetSMTPTimeout(AdvPropDlg.m_iPOP3Timeout);
				(*AccListIterator).SetLeaveMailOnServer(AdvPropDlg.m_LeaveMailOnServer);
				(*AccListIterator).SetRemoveAfter(AdvPropDlg.m_RemoveAfter);
			}
			if((GeneralPropDlg.m_bOnChange)||(ServersPropDlg.m_bOnChange)||(ServersPropDlg.m_bOnChange2)||(AdvPropDlg.m_bOnChange)||(ConnPropDlg.m_bOnChange))
			{
				(*AccListIterator).WriteAccountData();
				m_AccList.DeleteAllItems();
				OnTimer(1001);
			}
			
		}
		m_AccList.SetFocus();
		m_AccList.SetSelectionMark(m_iSelected);
		UpdateData(FALSE);
	}
	else 
	{
		CString sMsg;
		sMsg.LoadString(IDS_SEL_ACCOUNT);
		
		AfxMessageBox(sMsg);
	}
}

void CAccountsDlg::OnAddBtn() 
{
	CAddAccountDlg AddDlg;

	AddDlg.DoModal();
}

void CAccountsDlg::OnCloseBtn() 
{

	CDialog::OnCancel();	
}

BOOL CAccountsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString sMsg;
	sMsg.LoadString(IDS_ACC_NAME);

	CRect rcWindow;
	::GetClientRect(m_AccList.m_hWnd, &rcWindow);

	m_AccList.InsertColumn(0, sMsg, LVCFMT_LEFT, rcWindow.Width());
	m_AccList.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	SetTimer (1001, 100, NULL);		
	OwnerDrawButtons();

	return TRUE;
}

void CAccountsDlg::OnTimer(UINT nIDEvent) 
{
	AccList::iterator AccListIterator;

	KillTimer (1001);
	GetAccountList();

	int nIndex = 0;
	for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++, nIndex++)	
	{
		m_AccList.InsertItem(nIndex, (*AccListIterator).GetMailAccountName());
	}
	
	if (m_AccList.GetNextItem(-1, LVIS_SELECTED) == -1)
	{
		m_AccList.SetFocus();
		m_AccList.SetItemState(m_iSelected, LVIS_SELECTED, LVIS_SELECTED);
		m_AccList.SetSelectionMark(m_iSelected);			
		AccListIterator = AccountList.begin();
	}

	CDialog::OnTimer(nIDEvent);
}

LRESULT CAccountsDlg::OnAccInserted(WPARAM wParam, LPARAM lParam)
{
	AccList::iterator AccListIterator;
	GetAccountList();
	int nIndex = 0;
	m_AccList.DeleteAllItems();
	for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++, nIndex++)	
		m_AccList.InsertItem(nIndex, (*AccListIterator).GetMailAccountName());

	return 0;
}

void CAccountsDlg::OnRemoveBtn() 
{
	if (m_AccList.GetNextItem(-1, LVIS_SELECTED) != -1)
	{

		CString sUserFolder = EmailClientFolder;
		int iListPointer = m_AccList.GetSelectionMark();
		CString sMailAccName = m_AccList.GetItemText (iListPointer, 0);
		
		CString sMsg;
		sMsg.LoadString(IDS_DEL_ACCOUNT);
		
		int nRes = MessageBox(sMsg ,sMailAccName, MB_YESNO | MB_ICONQUESTION);
		if (nRes == IDYES)
		{
			int nIndex = 0;
			for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator ++, nIndex++)
			{
				if (!(*AccListIterator).GetMailAccountName().Compare(sMailAccName)) 
				{
					sUserFolder += (*AccListIterator).GetAccountName();
					break;
				}
			}
			
			DeleteDirectory (sUserFolder);
			m_AccList.DeleteItem (iListPointer);
	/*
			m_AccList.DeleteAllItems();
			GetAccountList();
			nIndex = 0;
			for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++, nIndex++)	
				m_AccList.InsertItem(nIndex, (*AccListIterator).GetMailAccountName());
	*/
		}
	}
	else
	{
		CString sMsg;
		sMsg.LoadString(IDS_NO_ACC_SEL);
		AfxMessageBox(sMsg);
	}
}

void CAccountsDlg::DeleteDirectory(CString Path)
{
	CString sDirPath = Path + "\\*.*";
	CString InsidePath = Path;
	InsidePath += "\\";
	
	WIN32_FIND_DATA fData;
	
	HANDLE fHnd = FindFirstFile(sDirPath, &fData);
	
	if (fHnd != INVALID_HANDLE_VALUE) 
	{
		if(FindNextFile(fHnd, &fData)) 
		{
			while (FindNextFile(fHnd, &fData)) 
			{								
				
				InsidePath += fData.cFileName;
				
				if ((fData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY) 
				{								
					DeleteDirectory(InsidePath);
				}
				else 
				{
					SetFileAttributes(sDirPath, FILE_ATTRIBUTE_ARCHIVE);					
					if (!DeleteFile(InsidePath))
					{	

					}
				}
				InsidePath = Path;
				InsidePath += "\\";
			}
		}
	}
	
	FindClose(fHnd);	
	
	SetFileAttributes(Path, FILE_ATTRIBUTE_ARCHIVE);	
	if (!RemoveDirectory(Path))		
	{

	}
	
}

void CAccountsDlg::OnDblclkAccountsList(NMHDR* pNMHDR, LRESULT* pResult) 
{

	OnPropBtn();
	
	*pResult = 0;
}

void CAccountsDlg::OnImportBtn() 
{
	CString sMsg;
	sMsg.LoadString(IDS_EXP_FILES);

	CFileDialog FileDlg (TRUE ,NULL ,NULL,OFN_HIDEREADONLY, sMsg, NULL);
	FileDlg.DoModal();
	int nIndex , nRecords;
	CString sFieldData;
	CMailAccount oMailAccount;
	CString sUserFolder;
	CString temp = FileDlg.m_ofn.lpstrFile;
	if(!(temp.IsEmpty()))
	{
		
		for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++)
		{
			sUserFolder = EmailClientFolder;
			sUserFolder += (*AccListIterator).GetAccountName();
			DeleteDirectory (sUserFolder);
		}
		AccountList.clear();
		
		CFile* pFile = new CFile();
		pFile->Open (FileDlg.m_ofn.lpstrFile, CFile::modeReadWrite);
		nRecords = pFile->GetLength();
		pFile->Close();
		delete pFile;
		
		
		nIndex=0;
		while(nIndex<nRecords)
		{
			oMailAccount.SetAccountFilePath(FileDlg.m_ofn.lpstrFile);
			nIndex = oMailAccount.ReadExport(nIndex);
			sFieldData = oMailAccount.GetAccountName();
			if (!CreateDirectory(EmailClientFolder + sFieldData, NULL))
			{
				sMsg.LoadString(IDS_ERR_CR_USER_FOLD);
				AfxMessageBox(sMsg);
			}
			else
			{
				CString AccSettingsFile = EmailClientFolder + sFieldData;
				AccSettingsFile += "\\";
				AccSettingsFile += "accountinfo.dat";
				oMailAccount.SetAccountFilePath(AccSettingsFile);
				oMailAccount.WriteAccountData();
				AccSettingsFile = EmailClientFolder;
				AccSettingsFile += sFieldData;
				AccSettingsFile += "\\";
				AccSettingsFile += INBOX_FOLDER_NAME;
				if (!CreateDirectory(AccSettingsFile, NULL))
				{
					sMsg.LoadString(IDS_ERR_CR_INBOX);
					AfxMessageBox(sMsg);
				}
				else {
					AccSettingsFile = EmailClientFolder;
					AccSettingsFile += sFieldData;
					AccSettingsFile += "\\";
					AccSettingsFile += OUTBOX_FOLDER_NAME;
					if (!CreateDirectory(AccSettingsFile, NULL))
					{					
						sMsg.LoadString(IDS_ERR_CR_OUTBOX);
						AfxMessageBox(sMsg);
					}
					else {
						AccSettingsFile = EmailClientFolder;
						AccSettingsFile += sFieldData;
						AccSettingsFile += "\\";
						AccSettingsFile += CONTACTS_FOLDER_NAME;
						if (!CreateDirectory(AccSettingsFile, NULL))
						{
							sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
							AfxMessageBox(sMsg);
						}
						else {
							AccSettingsFile = EmailClientFolder;
							AccSettingsFile += sFieldData;
							AccSettingsFile += "\\";
							AccSettingsFile += DELETED_FOLDER_NAME;
							if (!CreateDirectory(AccSettingsFile, NULL))
							{
								sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
								AfxMessageBox(sMsg);
								return;
							}
							else {
								AccSettingsFile = EmailClientFolder;
								AccSettingsFile += sFieldData;
								AccSettingsFile += "\\";
								AccSettingsFile += SENT_FOLDER_NAME;
								if (!CreateDirectory(AccSettingsFile, NULL))
								{
									sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
									AfxMessageBox(sMsg);
									return;
								}
								else {
									AccSettingsFile = EmailClientFolder;
									AccSettingsFile += sFieldData;
									AccSettingsFile += "\\";
									AccSettingsFile += DRAFT_FOLDER_NAME;
									if (!CreateDirectory(AccSettingsFile, NULL))
									{
										sMsg.LoadString(IDS_ERR_CR_CONTACTS);					
										AfxMessageBox(sMsg);
										return;
									}
								}
							}
						}
					}
				}
			}

			
			
		}
		
		nIndex = 0;
		m_AccList.DeleteAllItems();
		OnTimer(1001);
		
	}		
}

void CAccountsDlg::OnExportBtn() 
{
	CString sMsg;
	sMsg.LoadString(IDS_EXP_FILES);
	
	CFileDialog FileDlg (FALSE,LPCTSTR ("exp"),NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, sMsg, NULL);
	FileDlg.DoModal();
	
	CString temp = FileDlg.m_ofn.lpstrFile;
	if(!(temp.IsEmpty()))
	{
		CFile* pFile = new CFile();
		pFile->Open (FileDlg.m_ofn.lpstrFile, CFile::modeCreate);
		pFile->Close();
		for (AccListIterator = AccountList.begin(); AccListIterator != AccountList.end(); AccListIterator++)
		{
			(*AccListIterator).SetAccountFilePath(FileDlg.m_ofn.lpstrFile);
			(*AccListIterator).WriteExport();
		}
		delete pFile;
	}	
}

void CAccountsDlg::OnDefaultBtn() 
{
	m_AccList.SetFocus();
	m_iSelected = m_AccList.GetSelectionMark();
	ECSettings.iCurrentAccount = m_iSelected;

	HWND hParent = ::GetParent(m_hWnd);
	if (::IsWindow(hParent)) ::PostMessage(hParent, UWM_ACC_CHANGED, (WPARAM)m_iSelected, 0);
}

void CAccountsDlg::OnKeydownAccountsList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	if(pLVKeyDow->wVKey == VK_DELETE)
	{
		OnRemoveBtn();
	}
	else
		m_iSelected = m_AccList.GetSelectionMark();
	
	*pResult = 0;
}

void CAccountsDlg::OnSetFolder() 
{
	CString sSettFile = "ECInit.dat";
	CString sMsg;
	
	CString m_Path;
	BROWSEINFO bi = { 0 };
	sMsg.LoadString(IDS_SEL_ACC_FOLD);
	
	bi.lpszTitle = sMsg;
	LPITEMIDLIST pidl = SHBrowseForFolder ( &bi );
	if ( pidl != 0 )
	{
		TCHAR path[MAX_PATH];
		if ( SHGetPathFromIDList ( pidl, path ) )
		{
			m_Path = path;
			if (m_Path.Mid((m_Path.GetLength() - 1),1) == "\\") 
			{
				m_Path = m_Path.Mid(0, m_Path.GetLength() - 1);
			}

			m_Path += "?";
			BOOL bResult = FALSE;
			CFile* settFile = new CFile();
			bResult = settFile->Open (sSettFile, CFile::modeReadWrite);
			if(!bResult)
			{
				bResult = settFile->Open (sSettFile, CFile::modeCreate);
				if(!bResult)
				{
					sMsg.LoadString(IDS_ERR_CR_SETT);					
					AfxMessageBox(sMsg);
					OnOK();
				}
				else
				{
					settFile->Close();
					::SetFileAttributes((LPCTSTR)sSettFile, FILE_ATTRIBUTE_HIDDEN);			
					bResult = settFile->Open (sSettFile, CFile::modeReadWrite);
					settFile->SeekToBegin();
				}
			}
			if(!bResult)
			{
				CString sMsg;
				sMsg.LoadString(IDS_ERR_OP_SETT);				
				AfxMessageBox(sMsg);
				OnOK();
			}
			else
			{
				settFile->SetLength(0);
				
				settFile->Write(m_Path,m_Path.GetLength());
				m_Path.Empty();
				settFile->SeekToBegin();
				int size = settFile->GetLength();
				unsigned char* Buff;
				Buff = (unsigned char*) malloc(size);
				memset ((void *) Buff, 0x00, size);
				settFile->Read(Buff,size);
				m_Path = (CString) Buff;
/*
				if (m_Path.Find("KEY") == 0)
				{
					m_Path.SetAt(0, sKeyLetter[0]);
					m_Path.SetAt(1, ':');
					m_Path.SetAt(2, '\\');
				}
*/
				m_Path = m_Path.Left(m_Path.Find("?",0));
				sMsg.LoadString(IDS_MOVE_ACC_TO_NEW_FOLD);
				if(AfxMessageBox(sMsg, MB_YESNO) == IDYES)
				{
					MoveFolder(EmailClientFolder, m_Path + "\\Accounts\\");
					RemoveDirectory(EmailClientFolder);
				}
				else
				{
					if(!CreateDirectory(m_Path +"\\Accounts",NULL))
					{
						sMsg.LoadString(IDS_ERR_CR_FOLD);					
						AfxMessageBox(sMsg);
					}
				}
				EmailClientFolder = m_Path;
				EmailClientFolder += "\\Accounts\\";
				settFile->Close();
				m_AccList.DeleteAllItems();
				AccountList.empty();
				GetAccountList();
				OnTimer(1001);
			}
			
			delete settFile;
			
			
		}
	}

	::SetFileAttributes((LPCTSTR)sSettFile, FILE_ATTRIBUTE_HIDDEN);
}

BOOL CAccountsDlg::MoveFolder(CString SourcePath, CString DestPath)
{
	WIN32_FIND_DATA w32fd;
	HANDLE hSearch;
	CString SPath, DPath;
	CString ScanPath;
	
		CreateDirectory(DestPath, NULL);
		ScanPath = SourcePath;
		ScanPath += "\\";
		ScanPath += "*.*";
		hSearch = FindFirstFile(ScanPath, &w32fd);
		if (hSearch != INVALID_HANDLE_VALUE) 
		{
			while(1) 
			{
				if ((_stricmp(w32fd.cFileName, ".") != 0) && (_stricmp(w32fd.cFileName, "..") != 0))
				{
					DPath = DestPath;
					DPath += "\\";
					DPath += w32fd.cFileName;
					
					SPath = SourcePath;
					SPath += "\\";
					SPath += w32fd.cFileName;
					
					if (w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						MoveFolder(SPath, DPath);
						RemoveDirectory(SPath);
					}
					else 
					{
						if (!MoveFile(SPath, DPath)) return FALSE;

					}
				}
				
				if (!FindNextFile(hSearch, &w32fd)) break;
			};
			FindClose(hSearch);
		}

	return TRUE;
}

void CAccountsDlg::OwnerDrawButtons()
{
	m_btnAdd.SetMaskedBitmap( IDB_ADD, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnRemove.SetMaskedBitmap( IDB_REMOVE, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnProp.SetMaskedBitmap( IDB_PROPR, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnDefault.SetMaskedBitmap( IDB_DEFAULT, 16, 16, RGB( 255, 255, 255 ) );

	m_btnSetFolder.SetMaskedBitmap( IDB_FOLDER, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnExport.SetMaskedBitmap( IDB_EXPORT, 17, 17, RGB( 255, 255, 255 ) );
	
	m_btnImport.SetMaskedBitmap( IDB_IMPORT, 17, 17, RGB( 255, 255, 255 ) );
	
	m_btnClose.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );	
}
