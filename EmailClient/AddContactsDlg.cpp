// AddContactsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "AddContactsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddContactsDlg dialog


CAddContactsDlg::CAddContactsDlg(CWnd* pParent )
	: CDialog(CAddContactsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddContactsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


CAddContactsDlg::CAddContactsDlg(CContact contact, CWnd* pParent /*=NULL*/)
	: CDialog(CAddContactsDlg::IDD, pParent)
{
	m_contact = contact;
}


void CAddContactsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddContactsDlg)
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddContactsDlg, CDialog)
	//{{AFX_MSG_MAP(CAddContactsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddContactsDlg message handlers


void CAddContactsDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString sFullName, sLastName, sEmail;
	GetDlgItem(IDC_FULLNAME_EDIT)->GetWindowText(sFullName);
	GetDlgItem(IDC_EMAIL_EDIT)->GetWindowText(sEmail);
	if(sFullName.IsEmpty() || sEmail.IsEmpty())
	{
		CString sMsg;
		sMsg.LoadString(IDS_NEED_NAME_EMAIL);		
		AfxMessageBox(sMsg);
	}
	else{
		GetDlgItem(IDC_NICKNAME_EDIT)->GetWindowText(m_contact.m_NickName);
		GetDlgItem(IDC_FULLNAME_EDIT)->GetWindowText(m_contact.m_FullName);
		GetDlgItem(IDC_EMAIL_EDIT)->GetWindowText(m_contact.m_EmailAdress);
		GetDlgItem(IDC_CITY_EDIT)->GetWindowText(m_contact.m_City);
		GetDlgItem(IDC_COUNTRY_EDIT)->GetWindowText(m_contact.m_Country);
		GetDlgItem(IDC_STREET_EDIT)->GetWindowText(m_contact.m_Street);
		GetDlgItem(IDC_ZIP_EDIT)->GetWindowText(m_contact.m_Zip);
		GetDlgItem(IDC_PHONEMOBILE_EDIT)->GetWindowText(m_contact.m_PhoneMobile);
		GetDlgItem(IDC_PHONEOFFICE_EDIT)->GetWindowText(m_contact.m_PhoneOffice);
		GetDlgItem(IDC_PHONEHOME_EDIT)->GetWindowText(m_contact.m_PhoneHome);
		CDialog::OnOK();
	}
}

BOOL CAddContactsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if(!m_contact.m_EmailAdress.IsEmpty()){	//we are using the dialog to diplay a contact's properties
		GetDlgItem(IDOK)->SetWindowText("Ok");
		GetDlgItem(IDC_NICKNAME_EDIT)->SetWindowText(m_contact.m_NickName);
		GetDlgItem(IDC_FULLNAME_EDIT)->SetWindowText(m_contact.m_FullName);
		GetDlgItem(IDC_EMAIL_EDIT)->SetWindowText(m_contact.m_EmailAdress);
		GetDlgItem(IDC_CITY_EDIT)->SetWindowText(m_contact.m_City);
		GetDlgItem(IDC_COUNTRY_EDIT)->SetWindowText(m_contact.m_Country);
		GetDlgItem(IDC_STREET_EDIT)->SetWindowText(m_contact.m_Street);
		GetDlgItem(IDC_ZIP_EDIT)->SetWindowText(m_contact.m_Zip);
		GetDlgItem(IDC_PHONEMOBILE_EDIT)->SetWindowText(m_contact.m_PhoneMobile);
		GetDlgItem(IDC_PHONEOFFICE_EDIT)->SetWindowText(m_contact.m_PhoneOffice);
		GetDlgItem(IDC_PHONEHOME_EDIT)->SetWindowText(m_contact.m_PhoneHome);
	}
	else{	//we are using the dialog to add a new contact
	
	}

	OwnerDrawButtons();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

CContact CAddContactsDlg::getContact()
{
	return m_contact;
}

void CAddContactsDlg::OwnerDrawButtons()
{
	m_btnOk.SetMaskedBitmap( IDB_ADD, 16, 16, RGB( 255, 255, 255 ) );

	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );
}


