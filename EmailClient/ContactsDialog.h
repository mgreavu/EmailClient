#include "AddressBook.h"
#include "EmailClientDlg.h"

#if !defined(AFX_CONTACTSDIALOG_H__DBAB5AA0_E7EE_442C_A60E_D9C74578F616__INCLUDED_)
#define AFX_CONTACTSDIALOG_H__DBAB5AA0_E7EE_442C_A60E_D9C74578F616__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ContactsDialog.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CContactsDialog dialog

class CContactsDialog : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	int m_iSelected;
	CContactsDialog(CWnd* pParent = NULL);   // standard constructor
	CContactsDialog(CAddressBook& adressBook, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CContactsDialog)
	enum { IDD = IDD_CONTACTS_DIALOG };
	CListCtrl	m_ContactList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CContactsDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSXButton m_btnAdd;
	CSXButton m_btnRemove;
	CSXButton m_btnDetails;
	CSXButton m_btnExport;
	CSXButton m_btnImport;
	CSXButton m_btnCancel;
	// Generated message map functions
	//{{AFX_MSG(CContactsDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddBtn();
	afx_msg void OnDetailsBtn();
	afx_msg void OnRemoveBtn();
	afx_msg void OnExportBtn();
	afx_msg void OnImportBtn();
	afx_msg void OnKeydownContactsList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkListContacts(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void WriteContacts();
	void ReadContacts();
	void refresContactsList();
	CEmailClientDlg* parentWindow;
	CAddressBook* m_addressBook;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTACTSDIALOG_H__DBAB5AA0_E7EE_442C_A60E_D9C74578F616__INCLUDED_)
