// ReciveProgres.cpp : implementation file
//

#include "stdafx.h"
#include "Globals.h"
#include "AccountListTypes.h"
#include "Resource.h"
#include "ReciveProgres.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReciveProgres dialog


CReciveProgres::CReciveProgres(CWnd* pParent /*=NULL*/): CDialog(CReciveProgres::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReciveProgres)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CReciveProgres::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReciveProgres)
	DDX_Control(pDX, IDC_RECIVE_PROGRES, m_ReciveProgres);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CReciveProgres, CDialog)
	//{{AFX_MSG_MAP(CReciveProgres)
	ON_WM_TIMER()
	ON_MESSAGE(UWM_PROG_BAR, OnOpComplete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReciveProgres message handlers

BOOL CReciveProgres::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	dwTotalBytes = 0;
	dwBytesCurrently = 0;
	
	if(m_iSelected == (-1))
	{
		CString sMsg;
		sMsg.LoadString(IDS_ERR_NO_ACC_SEL);
		AfxMessageBox(sMsg);

		OnCancel();

		return TRUE;
	}

	m_Pop3Sock.SetMotherWindow(m_hWnd);

	AccListIterator = AccountList.begin();
	for (int nIndex = 0;nIndex < m_iSelected;nIndex++) AccListIterator++;

	CString sAccPath = EmailClientFolder;
	sAccPath += (*AccListIterator).GetAccountName();
	sAccPath += "\\";
	sAccPath += INBOX_FOLDER_NAME;
	sAccPath += "\\";

	m_Pop3Sock.SetAccountFolder(sAccPath);

	m_Pop3Sock.m_ServerAddress = (*AccListIterator).GetPOP3ServerName();
	m_Pop3Sock.m_ServerPort = (*AccListIterator).GetPOP3Port();
	m_Pop3Sock.m_UserName = (*AccListIterator).GetPOP3UserName();
	m_Pop3Sock.m_UserPassw = (*AccListIterator).GetPOP3Password();
	m_Pop3Sock.m_bLeaveMessages = (*AccListIterator).GetLeaveMailOnServer();
//	m_Pop3Sock.nrDays = (*AccListIterator).GetRemoveAfter();
	
	SetWindowText((*AccListIterator).GetPOP3ServerName());

	m_btnCancel.SetMaskedBitmap(IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ));
	SetTimer (1005, 60000*((*AccListIterator).GetPOP3Timeout()), NULL);	

	LOGFONT fnt;
    fnt.lfHeight = 16; 
    fnt.lfWidth = 0; 
    fnt.lfEscapement = 0; 
    fnt.lfOrientation = 0; 
    fnt.lfWeight = 400; 
    fnt.lfItalic = FALSE; 
    fnt.lfUnderline = 0; 
    fnt.lfStrikeOut = 0; 
    fnt.lfCharSet = 0; 
    fnt.lfOutPrecision = 0; 
    fnt.lfClipPrecision = 0; 
    fnt.lfQuality = ANTIALIASED_QUALITY; 
    fnt.lfPitchAndFamily = 0; 
    memmove (fnt.lfFaceName, "Arial", strlen("Arial")); 
	m_ReciveProgres.SetFont (fnt);		

	m_ReciveProgres.SetRange(0, 100);	
	if (!m_Pop3Sock.IsConnected()) m_Pop3Sock.ConnectToServer();
			
	return TRUE;
}

LRESULT CReciveProgres::OnOpComplete(WPARAM w, LPARAM l)
{
	switch(w) 
	{
		case PROG_BAR_RANGE:
			{
				dwTotalBytes = (DWORD)l;
			}
			break;

		case PROG_BAR_NOW:
			{
				dwBytesCurrently += (DWORD)l;
				
				float nPos = (float)dwBytesCurrently;
				nPos = 100*nPos/(float)dwTotalBytes;
				m_ReciveProgres.SetPos((int)nPos);
			}
			break;

		case PROG_BAR_CLOSE:
			{
				CDialog::OnCancel();
			}
			break;

		case PROG_BAR_FULL:
			{
				m_ReciveProgres.SetPos(100);
				if (!(DWORD)l)
				{
					CString sMsg;
					sMsg.LoadString(IDS_NO_NEW_MSGS);
					AfxMessageBox(sMsg, MB_ICONINFORMATION);
				}
			}
			break;
	}
	return 0;
}

void CReciveProgres::OnTimer(UINT nIDEvent) 
{
	switch(nIDEvent) 
	{
	case 1005:
		KillTimer(1005);

		CString sMsg;
		sMsg.LoadString(IDS_ERR_POP3_TIMED_OUT);		
		AfxMessageBox(sMsg);

		OnCancel();

		break;
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CReciveProgres::OnCancel() 
{
	if (m_Pop3Sock.IsConnected()) m_Pop3Sock.CloseConnection();
	else CDialog::OnCancel();
}
