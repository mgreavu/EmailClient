#if !defined(AFX_ACCOUNTSDLG_H__86D1C340_3AE8_4CE9_A69B_43D696A048CD__INCLUDED_)
#define AFX_ACCOUNTSDLG_H__86D1C340_3AE8_4CE9_A69B_43D696A048CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AccountsDlg.h : header file
//

#include "AccountListTypes.h"
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CAccountsDlg dialog

class CAccountsDlg : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	int m_iSelected;
	void DeleteDirectory (CString Path);
	LRESULT OnAccInserted (WPARAM wParam, LPARAM lParam);
	CAccountsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAccountsDlg)
	enum { IDD = IDD_ACCOUNTS_DIALOG };
	CListCtrl	m_AccList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAccountsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSXButton m_btnAdd;
	CSXButton m_btnRemove;
	CSXButton m_btnProp;
	CSXButton m_btnDefault;
	CSXButton m_btnSetFolder;
	CSXButton m_btnImport;
	CSXButton m_btnExport;
	CSXButton m_btnClose;
	// Generated message map functions
	//{{AFX_MSG(CAccountsDlg)
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnPropBtn();
	afx_msg void OnAddBtn();
	afx_msg void OnCloseBtn();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRemoveBtn();
	afx_msg void OnDblclkAccountsList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnImportBtn();
	afx_msg void OnExportBtn();
	afx_msg void OnDefaultBtn();
	afx_msg void OnKeydownAccountsList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetFolder();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL MoveFolder (CString SourcePath, CString DestPath);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACCOUNTSDLG_H__86D1C340_3AE8_4CE9_A69B_43D696A048CD__INCLUDED_)
