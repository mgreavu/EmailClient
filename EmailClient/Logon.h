#if !defined(AFX_LOGON_H__B3A4AF46_D2D2_4698_8A58_CFF8A8926B1A__INCLUDED_)
#define AFX_LOGON_H__B3A4AF46_D2D2_4698_8A58_CFF8A8926B1A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Logon.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CLogon dialog

class CLogon : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	CLogon(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLogon)
	enum { IDD = IDD_LOGON };
	CString	m_sPassword;
	CString	m_sUserName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogon)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSXButton m_btnOk;
	CSXButton m_btnCancel;
	// Generated message map functions
	//{{AFX_MSG(CLogon)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGON_H__B3A4AF46_D2D2_4698_8A58_CFF8A8926B1A__INCLUDED_)
