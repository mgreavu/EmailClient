#if !defined(AFX_SENDMAIL_H__EEA95AB1_6C66_4DA4_9CE5_5945B06A6F96__INCLUDED_)
#define AFX_SENDMAIL_H__EEA95AB1_6C66_4DA4_9CE5_5945B06A6F96__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SendMail.h : header file
//

#include "WinSock2.h"
#include "interface\XProgressBar.h"
#include "interface/Sxbutton.h"
#include "smtp\MailMsg.h"

#define UWM_ASYNCSELECT		WM_APP + 0x1200

#define TERM_MSG			"\r\n.\r\n"


#define COMM_READ_BUFF_SIZE		2048
#define COMM_WRITE_BUFF_SIZE	16384

#define SMTP_HELO					0x03
#define SMTP_QUIT					0x04
#define SMTP_MAIL_FROM				0x05
#define SMTP_RCPT_TO				0x06
#define SMTP_DATA					0x07
#define SMTP_BODY					0x08
#define SMTP_EHLO					0x09
#define SMTP_TERM					0x0A

#define SMTP_AUTH_LOGIN				0x10
#define SMTP_AUTH_LOGIN_USER		0x11	
#define SMTP_AUTH_LOGIN_PWD			0x12

#define SMTP_AUTH_CRAM_MD5			0x13
#define SMTP_AUTH_CRAM_MD5_REPLY	0x14


#define CMD_HELO			"HELO"
#define CMD_QUIT			"QUIT"
#define CMD_MAIL_FROM		"MAIL FROM: "
#define CMD_RCPT_TO			"RCPT TO: "
#define CMD_DATA			"DATA"
#define CMD_EHLO			"EHLO"
#define CMD_AUTH_LOGIN		"AUTH LOGIN"
#define CMD_AUTH_CRAM_MD5	"AUTH CRAM-MD5"

/////////////////////////////////////////////////////////////////////////////
// CSendMail dialog

class CSendMail : public CDialog
{
// Construction
public:
	CString SMTPUserName;
	BYTE bSendStatus;
	CString TheMessage;
	CString SMTPEmailAddress;
	UINT SMTPPort;
	CString SMTPPassword;
	CString SMTPServerName;
	CSendMail(CWnd* pParent, CMailMsg MailMsg);
	CMailMsg mailmsg;


// Dialog Data
	//{{AFX_DATA(CSendMail)
	enum { IDD = IDD_SENDMAIL_DIALOG };
	CSXButton	m_btnCancel;
	CXProgressBar	m_ProgBar;
	CButton	m_Frame;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSendMail)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString sMsg;
	CString FrameCaption;
	BOOL IsReplyComplete();
	CString sFullReply;
	CString CRAMMD5ReplyString;
	void ProcessSMTPServerReply ();
	void DisplayWSAError(WORD wsaErr);
	BOOL Connect();
	DWORD dwBodySentBytes;
	DWORD dwCurrentSendingBufferSize;
	void UpdateProgBar();

	SOCKET hClientSocket;
	SOCKADDR_IN SockAddr;
	unsigned char nOpType;
	BOOL m_bConnected;


	char m_receiveBuffer[COMM_READ_BUFF_SIZE];
	DWORD dwReceivedBytes;
	char m_sendBuffer[COMM_WRITE_BUFF_SIZE];
	DWORD dwSentBytes;

	char* lpBodySendBuffer;
	DWORD dwBodyBuffSize;

	// Generated message map functions
	//{{AFX_MSG(CSendMail)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENDMAIL_H__EEA95AB1_6C66_4DA4_9CE5_5945B06A6F96__INCLUDED_)
