#if !defined(AFX_SAVEATTACHDLG_H__D4DF7DF6_760A_4F9C_B1C9_EA7675CCA1F4__INCLUDED_)
#define AFX_SAVEATTACHDLG_H__D4DF7DF6_760A_4F9C_B1C9_EA7675CCA1F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SaveAttachDlg.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CSaveAttachDlg dialog

class CSaveAttachDlg : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	CSaveAttachDlg(CWnd* pParent = NULL);   // standard constructor
	CStringList m_AList;
	//CList<CString,CString&> m_AList;
	int m_AttachIndex;
	CString m_AttachName;
// Dialog Data
	//{{AFX_DATA(CSaveAttachDlg)
	enum { IDD = IDD_SAVEATTACHS };
	CListBox	m_List;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveAttachDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	INT width;
	void UpdateSearchListWidth(LPCSTR s);

	CSXButton m_btnOk;
	CSXButton m_btnCancel;
	// Generated message map functions
	//{{AFX_MSG(CSaveAttachDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeAttachlist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVEATTACHDLG_H__D4DF7DF6_760A_4F9C_B1C9_EA7675CCA1F4__INCLUDED_)
