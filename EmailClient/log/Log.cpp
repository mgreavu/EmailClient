// Log.cpp: implementation of the CLog class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "Log.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLog::CLog()
{
	fLog = NULL;
	memset (&FileName, 0x00, sizeof(char)*MAX_PATH);
	ZeroMemory (&TrackedModuleAKA, sizeof(char)*MAX_PATH);
	TrackedModuleVersion = 0;
	LogSeverity = LEVEL_INFORMATIONAL;
	MaxLogFileSize = 10485760;
}

CLog::~CLog()
{
	fLog = NULL;
}

void CLog::SetLogFile(const char *FLogName)
{
	if (FLogName == NULL) return;
	memset (&FileName, 0x00, MAX_PATH);
	memmove(&FileName, FLogName, strlen(FLogName)); 
	if (fLog != NULL) fclose (fLog);
	::InitializeCriticalSection (&CSLogrotateLock);	
	if ((fLog = fopen(FileName,"w+")) == NULL) return;

	HKEY  Key;
	DWORD dwType = REG_DWORD;
	DWORD dwSize = sizeof(DWORD);
	DWORD StoreValue = 0;

	if(::RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_SETTINGS_KEY, NULL, KEY_ALL_ACCESS, &Key) == ERROR_SUCCESS) 
	{		
		if(::RegQueryValueEx(Key, "LogMaxSize", NULL, &dwType, (LPBYTE) &StoreValue, &dwSize) == ERROR_SUCCESS) 
		{	
			if ((StoreValue >= 1048576) && (StoreValue <= 10485760)) MaxLogFileSize = StoreValue;

			StoreValue = 0;
			if(::RegQueryValueEx(Key, "LogWarnLevel", NULL, &dwType, (LPBYTE) &StoreValue, &dwSize) == ERROR_SUCCESS)
			{
				if ((StoreValue >= LEVEL_CRITICAL) && (StoreValue <= LEVEL_INFORMATIONAL)) LogSeverity = StoreValue;
			} 
		}
		::RegCloseKey(Key);
		fprintf(fLog, "\r\nLog System: Logfile settings (HKLM\\%s): LogMaxSize = %d, LogWarnLevel = %d\r\n", REG_SETTINGS_KEY, MaxLogFileSize, LogSeverity);
	}
	else fprintf(fLog, "\r\nLog System: Logfile settings (default values): LogMaxSize = %d, LogWarnLevel = %d\r\n", MaxLogFileSize, LogSeverity);
}

void CLog::InsertLine(BYTE Level, const char *format, ...)
{
	Logrotate(); 

	if (format == NULL) return;
	if (fLog == NULL) return;
	if ((Level < LEVEL_CRITICAL) || (Level > LEVEL_INFORMATIONAL)) return;

	if (LogSeverity < Level) return;

	::EnterCriticalSection(&CSLogrotateLock);

	switch (Level)
	{
		case LEVEL_INFORMATIONAL: {fprintf(fLog, "{DBG}  "); break;}
		case LEVEL_OPERATIONAL: {fprintf(fLog, "{OPR}  "); break;}
		case LEVEL_WARNING: {fprintf(fLog, "{WRN}  "); break;}
		case LEVEL_CRITICAL: {fprintf(fLog, "{CRI}  "); break;}
	}

	char buf[MAX_INTERNAL_BUFF_SIZE], *p = buf;	
	va_list	args;

	memset (&buf, 0x00, sizeof(char)*MAX_INTERNAL_BUFF_SIZE);
    va_start(args, format);
    p += _vsnprintf(p, MAX_INTERNAL_BUFF_SIZE - 1, format, args);
    va_end(args);
    *p++ = '\r';
    *p++ = '\n';
	fprintf (fLog, buf);
	fflush (fLog);

	::LeaveCriticalSection(&CSLogrotateLock);
}

void CLog::CloseLogFile()
{
	if (fLog != NULL) fclose (fLog);
	::DeleteCriticalSection (&CSLogrotateLock);
}

void CLog::InsertLineWithDateTime(BYTE Level, const char *format, ...)
{
	Logrotate();

	if (format == NULL) return;
	if (fLog == NULL) return;	
	if ((Level < LEVEL_CRITICAL) || (Level > LEVEL_INFORMATIONAL)) return;

	if (LogSeverity < Level) return;

	::EnterCriticalSection(&CSLogrotateLock);

	SYSTEMTIME st;
	GetLocalTime (&st);
	fprintf (fLog, "%02d-%02d-%d %02d:%02d:%02d  ", st.wDay, st.wMonth, st.wYear, st.wHour, st.wMinute, st.wSecond);

	switch (Level)
	{
		case LEVEL_INFORMATIONAL: {fprintf(fLog, "{DBG}  "); break;}
		case LEVEL_OPERATIONAL: {fprintf(fLog, "{OPR}  "); break;}
		case LEVEL_WARNING: {fprintf(fLog, "{WRN}  "); break;}
		case LEVEL_CRITICAL: {fprintf(fLog, "{CRI}  "); break;}
	}
	
	char buf[MAX_INTERNAL_BUFF_SIZE], *p = buf;	
	va_list	args;

	memset (&buf, 0x00, sizeof(char)*MAX_INTERNAL_BUFF_SIZE);
    va_start(args, format);
    p += _vsnprintf(p, MAX_INTERNAL_BUFF_SIZE - 1, format, args);
    va_end(args);
    *p++ = '\r';
    *p++ = '\n';
	fprintf (fLog, buf);
	fflush (fLog);

	::LeaveCriticalSection(&CSLogrotateLock);
}

void CLog::SetLogSeverityLevel(DWORD SeverityLevel)
{
	LogSeverity = SeverityLevel;	

	if (fLog == NULL) return;
	fprintf (fLog, "\r\nLog System: Warning Level set to ");

	switch (SeverityLevel)
	{
		case LEVEL_INFORMATIONAL: {fprintf(fLog, "{DBG}  "); break;}
		case LEVEL_OPERATIONAL: {fprintf(fLog, "{OPR}  "); break;}
		case LEVEL_WARNING: {fprintf(fLog, "{WRN}  "); break;}
		case LEVEL_CRITICAL: {fprintf(fLog, "{CRI}  "); break;}
	}
	fprintf(fLog, "\r\n\r\n");
	fflush (fLog);
}

void CLog::InsertLogSystemData(BYTE ModuleVer, const char* ModuleAKA)
{	
	strncpy (TrackedModuleAKA, ModuleAKA, strlen(ModuleAKA));
	TrackedModuleVersion = ModuleVer;

	if (fLog == NULL) return;

   char ModuleFName [MAX_PATH + 1];
   memset (ModuleFName, 0x00, sizeof(char)*(MAX_PATH + 1));

   if (!::GetModuleFileName(NULL, ModuleFName, MAX_PATH)) fprintf(fLog, "Log System: Tracking %s, Version: 0x%02X\r\n", "EC", TrackedModuleVersion);
   else fprintf(fLog, "Log System: Tracking %s, Version: 0x%02X\r\n", ModuleFName, TrackedModuleVersion);

	fprintf(fLog, "Log System: A.K.A. %s\r\n", TrackedModuleAKA);
	fprintf (fLog, "Log System: Build date: %s %s\r\n",  TEXT(__DATE__), TEXT(__TIME__));

	OSVERSIONINFOEX os = {0};
	os.dwOSVersionInfoSize = sizeof (OSVERSIONINFOEX);

	if (!::GetVersionEx ((LPOSVERSIONINFO)&os)) fprintf (fLog, "GetVersionEx() failed with error %d\r\n", ::GetLastError());
	else
	{
		fprintf (fLog, "Log System: Running on: Windows [Version %d.%d.%d], sp string: %s, sp mj: %d, sp mi: %d, prod type: %d\r\n", os.dwMajorVersion, os.dwMinorVersion, os.dwBuildNumber, os.szCSDVersion, os.wServicePackMajor, os.wServicePackMinor, os.wProductType );
	}	

	fflush (fLog);

	SetLogSeverityLevel(LogSeverity);
}

BOOL CLog::Logrotate()
{
	if (fLog != NULL) 
	{
		::EnterCriticalSection(&CSLogrotateLock);

		fseek (fLog, 0, SEEK_END);
		long lSize = ftell (fLog);
		if (lSize < MaxLogFileSize)	
		{
			::LeaveCriticalSection(&CSLogrotateLock);
			return FALSE;
		}
		
		fclose (fLog);
		fLog = NULL;
		fLog = fopen(FileName,"w+");
		fprintf(fLog, "\r\nLog System: Logrotate at full capacity: %d bytes\r\n", MaxLogFileSize);
		InsertLogSystemData(TrackedModuleVersion, TrackedModuleAKA);

		::LeaveCriticalSection(&CSLogrotateLock);		
	}

	return TRUE;
}
