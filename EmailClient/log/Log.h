// Log.h: interface for the CLog class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOG_H__3BC1D363_7A2D_462D_B658_05F3E67AB2A0__INCLUDED_)
#define AFX_LOG_H__3BC1D363_7A2D_462D_B658_05F3E67AB2A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "windows.h"

#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

#define MAX_INTERNAL_BUFF_SIZE	1024

#define LEVEL_CRITICAL				0x00
#define LEVEL_WARNING			  0x01
#define LEVEL_OPERATIONAL	    0x02
#define LEVEL_INFORMATIONAL   0x03

#define REG_SETTINGS_KEY	"Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon\\SASGina"
class CLog  
{
public:
	FILE* fLog;

	BOOL Logrotate ();
	void InsertLogSystemData (BYTE ModuleVer, const char* ModuleAKA);
	void SetLogSeverityLevel (DWORD SeverityLevel);
	void InsertLineWithDateTime (BYTE Level, const char *format, ...);
	void CloseLogFile();
	void InsertLine (BYTE Level, const char *format, ...);
	void SetLogFile (const char* FLogName);
	CLog();
	virtual ~CLog();

private:
	DWORD LogSeverity;
	DWORD MaxLogFileSize;

	char FileName[MAX_PATH];
	CRITICAL_SECTION CSLogrotateLock;
	char TrackedModuleAKA[MAX_PATH];
	BYTE TrackedModuleVersion;
};

#endif // !defined(AFX_LOG_H__3BC1D363_7A2D_462D_B658_05F3E67AB2A0__INCLUDED_)
