// AppSettings.h: interface for the CAppSettings class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPSETTINGS_H__5082F322_71F2_446A_BC97_13A7DDEB4CCA__INCLUDED_)
#define AFX_APPSETTINGS_H__5082F322_71F2_446A_BC97_13A7DDEB4CCA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "satanickey/ModMasscom.h"

#include <list>

using namespace std;

#define EC_MAIN_FOLDER		"Email Client"
#define EC_SETTINGS_FILE	"ec.ini"
#define BLACK_USB_SUPPORT_MODULE	"Masscom.dll"

#define KEY_ID		"1234567"
#define KEY_PASS	"1234567"

#define KEY_SUCCESS	6

#define MARK_ADDR			 0
#define MARK_LEN			 1
#define ID_ADDR				 1
#define ID_LEN				31
#define KEY_ADDR			49
#define KEY_LEN				32

typedef struct tagUserEncData
{
	char XSATAUID[32];
	unsigned char aesKey[32];
} UserEncData, *LPUSERENCDATA;

#define SZ_USERENCDATA	sizeof(UserEncData) 

typedef list<UserEncData> UserEncCodesList;

class CAppSettings  
{
public:
	BOOL IniWriteECSettings();
	BOOL IniReadECSettings ();
	SATAMasscomPointers IfSATAMasscom;
	int iCurrentAccount;
	CString AccountsFolder;
	int bAccountsFolderOnKey;
	char XSATAUID[32];
	unsigned char aesKey[32];
	UserEncCodesList UserEncList;

	CAppSettings();
	virtual ~CAppSettings();

private:
	CString sSettingsFile;
public:
	BOOL LoadBlackDll(void);
	BOOL UnloadBlackDll(void);
	BOOL ReadUserData(void);
	BOOL GetAllEncCodes(void);
};

#endif // !defined(AFX_APPSETTINGS_H__5082F322_71F2_446A_BC97_13A7DDEB4CCA__INCLUDED_)
