#if !defined(AFX_REMATTACHDLG_H__5AF15AE4_9AA6_4EFF_A478_9CEBD74A45C3__INCLUDED_)
#define AFX_REMATTACHDLG_H__5AF15AE4_9AA6_4EFF_A478_9CEBD74A45C3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RemAttachDlg.h : header file
//
#include "interface/SXButton.h"

/////////////////////////////////////////////////////////////////////////////
// CRemAttachDlg dialog

class CRemAttachDlg : public CDialog
{
// Construction
public:
	void OwnerDrawButtons();
	BOOL OnInitDialog();
	CRemAttachDlg(CWnd* pParent = NULL);   // standard constructor
	CStringList m_AList;
	int m_AttachIndex;
	CString m_AttachName;

// Dialog Data
	//{{AFX_DATA(CRemAttachDlg)
	enum { IDD = IDD_REMATTACHS };
	CListBox	m_List;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRemAttachDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSXButton m_btnOk;
	CSXButton m_btnCancel;
	// Generated message map functions
	//{{AFX_MSG(CRemAttachDlg)
	afx_msg void OnSelchangeRemattachlist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REMATTACHDLG_H__5AF15AE4_9AA6_4EFF_A478_9CEBD74A45C3__INCLUDED_)
