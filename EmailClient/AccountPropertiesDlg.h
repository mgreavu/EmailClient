#if !defined(AFX_ACCOUNTPROPERTIESDLG_H__52DB07B1_D7EC_47AE_A0A2_2A951984D461__INCLUDED_)
#define AFX_ACCOUNTPROPERTIESDLG_H__52DB07B1_D7EC_47AE_A0A2_2A951984D461__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AccountPropertiesDlg.h : header file
//
#include "interface/SXTabCtrl.h"
/////////////////////////////////////////////////////////////////////////////
// CAccountPropertiesDlg

class CAccountPropertiesDlg : public CPropertySheet
{
	DECLARE_DYNAMIC(CAccountPropertiesDlg)

// Construction
public:
	CAccountPropertiesDlg(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CAccountPropertiesDlg(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:

// Operations
public:
	CSXTabCtrl m_tabCtrl;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAccountPropertiesDlg)
	//}}AFX_VIRTUAL

// Implementation
public:
	void OwnerDrawButtons();
	virtual ~CAccountPropertiesDlg();

	// Generated message map functions
protected:

	CButton m_btnOk;
	CButton m_btnCancel;

//	void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();

//	virtual BOOL OnInitDialog();
	//{{AFX_MSG(CAccountPropertiesDlg)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACCOUNTPROPERTIESDLG_H__52DB07B1_D7EC_47AE_A0A2_2A951984D461__INCLUDED_)
