#ifndef __CONNECTDEFS_H
#define __CONNECTDEFS_H

/*/ Default MIA values
#define MIA_EMBEDDED_KEYA     "pro3softpro3soft"

// User keys defs
#define USER_KEY_LEN          16
#define HW_KEY_LEN            30


#define TSF_CMD_STRING_NR		5
#define TSF_ERR_STRING_NR      20
	  

#define WM_TSFC_BASE          WM_USER + 1000

#define WM_CM_OPOK            WM_TSFC_BASE + 500
#define WM_CM_OPERR           WM_TSFC_BASE + 501
#define WM_CM_CANCEL          WM_TSFC_BASE + 502

#define WM_USER_CANCEL        WM_TSFC_BASE + 601
#define WM_CMD_COMPLETE       WM_TSFC_BASE + 602
#define WM_THREAD_FINISHED    WM_TSFC_BASE + 603

#define MAX_SESSION_NAME      256

typedef struct tagMIASettings {
	char SessionName[MAX_SESSION_NAME];
	char LastUserName[30];
	char ConnType;
	
	DWORD ServerIP;
	USHORT ServerPort;

	DWORD LastUserID;
	char KeyA[16]; 
	char KeyB[16];
	char EmbeddedKeyA[16];
	char uAccessType;
} s_MIASettings;


extern s_MIASettings MIASettings;

extern BOOL MIAConnected;
extern BOOL OperationInProgress;

//Events, errors
extern  HANDLE  CMOpCompleteEvent;
extern  int     CMLastError;
extern  int     WSALastError;

extern  HANDLE  MIACmdCompleteEvent;
extern  int     MIALastCmdResult;

extern  UINT    MIAUserID;

extern  char    MIAAccessType;
extern  char    MIAEmbeddedKeyA[USER_KEY_LEN];
extern  char    MIAKeyA[USER_KEY_LEN];
extern  char    MIAKeyB[USER_KEY_LEN];*/

//Dial-Up functions
BOOL ActiveDialUpSession(LPSTR lpszSessionName, DWORD* errCode);
BOOL UseRASDialUp(int* useIt);
//initialize a dial-up connection on Windows NT systems
BOOL InitRASDialUp(LPSTR lpszSessionName, DWORD* errCode);
//initialize a dial-up connection on Windows 9x systems
BOOL InitRnauiDialUp(LPSTR lpszSessionName, DWORD* errCode, HANDLE* pHandle);

BOOL SetDialUpConnection();


//Error functions
void ErrorMessageBox(HWND hwnd, UINT IDString, UINT IDTitle, DWORD errCode);

#endif