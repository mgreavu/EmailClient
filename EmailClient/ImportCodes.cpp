// ImportCodes.cpp : implementation file
//

#include "stdafx.h"
#include "emailclient.h"
#include "ImportCodes.h"
#include "AppSettings.h"
#include "rijndael/rsalib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern CAppSettings ECSettings;
/////////////////////////////////////////////////////////////////////////////
// CImportCodes dialog


CImportCodes::CImportCodes(CWnd* pParent /*=NULL*/): CDialog(CImportCodes::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImportCodes)
	m_FilePath = _T("");
	//}}AFX_DATA_INIT
}


void CImportCodes::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportCodes)
	DDX_Control(pDX, IDC_FILEPATH, m_ctrlEdit);
	DDX_Text(pDX, IDC_FILEPATH, m_FilePath);
	DDV_MaxChars(pDX, m_FilePath, 256);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CImportCodes, CDialog)
	//{{AFX_MSG_MAP(CImportCodes)
	ON_BN_CLICKED(IDC_OPENFILE, OnOpenfile)
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImportCodes message handlers

void CImportCodes::OnOK() 
{
	UpdateData (TRUE);

	CString sMsg;

	if (m_FilePath.IsEmpty())
	{
		sMsg.LoadString(IDS_NO_FILE_SELECTED);
		::MessageBox (m_hWnd, sMsg, "EmailClient", MB_OK | MB_ICONEXCLAMATION);
		((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
		return;
	}
	byte AES[32];
	memset (AES, 0x00, 32);
	char Code[32];
	memset (Code, 0x00, 32);

	if (!ReadDataFromFile(m_FilePath.GetBuffer(m_FilePath.GetLength()), AES, Code)) 
	{
		sMsg.LoadString(IDS_CANNOT_READ_FROM_CODE_FILE);
		::MessageBox (m_hWnd, sMsg, "EmailClient", MB_OK | MB_ICONEXCLAMATION);
		((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
		return;
	}

	if (strlen(Code) != 31)
	{
		sMsg.LoadString(IDS_FILE_INCOMPATIBLE);
		::MessageBox (m_hWnd, sMsg, "EmailClient", MB_OK | MB_ICONEXCLAMATION);
		((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
		return;
	}

	CString sOrigKeyPath = sKeyDestPath;
	CString sNewName = sOrigKeyPath + '\\' + Code;
	sOrigKeyPath += m_FilePath.Right(m_FilePath.GetLength() - m_FilePath.ReverseFind('\\'));
	
	if (!CopyFile (m_FilePath, sOrigKeyPath, TRUE))
	{
		sMsg.LoadString(IDS_ERR_COPY_FILE);
		::MessageBox (m_hWnd, sMsg, "EmailClient", MB_OK | MB_ICONEXCLAMATION);
		((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
		return;		
	}

	if (!MoveFile (sOrigKeyPath, sNewName))
	{
		if (GetLastError() == 183)
		{
			sMsg.LoadString(IDS_OVERWRITE_CODE);
			if (::MessageBox (m_hWnd, sMsg, "EmailClient", MB_YESNO | MB_ICONQUESTION) == IDNO)
			{
				DeleteFile (sOrigKeyPath);
				((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
				return;
			}
			if (!DeleteFile (sNewName))
			{
				sMsg.LoadString(IDS_CANNOT_DELETE_OLD_CODE);
				::MessageBox (m_hWnd, sMsg, "EmailClient", MB_OK | MB_ICONEXCLAMATION);
				((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
				return;
			}
			if (!MoveFile (sOrigKeyPath, sNewName))
			{
				sMsg.LoadString(IDS_ERROR_IMPORTING);
				::MessageBox (m_hWnd, sMsg, "EmailClient", MB_OK | MB_ICONEXCLAMATION);
				((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
				return;
			}

		}
		else
		{
			sMsg.LoadString(IDS_ERROR_IMPORTING);
			::MessageBox (m_hWnd, sMsg, "EmailClient", MB_OK | MB_ICONEXCLAMATION);
			((CButton*)(GetDlgItem(IDC_OPENFILE)))->SetFocus();
			return;
		}
	}

	UserEncData UED = {0};
	strncpy (UED.XSATAUID, Code, 31);
	memmove (UED.aesKey, AES, 32);

	BOOL bExists = FALSE;
	UserEncCodesList::iterator UECListIterator;

	for (UECListIterator = ECSettings.UserEncList.begin(); UECListIterator != ECSettings.UserEncList.end(); UECListIterator++)
	{
		if (!strncmp ((*UECListIterator).XSATAUID, UED.XSATAUID, 31))
		{
			memmove ((*UECListIterator).aesKey, UED.aesKey, 32);
			bExists = TRUE;
			break;
		}
	}

	if (!bExists) ECSettings.UserEncList.push_back(UED);

	::MessageBox (m_hWnd, "User codes imported.", "Email Client", MB_OK | MB_ICONINFORMATION);

	if (m_font)::DeleteObject(m_font);
	CDialog::OnOK();
}

void CImportCodes::OnCancel() 
{
	if (m_font)::DeleteObject(m_font);
	
	CDialog::OnCancel();
}

void CImportCodes::OnOpenfile() 
{
	CString sMsg;
	
	CFileDialog	dlgFile (TRUE, NULL, NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR, NULL);
	sMsg.LoadString(IDS_PLEASE_SELECT_CODE);
	dlgFile.m_ofn.lpstrTitle = sMsg;
	dlgFile.DoModal();
	m_FilePath = dlgFile.m_ofn.lpstrFile;

	UpdateData (FALSE);
	
}

BOOL CImportCodes::ReadDataFromFile(LPSTR ImportFilePath, byte *AESKey, char *UserCode)
{
	HANDLE fHnd = INVALID_HANDLE_VALUE;
	DWORD nBytes = 0;
		
	byte AppKey[32];
	memset(AppKey, 0x00, 32);
	strncpy((LPSTR)AppKey, "MyPrivacyEasyDiskVerXFilesXKey", strlen("MyPrivacyEasyDiskVerXFilesXKey"));

	if ((fHnd = ::CreateFile(ImportFilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) return FALSE;
	
	::SetFilePointer(fHnd, 4, 0, FILE_BEGIN);

	char DataBuffer[32];

	memset(DataBuffer, 0, 32);
	if (!::ReadFile(fHnd, DataBuffer, 32, &nBytes, NULL)) 
	{
		::CloseHandle(fHnd);
		return FALSE;
	}
	
	if (AESDecrypt(AppKey, (byte*)UserCode, (byte*)DataBuffer, 32) != RE_SUCCESS)
	{
		::CloseHandle(fHnd);
		return FALSE;
	}

    memset(DataBuffer, 0, 32);
	if (!::ReadFile(fHnd, DataBuffer, 32, &nBytes, NULL)) 
	{
		::CloseHandle(fHnd);
		return FALSE;
	}
	
	if (AESDecrypt(AppKey, AESKey, (byte*)DataBuffer, 32) != RE_SUCCESS)
	{
		::CloseHandle(fHnd);
		return FALSE;
	}
	
	::CloseHandle(fHnd);
	
	return TRUE;		
}

BOOL CImportCodes::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	LOGFONT lf; 
	memset(&lf, 0, sizeof(LOGFONT)); 
    lf.lfCharSet = ANSI_CHARSET; 
    lf.lfHeight = 13; 
//	lf.lfWidth = 6;
	lf.lfWeight = FW_NORMAL;
    strcpy(lf.lfFaceName, "Verdana");
    m_font = ::CreateFontIndirect(&lf); 
    m_ctrlEdit.SetFont(CFont::FromHandle(m_font), TRUE); 
	m_ctrlEdit.SetBkColor (RGB(232,232,232));
	m_ctrlEdit.SetTextColor (RGB(32, 32, 32));
	m_ctrlEdit.SetReadOnly(TRUE);	
	
	return TRUE;
}

void CImportCodes::OnClose()
{
	OnCancel();
}
