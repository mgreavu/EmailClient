// Contact.cpp: implementation of the CContact class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EmailClient.h"
#include "Contact.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



CContact::CContact()
{
	m_FullName = "";
	m_NickName = "";
	m_EmailAdress = "";
	m_Street = "";
	m_City = "";
	m_Zip = "";
	m_Country = "";
	m_PhoneMobile = "";
	m_PhoneOffice = "";
	m_PhoneHome = "";
	
}
/*
CContact::CContact(const CContact& contact){
	m_FullName = contact.m_FullName;
	m_NickName = contact.m_NickName;
	m_EmailAdress = contact.m_EmailAdress;
	m_Street = contact.m_Street;
	m_City = contact.m_City;
	m_Zip = contact.m_Zip;
	m_Country = contact.m_Country;
	m_PhoneMobile = contact.m_PhoneMobile;
	m_PhoneOffice = contact.m_PhoneOffice;
	m_PhoneHome = contact.m_PhoneHome;
}
*/
CContact& CContact::operator =(CContact& contact){
	m_FullName = contact.m_FullName;
	m_NickName = contact.m_NickName;
	m_EmailAdress = contact.m_EmailAdress;
	m_Street = contact.m_Street;
	m_City = contact.m_City;
	m_Zip = contact.m_Zip;
	m_Country = contact.m_Country;
	m_PhoneMobile = contact.m_PhoneMobile;
	m_PhoneOffice = contact.m_PhoneOffice;
	m_PhoneHome = contact.m_PhoneHome;
	return (*this);
}



CContact::~CContact()
{

}

//DEL void CContact::Serialize(CArchive &ar)
//DEL {
//DEL 	CObject::Serialize (ar);
//DEL 	
//DEL 	try {
//DEL 	
//DEL 		if (ar.IsStoring())
//DEL 		{
//DEL 			ar  << m_FullName
//DEL 				<< m_NickName
//DEL 				<< m_EmailAdress
//DEL 				<< m_PhoneHome
//DEL 				<< m_PhoneMobile
//DEL 				<< m_PhoneOffice
//DEL 				<< m_City
//DEL 				<< m_Street
//DEL 				<< m_Zip
//DEL 				<< m_Country;
//DEL 		}
//DEL 		else
//DEL 		{
//DEL 			ar	>> m_NickName
//DEL 				>> m_EmailAdress
//DEL 				>> m_PhoneHome
//DEL 				>> m_PhoneMobile
//DEL 				>> m_PhoneOffice
//DEL 				>> m_City
//DEL 				>> m_Street
//DEL 				>> m_Zip
//DEL 				>> m_Country;
//DEL 		}
//DEL 	}
//DEL 	catch (CException* pException)
//DEL     {		
//DEL 		throw pException;
//DEL //		pException->Delete();
//DEL     }
//DEL 
//DEL }
