// SMTPAuthDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "SMTPAuthDlg.h"
#include "AccountListTypes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSMTPAuthDlg dialog


CSMTPAuthDlg::CSMTPAuthDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSMTPAuthDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSMTPAuthDlg)
	m_AuthOpt = -1;
	m_sSMTPName = _T("");
	m_sSMTPPsw = _T("");
	//}}AFX_DATA_INIT
}


void CSMTPAuthDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSMTPAuthDlg)
	DDX_Radio(pDX, IDC_USE_POP3_SETT, m_AuthOpt);
	DDX_Text(pDX, IDC_SMTP_NAME, m_sSMTPName);
	DDX_Text(pDX, IDC_SMTP_PASSW, m_sSMTPPsw);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSMTPAuthDlg, CDialog)
	//{{AFX_MSG_MAP(CSMTPAuthDlg)
	ON_BN_CLICKED(IDC_USE_SMTP_SETT, OnUseSmtpSett)
	ON_BN_CLICKED(IDC_USE_POP3_SETT, OnUsePop3Sett)
	ON_EN_CHANGE(IDC_SMTP_PASSW, OnChange)
	ON_EN_CHANGE(IDC_SMTP_NAME, OnChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSMTPAuthDlg message handlers

BOOL CSMTPAuthDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if((m_AuthOpt < 0) || (m_AuthOpt > 1))
	{
		m_AuthOpt = 0;
		(*AccListIterator).SetSMTPUsePOP3Settings(0);
		m_bOnChange = TRUE;
	}
	if(m_AuthOpt == 1)
		OnUseSmtpSett();
	else
		OnUsePop3Sett();

	OwnerDrawButtons();
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSMTPAuthDlg::OnUseSmtpSett() 
{
	UpdateData(TRUE);
	GetDlgItem(IDC_SMTP_NAME)->EnableWindow(TRUE);	
	GetDlgItem(IDC_SMTP_PASSW)->EnableWindow(TRUE);
	UpdateData(FALSE);
	m_bOnChange = TRUE;
}

void CSMTPAuthDlg::OnUsePop3Sett() 
{
	GetDlgItem(IDC_SMTP_NAME)->EnableWindow(FALSE);	
	GetDlgItem(IDC_SMTP_PASSW)->EnableWindow(FALSE);
	UpdateData(TRUE);
	m_bOnChange = TRUE;
}

void CSMTPAuthDlg::OnChange() 
{
	UpdateData(TRUE);
	m_bOnChange = TRUE;
}

void CSMTPAuthDlg::OnOK() 
{
	if(m_AuthOpt == 1)
	{
		UpdateData(TRUE);
		if(m_sSMTPName.IsEmpty())
		{	
			CString sMsg;
			sMsg.LoadString(IDS_ERR_NEED_NAME);
			AfxMessageBox(sMsg);

			return;
		}
	}
	CDialog::OnOK();
}

void CSMTPAuthDlg::OwnerDrawButtons()
{
	m_btnOk.SetMaskedBitmap( IDB_OK, 16, 16, RGB( 255, 255, 255 ) );

	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );
}
