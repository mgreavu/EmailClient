// EmailClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AccountListTypes.h"
#include "EmailClient.h"
#include "EmailClientDlg.h"
#include "MainToolbar.h"
#include "AddAccountDlg.h"
#include "ComposeNewMessage.h"
#include "ContactsDialog.h"
#include "AddContactsDlg.h"
#include "smtp/pop3.h"
#include "SendMail.h"
#include "ReciveProgres.h"
#include "ConnectDefs.h"
#include "Logon.h"
#include "ImportCodes.h"
#include "AccountsDlg.h"
#include "SaveAttachDlg.h"
#include "Globals.h"
#include "AppSettings.h"
#ifdef _LOG
#include "log/Log.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CAppSettings ECSettings;

#ifdef _LOG
extern CLog Log;
#endif
/////////////////////////////////////////////////////////////////////////////
// CEmailClientDlg dialog


CEmailClientDlg::CEmailClientDlg(CWnd* pParent /*=NULL*/): CDialog(CEmailClientDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEmailClientDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	bWindowInitialized = FALSE;
}

void CEmailClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEmailClientDlg)
	DDX_Control(pDX, IDC_FOLDER_TREE, m_cTreeCtrl);
	DDX_Control(pDX, IDC_LIST_MSGS, m_Msgs);
	DDX_Control(pDX, IDC_CONTACTS_LIST, m_ContactList);
	DDX_Control(pDX, IDC_EDITMailTxt, m_ctrlEditMailText);
	DDX_Control(pDX, IDC_EMAILTEXTGROUP, m_EmailTextGroup);
	DDX_Control(pDX, IDC_EMAILSGROUP, m_EmailsGroup);
	DDX_Control(pDX, IDC_ADDRBGROUP, m_AddrBGroup);
	DDX_Control(pDX, IDC_FOLDERSGROUP, m_FoldersGroup);
	DDX_Control(pDX, IDC_DELETE_MAIL, m_btnDeleteMail);
	DDX_Control(pDX, IDC_REPLY, m_btnReply);
	DDX_Control(pDX, IDC_FORWARD, m_btnForward);
	DDX_Control(pDX, IDC_CONTINUE_DRAFT, m_btnContinueDraft);
	DDX_Control(pDX, IDC_SAVE_ATTACH, m_btnSaveAttach);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CEmailClientDlg, CDialog)
	ON_MESSAGE(MYWM_OPCOMPLETE, OnOpComplete)
	ON_MESSAGE(UWM_ACC_CHANGED, OnAccChanged) 
	//{{AFX_MSG_MAP(CEmailClientDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TBN_DROPDOWN, IDC_TBMAIN, OnToolbarDropDown)
	ON_COMMAND(WM_TBM_MESSAGES, OnTbMessages)
	ON_COMMAND(WM_TBM_ADDRBOOK, OnTbAddrBook)
	ON_COMMAND(WM_TBM_ACCOUNTS, OnTbAccounts)
	ON_COMMAND(WM_TBM_EXIT, OnTbExit)
	ON_COMMAND(ID_ACCOUNTS_ADDACCOUNT, OnAccountsAddaccount)
	ON_COMMAND(ID_ACCOUNTS_VIEWACCOUNTS, OnAccountsViewaccounts)
	ON_COMMAND(ID_RECEIVE_MESSAGES, OnReceiveMessages)
	ON_COMMAND(ID_NEW_MESSAGE, OnNewMessage)
	ON_COMMAND(ID_SEND_MESSAGE, OnSendMessage)
	ON_COMMAND(ID_CONTACTS_ADDCONTACT, OnContactsAddcontact)
	ON_COMMAND(ID_CONTACTS_VIEWCONTACTS, OnContactsViewcontacts)
	ON_BN_CLICKED(IDC_SAVE_ATTACH, OnSaveAttach)
	ON_NOTIFY(TVN_SELCHANGED, IDC_FOLDER_TREE, OnSelchangedFolderTree)
	ON_BN_CLICKED(IDC_DELETE_MAIL, OnDeleteMail)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_MSGS, OnKeydownListMsgs)
	ON_BN_CLICKED(IDC_CONTINUE_DRAFT, OnContinueDraft)
	ON_BN_CLICKED(IDC_REPLY, OnReply)
	ON_BN_CLICKED(IDC_FORWARD, OnForward)
	ON_WM_TIMER()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_MSGS, OnDblclkListMsgs)
	ON_NOTIFY(NM_CLICK, IDC_LIST_MSGS, OnClickList)
	ON_NOTIFY(NM_SETFOCUS, IDC_FOLDER_TREE, OnSetfocusFolderTree)
	ON_NOTIFY(NM_KILLFOCUS, IDC_FOLDER_TREE, OnKillfocusFolderTree)
	ON_NOTIFY(NM_KILLFOCUS, IDC_LIST_MSGS, OnKillfocusListMsgs)
	ON_NOTIFY(NM_SETFOCUS, IDC_LIST_MSGS, OnSetfocusListMsgs)
	ON_EN_KILLFOCUS(IDC_EDITMailTxt, OnKillfocusEDITMailTxt)
	ON_EN_SETFOCUS(IDC_EDITMailTxt, OnSetfocusEDITMailTxt)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_MSGS, OnItemchangedListMsgs)
	ON_COMMAND(IDR_ADD_CODE, OnAddCode)
	ON_WM_SIZING()
	ON_WM_SIZE()
	ON_LBN_DBLCLK(IDC_CONTACTS_LIST, OnDblclkContactsList)
	ON_LBN_SETFOCUS(IDC_CONTACTS_LIST, OnSetfocusContactsList)
	ON_LBN_KILLFOCUS(IDC_CONTACTS_LIST, OnKillfocusContactsList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEmailClientDlg message handlers

BOOL CEmailClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	m_bSendingInProgres = FALSE;

	m_iSelection = ECSettings.iCurrentAccount;
	EmailClientFolder = ECSettings.AccountsFolder;

	RootFolder = EmailClientFolder;
	EmailClientFolder += "\\Accounts\\";
	
	SetClientToolbar();
	SetEmailList();

	GetAccountList();
	if(AccountList.begin() == AccountList.end()) m_iSelection = -1;

	if(m_iSelection > (-1))
	{
		int index;
		AccListIterator = AccountList.begin();
		for(index = 0; index < m_iSelection; index++)
		{
			AccListIterator++;
				if(AccListIterator == AccountList.end())
				{
					m_iSelection = index;
					break;
				}
		}

	}

	m_ContactList.SetItemHeight(16);
	m_ContactList.AddIcon(IDI_ICON_ADDRESS_CARD);
	
	if(m_iSelection != -1) 
	{
		ReadContacts();
		refresContactsList();
	}

	CRect rcSBRect;
	m_SBar.Create (WS_CHILD | WS_VISIBLE | CCS_BOTTOM | SBARS_SIZEGRIP , rcSBRect, this, 1234);

	if (m_imgTree.Create(16, 16, ILC_COLOR24 , 0, 10))
	{
		HINSTANCE hInst = ::GetModuleHandle(NULL);

		m_imgTree.Add((HICON)::LoadImage(hInst, MAKEINTRESOURCE(IDI_ICON_LOCAL_FOLDERS), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR | LR_LOADTRANSPARENT ));
		m_imgTree.Add((HICON)::LoadImage(hInst, MAKEINTRESOURCE(IDI_ICON_INBOX), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR | LR_LOADTRANSPARENT ));
		m_imgTree.Add((HICON)::LoadImage(hInst, MAKEINTRESOURCE(IDI_ICON_OUTBOX), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR | LR_LOADTRANSPARENT ));
		m_imgTree.Add((HICON)::LoadImage(hInst, MAKEINTRESOURCE(IDI_ICON_SENT), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR | LR_LOADTRANSPARENT ));
		m_imgTree.Add((HICON)::LoadImage(hInst, MAKEINTRESOURCE(IDI_ICON_TRASH), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR | LR_LOADTRANSPARENT ));
		m_imgTree.Add((HICON)::LoadImage(hInst, MAKEINTRESOURCE(IDI_ICON_DRAFTS), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR | LR_LOADTRANSPARENT ));

		m_cTreeCtrl.SetImageList (&m_imgTree, TVSIL_NORMAL); 
	}

	LOGFONT lf; 
	memset(&lf, 0, sizeof(LOGFONT)); 
    lf.lfCharSet = ANSI_CHARSET; 
    lf.lfHeight = 14; 
//	lf.lfWidth = 6;
	lf.lfWeight = FW_NORMAL;
    strcpy(lf.lfFaceName, "Verdana");
    HFONT m_font = ::CreateFontIndirect(&lf); 
    m_ctrlEditMailText.SetFont(CFont::FromHandle(m_font), TRUE); 
	m_ctrlEditMailText.SetBkColor (RGB(248, 248, 248));
	m_ctrlEditMailText.SetTextColor (RGB(32, 32, 128));
	m_ctrlEditMailText.SetReadOnly(TRUE);
	
	OwnerDrawButtons();
	GetClientRect(&rcInitial);
	
//	ReadLocalUserCode();

	bWindowInitialized = TRUE;

	::SetTimer (m_hWnd, 1984, 30, NULL);

	return TRUE;
}


void CEmailClientDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CEmailClientDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CEmailClientDlg::OnOK() 
{
	
}

void CEmailClientDlg::OnCancel() 
{
	if (MessageBox("Exit from Email Client ?", "Email Client", MB_YESNO | MB_ICONQUESTION) == IDNO) return;

	m_imgTree.DeleteImageList();

	CDialog::OnCancel();
}

void CEmailClientDlg::OnToolbarDropDown(NMHDR *pNotifyStruct, LRESULT *result)
{
	LPNMTOOLBAR ptb = (LPNMTOOLBAR) pNotifyStruct;
	
	switch (ptb->iItem)
	{
	case WM_TBM_MESSAGES: {
								ShowMessagesMenu ();
								break;
	}
	case WM_TBM_ACCOUNTS:
								ShowAccountsMenu();
								break;
	case WM_TBM_ADDRBOOK:		
								ShowContactsMenu();
								break;

	}
}

void CEmailClientDlg::OnTbMessages()
{
	OnReceiveMessages();
}

void CEmailClientDlg::OnTbAddrBook()
{
	DisplayAddrBook();
}

void CEmailClientDlg::OnTbAccounts()
{
	DisplayAccounts();
}

void CEmailClientDlg::OnTbExit()
{
	OnCancel();
}


void CEmailClientDlg::SetClientToolbar()
{	
	m_MainTb.Create (WS_CHILD | WS_VISIBLE | CCS_TOP | TBSTYLE_FLAT | TBSTYLE_WRAPABLE | TBSTYLE_TOOLTIPS, CRect(0,0,0,0), this, IDC_TBMAIN);
	m_MainTb.SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);

	m_MainTb.SetButtonSize(CSize(54, 32));
	m_MainTb.SetBitmapSize(CSize(54, 32));
	
	//Load the images for the buttons
	COLORMAP cmask;
	
	cmask.from = RGB(212, 210, 204);
	cmask.to   = ::GetSysColor(COLOR_BTNFACE);
	
	//tbMainBmp.LoadMappedBitmap(IDR_MAINTOOLBAR, IMAGE_BITMAP, &cmask, 1);
	tbMainBmp.LoadMappedBitmap(IDB_MAINTB_E, IMAGE_BITMAP, &cmask, 1);
	//tbMainBmp.LoadMappedBitmap(IDB_MAINTB, IMAGE_BITMAP, &cmask, 1);
	m_MainTb.AddBitmap(4, &tbMainBmp);
	
	m_MainTb.AutoSize();

	SetClientToolbarStrings();
	SetClientToolbarButtons();	
}

void CEmailClientDlg::SetClientToolbarStrings()
{
	CString s;
	int len;
	
	s.LoadString(IDS_TOOLBAR_STRINGS);
	len = s.GetLength();
	for(int i=0;i<len;i++)
	{
		if(s[i] == '\\')
			s.SetAt(i, '\0');
	}

	m_MainTb.AddStrings((LPCTSTR) s);
}

void CEmailClientDlg::SetClientToolbarButtons()
{
	TBBUTTON tb;
	int index = 0, tbindex = 0;
	

	while (tbMainStyles[index] != TBSTYLE_TERM) 
	{
		memset(&tb, 0, sizeof(TBBUTTON));
		tb.fsState = tbMainStates[index];
		tb.fsStyle = tbMainStyles[index];
		tb.iBitmap = tbindex;
		if (tb.fsStyle != TBSTYLE_SEP) tb.iBitmap = tbindex++;
		tb.iString = index;
		tb.idCommand = tbMainCommands[index];
		index++;
		
		m_MainTb.AddButtons(1, &tb);
	}
}

void CEmailClientDlg::ShowAccountsMenu()
{
	RECT rect;
	CMenu menu;
	CMenu* submenu = NULL;

	m_MainTb.GetItemRect(2, &rect);
	ClientToScreen(&rect);

	menu.LoadMenu(IDR_ACCOUNTS_MENU);
	submenu = menu.GetSubMenu(0);

	submenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rect.left, rect.bottom+2, this, NULL);

	submenu->DestroyMenu();
	menu.DestroyMenu();	
}

void CEmailClientDlg::OnAccountsAddaccount() 
{
	AddAccount();
}

void CEmailClientDlg::OnAccountsViewaccounts() 
{
	DisplayAccounts();
}

void CEmailClientDlg::DisplayAccounts()
{
	GetDlgItem(IDC_SAVE_ATTACH)->EnableWindow(FALSE);

	CAccountsDlg AccDlg;
	AccDlg.m_iSelected = m_iSelection;
	AccDlg.DoModal();
}

void CEmailClientDlg::AddAccount()
{
	CAddAccountDlg AddDlg;

	AddDlg.DoModal();
}

void CEmailClientDlg::ShowMessagesMenu()
{
	RECT rect;
	CMenu menu;
	CMenu* submenu = NULL;
	
	m_MainTb.GetItemRect(0, &rect);
	ClientToScreen(&rect);
	
	menu.LoadMenu(IDR_MESSAGES_MENU);
	submenu = menu.GetSubMenu(0);
	
	submenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rect.left, rect.bottom+2, this, NULL);
	
	submenu->DestroyMenu();
	menu.DestroyMenu();	
}

void CEmailClientDlg::OnReceiveMessages() 
{
	if(m_bSendingInProgres)
	{
		CString sMsg;	
		sMsg.LoadString(IDS_PROGR_SENDING_MAILS);		

		AfxMessageBox(sMsg, MB_ICONEXCLAMATION);
		
		return;
	}
	AccListIterator = AccountList.begin();
	for(int nIndex = 0; nIndex < m_iSelection;nIndex++)
		AccListIterator++;
	if((*AccListIterator).GetConnectionType() == 1)
	{
		if(!SetDialUpConnection())
			return;
	}
	if(!(*AccListIterator).GetPOP3RememberPassword())
	{
		CLogon lgdlg;
		if(lgdlg.DoModal() == IDOK)
		{
			(*AccListIterator).SetPOP3UserName(lgdlg.m_sUserName);
			(*AccListIterator).SetPOP3Password(lgdlg.m_sPassword);
		}
		else
		{
			return;
		}
	}
	
	CReciveProgres receiveWnd;
	receiveWnd.m_iSelected = m_iSelection;
	receiveWnd.DoModal();

	if (receiveWnd.dwTotalBytes) 
	{
		UpdateFolderTree();
		RefreshSelectedTreeFolder();
	}
}

void CEmailClientDlg::OnNewMessage() 
{
	if(AccountList.begin() == AccountList.end())
	{
		CString sMsg;	
		sMsg.LoadString(IDS_ERR_NO_MAIL_ACC);
		AfxMessageBox(sMsg);

		return;
	}

	CComposeNewMessage dlgMessage(this, COMPOSE_TYPE_NEW);
	dlgMessage.m_iSelected = m_iSelection;

	if(dlgMessage.DoModal() == IDOK)
	{
		m_strMsg += dlgMessage.MsgDestination;
		m_strMsg +='?';
		if(!m_bSendingInProgres)
		{
			m_bSendingInProgres = TRUE;
			StartSendMessage();
		}
		
		UpdateFolderTree();
		RefreshSelectedTreeFolder();
	}	
}

void CEmailClientDlg::OnSendMessage() 
{
	CString sMsg;
	int nIndex;

	if(m_iSelection == (-1))
	{
		sMsg.LoadString(IDS_ERR_NO_ACC_SEL);
		AfxMessageBox(sMsg);
		
		return;
	}
	if(m_bSendingInProgres)
	{
		sMsg.LoadString(IDS_PROGR_SENDING_MAILS);		
		AfxMessageBox(sMsg, MB_ICONEXCLAMATION);

		return;
	}

	AccListIterator = AccountList.begin();
	for(nIndex = 0; nIndex < m_iSelection; nIndex++) AccListIterator++;

	if((*AccListIterator).GetConnectionType() == 1)
	{
		if(!SetDialUpConnection()) return;
	}

	CString MsgDestination;
	CString MailFile;
	CString FilePath;
	
	int m_iNrFiles = 0;
	int m_iCurentMail = 0;
	int m_iIndex = 0;

	MsgDestination = EmailClientFolder;
	MsgDestination += (*AccListIterator).GetAccountName();
	MsgDestination += "\\";
	MsgDestination += OUTBOX_FOLDER_NAME;
	MsgDestination += "\\";
	
	WIN32_FIND_DATA w32fd = {0};
	HANDLE hSearch = INVALID_HANDLE_VALUE;

	if ((hSearch = ::FindFirstFile(MsgDestination + "*.mil", &w32fd)) != INVALID_HANDLE_VALUE) 
	{
		while(1) 
		{
			if ((strcmp (w32fd.cFileName, ".")) && strcmp(w32fd.cFileName,".."))
			{
				MailFile.Empty();
				MailFile += w32fd.cFileName;					
				MailFile += "?_!_!";

				FilePath = MailFile.Mid(0,MailFile.Find("?_!_!",0));

				CString path;
				path = MsgDestination;
				path += "\\";
				path += FilePath;
				DWORD size;
				CString sFileBuffer;
				CString DataFieldValue;
				CMailMsg msg;

				try 
				{
					CFile pFile (path, CFile::modeRead);
					size = pFile.GetLength();

					unsigned char* lpBuff = NULL;
					if ((lpBuff = (unsigned char*) malloc(size + 1)) == NULL)
					{
						pFile.Close();
						::FindClose(hSearch);
						::MessageBox(NULL, "Cannot allocate memory to read mail file", path, MB_OK | MB_ICONSTOP);
						
						return;
					}

					memset (lpBuff, 0x00, size + 1);						

					if (pFile.Read((void *) lpBuff, size) != size)
					{
						free(lpBuff);
						pFile.Close();
						::FindClose(hSearch);
						::MessageBox(NULL, "Cannot correctly read mail file", path, MB_OK | MB_ICONSTOP);

						return;
					}

					pFile.Close();

					LPSTR pCS = sFileBuffer.GetBuffer(size);
					ASSERT (pCS != NULL);
					
					memset(pCS, 0x00, size);
					memmove (pCS, lpBuff, size);
					sFileBuffer.ReleaseBuffer(size);			

					free(lpBuff);

					msg = ParseMessage(sFileBuffer);

					CSendMail dlgSendMail (this, msg);
					dlgSendMail.SMTPServerName = (*AccListIterator).GetSMTPServerName();
					dlgSendMail.SMTPPort = (*AccListIterator).GetSMTPPort();
					dlgSendMail.SMTPEmailAddress = (*AccListIterator).GetSMTPEmailAddress();
					dlgSendMail.SMTPPassword = (*AccListIterator).GetSMTPPassword();
					dlgSendMail.SMTPUserName = (*AccListIterator).GetSMTPUserName();
					dlgSendMail.TheMessage = sFileBuffer;

					::MessageBox (NULL, sFileBuffer, "CEmailClientDlg::OnSendMessage()", MB_OK | MB_ICONINFORMATION);
					
					sFileBuffer.Empty();

					dlgSendMail.DoModal();

					BYTE nSendStat = dlgSendMail.bSendStatus;

					if (!nSendStat)
					{
						CString OutString;
						CString InString;
						OutString = MsgDestination.Mid(0, MsgDestination.Find(OUTBOX_FOLDER_NAME,0));
						OutString += "\\";
						OutString += SENT_FOLDER_NAME;
						OutString += "\\";
						OutString += FilePath;
						InString = MsgDestination + FilePath;
						::MoveFile(InString, OutString);
					}
					else 
					{
						if ((nSendStat == 1) || (nSendStat == 3)) 
						{
							::PostMessage(m_hWnd, MYWM_OPCOMPLETE, 1, 0);
							break;
						}
						 else if (nSendStat == 2) 
						 {
							 ::PostMessage(m_hWnd, MYWM_OPCOMPLETE, 2, 0);
							 break;
						 }
					}				
				} 
				catch (CFileException *ex) 		
				{
					CString sMsg;
					sMsg.LoadString(IDS_ERR_RD_FILE);			
					AfxMessageBox(sMsg);
					
					ex->Delete();
				}
				
				m_iCurentMail++;

				m_iIndex = MailFile.Find("?_!_!",0) + 5;				
				m_iNrFiles++;
			}
			
			if (!::FindNextFile(hSearch, &w32fd)) break;
			
		}
		::FindClose(hSearch);
	}

	if (m_iNrFiles) 
	{
		UpdateFolderTree();
		RefreshSelectedTreeFolder();
	}
	else
	{
		sMsg.LoadString(IDS_NO_MSG_IN_OUTBOX);
		m_SBar.SetText (sMsg, 0, 0);
	}

}

void CEmailClientDlg::AddContact()
{
	if((m_iSelection > (-1)) && (AccountList.size() > m_iSelection))
	{
		int nIndex;
		AccListIterator = AccountList.begin();
		for(nIndex = 0; nIndex < m_iSelection;nIndex++)
			AccListIterator++;
		CString Path;
		Path = EmailClientFolder;
		Path += (*AccListIterator).GetAccountName();
		Path += "\\";
		Path += CONTACTS_FOLDER_NAME;
		Path += "\\";
		Path += "contacts.sav";
		BOOL bResult = FALSE;
		CFile* pFile = new CFile();
		bResult = pFile->Open (Path, CFile::modeReadWrite);
		if (bResult)
		{
			pFile->Close();
			delete pFile;
			m_addressBook.readAddressBook(Path);
		}
		CAddContactsDlg AddContactDlg;
		if(AddContactDlg.DoModal() == IDOK){
			m_addressBook.addContact(AddContactDlg.m_contact); 
			m_addressBook.saveAddressBook(Path);
		}
		
	}
	else
	{
		CString sMsg;
		sMsg.LoadString(IDS_ERR_NO_ACC_SEL);
		MessageBox(sMsg);
	}
}

void CEmailClientDlg::DisplayAddrBook()
{
	if((m_iSelection > (-1)) && (AccountList.size() > m_iSelection))
	{
		CContactsDialog contactDlg(this);
		contactDlg.m_iSelected = m_iSelection;
		contactDlg.DoModal();
		
		refresContactsList();
	}
	else
	{
		CString sMsg;		
		sMsg.LoadString(IDS_ERR_NO_ACC_SEL);
		AfxMessageBox(sMsg);
	}
}

void CEmailClientDlg::ShowContactsMenu()
{
	RECT rect;
	CMenu menu;
	CMenu* submenu = NULL;
	
	m_MainTb.GetItemRect(1, &rect);
	ClientToScreen(&rect);
	
	menu.LoadMenu(IDR_CONTACTS_MENU);
	submenu = menu.GetSubMenu(0);
	
	submenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rect.left, rect.bottom+2, this, NULL);
	
	submenu->DestroyMenu();
	menu.DestroyMenu();	

}

void CEmailClientDlg::OnContactsAddcontact() 
{
	AddContact();	
}

void CEmailClientDlg::OnContactsViewcontacts() 
{
	DisplayAddrBook();
}

void CEmailClientDlg::ReadContacts()
{
	if(m_iSelection == (-1))
		return;
	int nIndex;
	AccListIterator = AccountList.begin();
	for(nIndex = 0; nIndex < m_iSelection;nIndex++)
		AccListIterator++;
	CString Path;
	Path = EmailClientFolder;
	Path += (*AccListIterator).GetAccountName();
	Path += "\\";
	Path += CONTACTS_FOLDER_NAME;
	Path += "\\";
	Path += "contacts.sav";
	BOOL bResult = FALSE;
	CFile* pFile = new CFile();
	bResult = pFile->Open (Path, CFile::modeReadWrite);
	if (bResult)
	{
		pFile->Close();
		m_addressBook.readAddressBook(Path);
	}
	else
	{
		m_addressBook.clearAddressBook();
	}

	delete pFile;

}

void CEmailClientDlg::refresContactsList()
{
	int i = 0;
	CString	strText;

	m_ContactList.ResetContent();

	for (m_addressBook.ContactListIterator = m_addressBook.myContactList.begin() ; m_addressBook.ContactListIterator != m_addressBook.myContactList.end(); m_addressBook.ContactListIterator++, i++)
	{
		strText = (*(m_addressBook.ContactListIterator)).m_NickName;
		m_ContactList.AddString (strText);
		
	}
}


void CEmailClientDlg::SetEmailList()
{
	CString sMsg;
	CStringArray sArrHdr;
	
	m_Msgs.SetExtendedStyle(LVS_EX_FULLROWSELECT);	
	
	sMsg.LoadString(IDS_SUBJECT);
	sArrHdr.Add(sMsg);

	sMsg.LoadString(IDS_FROM);
	sArrHdr.Add(sMsg);

	sMsg.LoadString(IDS_DATE);
	sArrHdr.Add(sMsg);

	sMsg.LoadString(IDS_ATTACH);
	sArrHdr.Add(sMsg);

	CRect rect;
	m_Msgs.GetWindowRect(&rect);

	int w = rect.Width();
	int colwidths[NUMBER_OF_EMAIL_LIST_COLUMNS] = {9, 8, 7, 12};	// sixty-fourths

	int i;
	int total_cx = 0;
	LV_COLUMN lvcolumn;
	memset(&lvcolumn, 0, sizeof(lvcolumn));
	_TCHAR sColName[64];

	for (i = 0; i<NUMBER_OF_EMAIL_LIST_COLUMNS; i++)
	{
		lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;
		lvcolumn.fmt = LVCFMT_LEFT;
		memset(sColName, 0x00, sizeof(_TCHAR)*64);
		_tcsncpy (sColName, sArrHdr.GetAt(i), (sArrHdr.GetAt(i)).GetLength());
		lvcolumn.pszText = (_TCHAR*)&sColName;
		lvcolumn.iSubItem = i;
		lvcolumn.cx = (w * colwidths[i]) / 24;
		OrigColWidths[i] = lvcolumn.cx;

		total_cx += lvcolumn.cx;
		m_Msgs.InsertColumn(i, &lvcolumn);
	}

}

void CEmailClientDlg::OnClickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LPNMLISTVIEW lpItem = (LPNMLISTVIEW) pNMHDR;

	CString sFileBuffer;
	CString itemText;

	int crt = -1;
	if ((crt = lpItem->iItem) == -1)
	{
		*pResult = 0;
		return;
	}

	m_sCurrentSelectedFile = arrFilesInFolder.GetAt(crt);

	try 
	{
		CFile pFile (m_sCurrentSelectedFile, CFile::modeRead);
		int size = pFile.GetLength();

		unsigned char* lpBuff = NULL;
		if ((lpBuff = (unsigned char*) malloc(size + 1)) == NULL)
		{
			pFile.Close();
			::MessageBox(m_hWnd, "Cannot allocate memory to read mail file", m_sCurrentSelectedFile, MB_OK | MB_ICONSTOP);
			
			*pResult = 0;
			return;
		}

		memset (lpBuff, 0x00, size + 1);

		if (pFile.Read((void *) lpBuff, size) != size)
		{
			free(lpBuff);
			pFile.Close();
			::MessageBox(m_hWnd, "Cannot correctly read mail file", m_sCurrentSelectedFile, MB_OK | MB_ICONSTOP);
			
			*pResult = 0;
			return;
		}

		pFile.Close();

		LPSTR pCS = sFileBuffer.GetBuffer(size);
		ASSERT (pCS != NULL);
		memset(pCS, 0x00, size);
		memmove (pCS, lpBuff, size);
		sFileBuffer.ReleaseBuffer(size);
					
		free(lpBuff);

		CMailMsg msg = ParseMessage(sFileBuffer);
		
		GetDlgItem(IDC_EDITMailTxt)->SetWindowText(msg.m_TextBody);

		BOOL sendingmsgs = FALSE;
		if(m_bSendingInProgres)
		{
			HTREEITEM selItem = m_cTreeCtrl.GetSelectedItem();
			if (selItem == m_htreeOutbox) sendingmsgs = TRUE;
		}

		if (!sendingmsgs)
		{
			int ind = -1;
			ind = m_sCurrentSelectedFile.ReverseFind('\\');
			
			if(m_sCurrentSelectedFile.Find('u', ind) == ind + 1)
			{	
				CString newName = m_sCurrentSelectedFile;
				newName.Delete (ind+1, 1);
				CFile::Rename (m_sCurrentSelectedFile, newName);

				arrFilesInFolder.SetAt(crt, newName);

				m_sCurrentSelectedFile = newName;

				m_Msgs.SetBold(crt, 0, FALSE);
				m_Msgs.SetBold(crt, 1, FALSE);
				m_Msgs.SetBold(crt, 2, FALSE);
				m_Msgs.SetBold(crt, 3, FALSE);	

				UpdateFolderTree();
			}
		}

		sFileBuffer.Empty();
	} 
	catch (CFileException *ex) 		
	{				
		CString sMsg;						
		sMsg.LoadString(IDS_ERR_RD_FILE);			
		AfxMessageBox(sMsg);

		ex->Delete();
	}

	CString Atachments = m_Msgs.GetItemText(crt, 3);
	
	if(!Atachments.IsEmpty())
	{
		GetDlgItem(IDC_SAVE_ATTACH)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_SAVE_ATTACH)->EnableWindow(FALSE);
	}

	if(m_sCurrentSelectedFile.Find(DRAFT_FOLDER_NAME,0) != (-1))
	{
		GetDlgItem(IDC_CONTINUE_DRAFT)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_CONTINUE_DRAFT)->EnableWindow(FALSE);
	}

	*pResult = 0;
}

void CEmailClientDlg::OnSaveAttach() 
{
	int iItem = m_Msgs.GetNextItem(-1, LVIS_SELECTED);
	if (iItem == -1) return;

	CSaveAttachDlg* pDlgSaveAttach = new CSaveAttachDlg();
	if (pDlgSaveAttach == NULL) return;

	CString Files = m_Msgs.GetItemText(m_Msgs.GetSelectionMark(),3);
	int nrFis = atoi(Files.Mid(1,1));
	Files = Files.Mid(5);
				
	for(iItem=0; iItem<nrFis; iItem++)
	{
		pDlgSaveAttach->m_AList.AddTail(Files.Mid(0,Files.Find(";  ",0)));
		Files = Files.Mid(Files.Find(";  ",0) + 3);
	}
	
	if (pDlgSaveAttach->DoModal() == IDOK) 
	{
		CString numeFistmp;
		numeFistmp = pDlgSaveAttach->m_AttachName;
		numeFistmp = numeFistmp.Mid(0,numeFistmp.ReverseFind('('));
		numeFistmp.TrimRight();
			
		CFileDialog SaveDlg(FALSE, "", numeFistmp, OFN_OVERWRITEPROMPT, "All Files (*.*)|*.*||", this);	
		if (SaveDlg.DoModal() == IDOK) 
		{
			SaveAttachment (m_sCurrentSelectedFile, pDlgSaveAttach->m_AttachIndex, SaveDlg.GetPathName());
		}
	}

	delete pDlgSaveAttach;
}

void CEmailClientDlg::OnSelchangedFolderTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	GetDlgItem(IDC_SAVE_ATTACH)->EnableWindow(FALSE);
	GetDlgItem(IDC_CONTINUE_DRAFT)->EnableWindow(FALSE);
	
	RefreshSelectedTreeFolder();
	
	*pResult = 0;
}

void CEmailClientDlg::ViewFolder(CString Folder)
{
	if(m_iSelection == (-1))
	{
		GetDlgItem(IDC_EDITMailTxt)->SetWindowText("");
		m_Msgs.DeleteAllItems();
		return;
	}
	
	int nIndex = 0;
	int ind;
	CString MsgDestination;
	CString MailFile;
	POSITION pos;
	CString sFileBuffer;
	CString DataFieldValue;
	CString Files;
	int nrFiles;

	AccListIterator = AccountList.begin();
	for(nIndex = 0; nIndex < m_iSelection;nIndex++) AccListIterator++;
	
	MsgDestination = EmailClientFolder;
	MsgDestination += (*AccListIterator).GetAccountName();
	MsgDestination += "\\";
	MsgDestination += Folder;
	MsgDestination += "\\";
	
	nIndex = 0;

	GetDlgItem(IDC_EDITMailTxt)->SetWindowText("");
	m_Msgs.DeleteAllItems();
	arrFilesInFolder.RemoveAll();


	WIN32_FIND_DATA w32fd;
	HANDLE hSearch = INVALID_HANDLE_VALUE;

	if ((hSearch = FindFirstFile(MsgDestination + "*.mil", &w32fd)) != INVALID_HANDLE_VALUE) 
	{
		while(1) 
		{
			if ((strcmp (w32fd.cFileName, ".")) && strcmp(w32fd.cFileName,".."))
			{
				MailFile = MsgDestination + w32fd.cFileName;
				arrFilesInFolder.Add(MailFile);
								
				unsigned char* lpBuff = NULL;
				
				try 
				{
					CFile pFile (MailFile, CFile::modeRead);
					DWORD size = pFile.GetLength();

					unsigned char* lpBuff = NULL;
					if ((lpBuff = (unsigned char*) malloc(size + 1)) == NULL)
					{
						pFile.Close();
						::FindClose (hSearch);
						::MessageBox(m_hWnd, "Cannot allocate memory to read mail file", MailFile, MB_OK | MB_ICONSTOP);
						
						return;
					}

					memset (lpBuff, 0x00, size + 1);

					if (pFile.Read((void *) lpBuff, size) != size)
					{
						free(lpBuff);
						pFile.Close();
						::FindClose (hSearch);
						::MessageBox(m_hWnd, "Cannot correctly read mail file", MailFile, MB_OK | MB_ICONSTOP);

						return;
					}

					pFile.Close();

					LPSTR pCS = sFileBuffer.GetBuffer(size);
					ASSERT (pCS != NULL);
					memset(pCS, 0x00, size);
					memmove (pCS, lpBuff, size);
					sFileBuffer.ReleaseBuffer(size);
								
					free(lpBuff);

					CMailMsg msg = ParseMessage(sFileBuffer);

					CString subj, fnm;
					fnm = w32fd.cFileName;
					
					if(fnm.Find('u') == 0)
					{
						subj += msg.m_Subject;
						m_Msgs.InsertItem(nIndex, subj);
						
						if (nIndex%2) m_Msgs.SetItemText(nIndex, 0, subj, RGB(0,0,0), RGB(255,255,255));
						else m_Msgs.SetItemText(nIndex, 0, subj, RGB(0,0,0), RGB(232,232,255));

						m_Msgs.SetBold(nIndex, 0, TRUE);
						m_Msgs.SetBold(nIndex, 1, TRUE);
						m_Msgs.SetBold(nIndex, 2, TRUE);
						m_Msgs.SetBold(nIndex, 3, TRUE);						
					}
					else 
					{
						subj += msg.m_Subject;
						m_Msgs.InsertItem (nIndex, subj);
						
						if (nIndex%2) m_Msgs.SetItemText(nIndex, 0, subj, RGB(0,0,0), RGB(255,255,255));
						else m_Msgs.SetItemText(nIndex, 0, subj, RGB(0,0,0), RGB(232,232,255));
					}
					
					if(m_bCurentRead)
					{
						if (nIndex%2) m_Msgs.SetItemText(nIndex, 1, msg.m_ToAddress, RGB(0,0,0), RGB(255,255,255));
						else m_Msgs.SetItemText(nIndex, 1, msg.m_ToAddress, RGB(0,0,0), RGB(232,232,255));
					}
					else
					{
						CString sFromColumn;
						if (!msg.m_FromName.IsEmpty()) sFromColumn = msg.m_FromName;
						else sFromColumn = msg.m_FromAddr;

						if (nIndex%2) m_Msgs.SetItemText(nIndex, 1, sFromColumn, RGB(0,0,0), RGB(255,255,255));
						else m_Msgs.SetItemText(nIndex, 1, sFromColumn, RGB(0,0,0), RGB(232,232,255));
					}

					if (nIndex%2) m_Msgs.SetItemText(nIndex, 2, msg.m_Date, RGB(0,0,0), RGB(255,255,255));
					else m_Msgs.SetItemText(nIndex, 2, msg.m_Date, RGB(0,0,0), RGB(232,232,255));
				
					Files.Empty();
					nrFiles = msg.AttachmentList.GetCount();
					
					if(nrFiles > 0)
					{
						CString temp;
						temp.Format("(%d)  ", nrFiles);
						Files += temp;
						for(ind = 0; ind < nrFiles; ind++)
						{
							pos = msg.AttachmentList.FindIndex(ind);
							Files += msg.AttachmentList.GetAt(pos);
							Files += " (";
							pos = msg.AttachmentSize.FindIndex(ind);
							Files += msg.AttachmentSize.GetAt(pos);
							Files += ");  ";
						}
					}

					if (nIndex%2) m_Msgs.SetItemText(nIndex, 3, Files, RGB(0,0,0), RGB(255,255,255));
					else m_Msgs.SetItemText(nIndex, 3, Files, RGB(0,0,0), RGB(232,232,255));
									
					sFileBuffer.Empty();
					nIndex++;

				} 
				catch (CFileException *ex) 		
				{
					CString sMsg;					
					sMsg.LoadString(IDS_ERR_RD_FILE);			
					AfxMessageBox(sMsg);

					ex->Delete();
				}
				
			}
			
			if (!FindNextFile(hSearch, &w32fd)) break;
			
		}
		::FindClose(hSearch);
	}
}


void CEmailClientDlg::OnDeleteMail() 
{
	CString sMsg;

	POSITION pos = m_Msgs.GetFirstSelectedItemPosition();

	if (pos == NULL)
	{
		sMsg.LoadString(IDS_ERR_NO_ITEMS_SEL);	
		AfxMessageBox(sMsg);

		return;
	}

	sMsg.Empty();
	sMsg.LoadString(IDS_DEL_MSG);

	int bDeleteAll = IDNO;
	
	while (pos)
	{
		int nItem = m_Msgs.GetNextSelectedItem(pos);

		m_sCurrentSelectedFile.Empty();
		m_sCurrentSelectedFile = arrFilesInFolder.GetAt(nItem);
		
		if (m_sCurrentSelectedFile.Find (DELETED_FOLDER_NAME, 0) != -1)
		{
			if (bDeleteAll == IDNO)
			{
				if ((bDeleteAll = AfxMessageBox (sMsg ,MB_YESNO | MB_ICONQUESTION)) == IDYES)
				{
					::DeleteFile(m_sCurrentSelectedFile);
					GetDlgItem(IDC_SAVE_ATTACH)->EnableWindow(FALSE);
					continue;
				}
				
				return;
			}
			else
			{
				::DeleteFile(m_sCurrentSelectedFile);
				GetDlgItem(IDC_SAVE_ATTACH)->EnableWindow(FALSE);
			}
		}
		else
		{
			CString m_newSelectedFile = m_sCurrentSelectedFile;

			m_newSelectedFile.Replace(INBOX_FOLDER_NAME, DELETED_FOLDER_NAME);
			m_newSelectedFile.Replace(OUTBOX_FOLDER_NAME, DELETED_FOLDER_NAME);
			m_newSelectedFile.Replace(SENT_FOLDER_NAME, DELETED_FOLDER_NAME);
			m_newSelectedFile.Replace(DRAFT_FOLDER_NAME, DELETED_FOLDER_NAME);

			::MoveFile (m_sCurrentSelectedFile, m_newSelectedFile);
			GetDlgItem(IDC_SAVE_ATTACH)->EnableWindow(FALSE);
		}
	}	

	UpdateFolderTree();
	RefreshSelectedTreeFolder();
	
}

void CEmailClientDlg::OnKeydownListMsgs(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	if(pLVKeyDow->wVKey == VK_DELETE)
		OnDeleteMail();	
	*pResult = 0;
}

void CEmailClientDlg::OnContinueDraft() 
{
	m_sCrtSelFile = m_sCurrentSelectedFile;

	CComposeNewMessage dlgMessage(this, COMPOSE_TYPE_NEW);
	dlgMessage.m_sCrtSelFile = m_sCrtSelFile;

	if(AccountList.begin() == AccountList.end())
	{
		CString sMsg;
		sMsg.LoadString(IDS_ERR_NO_MAIL_ACC);
		AfxMessageBox(sMsg);

		return;
	}

	dlgMessage.m_iSelected = m_iSelection;
	if(dlgMessage.DoModal() == IDOK)
	{
		m_strMsg += dlgMessage.MsgDestination;
		m_strMsg +='?';
		if(!m_bSendingInProgres)
		{
			m_bSendingInProgres = TRUE;
			StartSendMessage();
		}
	}
	UpdateFolderTree();
	RefreshSelectedTreeFolder();
	
}


CString CEmailClientDlg::CountUnread(CString Folder, int& nTotal, int& nUnread)
{
	if(AccountList.begin() == AccountList.end()) return Folder;

	CString treeItemText;
	int nIndex;
	
	CString MsgDestination;
	CString number;

	AccListIterator = AccountList.begin();
	for(nIndex = 0; nIndex < m_iSelection;nIndex++) AccListIterator++;
	
	MsgDestination = EmailClientFolder;
	MsgDestination += (*AccListIterator).GetAccountName();
	MsgDestination += "\\";
	MsgDestination += Folder;
	MsgDestination += "\\";

	CString filesUnread;
	filesUnread.Empty();

	treeItemText = "";
	nIndex = 0;
	
	int noFilesUnread = 0;	
	nUnread = 0;
	nTotal = 0;
	
	WIN32_FIND_DATA w32fd = {0};
	HANDLE hSearch = INVALID_HANDLE_VALUE;

	if ((hSearch = ::FindFirstFile(MsgDestination + "*.mil", &w32fd)) != INVALID_HANDLE_VALUE) 	
	{
		while(1) 
		{
			if ((strcmp (w32fd.cFileName, ".")) && strcmp (w32fd.cFileName,".."))
			{
				if (w32fd.cFileName[0] == 'u')		// ie: u091830WedJul112007.mil
				{
					noFilesUnread++;
					nUnread++;
				}
				nTotal++;
			}
			memset (&w32fd, 0x00, sizeof(WIN32_FIND_DATA));
			if (!::FindNextFile(hSearch, &w32fd)) break;
		}

		::FindClose(hSearch);

		number.Empty();
		number.Format("(%d)", noFilesUnread);

		if (noFilesUnread) 
		{
			filesUnread += " ";
			filesUnread += number;
		}
	}
	
	treeItemText += Folder;
	treeItemText += filesUnread;
	
	return treeItemText;
}

void CEmailClientDlg::RefreshSelectedTreeFolder()
{
	HTREEITEM selItem = m_cTreeCtrl.GetSelectedItem();
	
	LVCOLUMN col = {0};
	col.mask = LVCF_TEXT;
	col.fmt = LVCFMT_LEFT;
	
	int nTotal = 0;
	int nUnread = 0;

	CString sMsg;

	if(selItem == m_htreeRoot)
	{
		m_Msgs.DeleteAllItems();
		m_SBar.SetText ("", 1, 0);
		m_SBar.SetText ("", 2, 0);
		
		return;
	}

	if(selItem == m_htreeInbox)
	{
		sMsg.LoadString(IDS_FROM);
		col.pszText = (char*)(LPCTSTR)sMsg;
		m_Msgs.SetColumn (1, &col);
		m_bCurentRead = FALSE;
		m_strInbox = CountUnread(INBOX_FOLDER_NAME, nTotal, nUnread);
		m_cTreeCtrl.SetItemText(m_htreeInbox, m_strInbox);		
		
		ViewFolder(INBOX_FOLDER_NAME);
	}
	else if(selItem == m_htreeOutbox)
	{
		sMsg.LoadString(IDS_TO);
		col.pszText = (char*)(LPCTSTR)sMsg;
		m_Msgs.SetColumn(1, &col);
		m_bCurentRead = TRUE;
		m_strOutbox = CountUnread(OUTBOX_FOLDER_NAME, nTotal, nUnread);	
		m_cTreeCtrl.SetItemText(m_htreeOutbox, m_strOutbox);	
		
		ViewFolder(OUTBOX_FOLDER_NAME);
	}
	else if(selItem == m_htreeSent)
	{
		sMsg.LoadString(IDS_TO);
		col.pszText = (char*)(LPCTSTR)sMsg;
		m_Msgs.SetColumn(1, &col);
		m_bCurentRead = TRUE;
		m_strSent = CountUnread(SENT_FOLDER_NAME, nTotal, nUnread);
		m_cTreeCtrl.SetItemText(m_htreeSent, m_strSent);

		ViewFolder(SENT_FOLDER_NAME);
	}
	else if(selItem == m_htreeDeleted)
	{
		sMsg.LoadString(IDS_FROM);
		col.pszText = (char*)(LPCTSTR)sMsg;
		m_Msgs.SetColumn(1, &col);
		m_bCurentRead = FALSE;
		m_strDeleted = CountUnread(DELETED_FOLDER_NAME, nTotal, nUnread);
		m_cTreeCtrl.SetItemText(m_htreeDeleted, m_strDeleted);

		ViewFolder(DELETED_FOLDER_NAME);
	}
	else if(selItem == m_htreeDraft)
	{
		sMsg.LoadString(IDS_TO);
		col.pszText = (char*)(LPCTSTR)sMsg;
		m_Msgs.SetColumn(1, &col);
		m_bCurentRead = TRUE;
		m_strDraft = CountUnread(DRAFT_FOLDER_NAME, nTotal, nUnread);
		m_cTreeCtrl.SetItemText(m_htreeDraft, m_strDraft);
	
		ViewFolder(DRAFT_FOLDER_NAME);
	}

	UpdateStatusBarMailPanels(nUnread, nTotal);

	if (m_Msgs.GetItemCount()) m_Msgs.SetCurSel(0);

}


void CEmailClientDlg::OnReply() 
{
	CString sMsg;
	
	POSITION pos = m_Msgs.GetFirstSelectedItemPosition();
	if (pos == NULL)
	{
		sMsg.LoadString(IDS_SEL_EMAIL_FIRST);
		AfxMessageBox(sMsg);

		return;
	}

	int ind;
	CString newName;
	ind = m_sCurrentSelectedFile.ReverseFind('\\');
	BOOL sendingmsgs = FALSE;
	if(m_bSendingInProgres)
	{
		HTREEITEM selItem = m_cTreeCtrl.GetSelectedItem();
		if(selItem == m_htreeOutbox)
			sendingmsgs = TRUE;
	}
	if(!sendingmsgs)
	{
	if(m_sCurrentSelectedFile.Find('u', ind) == ind + 1)
		m_sCurrentSelectedFile.Delete(ind+1, 1);
	}
	m_sCrtSelFile = m_sCurrentSelectedFile;

	CComposeNewMessage dlgMessage(this, COMPOSE_TYPE_REPLY);
	dlgMessage.m_sCrtSelFile = m_sCrtSelFile;

	if(AccountList.begin() == AccountList.end())
	{
		sMsg.LoadString(IDS_ERR_NO_MAIL_ACC);
		AfxMessageBox(sMsg);

		return;
	}
	dlgMessage.m_iSelected = m_iSelection;
	if(dlgMessage.DoModal() == IDOK)
	{
		m_strMsg += dlgMessage.MsgDestination;
		m_strMsg +='?';
		if(!m_bSendingInProgres)
		{
			m_bSendingInProgres = TRUE;
			StartSendMessage();
		}
	}

	UpdateFolderTree();	
}

void CEmailClientDlg::OnForward() 
{
	CString sMsg;	
	POSITION pos = m_Msgs.GetFirstSelectedItemPosition();
	if (pos == NULL)
	{
		sMsg.LoadString(IDS_SEL_EMAIL_FIRST);
		AfxMessageBox(sMsg);

		return;
	}

	int ind;
	CString newName;
	ind = m_sCurrentSelectedFile.ReverseFind('\\');
	BOOL sendingmsgs = FALSE;
	if(m_bSendingInProgres)
	{
		HTREEITEM selItem = m_cTreeCtrl.GetSelectedItem();
		if(selItem == m_htreeOutbox)
			sendingmsgs = TRUE;
	}
	if(!sendingmsgs)
	{
	if(m_sCurrentSelectedFile.Find('u', ind) == ind + 1)
		m_sCurrentSelectedFile.Delete(ind+1, 1);
	}

	m_sCrtSelFile = m_sCurrentSelectedFile;

	CComposeNewMessage dlgMessage(this, COMPOSE_TYPE_FWD);
	dlgMessage.m_sCrtSelFile = m_sCrtSelFile;

	if(AccountList.begin() == AccountList.end())
	{
		CString sMsg;		
		sMsg.LoadString(IDS_ERR_NO_MAIL_ACC);
		AfxMessageBox(sMsg);

		return;
	}
	dlgMessage.m_iSelected = m_iSelection;
	if(dlgMessage.DoModal() == IDOK)
	{
		m_strMsg += dlgMessage.MsgDestination;
		m_strMsg +='?';
		if(!m_bSendingInProgres)
		{
			m_bSendingInProgres = TRUE;
			StartSendMessage();
		}
	}

	UpdateFolderTree();
	
}

LRESULT CEmailClientDlg::OnOpComplete(WPARAM w, LPARAM l)
{
	CString OutString;
	CString sMsg;
	
	switch(w) 
	{
	case 0:
		OutString = msgFile;
		OutString.Replace (OUTBOX_FOLDER_NAME, SENT_FOLDER_NAME);
		::MoveFile(msgFile, OutString);

		UpdateFolderTree();
		RefreshSelectedTreeFolder();
		
		if(m_strMsg.Find('?',0) != (-1))
		{
			StartSendMessage();
		}
		else 
		{
			m_strMsg.Empty();
			m_bSendingInProgres = FALSE;

			sMsg.LoadString(IDS_MSG_SENT);
			m_SBar.SetText (sMsg, 0, 0);
		}
		break;

	case 1:
		{
			m_strMsg.Empty();
			m_bSendingInProgres = FALSE;

			sMsg.LoadString(IDS_ERR_SEND_MSGS2);
			m_SBar.SetText (sMsg, 0, 0);
		}
		break;

	case 2:
		{
			m_strMsg.Empty();
			m_bSendingInProgres = FALSE;

			sMsg.LoadString(IDS_ERR_AUTH);					
			m_SBar.SetText (sMsg, 0, 0);
		}
		break;
	}
	return 0;
}

void CEmailClientDlg::StartSendMessage()
{
	CString sFileBuffer;
	CMailMsg msg;
	CString sMsg;

	msgFile = m_strMsg.Left(m_strMsg.Find('?',0));
	m_strMsg = m_strMsg.Mid(m_strMsg.Find('?',0) + 1);
				
	try 
	{		
		CFile pFile (msgFile, CFile::modeRead);
		DWORD size = pFile.GetLength();

		unsigned char* lpBuff = NULL;
		if ((lpBuff = (unsigned char*) malloc(size + 1)) == NULL)
		{
			pFile.Close();
			::MessageBox(NULL, "Cannot allocate memory to read mail file", msgFile, MB_OK | MB_ICONSTOP);
			
			return;
		}

		memset (lpBuff, 0x00, size + 1);

		if (pFile.Read((void *) lpBuff, size) != size)
		{
			free(lpBuff);
			pFile.Close();
			::MessageBox(NULL, "Cannot correctly read mail file", msgFile, MB_OK | MB_ICONSTOP);

			return;
		}

		pFile.Close();

		LPSTR pCS = sFileBuffer.GetBuffer(size);
		ASSERT (pCS != NULL);
		memset(pCS, 0x00, size);
		memmove (pCS, lpBuff, size);
		sFileBuffer.ReleaseBuffer(size);
					
		free(lpBuff);
		
		msg = ParseMessage(sFileBuffer);

		CString status;				 
		sMsg.LoadString(IDS_SENDING_TO);
		status.Format(sMsg ,msg.m_ToAddress);		
		m_SBar.SetText (status, 0, 0);

		CSendMail dlgSendMail (this, msg);

/* SMTP Account Authentication */

		dlgSendMail.SMTPServerName = (*AccListIterator).GetSMTPServerName();
		dlgSendMail.SMTPPort = (*AccListIterator).GetSMTPPort();
		dlgSendMail.SMTPEmailAddress = (*AccListIterator).GetSMTPEmailAddress();
		dlgSendMail.SMTPPassword = (*AccListIterator).GetSMTPPassword();
		dlgSendMail.SMTPUserName = (*AccListIterator).GetSMTPUserName();

/* SMTP Account Authentication */

		dlgSendMail.TheMessage = sFileBuffer;

		sFileBuffer.Empty();
		
		dlgSendMail.DoModal();


		BYTE nSendStat = dlgSendMail.bSendStatus;

		if (!nSendStat) ::PostMessage(m_hWnd, MYWM_OPCOMPLETE, 0, 0);
		else if ((nSendStat == 1) || (nSendStat == 3)) ::PostMessage(m_hWnd, MYWM_OPCOMPLETE, 1, 0);
			 else if (nSendStat == 2) ::PostMessage(m_hWnd, MYWM_OPCOMPLETE, 2, 0);
		
//		SetTimer (1004, 60000*((*AccListIterator).GetSMTPTimeout()), NULL);	

	}
	catch (CFileException *ex) 		
	{
		sMsg.LoadString(IDS_ERR_RD_FILE);		
		::MessageBox (NULL, sMsg, msgFile, MB_OK | MB_ICONSTOP);

		ex->Delete();
	}
	
}

void CEmailClientDlg::OwnerDrawButtons()
{
	m_btnDeleteMail.SetMaskedBitmap( IDB_DELMAIL, 16, 16, RGB( 255, 255, 255 ) );
	m_btnReply.SetMaskedBitmap( IDB_REPLYMAIL, 20, 16, RGB( 255, 255, 255 ) );
	m_btnForward.SetMaskedBitmap( IDB_FORWARDMAIL, 23, 19, RGB( 255, 255, 255 ) );
	m_btnContinueDraft.SetMaskedBitmap( IDB_CONTDRAFT, 16, 16, RGB( 255, 255, 255 ) );
	m_btnSaveAttach.SetMaskedBitmap( IDB_SAVEATTACH, 16, 16, RGB( 255, 255, 255 ) );

}

void CEmailClientDlg::OnTimer(UINT nIDEvent) 
{
	CString sMsg;

	switch(nIDEvent) 
	{
	case 1984:
		{
			KillTimer(1984);
			OnSize (SIZE_RESTORED, rcInitial.Width(), rcInitial.Height());
			
			CRect rcSBRect;
			::GetClientRect(m_SBar.m_hWnd, &rcSBRect);

			int SBarWidths[4] = {0,0,0, -1};	
			SBarWidths[0] = rcSBRect.Width() - 200;
			SBarWidths[1] = rcSBRect.Width() - 100;		
			SBarWidths[2] = rcSBRect.Width();
			SBarWidths[3] = -1;			
			m_SBar.SetParts (4, SBarWidths);

			int nTotal = 0;
			int nUnread = 0;

			m_strDraft = CountUnread (DRAFT_FOLDER_NAME, nTotal, nUnread);
			m_strDeleted = CountUnread (DELETED_FOLDER_NAME, nTotal, nUnread);
			m_strSent = CountUnread (SENT_FOLDER_NAME, nTotal, nUnread);
			m_strOutbox = CountUnread (OUTBOX_FOLDER_NAME, nTotal, nUnread);
			m_strInbox = CountUnread (INBOX_FOLDER_NAME, nTotal, nUnread);

			sMsg.LoadString(IDS_LOCAL_FOLDERS);
			m_htreeRoot = m_cTreeCtrl.InsertItem(sMsg, 0,0, TVI_ROOT,TVI_LAST);
			
			m_htreeInbox = m_cTreeCtrl.InsertItem(m_strInbox, 1,1, m_htreeRoot, NULL);
			m_htreeOutbox = m_cTreeCtrl.InsertItem(m_strOutbox, 2,2,m_htreeRoot, NULL);
			m_htreeSent = m_cTreeCtrl.InsertItem(m_strSent, 3,3,m_htreeRoot, NULL);
			m_htreeDeleted = m_cTreeCtrl.InsertItem(m_strDeleted, 4,4,m_htreeRoot, NULL);
			m_htreeDraft = m_cTreeCtrl.InsertItem(m_strDraft, 5,5, m_htreeRoot, NULL);

			m_cTreeCtrl.Select(m_htreeInbox,TVGN_CARET);

			m_Msgs.SetSelectionMark(0);
			m_Msgs.SetItemState(0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);	


			break;
		}

	case 1004:
		{
			KillTimer(1004);
			
			sMsg.LoadString(IDS_ERR_SMTP_TIMED_OUT);
			AfxMessageBox(sMsg);

			m_strMsg.Empty();
			m_bSendingInProgres = FALSE;

			CString sMsg;
			sMsg.LoadString(IDS_ERR_SEND_MSGS);		
			m_SBar.SetText (sMsg, 0, 0);
			
			break;
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CEmailClientDlg::OnDblclkListMsgs(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LPNMLISTVIEW lpItem = (LPNMLISTVIEW) pNMHDR;
	int crt;
	if ((crt = lpItem->iItem) == -1)
	{
		*pResult = 0;
		return;
	}

	if(m_Msgs.GetSelectionMark() != (-1))
	{
		OnReply();
	}
	
	*pResult = 0;
}

void CEmailClientDlg::OnSetfocusFolderTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_FoldersGroup.ActivateGroupBox(TRUE);	

	*pResult = 0;
}

void CEmailClientDlg::OnKillfocusFolderTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_FoldersGroup.ActivateGroupBox(FALSE);	
	
	*pResult = 0;
}

void CEmailClientDlg::OnSetfocusListMsgs(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_EmailsGroup.ActivateGroupBox(TRUE);	
	
	*pResult = 0;
}

void CEmailClientDlg::OnKillfocusListMsgs(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_EmailsGroup.ActivateGroupBox(FALSE);	
	
	*pResult = 0;
}

void CEmailClientDlg::OnSetfocusEDITMailTxt() 
{
	m_EmailTextGroup.ActivateGroupBox(TRUE);	
}

void CEmailClientDlg::OnKillfocusEDITMailTxt() 
{
	m_EmailTextGroup.ActivateGroupBox(FALSE);	
}


void CEmailClientDlg::OnItemchangedListMsgs(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	
	if (pNMListView->uNewState & LVIS_SELECTED)
	{
		OnClickList (pNMHDR, pResult);
/*
		int crt = pNMListView->iItem;

		if (crt != -1)
		{
			m_Msgs.EnsureVisible(crt, FALSE);
			m_Msgs.SetItemState(crt, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
			m_Msgs.SetSelectionMark(crt);
		}
*/
	}

	*pResult = 0;
}

BOOL CEmailClientDlg::ReadLocalUserCode()
{
	BOOL bInitStatus = FALSE;
	CString sID, sKey;

	sID = "1234567890123456789012345678901";
	sKey = "12345678901234567890123456789012";

	if ((!sID.IsEmpty()) && (!sKey.IsEmpty())) 
	{
		XSATAUID = sID.GetBuffer(31);
		memset(AESKey, 00, 33);
		memmove (AESKey, sKey.GetBuffer(32), 32);
		bInitStatus = TRUE;
	}
/*
	unsigned char res = KeyLogin(KEY_ID, KEY_PASSW);

	if (res != KEY_SUCCESS) return FALSE;

	if (IsDataWritten()) 
	{		
		sID = readID();
		sKey = readKey();

		if ((!sID.IsEmpty()) && (!sKey.IsEmpty())) 
		{
			XSATAUID = sID.GetBuffer(31);
			memset(AESKey, 00, 33);
			memmove (AESKey, sKey.GetBuffer(32), 32);
			bInitStatus = TRUE;
		}
	}

	KeyLogout();
*/
	return bInitStatus;

}

void CEmailClientDlg::OnAddCode() 
{
	CImportCodes dlgImport;
	dlgImport.sKeyDestPath = RootFolder + "\\Keys";
	dlgImport.DoModal();	
}

void CEmailClientDlg::OnSizing(UINT fwSide, LPRECT pRect) 
{
	CDialog::OnSizing(fwSide, pRect);
	
   if(pRect->right - pRect->left <= rcInitial.Width())
     	pRect->right = pRect->left + rcInitial.Width();
	
     if(pRect->bottom - pRect->top <= rcInitial.Height())
     	pRect->bottom = pRect->top + rcInitial.Height();
	
}

void CEmailClientDlg::OnSize(UINT nType, int cx, int cy) 
{
//	CDialog::OnSize(nType, cx, cy);
	
	if (bWindowInitialized) m_MainTb.AutoSize();

	BOOL bRepaint = TRUE;
	
	CRect rcWindow;
	GetClientRect(&rcWindow);

	CRect rcSBRect;
	::GetClientRect (m_SBar.m_hWnd, rcSBRect);

	::MoveWindow (m_SBar.m_hWnd, 0, rcWindow.bottom - rcSBRect.Height(), rcWindow.Width(), rcSBRect.Height(), TRUE);

	::GetClientRect (m_SBar.m_hWnd, rcSBRect);

	int SBarWidths[4] = {0,0,0, -1};	
	SBarWidths[0] = rcSBRect.Width() - 200;
	SBarWidths[1] = rcSBRect.Width() - 100;		
	SBarWidths[2] = rcSBRect.Width();
	SBarWidths[3] = -1;
	
	if (bWindowInitialized)	m_SBar.SetParts (4, SBarWidths);


	CRect rcTBar;
	::GetClientRect (m_MainTb.m_hWnd, &rcTBar);
	rcTBar.bottom += 5;		// cresc spatiul dintre toolbar si controalele de sub ea

	CRect rcFoldersGroup;
	::GetClientRect (m_FoldersGroup.m_hWnd, &rcFoldersGroup);
	
	CRect rcEmailsGroup;
	::GetClientRect (m_EmailsGroup.m_hWnd, &rcEmailsGroup);

	CRect rcButton;

	::GetClientRect(m_btnDeleteMail.m_hWnd, &rcButton);
	int nYBtnOffset = rcWindow.bottom - rcSBRect.Height() - rcButton.Height() - 25;


// stanga
	
//	::MoveWindow (m_FoldersGroup.m_hWnd, 10, rcTBar.Height(), rcFoldersGroup.Width(), 0.375* cy, bRepaint);	
	if (bWindowInitialized) m_FoldersGroup.SetGroupBoxPos(NULL, 10, rcTBar.Height(), rcFoldersGroup.Width(), 0.375* cy, SWP_NOZORDER);
	::MoveWindow (m_cTreeCtrl.m_hWnd, 20, rcTBar.Height() + 17, rcFoldersGroup.Width() - 20, 0.375* cy - 25, bRepaint);
//	::MoveWindow (m_AddrBGroup.m_hWnd, 10, rcTBar.Height() + 0.375*cy + 10, rcFoldersGroup.Width(), nYBtnOffset - (rcTBar.Height() + 0.375*cy + 10), bRepaint);
	if (bWindowInitialized) m_AddrBGroup.SetGroupBoxPos(NULL, 10, rcTBar.Height() + 0.375*cy + 10, rcFoldersGroup.Width(), nYBtnOffset - (rcTBar.Height() + 0.375*cy + 10), SWP_NOZORDER);
	::MoveWindow (m_ContactList.m_hWnd, 20, rcTBar.Height() + 0.375*cy + 29, rcFoldersGroup.Width() - 20, nYBtnOffset - (rcTBar.Height() + 0.375*cy + 10) - 25, bRepaint);

// dreapta
	
//	::MoveWindow (m_EmailsGroup.m_hWnd, 20 + rcFoldersGroup.Width(), rcTBar.Height(), (cx - 30 - rcFoldersGroup.Width()), 0.375* cy, bRepaint);	
	if (bWindowInitialized) m_EmailsGroup.SetGroupBoxPos(NULL, 20 + rcFoldersGroup.Width(), rcTBar.Height(), (cx - 30 - rcFoldersGroup.Width()), 0.375* cy, SWP_NOZORDER);
	::MoveWindow (m_Msgs.m_hWnd, 30 + rcFoldersGroup.Width(), rcTBar.Height() + 18, (cx - 30 - rcFoldersGroup.Width()) - 20, 0.375* cy - 25, bRepaint);
//	::MoveWindow (m_EmailTextGroup.m_hWnd, 20 + rcFoldersGroup.Width(), rcTBar.Height() + 0.375* cy + 10, (cx - 30 - rcFoldersGroup.Width()), nYBtnOffset - (rcTBar.Height() + 0.375*cy + 10), bRepaint);	
	if (bWindowInitialized) m_EmailTextGroup.SetGroupBoxPos(NULL, 20 + rcFoldersGroup.Width(), rcTBar.Height() + 0.375* cy + 10, (cx - 30 - rcFoldersGroup.Width()), nYBtnOffset - (rcTBar.Height() + 0.375*cy + 10), SWP_NOZORDER);
	::MoveWindow (m_ctrlEditMailText.m_hWnd, 30 + rcFoldersGroup.Width(), rcTBar.Height() + 0.375* cy + 29, (cx - 30 - rcFoldersGroup.Width()) - 20, nYBtnOffset - (rcTBar.Height() + 0.375*cy + 10) - 25, bRepaint);


	if (bWindowInitialized)
	{
		CRect rcEmailList;
		::GetClientRect(m_Msgs.m_hWnd, &rcEmailList);
		int w = rcEmailList.Width();

		int total_cx = 0;
		float OrigWidth = rcInitial.Width();
		float fCX = cx;

		for (int i=0; i<NUMBER_OF_EMAIL_LIST_COLUMNS; i++)
		{	
			if (i < NUMBER_OF_EMAIL_LIST_COLUMNS - 1) 
				m_Msgs.SetColumnWidth (i, OrigColWidths[i] * (fCX/OrigWidth));
			else 
				m_Msgs.SetColumnWidth (i, w - total_cx + 2);

			total_cx += OrigColWidths[i] * (fCX/OrigWidth);
		}		
	}


// butoanele

	::GetClientRect(m_btnDeleteMail.m_hWnd, &rcButton);
	nYBtnOffset = rcWindow.bottom - rcSBRect.Height() - rcButton.Height() - 15;
	int nXBtnOffset = 20 + rcFoldersGroup.Width();
	int nLastBtnWidth = rcButton.Width();
	::MoveWindow (m_btnDeleteMail.m_hWnd, nXBtnOffset, nYBtnOffset, rcButton.Width(), rcButton.Height(), bRepaint);	

	::GetClientRect(m_btnReply.m_hWnd, &rcButton);
	nXBtnOffset += (5 + nLastBtnWidth);
	nLastBtnWidth = rcButton.Width();
	::MoveWindow (m_btnReply.m_hWnd, nXBtnOffset, nYBtnOffset, rcButton.Width(), rcButton.Height(), bRepaint);	

	::GetClientRect(m_btnForward.m_hWnd, &rcButton);
	nXBtnOffset += (5 + nLastBtnWidth);
	nLastBtnWidth = rcButton.Width();
	::MoveWindow (m_btnForward.m_hWnd, nXBtnOffset, nYBtnOffset, rcButton.Width(), rcButton.Height(), bRepaint);	

	::GetClientRect(m_btnContinueDraft.m_hWnd, &rcButton);
	nXBtnOffset += (5 + nLastBtnWidth);
	nLastBtnWidth = rcButton.Width();
	::MoveWindow (m_btnContinueDraft.m_hWnd, nXBtnOffset, nYBtnOffset, rcButton.Width(), rcButton.Height(), bRepaint);	

	::GetClientRect(m_btnSaveAttach.m_hWnd, &rcButton);
	::MoveWindow (m_btnSaveAttach.m_hWnd, cx - rcButton.Width() - 10, nYBtnOffset, rcButton.Width(), rcButton.Height(), bRepaint);
}

void CEmailClientDlg::OnDblclkContactsList() 
{
 	int iSelectedIndex = LB_ERR;
 
 	if ((iSelectedIndex = m_ContactList.GetCurSel()) == LB_ERR) return;
 	
 	CString selectedNick;
 	m_ContactList.GetText (iSelectedIndex, selectedNick);
 	
 	if(!selectedNick.IsEmpty())
 	{
 		CContact retrievedContact;
 		m_addressBook.getContact(retrievedContact, selectedNick, CAddressBook::NICK);
 
 		CComposeNewMessage NewMessage(this, COMPOSE_TYPE_NEW);
		NewMessage.m_sCrtSelFile = m_sCrtSelFile;
 
 		NewMessage.m_sTo = retrievedContact.m_EmailAdress;
 		NewMessage.m_iSelected = m_iSelection;
 		if(NewMessage.DoModal() == IDOK)
 		{
 			m_strMsg += NewMessage.MsgDestination;
 			m_strMsg +='?';
 			if(!m_bSendingInProgres)
 			{
 				m_bSendingInProgres = TRUE;
 				StartSendMessage();
 			}
			
			UpdateFolderTree();
 			RefreshSelectedTreeFolder();
 		}
 	}
	
}

void CEmailClientDlg::UpdateFolderTree()
{
	int nTotal = 0;
	int nUnread = 0;

	HTREEITEM selItem = m_cTreeCtrl.GetSelectedItem();

	m_strInbox = CountUnread(INBOX_FOLDER_NAME, nTotal, nUnread);
	m_cTreeCtrl.SetItemText(m_htreeInbox, m_strInbox);
	if (selItem == m_htreeInbox) UpdateStatusBarMailPanels(nUnread, nTotal);

	m_strOutbox = CountUnread(OUTBOX_FOLDER_NAME, nTotal, nUnread);	
	m_cTreeCtrl.SetItemText(m_htreeOutbox, m_strOutbox);
	if (selItem == m_htreeOutbox) UpdateStatusBarMailPanels(nUnread, nTotal);
	
	m_strSent = CountUnread(SENT_FOLDER_NAME, nTotal, nUnread);
	m_cTreeCtrl.SetItemText(m_htreeSent, m_strSent);
	if (selItem == m_htreeSent) UpdateStatusBarMailPanels(nUnread, nTotal);

	m_strDeleted = CountUnread(DELETED_FOLDER_NAME, nTotal, nUnread);
	m_cTreeCtrl.SetItemText(m_htreeDeleted, m_strDeleted);
	if (selItem == m_htreeDeleted) UpdateStatusBarMailPanels(nUnread, nTotal);
	
	m_strDraft = CountUnread(DRAFT_FOLDER_NAME, nTotal, nUnread);
	m_cTreeCtrl.SetItemText(m_htreeDraft, m_strDraft);
	if (selItem == m_htreeDraft) UpdateStatusBarMailPanels(nUnread, nTotal);

}

void CEmailClientDlg::UpdateStatusBarMailPanels(int nUnread, int nTotal)
{
	CString sMsg;
	_TCHAR sLocalStr[32];
	memset (sLocalStr, 0x00, 32*sizeof(_TCHAR));
	sMsg.LoadString(IDS_UNREAD);
	_stprintf (sLocalStr, _TEXT(" %s %d"), sMsg, nUnread);
	m_SBar.SetText (sLocalStr, 1, 0);

	memset (sLocalStr, 0x00, 32*sizeof(_TCHAR));
	sMsg.LoadString(IDS_TOTAL);
	_stprintf (sLocalStr, _TEXT(" %s %d"), sMsg, nTotal);
	m_SBar.SetText (sLocalStr, 2, 0);
}

LRESULT CEmailClientDlg::OnAccChanged(WPARAM wParam, LPARAM lParam)
{
	m_iSelection = (int)wParam;
	
	if(m_iSelection == -1) return 0;
	ECSettings.IniWriteECSettings();
	
	ReadContacts();
	refresContactsList();
	RefreshSelectedTreeFolder();
	
	return 0;
}

void CEmailClientDlg::OnSetfocusContactsList() 
{
	m_AddrBGroup.ActivateGroupBox(TRUE);
	
}

void CEmailClientDlg::OnKillfocusContactsList() 
{
	m_AddrBGroup.ActivateGroupBox(FALSE);
	
}
