// ServersPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "ServersPropertiesDlg.h"
#include "SMTPAuthDlg.h"
#include "AccountListTypes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServersPropertiesDlg property page

IMPLEMENT_DYNCREATE(CServersPropertiesDlg, CPropertyPage)

CServersPropertiesDlg::CServersPropertiesDlg() : CPropertyPage(CServersPropertiesDlg::IDD)
{
	//{{AFX_DATA_INIT(CServersPropertiesDlg)
	m_SmtpLogonOpt = -1;
	m_SMTPAuthOpt = FALSE;
	m_RememberPassword = FALSE;
	m_sPOP3AcountName = _T("");
	m_sPOP3AcountPsw = _T("");
	m_sPOP3ServerAddr = _T("");
	m_sSMTPServerAddr = _T("");
	//}}AFX_DATA_INIT
	m_psp.dwFlags &= ~PSP_HASHELP;
}

CServersPropertiesDlg::~CServersPropertiesDlg()
{
}

void CServersPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServersPropertiesDlg)
	DDX_Check(pDX, IDC_SMTP_SERVER_AUTH_OPTION, m_SMTPAuthOpt);
	DDX_Check(pDX, IDC_REMEMBER_PASSW_OPTION, m_RememberPassword);
	DDX_Text(pDX, IDC_POP3_ACC_NAME, m_sPOP3AcountName);
	DDX_Text(pDX, IDC_POP3_ACC_PASSW, m_sPOP3AcountPsw);
	DDX_Text(pDX, IDC_POP3_SERVER_ADDR, m_sPOP3ServerAddr);
	DDX_Text(pDX, IDC_SMTP_SERVER_ADDR, m_sSMTPServerAddr);
	DDX_Control(pDX, IDC_SMTP_SETT_BTN, m_btnSMTPSett);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CServersPropertiesDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CServersPropertiesDlg)
	ON_BN_CLICKED(IDC_SMTP_SETT_BTN, OnSmtpSettBtn)
	ON_BN_CLICKED(IDC_SMTP_SERVER_AUTH_OPTION, OnSmtpServerAuthOption)
	ON_EN_CHANGE(IDC_POP3_SERVER_ADDR, OnChange)
	ON_EN_CHANGE(IDC_POP3_ACC_PASSW, OnChange)
	ON_EN_CHANGE(IDC_POP3_ACC_NAME, OnChange)
	ON_EN_CHANGE(IDC_SMTP_SERVER_ADDR, OnChange)
	ON_BN_CLICKED(IDC_REMEMBER_PASSW_OPTION, OnRememberPasswOption)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServersPropertiesDlg message handlers

void CServersPropertiesDlg::OnSmtpSettBtn() 
{
		int tmpAuthOpt = SettDlg.m_AuthOpt;
		CString tmpsSMTPName = SettDlg.m_sSMTPName;
		CString tmpsSMTPPsw = SettDlg.m_sSMTPPsw;
		SettDlg.m_bOnChange =FALSE;
	if(SettDlg.DoModal() == IDOK)
	{
		if(SettDlg.m_bOnChange)
		{
			m_bOnChange2 = TRUE;
		}
	}
	else
	{
		SettDlg.m_AuthOpt = tmpAuthOpt;
		SettDlg.m_sSMTPName = tmpsSMTPName;
		SettDlg.m_sSMTPPsw = tmpsSMTPPsw;
		SettDlg.m_bOnChange = FALSE;
	}
}

void CServersPropertiesDlg::OnSmtpServerAuthOption() 
{
	UpdateData(TRUE);
	m_bOnChange = TRUE;
	if (m_SMTPAuthOpt) 
		GetDlgItem(IDC_SMTP_SETT_BTN)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_SMTP_SETT_BTN)->EnableWindow(FALSE);
}

BOOL CServersPropertiesDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_sPOP3ServerAddr = (*AccListIterator).GetPOP3ServerName();
	m_sSMTPServerAddr = (*AccListIterator).GetSMTPServerName();
	m_sPOP3AcountName = (*AccListIterator).GetPOP3UserName();
	

	m_RememberPassword = (*AccListIterator).GetPOP3RememberPassword();
	m_SMTPAuthOpt = (*AccListIterator).GetSMTPServerRequiresAuthentication();
	if (m_SMTPAuthOpt) 
		GetDlgItem(IDC_SMTP_SETT_BTN)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_SMTP_SETT_BTN)->EnableWindow(FALSE);
	if(m_RememberPassword)
	{
		GetDlgItem(IDC_POP3_ACC_PASSW)->EnableWindow(TRUE);
		m_sPOP3AcountPsw = (*AccListIterator).GetPOP3Password();
	}
	else
	{
		GetDlgItem(IDC_POP3_ACC_PASSW)->EnableWindow(FALSE);
	}
		
	UpdateData (FALSE);

	SettDlg.m_AuthOpt = (*AccListIterator).GetSMTPUsePOP3Settings();
	SettDlg.m_sSMTPName = (*AccListIterator).GetSMTPUserName();
	SettDlg.m_sSMTPPsw = (*AccListIterator).GetSMTPPassword();

	OwnerDrawButtons();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CServersPropertiesDlg::OnChange() 
{
	UpdateData(TRUE);
	m_bOnChange = TRUE;
}

void CServersPropertiesDlg::OnRememberPasswOption() 
{
	UpdateData(TRUE);
	m_bOnChange = TRUE;
	if(m_RememberPassword)
	{
		GetDlgItem(IDC_POP3_ACC_PASSW)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_POP3_ACC_PASSW)->EnableWindow(FALSE);
	}
}

void CServersPropertiesDlg::OwnerDrawButtons()
{
	m_btnSMTPSett.SetMaskedBitmap( IDB_SETTINGS, 16, 16, RGB( 255, 255, 255 ) );
}
