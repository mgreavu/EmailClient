// AdvancedPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EmailClient.h"
#include "AdvancedPropertiesDlg.h"
#include "AccountListTypes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAdvancedPropertiesDlg property page

IMPLEMENT_DYNCREATE(CAdvancedPropertiesDlg, CPropertyPage)

CAdvancedPropertiesDlg::CAdvancedPropertiesDlg() : CPropertyPage(CAdvancedPropertiesDlg::IDD)
{
	//{{AFX_DATA_INIT(CAdvancedPropertiesDlg)
	m_LeaveMailOnServer = FALSE;
	m_iPOP3Port = 0;
	m_iSMTPPort = 0;
	m_iPOP3Timeout = 0;
	m_RemoveAfter = 0;
	//}}AFX_DATA_INIT
	m_psp.dwFlags &= ~PSP_HASHELP;
}

CAdvancedPropertiesDlg::~CAdvancedPropertiesDlg()
{
}

void CAdvancedPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAdvancedPropertiesDlg)
	DDX_Check(pDX, IDC_LEAVE_MSGS_ON_SERVER_OPTION, m_LeaveMailOnServer);
	DDX_Text(pDX, IDC_POP3_PORT, m_iPOP3Port);
	DDX_Text(pDX, IDC_SMTP_PORT, m_iSMTPPort);
	DDX_Text(pDX, IDC_TIMEOUT, m_iPOP3Timeout);
	DDX_Text(pDX, IDC_REMOVE_AFTER, m_RemoveAfter);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAdvancedPropertiesDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CAdvancedPropertiesDlg)
	ON_BN_CLICKED(IDC_DEFAULT_PORTS_BTN, OnDefaultPortsBtn)
	ON_EN_CHANGE(IDC_POP3_PORT, OnChange)
	ON_BN_CLICKED(IDC_LEAVE_MSGS_ON_SERVER_OPTION, OnLeaveMsgsOnServerOption)
	ON_EN_CHANGE(IDC_SMTP_PORT, OnChange)
	ON_EN_CHANGE(IDC_TIMEOUT, OnChange)
	ON_EN_CHANGE(IDC_REMOVE_AFTER, OnChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdvancedPropertiesDlg message handlers

BOOL CAdvancedPropertiesDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_iSMTPPort = (*AccListIterator).GetSMTPPort();
	m_iPOP3Port = (*AccListIterator).GetPOP3Port();
	m_iPOP3Timeout = (*AccListIterator).GetPOP3Timeout();
	
	m_LeaveMailOnServer = (*AccListIterator).GetLeaveMailOnServer();
	m_RemoveAfter = (*AccListIterator).GetRemoveAfter();
	
	if(m_LeaveMailOnServer)
		GetDlgItem(IDC_REMOVE_AFTER)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_REMOVE_AFTER)->EnableWindow(FALSE);

	UpdateData (FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAdvancedPropertiesDlg::OnDefaultPortsBtn() 
{
	m_iSMTPPort = 25;
	m_iPOP3Port = 110;
	UpdateData(FALSE);
	m_bOnChange = TRUE;
}

void CAdvancedPropertiesDlg::OnChange() 
{
	UpdateData(TRUE);
	m_bOnChange = TRUE;	
}

void CAdvancedPropertiesDlg::OnLeaveMsgsOnServerOption() 
{
	UpdateData(TRUE);
	m_bOnChange = TRUE;	
	if(m_LeaveMailOnServer)
	{
		GetDlgItem(IDC_REMOVE_AFTER)->EnableWindow(TRUE);
		m_RemoveAfter = (*AccListIterator).GetRemoveAfter();
		UpdateData(FALSE);
	}
	else
		GetDlgItem(IDC_REMOVE_AFTER)->EnableWindow(FALSE);
}
