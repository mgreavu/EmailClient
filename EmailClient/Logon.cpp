// Logon.cpp : implementation file
//

#include "stdafx.h"
#include "AccountListTypes.h"
#include "emailclient.h"
#include "Logon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogon dialog


CLogon::CLogon(CWnd* pParent /*=NULL*/)
	: CDialog(CLogon::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLogon)
	m_sPassword = _T("");
	m_sUserName = _T("");
	//}}AFX_DATA_INIT
}


void CLogon::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogon)
	DDX_Text(pDX, IDC_PASSWORD, m_sPassword);
	DDX_Text(pDX, IDC_USERNAME, m_sUserName);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLogon, CDialog)
	//{{AFX_MSG_MAP(CLogon)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogon message handlers

BOOL CLogon::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText ("Logon - " + (*AccListIterator).GetPOP3UserName());
	CString sMsg;
	sMsg.LoadString(IDS_ENTER_USER_PASS);
	GetDlgItem(IDC_SERVER)->SetWindowText(sMsg + (*AccListIterator).GetPOP3UserName());
	GetDlgItem(IDC_USERNAME)->SetWindowText((*AccListIterator).GetPOP3UserName());
	SetTimer (1003, 100, NULL);	
	
	OwnerDrawButtons();
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLogon::OnOK() 
{
	UpdateData(TRUE);
	CDialog::OnOK();
}

void CLogon::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 1003)	
	{
		KillTimer(1003);
		GetDlgItem (IDC_PASSWORD)->SetFocus();
	}
	CDialog::OnTimer(nIDEvent);
}

void CLogon::OwnerDrawButtons()
{
	m_btnOk.SetMaskedBitmap( IDB_ADD, 16, 16, RGB( 255, 255, 255 ) );
	
	m_btnCancel.SetMaskedBitmap( IDB_CANCEL, 16, 16, RGB( 255, 255, 255 ) );
}
